/* Jean Varlet*/

var arrayPagamento = new Array();

var _CODCOLIGADA = 1;
var _IDMOV = "-1";
var _CODTMV = "2.1.93"; //temp -- falta cadastrar o mov definitivo - > 2.1.93 !!
var _CODFILIAL = 1;
var _CODCOLCFO = 1;
var _CODCFO; // // ok 
var _DATAEMISSAO ;//ok  falta formatador da data 
var _DATASAIDA = dataRmHoje();
var _DATAMOVIMENTO = dataRmHoje();
var _DATAENTREGA ; // data da realização do produto -- 
var _CODLOC = 1;
var _SEGUNDONUMERO ;
var _CODCOLCXA = 1;
var _CODCXA = '158';
var _CODCPG ;
var _CODTB1FLX ;
var _CODTB2FLX ; // conferir --- 
var _HISTORICOCURTO = ' Historico do pagamento  ';   // geraHistorico(codEvento, nomeServico, dataInicio, dataFim, nomeParticipantePF);
var _PRECO ; // ok
var _CODTB1FAT;// ok
var _CAMPOLIVRE2;
var _CAMPOLIVRE3;
//Dados se lançamento para empresa Cliente -> Aproveitar nos relatórios ---
var _cnpjEmpresaVinculada; 
var _codCfoEmpresa; 
var _razaoEmpresa; 
var _fantasiaEmpresa; 
var _fornecedorEmpresa;
var _inscricaoSAS;
/*Checa Identificador de Pagamentos */
function checaSegundoNumero(){	
	let posicao = $(".identificadorGenericoPagamento:last").attr("id");	
	if($("#"+identificadorGenericoPagamento).val() == '' || $("#"+identificadorGenericoPagamento).val() == null ){
		$("#"+identificadorGenericoPagamento).addClass('has-error');
		FLUIGC.toast({
			title: 'Erro',
			message: '<br>O Código de Autorização da Transação não foi preenchido.',
			type: 'danger'
		});		
		return false;
	}else{		
		return true;
	}
}
/*Repetição -- Controla comportamento dos campos dentro dos filhos, caso seja necessário adicionar mais filhos
 * após gerar movimento --- Quando o valor pago ou a soma dos valores pagos é menor que o valor total da contratação*/
function auxiliarAddPagamento(){
	$(".formaPagamento").click(function(){
		console.log('Click - formaPagamento');
	    let id = $(this).attr("id");
	    let number = id.split('___')[1];
	    $(this).change(function(){
	        if($(this).val() != '0'){
	        	FLUIGC.calendar('#dataPagamentoGenerico___'+number, {
	        		//showToday: false,
	    			pickDate: true,
	    			pickTime: false
	    		});
	        	if($("#proximoPagamento").val() != 0){
	        		$(".identificadorGenericoPagamento:last").click(function(){
	        			$(".valorGenerico:last").val($("#proximoPagamento").val());	        			
	        		});	 
	        	}
	        	$("#valorGenericoPagamento___"+number).val(retornaValorPagar());
	        	
	        	$("#valorGenericoPagamento___"+number).click(function(){
	        		$(this).val(''); 
	        	});
	        	$("#valorGenericoPagamento___"+number).focusout(function(){	        		
	        		$(this).val(reais($(this).val()));	        		
	        	});
	        	$("#bntlancMovimento___"+number).prop("disabled", false);	        	
	        	$("#dataPagamentoGenerico___"+number).change(function(){
	        		var dataInicio;
	        		var dataPagamento = 'dataPagamentoGenerico___'+number;	        		
	        			if(retornaNatureza() == 'consultoriaSGF' || retornaNatureza() == 'instrutoriaSGF' ){
		        			//consultoriaSGF
		        			dataInicio = 'datainicial';	        			
		        		}else if(retornaNatureza() == 'instrutoriaSAS'){
		        			//instrutoriaSAS
		        			dataInicio = 'dataIniEventoSaSselecionado';		        		
			        	}else if(retornaNatureza() == 'outrosServicos'){
			        		//instrutoriaSAS
			        		if(isEmpty($("#dataInicioOutros").val()) ){
			        			if($("#toaster").children().length <= 3){
			        				FLUIGC.toast({
					       			     title: 'Data de Início',
					       			     message: '<br>É necessário informar a data de início primeiro' ,
					       			     type: 'danger'

					       				});			        				
			        			}
			        			
			        		}else{
			        			dataInicio = 'dataInicioOutros';
			        		}			        		
			        	}	        			
	        			//comparar data inserida com a data de inicio do serviço
		        		validarDataPagamento(dataPagamento, dataInicio);    		
	        	
	        	});
	            if($(this).val() == '1'|| $(this).val() == '2'){                
	                //$("#ca"+number).fadeIn();
	                //$("#bo"+number).fadeOut();
	               //$("#de"+number).fadeOut();
	                $("#pa"+number).fadeIn();
	                
                    $("#anexoGenerico___"+number).click(function(){	    
                    $('#anexoGenerico___'+number).off().on('change', function(){
                    fnCarregaDocAnexo(this, 'pagamento');
                        });
                    });
	            }else if($(this).val() == '3'){
	            	$("#pa"+number).fadeIn();
	            	$("#condicaoPagamentoGenerico___"+number).val('1');
	            	$("#condicaoPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).val('');
                    $("#anexaReciboDeposito___"+number).click(function(){    
                    	$('#anexoGenerico___'+number).off().on('change', function(){
                            fnCarregaDocAnexo(this, 'pagamento');
                        });

                    });
	            }else if($(this).val() == '4'){
	            	$("#pa"+number).fadeIn();
	            	$("#condicaoPagamentoGenerico___"+number).val('1');
	            	$("#condicaoPagamentoGenerico___1").prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).val('');
                    $("#anexaReciboBoleto____"+number).click(function(){    
                    $('#anexoGenerico___'+number).off().on('change', function(){
                        fnCarregaDocAnexo(this, 'pagamento');
                    });

                    });

	            }
	        }
	        
	    });
	    console.log(id);
	    
	});
	
}
/* */
function desabilitaMovimentado(){ //pagamentosRecibos___ < -- select de pagamentos
    let posicao; 
    if(floatReais($("#proximoPagamento").val()) == 0){
    	posicao = $(".valorGenerico:last").attr("id").split('___')[1];    	
    }else{
    	posicao = $(".valorGenerico:last").attr("id").split('___')[1] - 1;
    }   
    console.log('posicao -> ' + posicao);
    $("#formaPagamento___"+posicao).prop("disabled", true);
    disableDivItens(posicao);
}

function disableDivItens(...divId){
   divId.forEach(d =>{
        $("#pa"+divId).find("input,select, radio, button").prop("disabled", true);
    })
}
/*
 * Checa se é pagamento único, caso não, checa se é o primeiro pagamento parcial ou não...
 * */
function countPagamentos(){
	let counter = $(".valorGenerico").length;
	let valorBruto = $("#fieldValorBruto").val();
	let desconto = $("#existeDesconto").val();
	let rateio = $("#existeRateio").val();	
	let valorPago = floatReais($(".valorGenerico:last").val());
	let valorTotal;
	let dividido = $("#pagamentodividido").val();
	let somaValores =  totalPagamentos();
	let proxPagamento;
	if(desconto ==='nao' && rateio === 'nao'){
		valorTotal = floatReais($("#fieldValorBruto").val());
		if(dividido === 'nao'){
			//Se  for o primeiro pagamento da divisão ou pagamento único---
			proxPagamento = valorTotal - valorPago;
			if(proxPagamento == 0){
				$("#addPagamento").hide();
				$("#buttonEmtRecibo").fadeIn();
				$("#bntEmtRecibo").prop("disabled",false);
				$("#proximoPagamento").val(reais('0'));
				$("#somaPagamentos").val(reais(somaValores));
				desabilitaMovimentado();	
			}else{
				$("#addPagamento").hide();
				$("#pagamentodividido").val('sim');
				wdkAddChild('tablepagamentos');
				$("#proximoPagamento").val(reais(proxPagamento));
				auxiliarAddPagamento();
				desabilitaMovimentado();
				$("#somaPagamentos").val(reais(somaValores));
			}			
		}else if(dividido === 'sim'){
			//Se o pagamento atual, não for o primeiro pagamento ---nesse caso, utiliza a fn totalPagamentos() para subtrair do total
			proxPagamento = valorTotal - somaValores;
			if(proxPagamento == 0){
				$("#addPagamento").hide();
				$("#buttonEmtRecibo").fadeIn();
				$("#bntEmtRecibo").prop("disabled",false);
				$("#proximoPagamento").val(reais('0'));
				$("#somaPagamentos").val(reais(somaValores));
				desabilitaMovimentado();	
			}else{
				$("#addPagamento").hide();
				$("#pagamentodividido").val('sim');
				wdkAddChild('tablepagamentos');
				$("#proximoPagamento").val(reais(proxPagamento));
				auxiliarAddPagamento();
				desabilitaMovimentado();	
				$("#somaPagamentos").val(reais(somaValores));
			}
		}
			/*
		if(valorTotal > valorPago){
			$("#pagamentodividido").val('sim');
			proxPagamento = valorTotal - valorPago
			$("#proximoPagamento").val(reais(proxPagamento))
			wdkAddChild('tablepagamentos');
			auxiliarAddPagamento();
			desabilitaMovimentado();
			$("#addPagamento").hide();
			$("#buttonEmtRecibo").fadeOut();
		}else if(valorTotal == valorPago || valorTotal == totalPagamentos()){
			desabilitaMovimentado();
			$("#buttonEmtRecibo").fadeIn();			
		}else if(somaValores == valorTotal){
						
		}		
	}else if(desconto === 'sim' && rateio === 'nao'){
		valorTotal = floatReais($("#totalDesconto").val());
		if(valorTotal > valorPago){
			$("#pagamentodividido").val('sim');
			$("#proximoPagamento").val(valorTotal - valorPago)
			wdkAddChild('tablepagamentos');
			auxiliarAddPagamento();
			desabilitaMovimentado();
			$("#buttonEmtRecibo").fadeOut();
		}
		
	}else if( rateio === 'sim'){
		valorTotal = floatReais($("#valorCliente").val());
		if(valorTotal > valorPago){
			$("#pagamentodividido").val('sim');
			$("#proximoPagamento").val(valorTotal - valorPago)
			wdkAddChild('tablepagamentos');
			auxiliarAddPagamento();
			desabilitaMovimentado();
			$("#buttonEmtRecibo").fadeOut();
		}
		*/
	}else if(desconto === 'sim' && rateio === 'nao'){
		valorTotal = floatReais($("#totalDesconto").val());
		if(dividido === 'nao'){
			//Se  for o primeiro pagamento da divisão ou pagamento único---
			proxPagamento = valorTotal - valorPago;
			if(proxPagamento == 0){
				$("#addPagamento").hide();
				$("#buttonEmtRecibo").fadeIn();
				$("#bntEmtRecibo").prop("disabled",false);
				$("#proximoPagamento").val(reais('0'));
				$("#somaPagamentos").val(reais(somaValores));	
				desabilitaMovimentado();
			}else{
				$("#addPagamento").hide();
				$("#pagamentodividido").val('sim');
				wdkAddChild('tablepagamentos');
				$("#proximoPagamento").val(reais(proxPagamento));
				auxiliarAddPagamento();
				desabilitaMovimentado();
				$("#somaPagamentos").val(reais(somaValores));
			}			
		}else if(dividido === 'sim'){
			//Se o pagamento atual, não for o primeiro pagamento ---nesse caso, utiliza a fn totalPagamentos() para subtrair do total
			proxPagamento = valorTotal - somaValores;
			if(proxPagamento == 0){
				$("#addPagamento").hide();
				$("#buttonEmtRecibo").fadeIn();
				$("#bntEmtRecibo").prop("disabled",false);
				$("#proximoPagamento").val(reais('0'));
				$("#somaPagamentos").val(reais(somaValores));
				desabilitaMovimentado();
			}else{
				$("#addPagamento").hide();
				$("#pagamentodividido").val('sim');
				wdkAddChild('tablepagamentos');
				$("#proximoPagamento").val(reais(proxPagamento));
				auxiliarAddPagamento();
				desabilitaMovimentado();	
				$("#somaPagamentos").val(reais(somaValores));
			}
		}
	}else if( rateio === 'sim'){
		valorTotal = floatReais($("#valorCliente").val());
		if(dividido === 'nao'){
			//Se  for o primeiro pagamento da divisão ou pagamento único---
			proxPagamento = valorTotal - valorPago;
			if(proxPagamento == 0){
				$("#addPagamento").hide();
				$("#buttonEmtRecibo").fadeIn();
				$("#bntEmtRecibo").prop("disabled",false);
				$("#proximoPagamento").val(reais('0'));
				$("#somaPagamentos").val(reais(somaValores));	
				desabilitaMovimentado();
			}else{
				$("#addPagamento").hide();
				$("#pagamentodividido").val('sim');
				wdkAddChild('tablepagamentos');
				$("#proximoPagamento").val(reais(proxPagamento));
				auxiliarAddPagamento();
				desabilitaMovimentado();
				$("#somaPagamentos").val(reais(somaValores));
			}			
		}else if(dividido === 'sim'){
			//Se o pagamento atual, não for o primeiro pagamento ---nesse caso, utiliza a fn totalPagamentos() para subtrair do total
			proxPagamento = valorTotal - somaValores;
			if(proxPagamento == 0){
				$("#addPagamento").hide();
				$("#buttonEmtRecibo").fadeIn();
				$("#bntEmtRecibo").prop("disabled",false);
				$("#proximoPagamento").val(reais('0'));
				$("#somaPagamentos").val(reais(somaValores));
				desabilitaMovimentado();
			}else{
				$("#addPagamento").hide();
				$("#pagamentodividido").val('sim');
				wdkAddChild('tablepagamentos');
				$("#proximoPagamento").val(reais(proxPagamento));
				auxiliarAddPagamento();
				desabilitaMovimentado();	
				$("#somaPagamentos").val(reais(somaValores));
			}
		}
	}	
	
}

// função para alterar dados por natureza da contratacao
function selecionaNatureza(){
	
	if($("#tipoSerConsultoria").is(':checked') == true ){		
		//Consultoria/Instrutoria
		if($("#codCfoEmpresa").val() !='0'){			
			_CODCFO =$("#codCfoEmpresa").val();
			
		}else{
			_CODCFO= $("#cliForForn").val();// checar 			
		}		
		//Seleciona o subserviço de consultoria/Instrutoria
		if($("#selConsultInstConsultoria").is(':checked') == true ){
			//_CODTB1FAT = retornaProduto('consultoria');
			_CODTB1FAT = retornaProduto('cursos');
			
		}else if($("#selConsultInstInstrutoria").is(':checked') == true){
			_CODTB1FAT = retornaProduto('cursos');
			_CODCFO = $("#codcfoCliente1").val();
		}
		_DATAENTREGA = dataRmEntrega($("#datafinal").val());
		_HISTORICOCURTO += $("#eventoTipo1").val() +':'+ $("#eventoNome1").val() + ' - ' +$("#eventoDataInicio1").val()+ ' a ' + $("#eventoDataFim1").val()+
		' - ' + $("#nomeCliente").val();
		_CAMPOLIVRE3 = $("#codConsultoria").val();
		_CAMPOLIVRE2 = 'Falta Localizar Número insc. SAS ';
		
	}
	if($("#tipoSerInstrutoria").is(':checked')== true){	
		//SAS Eventos
		if($("#codCfoEmpresa").val() != "0" ){			
			_CODCFO =$("#codCfoEmpresa").val();
		}else{
			_CODCFO= $("#codcfoCliente1").val();	
		}		
		_CODTB1FAT = retornaProduto('cursos');
		_HISTORICOCURTO += $("#eventoTipo1").val() +':'+ $("#eventoNome1").val() + ' - ' +$("#eventoDataInicio1").val()+ ' a ' + $("#eventoDataFim1").val()+
		' - ' + $("#nomeCliente").val();
		_DATAENTREGA = dataRmEntrega($(".dataPagamentoGenerico:last").val());
		_CAMPOLIVRE3 = $("#eventoCodigo1").val();
		_CAMPOLIVRE2 = 'Falta Localizar Número insc. SAS ';	
	}
	if($("#tipoSerOutros").is(':checked')== true){
		// Outros tipos ---
		if($("#codCfoEmpresa").val() != null && $("#codCfoEmpresa").val() != undefined  && $("#codCfoEmpresa").val() != "0"){			
			_CODCFO =$("#codCfoEmpresa").val();
		}else{
			_CODCFO= $("#codcfoClienteOutros1").val();	
		}		
		_CODTB1FAT = retornaProduto('cursos'); // alterar para trazer o valor selecionado
		_HISTORICOCURTO += $("#servicosSeleciona").val() +':'+ $("#tituloOutros").val() + ' - ' +$("#dataInicioOutros").val()+ ' a ' + $("#dataFimOutros").val()+
		' - ' + $("#nomeClienteOutros1").val();
		_DATAENTREGA = dataRmEntrega($(".dataPagamentoGenerico:last").val());
		_CAMPOLIVRE3 = $("#codServicoOutros").val();
		_CAMPOLIVRE2 = 'Tipo de Evento Outros, sem insc. SAS ';	
	}

}
function dataRmEntrega(data){
	var _d = new Date();
	var _dia = data.split('/')[0];
	var _mes = data.split('/')[1];
	var _ano = data.split('/')[2];
	var _hora = _d.getHours() ;
	var _minutos = _d.getMinutes() ;
	var _segundos = _d.getSeconds();
	if(_hora < 10){
		var zeroh = "0";
		_hora = zeroh + _hora;	
	}
	if(_dia < 10){
		var zerod = "0";
		_dia = zerod + _dia;		
	}
	if(_minutos < 10){
		var zeromin = "0";
		_minutos = zeromin + _minutos;		
	}
	if(_segundos < 10){
		var zeros = "0";
		_segundos = zeros + _segundos;		
	}
	var retorno = _ano+"-"+_mes+"-"+_dia+"T"+_hora+":"+_minutos+":"+_segundos;
	return retorno;
}
function dataRmHoje(){	
	var d = new Date();
	var dia = d.getDate();
	var mes = d.getMonth()+1 ;
	var ano = d.getFullYear();
	var hora = d.getHours() ;
	var minutos = d.getMinutes() ;
	var segundos = d.getSeconds();
	if(mes < 10){
		var zerom = "0";
		mes =  zerom + mes;
	}
	if(hora < 10){
		var zeroh = "0";
		hora = zeroh + hora;	
	}
	if(dia < 10){
		var zerod = "0";
		dia = zerod + dia;		
	}
	if(minutos < 10){
		var zeromin = "0";
		minutos = zeromin + minutos;		
	}
	if(segundos < 10){
		var zeros = "0";
		segundos = zeros + segundos;		
	}
	var retorno = ano+"-"+mes+"-"+dia+"T"+hora+":"+minutos+":"+segundos;
	return retorno;
}

function retornaCODCPG(formaPagamento){
	formaPagamento = $(".formaPagamento:last").val(); //gambiarra
	let posicao = $(".formaPagamento:last").attr("id");
	let dataPagamento = $(".dataPagamentoGenerico:last").attr("id");
	let valorPagamento = $(".valorGenerico:last").attr("id");
	let identificador = $(".identificadorGenericoPagamento:last").attr("id");
	let condicao = $(".condicaoPagamentoGenerico:last").attr("id");
	let bandeira = $(".bandeiraPagamentoGenerico:last").attr("id");
		 _CODCPG = '';
		 _CODTB1FLX = '';
		 _CODTB2FLX = ''; 
		if(formaPagamento == 4 ){//Boleto Bancário			
			arrayPagamento.push("01", "01.02.0001", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
					$("#"+condicao).val());			
			 _CODCPG = "01";
			 _CODTB1FLX = arrayPagamento[1];
			 _CODTB2FLX = ""; 			
		}else if(formaPagamento == 2){			
			if($("#"+bandeira).val() == 'amex'){				
				arrayPagamento.push("14", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
						$("#"+condicao).val());				
			}
			if($("#"+bandeira).val() == 'credsystem'){				
				arrayPagamento.push("03", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
						$("#"+condicao).val());				
			}
			if($("#"+bandeira).val() == 'diners'){				
				arrayPagamento.push("07", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
						$("#"+condicao).val());	
			}
			if($("#"+bandeira).val() == 'elo'){				
				arrayPagamento.push("09", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
						$("#"+condicao).val());		
			}
			if($("#"+bandeira).val() == 'hipercard'){				
				arrayPagamento.push("13", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
						$("#"+condicao).val());				
			}
			if($("#"+bandeira).val() == 'visa' || $("#"+bandeira).val() == 'master'){				
				arrayPagamento.push("05", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
						$("#"+condicao).val());				
			}			
			 _CODCPG = $("#"+condicao).val(); 
			 _CODTB1FLX = arrayPagamento[1];
			 _CODTB2FLX = arrayPagamento[0];			
		}
		else if(formaPagamento == 1){			
			arrayPagamento.push("05", "01.02.0108", $("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
					$("#"+condicao).val());				
			 _CODCPG = "01";
			 _CODTB1FLX = arrayPagamento[1];
			 _CODTB2FLX = arrayPagamento[0];			 
		}else if(formaPagamento == 3){			
			arrayPagamento.push("04", "01.02.0001",$("#"+dataPagamento).val(), $("#"+valorPagamento).val(), $("#"+identificador).val(), 
					$("#"+condicao).val());				
			 _CODCPG = "01";
			 _CODTB1FLX = arrayPagamento[1];
			 _CODTB2FLX = arrayPagamento[0];			
		}
		
}
/*Valida campos de pagamento - Checa os dados dos campos do último filho -- Pagamento em andamento
 * Caso retorne false, cancela lançamento do movimento
 * Jean Varlet 10/02/2020*/
function checaPreenchimento(){
	if(isEmpty($(".dataPagamentoGenerico:last").val()) ){
		FLUIGC.toast({
			     title: 'Data do Pagamento',
			     message: '<br>É necessário informar a data Pagamento' ,
			     type: 'warning'
				});	
		console.log('checaPreenchimento()->false');
		return false;
	}else if(isEmpty($(".identificadorGenericoPagamento:last").val()) ){
		FLUIGC.toast({
			     title: 'Identificador',
			     message: '<br>É necessário informar o Código do Pagamento' ,
			     type: 'warning'
				});	
		console.log('checaPreenchimento()->false');
		return false;
	}else if(isEmpty($(".condicaoPagamentoGenerico:last").val()) ){
		FLUIGC.toast({
			title: 'Condição',
			message: '<br>É necessário informar a Condição do Pagamento' ,
			type: 'warning'
		});	
		console.log('checaPreenchimento()->false');
		return false;
	}else if(isEmpty($(".bandeiraPagamentoGenerico:last").val()) ){
        if($(".formaPagamento:last").val() == 1 || $(".formaPagamento:last").val() == 2 ){
			FLUIGC.toast({
				title: 'Bandeira',
				message: '<br>É necessário informar a Bandeira de Pagamento' ,
				type: 'warning'
			});	
			console.log('checaPreenchimento()->false');
			return false;
		}
	}else{
		console.log('checaPreenchimento()->true');
		return true;
	}
}
function selTipoPagamento(){
	let formaPagamento = $(".formaPagamento:last").attr("id");
	let identificador = $(".identificadorGenericoPagamento:last").val();
	let valorPagamento = $(".valorGenerico:last").val();
	if (valorPagamento != '' ){
		_PRECO = floatReais(valorPagamento);
	}		
	_DATAEMISSAO = dataRmHoje()
	_SEGUNDONUMERO = identificador;
	if($("#"+formaPagamento).val() == 0  ){
		FLUIGC.toast({
				title: 'Aviso',
				message: '<br>É necessário Informar a forma de Pagamento Primeiro',
				type: 'danger'
			});
		
	}else if($("#"+formaPagamento).val() == 1  ){				
		retornaCODCPG("1");		
	}else if($("#"+formaPagamento).val() == 2  ){		
		retornaCODCPG("2");			
	}else if($("#"+formaPagamento).val() == 3  ){		
		retornaCODCPG("3");
	}else if($("#"+formaPagamento).val() == 4  ){		
		retornaCODCPG("4");
	}
}
function retornaProduto(idProduto){	
	if(idProduto == 'cursos'){		
		return  '04.02.01';		
	}else if(idProduto == 'consultoria'){		
		return  '04.02.02';		
	}else if(idProduto == 'feira' || idProduto == 'locacao' || idProduto == 'apoio'){		
		return  '04.02.03';
	}else if(idProduto == 'palestra' || idProduto == 'seminario' || idProduto == 'workshop' || idProduto == 'congresso' || idProduto == 'missao'){		
		return  '04.02.04';		
	}else if(idProduto == 'empretec'){		
		return  '04.02.05';
	}	
}
function geraMovimento(){	
	var myLoading = FLUIGC.loading(window);	 
	
	if(checaPreenchimento()){
		
		myLoading.show();
 	setTimeout(function(){
				selecionaNatureza();				
 				selTipoPagamento(); 	
 				if(_CODCPG.length == 1){
 					_CODCPG = "0"+_CODCPG;
 				}		
 				var citens = new Array();
 				citens.push(DatasetFactory.createConstraint("CODCOLIGADA", _CODCOLIGADA, _CODCOLIGADA, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("IDMOV", _IDMOV, _IDMOV, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODTMV", _CODTMV, _CODTMV, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODFILIAL", _CODFILIAL, _CODFILIAL, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODCOLCFO", _CODCOLCFO, _CODCOLCFO, ConstraintType.MUST));		
 				citens.push(DatasetFactory.createConstraint("CODCFO", _CODCFO, _CODCFO, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("DATAEMISSAO", _DATAEMISSAO, _DATAEMISSAO, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("DATASAIDA", _DATASAIDA, _DATASAIDA, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("DATAENTREGA", _DATAENTREGA, _DATAENTREGA, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("DATAMOVIMENTO", _DATAMOVIMENTO, _DATAMOVIMENTO, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODLOC", _CODLOC, _CODLOC, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("SEGUNDONUMERO", _SEGUNDONUMERO, _SEGUNDONUMERO, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODCOLCXA", _CODCOLCXA, _CODCOLCXA, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODCXA", _CODCXA, _CODCXA, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODCPG", _CODCPG, _CODCPG, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODTB1FLX", _CODTB1FLX, _CODTB1FLX, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODTB2FLX", _CODTB2FLX, _CODTB2FLX, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("HISTORICOCURTO", _HISTORICOCURTO, _HISTORICOCURTO, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("IDPRD", "607", "607", ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("QUANTIDADE", 1, 1, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("PRECOUNITARIO", _PRECO.toString().replace('.', ','), _PRECO.toString().replace('.', ','), ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODTB1FAT", _CODTB1FAT, _CODTB1FAT, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODUND", "UN", "UN", ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("QUANTIDADETOTAL", 1, 1, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("NSEQITMMOV", 1, 1, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CODCCUSTO", '00996.000008.003', '00996.000008.003', ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("IDMOVRATCCU", '-1', '-1', ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CAMPOLIVRE3", _CAMPOLIVRE3, _CAMPOLIVRE3, ConstraintType.MUST));
 				citens.push(DatasetFactory.createConstraint("CAMPOLIVRE2", _CAMPOLIVRE2,_CAMPOLIVRE2, ConstraintType.MUST)); 				
 				console.log(citens);
 				//citens.push(DatasetFactory.createConstraint("ITENS", xmlItens, xmlItens, ConstraintType.MUST)); // o xml <--- 
 				var dts = DatasetFactory.getDataset("dsInseriMovWSNative", null, citens, null); 				
 				dts; 				
 				if(dts.values[0]["Result"].split(";")[0] == 1){ 
 					var retornoMovimento = dts.values[0]["Result"].split(";")[1];
 					$("#movimentoGeradoRM").val(retornoMovimento + ',');
 	 				FLUIGC.toast({
 	                    title: 'Movimento!',
 	                    message: '<br>Movimento Lançado Com Sucesso!' + dts.values[0]["Result"].split(";")[1],
 	                    type: 'success'
 	 				}); 	 				
 	 				notificarGerenteContratoAssinaturaMov(); 	 			
 	 				if($("#cpfCnpjOk").val() == '' || 'Carregando...'){ 	 					
 	 					notificarGerenteAgenciaIrregularidade();
 	 				}
 	 				countPagamentos();
 	 				myLoading.hide();
 				}else{ 					
 					FLUIGC.toast({
 	                    title: 'Movimento!',
 	                    message: '<br>Não Foi possível Lançar o Movimento!',
 	                    type: 'danger'

 	 					});
 					//countPagamentos();// temporário --- remover
 					myLoading.hide();
 				} 				
		},3000);
	}else{
		console.log('Não Validou Campos de PAgamento')
		myLoading.hide();
	}
}