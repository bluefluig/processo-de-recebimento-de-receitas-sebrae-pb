
function gerarContrato() {
	var txt_nomepessoa  = $("#tbNomePessoa").val();
	var txt_agencia   	= $("#tbAgencia").val();
	var txt_contratos  	= $("#rm_contratosativos").val();
	var txt_projeto   	= $("#rmprojeto").val();
	
	var txt_area			= $("#areaconh").val();
	var txt_subareaconh		= $("#subareaconh").val();
	var txt_tituloEvento	= $("#tituloEvento").val();
	var txt_pubAlvo			= $("#pubAlvo").val();
	var txt_localEvento		= $("#localEvento").val();
	var txt_descricaoEven	= $("#descricaoEven").val();
	
	var NumMovimento		= ($("#nmRespostaWSPag").val()).substring(0, ($("#nmRespostaWSPag").val()).indexOf("-"));
	var AnoEmissao			= '2019'; //($("#datainicial").val()).substring(6,10);
	
	var dataFinalFormat		= $("#datafinalFormat").val().split('-').reverse().join('/');
	
	//Empresa
	var txt_forn_razaosocial	= $("#rm_fcforazaoForn").val();
	var txt_forn_endereco		= $("#rm_fcfoendForn").val();
	var txt_forn_replegal		= $("#respTecnico").val();
	
	var txt_forn_cnpj			= ($("#cpfconsultaForn").val()).substring(0,2) + '.' + ($("#cpfconsultaForn").val()).substring(2,5) + '.' + ($("#cpfconsultaForn").val()).substring(5,8) + '/' + ($("#cpfconsultaForn").val()).substring(8,12) + '-' + ($("#cpfconsultaForn").val()).substring(12,14);
	
	var txt_forn_prestador		= $("#rm_fcfonomeForn").val();
	
	var txt_VALOR 				= $("#cargaHorariaValorTotContrat").val().replace(".",",").extenso(true);
	var txt_VALORHORA 			= $("#valorHoraContrat").val().replace(".",",").extenso(true);
	
	//Agendamento Geral	
	var txt_tiposervico			= $("#tipoSer").val();
	var txt_protocolo			= $("#tbProtocolo").val();
	
	var publicoalvo				= '';
	var txt_atendespont			= $('input[name="existeConsEspontaea"]:checked').val();
	var txt_atendespont2		= $('input[name="_existeConsEspontaea"]:checked').val();
	
	var listaClientes = [];
	
	var nomecli = '';
	
	$('input[id^="atividade___"]').each(function(index, value){
        var seq			= 1; //puxa o primeiro cliente
        var nomecliente = $("#rm_fcforazao___" + seq).val();
        
        nomecli = nomecliente;
        
        listaClientes.push(
        	[
        		nomecliente
        	]
        )
	});
	
	var artesao					= $("#artesao").is(':checked');
	var artesao2				= $("#_artesao").is(':checked');
	
	var epp						= $("#epp").is(':checked');
	var epp2					= $("#_epp").is(':checked');
	
	var me						= $("#me").is(':checked');
	var me2						= $("#_me").is(':checked');
	
	var mei						= $("#mei").is(':checked');
	var mei2					= $("#_mei").is(':checked');
	
	var potencialempreendedor	= $("#potencialempreendedor").is(':checked') ;
	var potencialempreendedor2	= $("#_potencialempreendedor").is(':checked') ;
	
	var potencialempresario		= $("#potencialempresario").is(':checked');
	var potencialempresario2	= $("#_potencialempresario").is(':checked');
	
	var produtorrural			= $("#produtorrural").is(':checked');
	var produtorrural2			= $("#_produtorrural").is(':checked');
	
	var outros					= $("#outros").is(':checked');
	var outros2					= $("#_outros").is(':checked');
	
	var publicoalvo2 = '';
	
	if (artesao == true || artesao2 == true) {
		publicoalvo2 += " Artesão."
	}
	
	if (epp == true || epp2 == true) {
		publicoalvo2 += " EPP."
	}
	
	if (me == true || me2 == true) {
		publicoalvo2 += " ME."
	}
	
	if (mei == true || mei2 == true) {
		publicoalvo2 += " MEI."
	}
	
	if (potencialempreendedor == true || potencialempreendedor2 == true) {
		publicoalvo2 += " Potencial Empreendedor."
	}
	
	if (potencialempresario == true || potencialempresario2 == true) {
		publicoalvo2 += " Potencial Empresário."
	}
	
	if (produtorrural == true || produtorrural2 == true) {
		publicoalvo2 += " Produtor Rural."
	}
	
	if (outros == true || outros2 == true) {
		publicoalvo2 += " Outros"
	}
	
	//console.log(publicoalvo2);
	
	if (txt_atendespont == 'Nao' || txt_atendespont2 == 'Nao') {
		publicoalvo = nomecli;
	} else if (txt_atendespont == 'Sim' || txt_atendespont2 == 'Sim')  {
		publicoalvo = publicoalvo2;
	}
	
	//Evento
	var txt_descservico			= $("#descricaoEven").val();
	var txt_titulosolucao		= $("#nomeevento").val();
	var txt_evento_rua			= $("#ruaEvento").val();
	var txt_evento_bairro		= $("#bairroEvento").val();
	var txt_evento_cidade		= $("#cidadeEvento").val();
	var txt_evento_estado		= $("#estadoEvento").val();
	var txt_evento_cep			= $("#cepEvento").val();
	var txt_complendereco		= $("#complementoEnd").val();
	
	var txt_periodoinicial 		= $("#datainicial").val().split('-').reverse().join('/');
	
	var txt_periodofinal		= $("#datafinal").val().split('-').reverse().join('/');
	var txt_horinicial			= $("#horarioIni").val();
	var txt_horfinal			= $("#horarioFim").val();
	
	//Contratação
	var txt_justificativa		= $("#necessidadeContrato").val();
	var txt_valorhora			= $("#valorHoraContrat").val().replace(".",",");
	var txt_totalhora			= $("#cargaHorariaValorTotContrat").val().replace(".",",");
	var txt_chtotal				= $("#cargaHorariaTotal").val().replace(".00","");
	
	var codigoSGF				= $("#codigoSGF").val();
	
	var cronogramaFisFin = [];
	
	/*Montando cabeçalho da table de Cronograma Físico Financeiro*/
	cronogramaFisFin.push(
		[
			{text: 'Atividade', style: 'tableHeader', alignment:'center'}, 
			{text: 'Início', style: 'tableHeader', alignment:'center'}, 
			{text: 'Término', style: 'tableHeader', alignment:'center'}, 
			{text: 'Carga Horária', style: 'tableHeader', alignment:'center'},
			{text: 'Previsão de Pagamento', style: 'tableHeader', alignment:'center'}
		]);
	
	/*Inserindo os dados da table de Cronograma Físico Financeiro*/
	$('input[id^="atividade___"]').each(function(index, value){
        var seq = $(this).attr("id").split("___")[1];

        var cff_atividade = $("#atividade___" + seq).val();        
        var cff_dtIni = $("#dtInicio___" + seq).val().split('-').reverse().join('/');
        var cff_dtTerm = $("#dtTermino___" + seq).val().split('-').reverse().join('/');
        var cff_chMes = $("#chMes___" + seq).val();
        var cff_dtPrev = $("#dtPrevisaoPgto___" + seq).val().split('-').reverse().join('/');
        
        cronogramaFisFin.push(
    			[
    				{text: cff_atividade, alignment:'center'},
    				{text:cff_dtIni, alignment:'center'},
    				{text:cff_dtTerm, alignment:'center'},
    				{text:cff_chMes, alignment:'center'},
    				{text:cff_dtPrev, alignment:'center'}
    			]
    	);
	});
		
	var docDefinition = { 
			
			pageOrientation: 'portrait',
			pageMargins: [60, 100, 60, 100],
			
			footer: {
				columns: [
					{
						text: 'Teste znczc  nzc zcn cznda adna nanad'
					}
				]
			},
			
			footer: function (currentPage, pageCount) {
				return {
						columns: [
							[
								{ text: 
			                    	[
			                    		'_______________________________________________________________________________________\n',
			                    		'Serviço de Apoio às Micro e Pequenas Empresas da Paraíba\n',
					                    'Avenida Maranhão, 983 - Bairro dos Estados - 58030-261 - João Pessoa - Paraíba\n',
					                    'Telefones: (83) 2108-1100 / 0800 570 0800\n',
					                    'www.sebraepb.com.br\n', 
					                    'CNPJ: 09.139.551/0001-05\n'
			                    	],
			                    	fontSize:8, 
			                    	alignment:'center', 
			                    	color:'gray'
			                    },
			                    { text: 'Página ' + currentPage.toString() + ' de ' + pageCount, fontSize:8, alignment:'right', color:'gray', margin: [0,0,40,0] }
			                ],
			            ]
					}
			},
			
			header: {
		        columns: [
		            {
		               image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAABJCAMAAAAHdUQUAAAAhFBMVEX///8vc7IqcbFPhLYAY6oiba79//vo8fRQhbrh6fH1+PdAfbc5drb///1GgrYAX6v4+/4tc6xvmsVUir3N2ukaa6/w9Pa80OGOrMyguNR/osi1x93Z5e4NY6bF0+GJqs1fjLhslMODp8Shv9KRsMtzncKtxNVAea4ATqQAU6Wqw9xdksM/oDCXAAAGoElEQVRogeVa63abOBAGGYVbRAxGYBuI7Taxd533f7+VZkYgIMmp2kbtOTu/jBCab+4jyUHws9SHuQOF959m5Eh3HjpQlPrCdXTCxXzBekiYC6yzL1xx6IRr7wtXLV3MKJ994Tpw5kAy9oXr4oKLF1tfuDI3Er5wBYFY0npkfOEP1d9JQjy4kDd9ierRgcKNL1yZU1bllS9c6eCUVTtfuDq3bF/7wnVyaSZY2fjCtXNRF0t8wRKRE66TL1z1P08O9G/nKX+JB9cPfo3db1nld5Our+bXHwViESBp6u6w3x+6+vOo1lO3Fs1ebrcfvppP+FFgQXsppSIu5cCqnmwZl8WCct2oz7aOxaa6Gkma2Z6yKDanfu4VdWm2k9bw9pwv2bzSq76QbCp5bMBdqGhXTWl0VYzuvDSkP+KSd8i+HkqL1EJc7mfACoav2Os0JuIVF4nbFNFFgEmNEDLahq7TOu/VcLUs2mzACnhYl6fI2nGIgyzpg5sFtl0VW47LtZAmmVRW4MiSH+EF7RFLTiR5mSn5Ea16VHY3fDLNt8JHqaYa6MzqJdJJnsjysD1JwyRxYVEMekQLylMsRFzgt1A6MmDDws3OUFLp+bAOOx8UHUkBUitY0MNJUZUT5GICUE3qH/pJi2c01mZiU1jWki/o/3LC1UvEG8wJp8gWPRPlGDSuGPWOXaB4Ral243fP2l78DuDldRxuQusri7Y5A62gS7VPkaLhTf++IuBl9/SCw7glFDkCa0bAnOToB8sjxpl5UM6HzbxVEU1RShZDFktrIACJEsvlUcwGkOQZMEMn0B2pCPbcUqQKTvtJoDgqDBJgl49xepXvSm8sxy6rPfENV8gW+gUkrMJ17xI8ttdPZ3RpWofcg2qpACOznRAYzdG43vF96UkuJfNQvNRTRVYxEJaLgAKNotfxgx7IOoQFSqHuh9M0YBe1Ri+g5ag2eIfULFlgtFiI8IvGRG9Z8oGfr7F5WXPANcZQyME3qYd+BCm5TqsbDK4ecSW98oMWuEs+WqcDp9fpFJOcOUkRzVCGM2I5lY+DnddUEnkFo4h1luSQho9svsqRundBgNmgSAFm/HYyxhGZTv8hT8dpEP76Tb3sMc1ZmVBpeJbXOX8GTV6WaV3nboF+a+Miz6YwGV+8fR9dRijjlSbmWm5zX0tvEGsLnMJBWigk+A5lAJPsOXvSCsYAZByqPNbAM5p+KTcLX0zM9Tr/szC2zG0cCsOAjVy4XbkC0R/e5GAEhlcpypVXI0GxxQM5+a1TdLjBHPS7Bt2B6/xHOhhasggUNP4dnjKY98jJj4nlxKVa5oVtfaKyJnUbQml9ecB9x2F09QwrKDwhYHZsmia+YriY7InfMOKLDxzdEmsEf11wCR6gTcvw2ChIUVDZiTFL9osP0OswZswk0MsBdYcRiOOQY0QQm+qOhA8cBW4/yKoV2PTJuCjGLFTV81RgLMqgsJc78hz0DpD9bfrUgGSgL/G6apcmFWEzMSylDxJAMsSQzESDOtY+SeV0Z5+2ab1bNVClXnLadGomsDqc2DSNEvfU9sFMVgT6wA6LGt/OuQRBbiuVOhGIVCyn7Nt00gWHkc9WVhRpgmrRfp9S94PLYv/DoUfBuL6NXUxOMMESyGU3nZVlKNgFxeTf6r7vdtQH6leY/tjj1K8/vowOFZ61A2/IV3R1MYD3oHeBXqiKlcqOWJCmKKOCDDXVJA1rW3CBSVdK0rDlgMXKqNdHonTPYtyUQRUU1MSV2iolWS7SeE33g5VSkHmUGWKoM9K6WEBLwFRxN73qmD1Nw5PPnZLxEnyQVrbf6KZ/fSDHImSJ82UK+qKcrJMMNhnMOpFuMBuBkde3TJzKR7OzMr1S22kLIdAMy01KWGoviuYbFymLGuejX+cZxg8+qX1VBx/I71aoNTke6utO1g4HOusfq1d7LuWgOKh0fz4YN+hvmwUlR2XdNp+ek+Ry6kyA93miB8kIDU65tQJGN2f7ROOhwrfnLGhWXDY70+4pAbO+btu27pvPDyl++CB+fZLw7ofvr/fXnEL8DnIUxpfsWRq7kKdrIdXCwi7uB2mIVlXvq+jdCvwRlcud1FeRCNgyq35G/i61G6dzaNzK+aBnp2sO7u2aY+/0nwnu7ZojcbrUzn3BCm6JC3m75vhr6aOL9Q+u2//fpHaXbuQLWMZdSK620F9FPXepQtbJ7heT26W2v2bC7R9y3Fs8Fi6wrHP7L6bYBdZsI/u1VMvVXu8T8vcPuXvuUhzzX/uH3H+OaoCzwPI04wAAAABJRU5ErkJggg==',
		               width: 100,
		               height: 50,
		               margin:[0,15,-250,0],
		               alignment: 'center'
		            }
		        ]
		    },
		    
		    content: [
		    	{
		    		text:[
		    		{text: 'ORDEM DE SERVIÇO N° ' + NumMovimento + '/' + AnoEmissao, alignment:'center', bold:true, margin:[0,20,0,0]},
		    		{text: '\nSISTEMA DE GESTÃO DE FORNECEDORES - SGF', alignment:'center', fontSize:9, bold:true, margin:[0,20,0,0]}
		    		]
		    	},
				{
					alignment:'justify',
					fontSize:8,
					text:[
						 
					     {text:'\n\n\n\nDAS PARTES CONTRATANTES\n\n',bold:true},
					     'I. O ', {text:'SERVIÇO DE APOIO ÀS MICRO E PEQUENAS EMPRESAS DO ESTADO DA PARAÍBA – SEBRAE/PB',bold:true}, 'Serviço Social Autônomo, ',
					     'inscrito no CNPJ sob o nº. 09.139.551/0001-05, com sede na cidade de João Pessoa, na Avenida Maranhão, 983, Bairro dos Estados, ',
					     'doravante denominado ', {text:'CONTRATANTE',bold:true}, ', e de outro lado,\n\n',
					     
					     'II. A empresa ', txt_forn_razaosocial, ', inscrita no CNPJ sob o nº ', txt_forn_cnpj, ', doravante designada CONTRATADA,', 
					     'com sede ', txt_forn_endereco, ', neste ato representada por ', txt_forn_replegal, '.\n\n',
					     
					     {text:'DA FUNDAMENTO LEGAL\n\n',bold:true},
					     
					     'Esta contratação decorre de processo de credenciamento de Prestadores de Serviços de Consultoria e/ou Instrutoria do Sistema SEBRAE, mediante demanda',
					     'e em regime de não exclusividade, com base no art. 43, do Regulamento de Licitações e de Contratos do Sistema SEBRAE e nos arts. 593 e seguintes do ',
					     'Código Civil Brasileiro, nos termos e nas condições do respectivo Edital de Credenciamento, que integram este Contrato e que as Partes signatárias declaram conhecer e aceitar.\n\n',
					     
					     {text:'CLÁUSULA PRIMEIRA – DO OBJETO\n\n',bold:true},
					     
					     'Este contrato tem por objeto a prestação de serviços de ', txt_tiposervico,' pela CONTRATADA na Área de Conhecimento ', txt_area,' e ',
					     'Subárea ', txt_subareaconh,', conforme solicitação FLUIG nº ', txt_protocolo,', de ', txt_nomepessoa,', de acordo com a descrição abaixo:\n\n',
					     
					     'Público Alvo: ', publicoalvo, '\n',
					     'Justificativa: ', txt_justificativa, '\n\n',
					     
					     'Descrição do serviço: ', txt_descservico, '\n',
					     'Título da solução: ', txt_titulosolucao, '\n',
					     'Local do evento: ', txt_evento_rua, ' - ', txt_evento_bairro, ', CEP: ', txt_evento_cep, ', ', txt_evento_cidade, '/', txt_evento_estado, '\n',
					     'Complemento do endereço: ', txt_complendereco, '\n',
					     'Período do serviço de ', txt_periodoinicial, ' a ', txt_periodofinal, '\n',
					     'Horário: ', txt_horinicial, ' às ', txt_horfinal, '\n',
					     'Carga horária total: ', txt_chtotal, ' horas\n\n',
					     
					     {text:'CLÁUSULA SEGUNDA – DA EXECUÇÃO DOS SERVIÇOS\n\n',bold:true},
					     
					     'Os serviços objeto deste contrato serão executados por ', txt_forn_replegal,', profissional devidamente indicado pela CONTRATADA.\n\n'
					]
					     
				},
					     
				{
					alignment:'justify',
					fontSize:8,
					text:[	
						 					     
					     'Parágrafo Único. Os serviços contratados deverão ser executados, exclusivamente, pelos profissionais da contratada, sócios ou empregados, que tenham sido indicados no ',
					     'processo de credenciamento, sob pena de rescisão deste instrumento e aplicação das penalidades nele previstas.\n\n',
					     
					     {text:'CLÁUSULA TERCEIRA - OBRIGAÇÕES DAS PARTES\n\n',bold:true},
					     
					     
					     {text:'São obrigações do SEBRAE/PB:\n\n',bold:true},
					     					     
					     'I - Prestar informações e esclarecimentos que venham a ser solicitados pelo contratado;\n\n',
					     'II – Notificar o contratado, por escrito, sobre imperfeições, falhas ou irregularidades constatadas na entrega dos serviços para que sejam adotadas medidas corretivas;\n\n',
					     'III – Proporcionar as facilidades necessárias para que o contratado possa entregar os serviços dentro das normas estabelecidas pelo SEBRAE;\n\n',
					     'IV – Impedir que terceiros estranhos ao Contrato intervenham na execução do objeto, ressalvados os casos autorizados pelo SEBRAE;\n\n',
					     'V – Exigir o fiel cumprimento de todos os requisitos acordados, avaliando também a qualidade dos serviços apresentados, podendo rejeitá-los no todo ou em parte.\n\n',
					     
					     {text:'São obrigações da CONTRATADA:\n\n',bold:true},
					     					     
					     'I - Fornecer o objeto contratado em estrita observância ao expresso e previamente autorizado pelo SEBRAE;\n\n',
					     'II - Cumprir integralmente este Instrumento, observando as regras contidas no edital de credenciamento, cabendo ainda ao Contratado a exclusiva responsabilização legal, administrativa e técnica pela execução das atividades inerentes ao objeto contratado;\n\n',
					     'III – Prestar informações e esclarecimentos que venham a ser solicitado pelo SEBRAE, atendendo de imediato às reclamações;\n\n',
					     'IV – Reparar, corrigir, remover ou substituir, às suas expensas, no todo ou em parte, o objeto, quando ocorrem vícios, defeitos, incorreções resultantes da execução, mesmo após ter sido recebido definitivamente pelo SEBRAE;\n\n',
					     'V- Manter entendimento com o SEBRAE, objetivando evitar transtornos e atrasos na entrega do objeto, mantendo sempre a Instituição informada de dados relevantes;\n\n',
					     'VI - Manter sua regularidade fiscal durante todo o período de execução deste Contrato;\n\n',
					     'VII - Responder por quaisquer danos causados ao SEBRAE/PB ou a terceiros, decorrentes de sua culpa ou dolo na execução do contrato, não cabendo a alegação de ausência ou falha na fiscalização ou o acompanhamento do contrato pelo SEBRAE/PB como excludente ou atenuante da responsabilidade da Contratada;\n\n',
					     'VIII - Zelar integralmente pelas informações obtidas, resguardando sempre seu sigilo e confidencialidade não podendo utilizá-las em benefício próprio ou de terceiros, sob pena de sanções legais pertinentes;\n\n',
					     'IX - Observar e cumprir a política de segurança da tecnologia da informação e comunicação do SEBRAE/PB.\n\n',
					     
					     
					     {text:'CLÁUSULA QUARTA – DO VALOR DO CONTRATO E DA FORMA DE PAGAMENTO\n\n',bold:true},
					     
					     
					     'O SEBRAE/PB pagará à CONTRATADA, pelo cumprimento integral do objeto deste contrato, o total de R$ ', txt_totalhora, ' (', txt_VALOR, '), considerando o valor unitário de R$ ', txt_valorhora, ' (', txt_VALORHORA, ') por hora contratada.\n\n',
					     'I - O pagamento será realizado em horário bancário, após o recebimento da Nota Fiscal, devidamente atestados pelo setor competente e de acordo com as condições estabelecidas neste contrato.\n\n',
					     'II - O SEBRAE/PB realiza pagamentos a fornecedores em duas datas preestabelecidas: 15 e 30 de cada mês.\n\n',
				       
				]
			},
					     
			{
				alignment:'justify',
				fontSize:8,
				ol: [
					{text: 'As notas fiscais deverão ser apresentadas, após a realização dos serviços ou entrega dos bens, com antecedência mínima de 08 (oito) dias corridos, em relação a uma das datas disponíveis no calendário de pagamento, hipótese na qual serão adimplidas na data de pagamento mais próxima:\n', counter: '(a)'},
					{text: 'As notas apresentadas, aprovadas, atestadas e encaminhadas à Unidade de Gestão Administrativa e Logística entre os dias 23 (vinte e três) do mês anterior e 07 (sete) do mês corrente serão pagas no dia 15 (quinze);\n', counter: '(b)'},
					{text: 'As notas apresentadas, aprovadas, atestadas encaminhadas à Unidade de Gestão Administrativa e Logística entre os dias 08 (oito) e 22 (vinte e dois) do mês corrente serão pagas no dia 30.\n\n', counter: '(c)'}
				]
			},
			
			{
				alignment:'justify',
				fontSize:8,
				text:[
					'III - O pagamento somente será efetuado após o “atesto”, pelo responsável designado pela Contratante, da Nota Fiscal apresentada pela Contratada, que conterá o detalhamento dos serviços executados.\n\n',
					]
			},
			
			{
				alignment:'justify',
				fontSize:8,
				ol: [
					{text: ' O “atesto” fica condicionado à verificação da conformidade da Nota Fiscal apresentada pela Contratada com os serviços efetivamente prestados.', counter: '(a)'}
				]
			},
			
			{
				alignment:'justify',
				fontSize:8,
				text:[	
					     
					     '\nIV - O pagamento dos serviços executados pela Contratada e aceitos definitivamente pelo SEBRAE/PB será efetuado em conformidade com o Cronograma Físico Financeiro estabelecido abaixo:'
					]
			},
			
			/*Monta uma table com o Cronograma Físico Financeiro*/
			{text: 'Cronograma Físico Financeiro\n', margin: [0, 20, 0, 8]}, 
			{
		    	style: 'table',
		    	alignment:'justify',
		    	fontSize:8,
				table: {
					body: cronogramaFisFin
				}
			},
						  
			{
				alignment:'justify',
				fontSize:8,
				text:[	
					     
					     '\nV - São de responsabilidade exclusiva da Contratada todos os custos e despesas referentes à prestação dos serviços, tais como custos diretos e indiretos (inclusive tributos, encargos sociais e trabalhistas, contribuições parafiscais, transporte, seguro, insumos), além de quaisquer outros necessários ao cumprimento integral do objeto do contrato.\n\n',
					     'VI - A nota fiscal não aprovada pelo SEBRAE/PB será devolvida à Contratada para as necessárias correções, acompanhada das informações que motivaram sua rejeição.\n\n',	
					     'VII - Na hipótese do subitem anterior, o pagamento ficará pendente até que a Contratada promova as medidas saneadoras necessárias. Nessa situação, o prazo para pagamento iniciar-se-á após a regularização das pendências, mediante a reapresentação da nota fiscal devidamente corrigida.\n\n',
					     'VIII - Para fazer jus ao pagamento, a fornecedora adjudicatária deverá apresentar junto às notas fiscais, comprovação de sua adimplência com a Seguridade Social (Certidão Negativa de Débito - CND), com o FGTS (Certificado de Regularidade do FGTS), com a Fazenda Federal e a Justiça Trabalhista, bem como a regularidade de impostos e taxas que porventura incidam sobre os objetos licitados.\n\n',
					     
					     {text:'CLÁUSULA QUINTA – DA VIGÊNCIA \n\n',bold:true},
					     
					     'O prazo de vigência do presente contrato terá início em ', dataAtual(),' e término em ', dataFinalFormat /*format(add(parse(txt_periodofinal), 60))*/ /*dataAtualFim()*/, '.\n\n',
					     
					     {text:'CLÁUSULA SEXTA – DAS PRÁTICAS ANTICORRUPÇÃO\n\n',bold:true},
					     
					     'I - As partes concordam que executarão as suas obrigações de forma ética e de acordo com os princípios aplicáveis ao Sistema SEBRAE previstos no art. 2º do seu Regulamento de Licitações e Contratos.\n\n',
					     'II - A Contratada assume que é expressamente contrária à prática de atos que atentem contra o patrimônio e a imagem do Sistema SEBRAE.\n\n',
					     'III - Nenhuma das partes poderá oferecer, dar ou se comprometer a dar, a quem quer que seja, ou aceitar ou se comprometer a aceitar de quem quer que seja, tanto por conta própria quanto por meio de outrem, qualquer pagamento, doação, compensação, vantagens financeiras ou não financeiras ou benefícios de qualquer espécie que constituam prática ilegal ou de corrupção sob as leis de qualquer país, seja de forma direta ou indireta quanto ao objeto deste contrato, ou de forma que não relacionada a este contrato, devendo garantir, ainda, que seus prepostos e colaboradores ajam da mesma forma.\n\n',
					     'IV - As partes se comprometem a estabelecer, de forma clara e precisa, os deveres e as obrigações de seus agentes e/ou empregados em questões comerciais, para que estejam sempre em conformidade com as leis, as normas vigentes e as determinações do instrumento contratual correspondente.\n\n',

					     {text:'CLÁUSULA SÉTIMA – DA VEDAÇÃO AO NEPOTISMO\n\n',bold:true},
					     
					     //'I - Mediante a lavratura do presente termo de contrato, as partes ratificam o conhecimento prévio acerca da impossibilidade de contratação de empresas cujos sócios ou administradores tenham relação de parentesco com funcionários investidos em cargo de direção, chefia ou assessoramento no âmbito do SEBRAE/PB.\n\n',
					     'Mediante a lavratura do presente termo de contrato, as partes ratificam o conhecimento prévio acerca da impossibilidade de contratação de empresas cujos sócios, administradores ou empregados tenham relação de parentesco, em linha reta ou colateral, por consanguinidade ou afinidade, até o segundo grau, com conselheiro, diretor ou empregado do SEBRAE/PB.\n\n',
					     
					     {text:'CLÁUSULA OITAVA – DAS PENALIDADES\n\n',bold:true},
					     
					     'Em caso de descumprimento parcial ou total do Contrato, o Contratado ficará sujeito às seguintes penalidades:\n\n',
					     'I – Advertência;\n\n',
					     'II - Multa de 1% (um por cento) por dia sobre o valor atualizado do contrato, limitado a 10 (dez) dias, no caso de atraso na entrega dos serviços contratados, não ultrapassando 10% (dez por cento);\n\n',
					     'III - Multa de até 20% (vinte por cento) sobre o valor atualizado do contrato, no caso de execução deficiente, parcial, irregular ou inadequada;\n\n',
					     'IV – Rescisão unilateral do contrato, na hipótese de ocorrer:\n\n',
					     
					    ]
			},
			
			{
				alignment:'justify',
				fontSize:8,
				ol: [
					{text: 'o previsto nos incisos II e III;\n', counter: 'a'},
					{text: 'a extrapolação dos 10 (dez) dias previstos no inciso II, sem prejuízo do pagamento das respectivas multas;\n\n', counter: 'b'}
				]
			},
			
			{
				alignment:'justify',
				fontSize:8,
				text:[	
					     
					     'V – Descredenciamento, caso descumpra o disposto no Edital de Credenciamento aplicável;\n\n',
					     'VI – Suspensão temporária de licitar e contratar com o Sistema SEBRAE, pelo prazo de até 02 (dois) anos.\n\n',
					     
					     {text:'CLÁUSULA NONA - DOS DIREITOS AUTORIAIS E PATRIMONIAIS\n\n',bold:true},
					     
					     'Caso o resultado da execução deste Contrato seja produto/obra sujeito ao regime da propriedade intelectual, a Contratada deverá obter da (s) pessoa (s) física(s) titular(es) dos ',
					     'direitos autorais, por meio de Termo de Cessão de Direitos Autorais Patrimoniais, nos termos da Lei n° 9.610/98, a cessão dos direitos autorais patrimoniais ao SEBRAE/PB de forma total, irrevogável e irretratável;\n\n',
					     
					     {text:'CLÁUSULA DÉCIMA – DO FORO\n\n',bold:true},
					     
					     'O foro para dirimir questões relativas ao presente contrato será o da Comarca de João Pessoa-PB, Estado da Paraíba, com exclusão de qualquer outro.\n\n',
					      
					   
					     {text:'João Pessoa/PB, '+data(),alignment:'center'},
					   
					     '\n\n\n\n',
					     'ASSINATURAS\n\n\n',
					   
					     'Pelo SEBRAE [Assinatura Eletrônica]:\n\n',
					   
					     'Pelo(a) ', txt_forn_razaosocial,': Aceite registrado sob o código de contratação do SGF n° ', codigoSGF,'\n\n\n'
					     
					]
				
				}
		    	
		    ]
	};
	
	// open the PDF in a new window
	//pdfMake.createPdf(docDefinition).open();
	
	const pdfd = pdfMake.createPdf(docDefinition);
    pdfd.getBase64((data) => {
        var form = new FormData();
        form.append('data', data);
        $.ajax({
            url: 'http://dev.sebraepb.com.br/assigns/ugal/embbeded2.php',
            data: form,
            processData: false,
            contentType: false,
            type: 'POST',
            crossDomain: true,
            success: function (ret) {
                console.log(ret);
                window.open(ret, "_blank")
            }
        });
    });

	// print the PDF
	//pdfMake.createPdf(docDefinition).print();

	// download the PDF
	//pdfMake.createPdf(docDefinition).download('optionalName.pdf');
}

function data(){
	var dataAtual = new Date();
	var dia = dataAtual.getUTCDate();
	var mes = mesPorExtenso(dataAtual.getMonth());
	var ano = dataAtual.getFullYear()
	
	//return dia+' de de '+ano+'.';
	return dia + ' de ' + mes + ' de ' + ano + '.';
}

function dataAtual(){
	var dataAtual = new Date();
	var dia = dataAtual.getUTCDate();
	var mes = dataAtual.getMonth()+1;
	var ano = dataAtual.getFullYear();
	
	//return dia+' de de '+ano+'.';
	return dia + '/' + mes + '/' + ano;
}

function dataAtualFim(){
	var dataAtual = new Date();
	var newdate = new Date();
	newdate.setDate(dataAtual.getDate()+60);
	var dia = newdate.getUTCDate();
	var mes = newdate.getMonth();
	var ano = newdate.getFullYear()
	
	//return dia+' de de '+ano+'.';
	return dia + '/' + mes + '/' + ano;
}

function parse(dateStr) {
	var parts = dateStr.split('/').map(function (digits) {
		// Beware of leading zeroes...
		return parseInt(digits, 10);
	});
	return new Date(1900 + parts[2], parts[1], parts[0]);
}

function format(date) {
	var parts = [
		date.getDate(),
	    date.getMonth(),
	    date.getYear()
	  ];
	if (parts[0] < 10) {
		parts[0] = '0' + parts[0];
	}
	if (parts[1] < 10) {
		parts[1] = '0' + parts[1];
	}
	return parts.join('/');
}

function add(date, days) {
	date.setDate(date.getDate() + days);
	return date;
}

function mesPorExtenso(mes){
	var arrayMes = new Array(12);
	
	arrayMes[0] = "Janeiro";
	arrayMes[1] = "Fevereiro";
	arrayMes[2] = "Março";
	arrayMes[3] = "Abril";
	arrayMes[4] = "Maio";
	arrayMes[5] = "Junho";
	arrayMes[6] = "Julho";
	arrayMes[7] = "Agosto";
	arrayMes[8] = "Setembro";
	arrayMes[9] = "Outubro";
	arrayMes[10] = "Novembro";
	arrayMes[11] = "Dezembro";
	
	return arrayMes[parseInt(mes.substr(3,5))-1];
}
String.prototype.extenso = function(c){
	var ex = [
		["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
		["dez", "vinte", "trinta", "quarenta", "cinqüenta", "sessenta", "setenta", "oitenta", "noventa"],
		["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
		["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
	];
	var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
	for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
		j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
		if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
		for(a = -1, l = v.length; ++a < l; t = ""){
			if(!(i = v[a] * 1)) continue;
			i % 100 < 20 && (t += ex[0][i % 100]) ||
			i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
			s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
			((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
		}
		a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
		a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
	}
	return r.join(e);
}

