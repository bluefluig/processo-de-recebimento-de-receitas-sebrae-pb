/*
 * Jean Varlet * 
 * 
 * 
 */

var red, blue, purple, cyan, green, yellow, reset;
red   = '\u001b[31m';
blue  = '\u001b[34m';
purple = '\u001b[35m';
cyan = '\u001b[36m';
green = '\u001b[32m';
yellow = '\u001b[33m';
reset = '\u001b[0m';

console.log(purple + 'Call -> functions.js --- ok ');

var solicitante = $("#codigoSolicitante").val();

/* Document.ready begin -> */
var colabLogado1 = parent.WCMAPI.getUserCode();
$("document").ready(function(){
	
	console.log(MODO_ATUAL);
	console.log(ATIVIDADE_ATUAL);
	var myLoading = FLUIGC.loading(window);
	var teste16 = '200,00';
	console.log('extenso -> ' + teste16.extenso(true));
	//retornaDadosLogado();
	/* Jean Varlet  - Alterando comportamento para os dados do responsável pela solicitação -- 
	loadUserData();
	setTimeout(function(){
	    debuginho();
	},500);
	*/
	//debuginho();
	$("#bntlancMovimento").prop("disabled",true );
	$("#bntEmtRecibo").prop("disabled",true );
	$("#bntEmtContrato").prop("disabled",true );
	$("#formasDePagamentoNovo").hide();	
	$("#dadosConsultoriaSelecionada").hide();
	$("#ProjetoAcaoSelecionar").hide();
	$("#dadosClienteOutrosSelecionado2").hide();
	$("#dadosClienteOUtrosSelecionado1").hide();
	$("#dadosServicoOutros2").hide();
	$("#tipoServicoOutros").hide();
	$("#exibeOutrosServicosText").hide();
	$("#listaConsultorias").hide();
	$("#exibeConsultoria").hide(); 
	$("#Consultoria").hide(); 
	$("#Instrutoria").hide();
	$("#OutrosServs").hide();	
	$("#consultaConsultoria").hide(); 
	$("#consultaInstrutoria").hide();
	$("#consultaFornecedorConsultoria").hide();
	$("#selecionaConsultInstrut").hide();
	$("#consultaFornecedorConsultoriaData").hide();
	$("#painelCertificado").hide();
	$("#dadosEventoSASselecionado").hide();
	$("#tbCodPessoa").hide();
	$("#dadosEmpresa").hide();	
	$("#dadosClienteSAS1").hide();
	$("#dadosClienteSAS2").hide();
	$("#dadosClienteSAS3").hide();
	$("#dadosClienteSAS4").hide();
	$("#uploads").hide();
	$("#subirContratoAssinatura").hide();
	$("#etapaAnexarNotaFiscal").hide();
	$("#cancelaExecucao").hide();
	$("#dadosEmpresaCliente").hide();
	//divs v1 2020 - 
	$("#gerentehomologa").hide();  // atividade -> 31 ---
	$("#vinculoEmpresaCliente").hide();  // atividade -> 4 - vincular empresa Cliente ---
	$("#newDescontoArea").hide();
	$("#modalRegrasDesconto").hide();
	$("#newRateioArea").hide();
	
	$("#buttonEmtRecibo").hide();
	$("#buttonMovFinanceiro").hide();
	$("#valorTotalBruto").hide();
	$("#pagamentoGenerico").hide();
	$("#DivJustificaNaoNF").hide();
	$("#gerenteAtesta").hide();
	$("#contabilizaReceita").hide();
	
	
	
	$("#bntVinculaEmpresa").keypress(function(e){		
		if(e.which == 13){	
			 event.preventDefault()
			console.log(	'Botao pressionado com Enter -> ' + $(this).attr('id')	);
		}		
	});

	$("#bntVinculaEmpresa").click(function(){		
		modalBuscaEmpresa();
	});
	

	/**Controle para os campos referentes as formas de pagamentos
	 * @param none
	 * @return none
	 *
	 * */
	$("#addPagamento").click(function(){		
	if(retornaNatureza() == 'outrosServicos'){		
		if(isEmpty($("#somaSubTotalOutros").val())){
			$("#valorTotalBruto").fadeOut();
			FLUIGC.toast({
			     title: 'Valor',
			     message: '<br>É necessário informar o valor primeiro' ,
			     type: 'danger'
				});
			return false;			
		}else{			
			$("#valorTotalBruto").fadeIn();
			valorBrutoServico();
		}		
	}
	/*Após o usuário clicar para  exibir o primeiro filho de pagamento, esconder o botão addPagamento 
	 * O evento de adição de fihos será controlado pelo botão de gerar movimentos --- */
	$("#addPagamento").hide();
	wdkAddChild('tablepagamentos');
	
	$(".formaPagamento").click(function(){
		console.log('Click - formaPagamento');
	    let id = $(this).attr("id");
	    let number = id.split('___')[1];
	    $(this).change(function(){
	        if($(this).val() != '0'){
	        	
				$(".bntlancMovimento").click(function(){
					$("#perDesconto").prop("readonly",true);
	        		$("#rateioSebrae").prop("readonly",true);
				})
	        	FLUIGC.calendar('#dataPagamentoGenerico___'+number, {
	        		//showToday: false,
	    			pickDate: true,
	    			pickTime: false
	    		});
	        	if($("#proximoPagamento").val() != 0){
	        		$(".identificadorGenericoPagamento:last").click(function(){
	        			$(".valorGenerico:last").val(reais($("#proximoPagamento").val()));	        			
	        		});	        		
	        	}
	        	$("#valorGenericoPagamento___"+number).val(retornaValorPagar());
	        	$("#valorGenericoPagamento___"+number).click(function(){
	        		$(this).val(''); 
	        	});
	        	$("#valorGenericoPagamento___"+number).focusout(function(){	
	        		if(isEmpty($(this).val()) ){
	        			$(this).val(reais('0'));
	        		}else{
	        			$(this).val(reais($(this).val()));		
	        		}
	        		        		
	        	});
	        	
	        	$("#bntlancMovimento___"+number).prop("disabled", false);	        	
	        	$("#dataPagamentoGenerico___"+number).change(function(){
	        		var dataInicio;
	        		var dataPagamento = 'dataPagamentoGenerico___'+number;	        		
	        			if(retornaNatureza() == 'consultoriaSGF' || retornaNatureza() == 'instrutoriaSGF' ){
		        			//consultoriaSGF
		        			dataInicio = 'datainicial';	        			
		        		}else if(retornaNatureza() == 'instrutoriaSAS'){
		        			//instrutoriaSAS
		        			dataInicio = 'dataIniEventoSaSselecionado';		        		
			        	}else if(retornaNatureza() == 'outrosServicos'){
			        		//instrutoriaSAS
			        		if(isEmpty($("#dataInicioOutros").val()) ){
			        			if($("#toaster").children().length <= 3){
			        				FLUIGC.toast({
					       			     title: 'Data de Início',
					       			     message: '<br>É necessário informar a data de início primeiro' ,
					       			     type: 'danger'
					       				});			        				
			        			}			        			
			        		}else{
			        			dataInicio = 'dataInicioOutros';
			        		}			        		
			        	}	        			
	        			//comparar data inserida com a data de inicio do serviço
		        		validarDataPagamento(dataPagamento, dataInicio);	        		
	        	});
	            if($(this).val() == '1'|| $(this).val() == '2'){	               
	                $("#pa"+number).fadeIn();	                
                    $("#anexoGenerico___"+number).click(function(){	    
                    $('#anexoGenerico___'+number).off().on('change', function(){
                    fnCarregaDocAnexo(this, 'pagamento');
                        });
                    });
	            }else if($(this).val() == '3'){
	            	$("#pa"+number).fadeIn();
	            	$("#condicaoPagamentoGenerico___"+number).val('1');
	            	$("#condicaoPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).val('');
                    $("#anexaReciboDeposito___"+number).click(function(){    
                    	$('#anexoGenerico___'+number).off().on('change', function(){
                            fnCarregaDocAnexo(this, 'pagamento');
                        });
                    });
	            }else if($(this).val() == '4'){
	            	$("#pa"+number).fadeIn();
	            	$("#condicaoPagamentoGenerico___"+number).val('1');
	            	$("#condicaoPagamentoGenerico___1").prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).val('');
                    $("#anexaReciboBoleto____"+number).click(function(){    
                    $('#anexoGenerico___'+number).off().on('change', function(){
                        fnCarregaDocAnexo(this, 'pagamento');
                    });

                    });

	            }
	        }
	        
	    });
	    console.log(id);
	    
	});
    
})
$("#rateioSebrae").click(function(){
    if($(".formaPagamento:visible").length){
        $("tr:visible", $("#formasDePagamentoNovo")).remove();
       
    }
     wdkAddChild('tablepagamentos');
    $(".formaPagamento").click(function(){
		console.log('Click - formaPagamento');
	    let id = $(this).attr("id");
	    let number = id.split('___')[1];
	    $(this).change(function(){
	        if($(this).val() != '0'){
	        	//$("#perDesconto").prop("disabled",true);//Jean Varlet 10/02/2020 -
	        	//$("#rateioSebrae").prop("disabled",true);//Jean Varlet 10/02/2020 -
				$(".bntlancMovimento").click(function(){
					$("#perDesconto").prop("readonly",true);
	        		$("#rateioSebrae").prop("readonly",true);
				})
	        	FLUIGC.calendar('#dataPagamentoGenerico___'+number, {
	        		//showToday: false,
	    			pickDate: true,
	    			pickTime: false
	    		});
	        	if($("#proximoPagamento").val() != 0){
	        		$(".identificadorGenericoPagamento:last").click(function(){
	        			$(".valorGenerico:last").val(reais($("#proximoPagamento").val()));	        			
	        		});	        		
	        	}
	        	$("#valorGenericoPagamento___"+number).val(retornaValorPagar());
	        	$("#valorGenericoPagamento___"+number).click(function(){
	        		$(this).val(''); 
	        	});
	        	$("#valorGenericoPagamento___"+number).focusout(function(){	
	        		if(isEmpty($(this).val()) ){
	        			$(this).val(reais('0'));
	        		}else{
	        			$(this).val(reais($(this).val()));		
	        		}
	        		        		
	        	});
	        	
	        	$("#bntlancMovimento___"+number).prop("disabled", false);	        	
	        	$("#dataPagamentoGenerico___"+number).change(function(){
	        		var dataInicio;
	        		var dataPagamento = 'dataPagamentoGenerico___'+number;	        		
	        			if(retornaNatureza() == 'consultoriaSGF' || retornaNatureza() == 'instrutoriaSGF' ){
		        			//consultoriaSGF
		        			dataInicio = 'datainicial';	        			
		        		}else if(retornaNatureza() == 'instrutoriaSAS'){
		        			//instrutoriaSAS
		        			dataInicio = 'dataIniEventoSaSselecionado';		        		
			        	}else if(retornaNatureza() == 'outrosServicos'){
			        		//instrutoriaSAS
			        		if(isEmpty($("#dataInicioOutros").val()) ){
			        			if($("#toaster").children().length <= 3){
			        				FLUIGC.toast({
					       			     title: 'Data de Início',
					       			     message: '<br>É necessário informar a data de início primeiro' ,
					       			     type: 'danger'
					       				});			        				
			        			}			        			
			        		}else{
			        			dataInicio = 'dataInicioOutros';
			        		}			        		
			        	}	        			
	        			//comparar data inserida com a data de inicio do serviço
		        		validarDataPagamento(dataPagamento, dataInicio);	        		
	        	});
	            if($(this).val() == '1'|| $(this).val() == '2'){	               
	                $("#pa"+number).fadeIn();	                
                    $("#anexoGenerico___"+number).click(function(){	    
                    $('#anexoGenerico___'+number).off().on('change', function(){
                    fnCarregaDocAnexo(this, 'pagamento');
                        });
                    });
	            }else if($(this).val() == '3'){
	            	$("#pa"+number).fadeIn();
	            	$("#condicaoPagamentoGenerico___"+number).val('1');
	            	$("#condicaoPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).val('');
                    $("#anexaReciboDeposito___"+number).click(function(){    
                    	$('#anexoGenerico___'+number).off().on('change', function(){
                            fnCarregaDocAnexo(this, 'pagamento');
                        });
                    });
	            }else if($(this).val() == '4'){
	            	$("#pa"+number).fadeIn();
	            	$("#condicaoPagamentoGenerico___"+number).val('1');
	            	$("#condicaoPagamentoGenerico___1").prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).prop("disabled", true);
	            	$("#bandeiraPagamentoGenerico___"+number).val('');
                    $("#anexaReciboBoleto____"+number).click(function(){    
                    $('#anexoGenerico___'+number).off().on('change', function(){
                        fnCarregaDocAnexo(this, 'pagamento');
                    });

                    });

	            }
	        }
	        
	    });
	    console.log(id);
	    
	});
})
	
	$("#btnProjAcao").click(function(){
		//Exibe Formas de pagamento -- 
		 var myLoading = FLUIGC.loading(window);
		 myLoading.show();	
		 setTimeout(function(){
			//$("#bntCadClienteAcesso").click();
			myLoading.hide();
		},500);
		//$("#formasDePagamentoNovo").fadeIn();
		//$("#formaPagamento").prop("disabled", false);
		
	});
	//Controla exibição do rateio em outros serviços  --- 
	$("#servicosSeleciona").click(function(){
	    $(this).change(function(){
	    	if(retornaNatureza() == 'outrosServicos'){
	    		$("#formasDePagamentoNovo").fadeIn();
	    		$("#formaPagamento").prop("disabled", false);
	    		
	    	}else{
	    		$("#formasDePagamentoNovo").fadeOut();
	    		$("#formaPagamento").prop("disabled", false);
	    		
	    	}
	        console.log('valor -> ' + $(this).val());
	        if($(this).val() != '0' && $(this).val() != 'outros' && $(this).val() != 'feiraestandes' ){
	            $("#newRateioArea").fadeIn();
	            $("#newDescontoArea").fadeIn();
	            
	         }else{
	             $("#newRateioArea").fadeOut();
	             $("#newDescontoArea").fadeIn();
	        }
	    });
	});
	
	$("#perDesconto").change(function(){
		if($(this).val() <= 50 && $(this).val() > 0){
			if(!isEmpty($("#fieldValorBrutoh").val())){
				if(typeof($("#fieldValorBrutoh").val()) === 'string'){
					newDesconto( $("#fieldValorBrutoh").val() , $(this).val());	
					$("#subTotalDesconto").val(reais(newDesconto( $("#fieldValorBrutoh").val() , $(this).val())));
					$("#totalDesconto").val(reais(parseFloat($("#fieldValorBrutoh").val()) - floatReais($("#subTotalDesconto").val())));
					$("#existeDesconto").val('sim');
					
				}
				
			}
		}else if($(this).val() > 50){
			$(this).val("50");
			FLUIGC.toast({
				title: 'Aviso!',
				message: '<br>O percentual máximo é de 50%.',
				type: 'danger'
			});
			
		}
		if($(this).val() == 0){
			$("#subTotalDesconto").val(reais('0'));
			$("#totalDesconto").val(reais($("#fieldValorBrutoh").val()));
			$("#existeDesconto").val('nao');
			console.log('existeDesconto -> Alterado -> '+$("#existeDesconto").val() );
			
		}
	
	});	
	/*
	 * Realiza o calculo do Rateio para pagamento cliente/sebrae -
	 * Alterada 28/01/2020 --
	 * Jean Varlet - 
	 * */		
	$("#rateioSebrae").change(function(){		
		let percentRateio = $(this).val(); //ok
		let valorCliente ;
		let valorSebrae ;
		let formaPagamento = $("#formaPagamento").val();
		let valorTotal;
		let retorno;
		let desconto;
		let temp;
		if($("#existeDesconto").val() != 'nao'){
			desconto = true;			
		}else{
			desconto = false;
		}
		if($("#rateioSebrae").val() == 0){
			$("#existeRateio").val('nao');
		}
		if($("#rateioSebrae").val() > 0){
			$("#existeRateio").val('sim');
		}
		
						
		if($("#rateioSebrae").val() > 50){
			$(this).val('0');
			$("#existeRateio").val('nao');
			FLUIGC.toast({
				title: 'Aviso!',
				message: '<br>O percentual máximo é de 50%.',
				type: 'danger'
			});
			
		}
		if(desconto == true){
			//parseFloat($("#fieldValorBrutoh").val()).toPrecision($("#fieldValorBrutoh").val().length);
			console.log('Possui desconto -> ');	
			temp = floatReais($("#totalDesconto").val());
			//valorTotal = parseFloat(temp).toPrecision(temp.length);
			valorTotal = parseFloat(temp).toPrecision(temp.length);			
		}else{
			console.log('Não possui desconto ->');
			temp = floatReais($("#fieldValorBruto").val());
			valorTotal = parseFloat($("#fieldValorBrutoh").val()).toPrecision($("#fieldValorBrutoh").val().length);
			fillDiscountArea();
		}
		retorno = (valorTotal/100) * percentRateio;
		valorCliente = valorTotal - retorno;
		valorPagamento = valorCliente;
		valorSebrae = valorTotal - valorCliente;
		$("#valorCliente").val(reais(valorCliente));
		$("#valorMaquinetaPagamento").val(reais(valorPagamento))
		$("#valorSebrae").val(reais(valorSebrae));		
			
	});		
	
	$("#cepOutros").blur(function(){		
		pesquisacep(this.value);
	})
	
	$("#horarioFimOutros").change(function(){				
		validaHorarios();
	});
	
	$("#servicosSeleciona").change(function(){
		if($("#servicosSeleciona").val() != 0 && $("#servicosSeleciona").val() == 'outros' ){
			$("#exibeOutrosServicosText").fadeIn();
	    }else if( $("#servicosSeleciona").val() != 'outros'){
			$("#exibeOutrosServicosText").fadeOut();
		}
	});		
	
	$("#somaSubTotal").focus(function(){
		if($(this).val() != '' && $(this).val() != null){			
			$(this).val('');
			$("#valorTotalFinal").val('');
			$("#formasDePagamentoNovo").fadeIn();
		}		
	});
	$("#somaSubTotal").blur(function(){		
		if(!isEmpty($(this).val()) ){			
			$(this).val(reais($(this).val()));			
		}else{			
			$(this).val('');
		}		
	});
	
	$("#descontoInformado").focus(function(){
		if($(this).val() != '' && $(this).val() != null){
			
			$(this).val('');
			$("#formasDePagamentoNovo").fadeIn();
		}
		
	});
	$("#descontoInformado").blur(function(){		
		if(!isEmpty($(this).val()) && typeof($(this).val() !="string")){
			$(this).val(reais($(this).val()));
			if(!isEmpty($("#somaSubTotal").val())){				
				fnAplicaDescontoEventos();				
			}else{				
				$("#descontoInformado").val('');
				$("#somaSubTotal").val('');
			}			
		}else if(typeof($(this).val() == "string")){
			$("#descontoInformado").val('');
			//$("#somaSubTotalOutros").val('');
		}			
	});	
	$("#valorTotalFinal").blur(function(){		
		if(!isEmpty($(this).val()) ){			
			$(this).val(reais($(this).val()));			
		}else{			
			$(this).val('');
		}		
	});
	
	// Outros Serviços -- Desconto --- 
	$("#somaSubTotalOutros").focus(function(){
		if($(this).val() != '' && $(this).val() != null){			
			$(this).val('');
			$("#valorTotalFinalOutros").val('');
			$("#formasDePagamentoNovo").fadeIn();
		}		
	});
	$("#somaSubTotalOutros").keypress(function(e){		
		if(e.which == 13){			
			$(this).blur();
			$("#formasDePagamentoNovo").fadeIn();			
			valorBrutoServico();
		}		
	});

	$("#descontoInformadoOutros").focus(function(){
		if($(this).val() != '' && $(this).val() != null){			
			$(this).val('');
			$("#valorTotalFinalOutros").val('');			
		}		
	});
	$("#descontoInformadoOutros").blur(function(){		
		if(!isEmpty($(this).val()) && typeof($(this).val() !="string")){
			$(this).val(reais($(this).val()));
			if(!isEmpty($("#somaSubTotalOutros").val())){				
				fnAplicaDescontoEventos();				
			}else{				
				$("#descontoInformadoOutros").val('');
				$("#somaSubTotalOutros").val('');
			}			
		}else if(typeof($(this).val() == "string")){
			$("#descontoInformadoOutros").val('');
			//$("#somaSubTotalOutros").val('');
		}			
	});	
	$("#somaSubTotalOutros").blur(function(){		
		if(!isEmpty($(this).val()) ){			
			$(this).val(reais($(this).val()));			
		}else{			
			$(this).val('');
		}		
	});
	$("#valorTotalFinalOutros").blur(function(){		
		if(!isEmpty($(this).val()) ){			
			$(this).val(reais($(this).val()));			
		}else{			
			$(this).val('');
		}		
	});
	// Outros Serviços -- Desconto --- 
	
	//Atribui função para consultar consultorias do Fluig --- Remover --- 
	$("#buscarConsultoriasData").click(function(){
		if(!isEmpty($("#dataIniPesqConsult").val()) && !isEmpty($("#dataFimPesqConsult").val()) && !isEmpty($("#cpfconsultaFornecedor").val())){
			if(comparaDatas($("#dataIniPesqConsult").val(),$("#dataFimPesqConsult").val()) == true){
				myLoading.show();
				//retorna  a tabela com as consultorias vinculadas ao cpf/cnpj do consultor
				fnRetornaConsultoria($("#ConsultoriaInstrutoria").val(),
					 $("#dataIniPesqConsult").val(),
					 $("#dataFimPesqConsult").val(),
					 $("#cpfconsultaFornecedor").val()
				);				
			}else{
				
				FLUIGC.toast({
					title: 'Erros Datas!',
					message: '<br>A data Início não pode ser maior que a data Fim.',
					type: 'danger'
				});
				
			}
			
		}else{
			
			
		}
		
		
	});
	//Jean Varlet 14/11/2019 --- Radio Seleciona entre consultoria/instrutoria -- 
	//Obs - Esse é o radio que vem após selecionar Consultoria/Instrutoria e informar o cpf do cliente!!!
	$(".selConsultInst").click(function(){
		
		if($("#selConsultInstConsultoria").is(':checked') == true ){
			//Diferenciar os parametros passados para a funcao que retorna a tabela ---
			
			$("#ConsultoriaInstrutoria").val('Consultoria');
			$("#consultaFornecedorConsultoriaData").fadeIn();
			$("#newRateioArea").fadeIn();
			
		}else if($("#selConsultInstInstrutoria").is(':checked') == true){
			$("#newRateioArea").fadeOut();
			$("#ConsultoriaInstrutoria").val('Instrutoria');
			$("#consultaFornecedorConsultoriaData").fadeIn();
		}
		
	});
	$("#bntEmtRecibo").click(function(){				
		gerarRecibo();			
	});
	
	//Atualizada em 12/11/2019 - Jean Varlet - Comportar 3 naturezas. 
	//Incluir Função para sequenciar os botões de relatórios e movimento RM
	$(".consultInst").click(function(){		
		var naturezaSel;//
		if($("#tipoSerInstrutoria").is(':checked') == true ){				
			naturezaSel = 2; 
			
			alternaNaturezas(naturezaSel);		
			
			limparDiv('exibeConsultoria');
			limparDiv('OutrosServs');
			$("#vinculoEmpresaCliente").show();	
			seqButtons(naturezaSel);	
			/*
			$("#bntEmtRecibo").mouseover(function(){				
				validaFormaPagamento(naturezaSel);				
			});
			*/
			$(" tr:not('.info')", $("#listaConsultorias")).remove();
			
		}else if($("#tipoSerConsultoria").is(':checked')== true){
			
			naturezaSel = 1; 
			alternaNaturezas(naturezaSel);	
			
			$("#vinculoEmpresaCliente").show()
			limparDiv('dadosClienteSelecionado1','dadosClienteSelecionado2');
			limparDiv('OutrosServs');
			
			//limparDiv('formasDePagamentoNovo');
			
			seqButtons(naturezaSel);
			
			
			$(" tr:not('.info')", $("#listaEnvioApoio")).remove();			
			
		}else if($("#tipoSerOutros").is(':checked')== true){			
			naturezaSel = 3; 
			alternaNaturezas(naturezaSel);	
			
			gerarCodigoOutrosServicos();
			
			limparDiv('dadosClienteSelecionado1','dadosClienteSelecionado2');
			limparDiv('exibeConsultoria');
			//limparDiv('formasDePagamentoNovo');
			
			seqButtons(naturezaSel);
			$("#vinculoEmpresaCliente").show()
			$("#bntEmtContrato").mouseover(function(){				
				validaFormaPagamento(naturezaSel);				
			});	
			$("#somaSubTotalOutros").change(function(){
				let value = $(this).val();
				if(value != ""){
					$("#fieldValorBruto").val(reais(value));
					$("#fieldValorBrutoh").val(value);	
				}
				
				
			})
			$("#somaSubTotalOutros").click(function(){
			    $("#subTotalDesconto").val('');
			    $("#totalDesconto").val('');
			    $("#valorSebrae").val('');
			    $("#valorCliente").val('');
			    $("#perDesconto").val(0);
			    $("#rateioSebrae").val(0);
			
			})
$("#perDesconto").click(function(){
    if(zerarRateio()){
        $("#subTotalDesconto").val(reais(newDesconto(floatReais($("#somaSubTotalOutros").val()).toString(),$("#perDesconto").val())));
        $("#totalDesconto").val(reais( floatReais($("#somaSubTotalOutros").val()) - floatReais($("#subTotalDesconto").val())));
    }
  
});
$("#rateioSebrae").click(function(){
    if(checkDiscount()){
        $("#valorSebrae").val( reais(newDesconto(floatReais($("#totalDesconto").val()).toString(),$("#rateioSebrae").val() ) )  );
        $("#valorCliente").val( reais(floatReais($("#totalDesconto").val()) - floatReais($("#valorSebrae").val())));  
    }else{
        $("#valorSebrae").val( reais(newDesconto(floatReais($("#somaSubTotalOutros").val()).toString(),$("#rateioSebrae").val() ) )  );
        $("#valorCliente").val( reais(floatReais($("#totalDesconto").val()) - floatReais($("#somaSubTotalOutros").val())));  
    }
});
function checkDiscount(){
    if($("#perDesconto").val() != 0){
        return true;
    }else{
        return false;
    }
}
function zerarRateio(){
    $("#rateioSebrae").val(0);
    $("#valorSebrae").val('');
    $("#valorCliente").val('');
    return true;
}
			
		
			
			if($("#formaPagamento").val() == "1" || "2"){
				if($("#valorTotalFinalOutros").val() != null || $("#valorTotalFinalOutros").val() != ''){
					$("#dataMaquinetaPagamento").click(function(){
						
						$("#valorMaquinetaPagamento").val($("#valorTotalFinalOutros").val());
					});
					
				}
					
					
			}else if($("#formaPagamento").val() == "3" ){
				if($("#valorTotalFinalOutros").val() != null || $("#valorTotalFinalOutros").val() != ''){
					$("#dataDepositoPagamento").click(function(){
						
						$("#valorDepositoPagamento").val($("#valorTotalFinalOutros").val());
					});
					
				}
					
					
			}else if($("#formaPagamento").val() == "4" ){
				if($("#valorTotalFinalOutros").val() != null || $("#valorTotalFinalOutros").val() != ''){
					$("#dataBoletoPagamento").click(function(){
						
						$("#valorBoletoPagamento").val($("#valorTotalFinalOutros").val());
					});					
				}		
				
			}					
			
			
			//JEan Varlet -- 18/11/2019
			
			
			$(" tr:not('.info')", $("#listaEnvioApoio")).remove();
			$(" tr:not('.info')", $("#listaConsultorias")).remove();
		}
		
		
		
	});
	
	
	$("#divtabelaCliente").show();
	$(".zoom-preview").hide();
	$("#notificaIrregular").hide();
	

	
//Alteração Jailma -- Adicionar pagamento  ############################################################
	
$("#formaPagamento").prop('disabled', true);


// Esconde modalidades de pagamento para posteriormente exibir via selecao ---
$("#cartaoHidden").hide();
$("#depositoHidden").hide();
$("#boletoHidden").hide();
		
		//Jean Varlet - Alterada 12/11/2019 -- add -> remover Toast, remover has-error 
		//Necessita criar Função para destruir dados caso volte para opção -> 0 ou mude de opção; -- feito!!
		//chamar função para checar se um arquivo foi anexado, caso o usuário mude de forma de pagamento. Caso sim, apagar arquivo pelo id -> removerArquivoPasta(idArquivo)
		
$("#formaPagamento").change( function(){

			let valor = $(this).val();
			
			//credito
			if(valor == 1  ){
				$("#cartaoHidden").show();
				$("#depositoHidden").hide();
				$("#boletoHidden").hide();
				$("#condicaoMaquinetaPagamento").prop('disabled', true);				
				limparDiv('depositoHidden', 'boletoHidden');
				//debito
			}else if( valor == 2){
				$("#cartaoHidden").show();
				$("#depositoHidden").hide();
				$("#boletoHidden").hide();
				$("#condicaoMaquinetaPagamento").prop('disabled', false);				
				limparDiv('depositoHidden', 'boletoHidden');
				//deposito
			}else if(valor == 3 ){
				// Deposito
				$("#cartaoHidden").hide();
				$("#depositoHidden").show();
				$("#boletoHidden").hide();				
				limparDiv('cartaoHidden', 'boletoHidden');
			}else if(valor == 4 ){
				// Boleto 
				$("#cartaoHidden").hide();
				$("#depositoHidden").hide();
				$("#boletoHidden").show();				
				limparDiv('depositoHidden', 'cartaoHidden');
			}
			
			else if(valor == 0){				
				$(".alert-dismissible").remove()
				$(`#formaPagamento`).parent().removeClass('has-error');			
				$("#cartaoHidden").hide();
				$("#depositoHidden").hide();
				$("#boletoHidden").hide();				
				limparDiv('depositoHidden', 'cartaoHidden','boletoHidden' );
			}

		});

		//Inicializa campos com formato date

		FLUIGC.calendar('#dataDepositoPagamento', {
			pickDate: true,
			pickTime: false
		});

		
		FLUIGC.calendar('#dataBoletoPagamento', {
			pickDate: true,
			pickTime: false
		});

		FLUIGC.calendar('#dataMaquinetaPagamento', {
			pickDate: true,
			pickTime: false
		});
		
		//Jean Varlet 14/11/2019 --- Campos pesquisa consultoria por data--
		
		FLUIGC.calendar('#dataIniPesqConsult', {
			pickDate: true,
			pickTime: false
		});
		
		FLUIGC.calendar('#dataFimPesqConsult', {
			pickDate: true,
			pickTime: false
		});
		var dia = new Date();
		dia.setDate(dia.getDate() );
		FLUIGC.calendar('#dataInicioOutros', {
			minDate: dia,
			pickDate: true,
			pickTime: false
		});
		FLUIGC.calendar('#dataFimOutros', {
			minDate: dia,
			pickDate: true,
			pickTime: false
		});
	
//Alteração Jailma -- Adicionar pagamento  ############################################################
	 
	 $("#dataInicioOutros").focusout(function(){
		 
		 
	 });
	
	
	$(".btn:contains('Enviar')").mouseover(function(){		
		$("#valorTotalFinal2").val($("#valorTotalFinal").val());
		$("#somaSubTotal2").val($("#somaSubTotal").val());
	    console.log('trigger -> Enviar  ');
	});
	
	$("#descontoInformado").change(function(){
		if(typeof($("#descontoInformado").val()) == 'string'){
			
			fnAplicaDescontoEventos();
			
		}else{
			$(this).val('');
			FLUIGC.toast({
				title: 'Valor Desconto!',
				message: '<br>O valor de desconto não pode ser maior que o Sub Total',
				type: 'danger'
			});
		}
		
		
	});
	/*
	$("#descontoInformadoOutros").change(function(){
		
		fnAplicaDescontoEventos();
		
	});
	*/
	$("#valorTotalFinal").change(function(){
		
		$("#valorTotalFinal2").val($("#valorTotalFinal").val());
		
	});
	$("#valorTotalFinalOutros").change(function(){
		
		$("#valorTotalFinal2Outros").val($("#valorTotalFinalOutros").val());
		
	});
	$("#somaSubTotal").change(function(){
		
		if(typeof($("#somaSubTotal").val()) == 'string'){
			
			$("#somaSubTotal2").val(floatReais($("#somaSubTotal").val()));
		}
		
		
	});
	$("#somaSubTotalOutros").change(function(){
		
		$("#valorTotalFinal2Outros").val($("#somaSubTotalOutros").val());
		
	});

	// Solicitado por Jailma em 21/10/2019 --- esconder área
	//$("#area_Criacao").hide();
	$("#area_CriacaoHidden").hide();
	$("#dadosClienteSelecionado1").hide();
	$("#dadosClienteSelecionado2").hide();
	// esconde o zoom na área da busca do cliente
	$("#zoomClienteTemp").hide();
	
	
	
	$("#cpfconsultasas").blur(function(){		
		var campo = $(this).attr('id');		
		console.log('Campo consultado - > ' + campo );			
		fnCampoCpfSF(campo);		
	});	
	$("#cpfconsultaFornecedor").blur(function(){		
		var campo = $(this).attr('id');
		fnCampoCpfSF(campo);			
	});
	$("#cpfconsultasasOutros").blur(function(){		
		var campo = $(this).attr('id');
		fnCampoCpfSF(campo);			
	});	

	  $('#colabzoom').one('click', function (e) {
		 // $('#colabzoom').children().one('click', function (e) {
		  
          var codigopessoa = document.getElementById("tbCodPessoa").value;
          reloadZoomFilterValues('rm_dadoscolab', 'CODPESSOA,'+codigopessoa); 
      });
	
		$(function() {
        	$("div.container").show();
        	// FLUIGC.calendar('#dataCriacao', { minDate: new Date(),});
        	FLUIGC.calendar('#datainicial', { minDate: new Date(),});
        	FLUIGC.calendar('#datafinal', { minDate: new Date(),});
        	FLUIGC.calendar('#dataPrevisao', { minDate: new Date(),});
        	FLUIGC.calendar('#dataEvento', { minDate: new Date(),}); // Avaliar 
																		// --
																		// Jean
        });
		
		function retornaData(){
			var data = new Date();
			var dia = data.getDate();
			var mes = data.getMonth()+1;
			var ano = data.getFullYear();
			res = dia+"/"+mes+"/"+ano;
			document.getElementById("dataEven").value = res;
		};
		// cloned functions ---
		
if(MODO_ATUAL == 'VIEW'){	
	if(ATIVIDADE_ATUAL == 28){		
		if($("#tipoSerInstrutoria").is(':checked') == true ){	
			
			$("#consultaInstrutoria").fadeIn();				
			$("#Instrutoria").fadeIn();
			$("#Consultoria").fadeOut();
			$("#OutrosServs").fadeOut();
			//$("#consultaFornecedorConsultoria").fadeOut();
			$("#selecionaConsultInstrut").fadeOut();
			$("#consultaConsultoria").fadeOut();
			$("#consultaFornecedorConsultoriaData").fadeOut();
			$("#dadosClienteOUtrosSelecionado1").fadeOut();
			$("#dadosClienteOutrosSelecionado2").fadeOut();
			$("#ProjetoAcaoSelecionar").fadeOut();
			$("#dadosServicoOutros2").fadeOut();
			$("#tipoServicoOutros").fadeOut();
			//JEan Varlet -- 18/11/2019
			$("#formasDePagamentoNovo").fadeOut();
			$("#dadosEventoSASselecionado").fadeOut();
			$("#consultaFornecedorConsultoriaData").fadeOut();
		
		}else if($("#tipoSerConsultoria").is(':checked')== true){
			
			$("#consultaConsultoria").fadeIn();			
			$("#Instrutoria").fadeOut();
			$("#Consultoria").fadeIn();
			$("#OutrosServs").fadeOut();
			$("#consultaInstrutoria").fadeOut();
			$("#dadosClienteSelecionado1").fadeOut();
			$("#dadosClienteSelecionado2").fadeOut();
			$("#dadosClienteOUtrosSelecionado1").fadeOut();
			$("#dadosClienteOutrosSelecionado2").fadeOut();
			$("#ProjetoAcaoSelecionar").fadeOut();
			$("#dadosServicoOutros2").fadeOut();
			$("#tipoServicoOutros").fadeOut();
			//JEan Varlet -- 18/11/2019
			$("#dadosEventoSASselecionado").fadeOut();
			$("#formasDePagamentoNovo").fadeOut();			
			$("#dadosClienteSAS1").fadeOut();
			$("#dadosClienteSAS2").fadeOut();
			$("#dadosClienteSAS3").fadeOut();
			$("#dadosClienteSAS4").fadeOut();			
			
		}else if($("#tipoSerOutros").is(':checked')== true){			
			$("#Instrutoria").fadeOut();
			$("#Consultoria").fadeOut();
			$("#OutrosServs").fadeIn();
			$("#consultaInstrutoria").fadeOut();
			$("#consultaInstrutoria").fadeOut();
			//$("#consultaFornecedorConsultoria").fadeOut();
			$("#selecionaConsultInstrut").fadeOut();
			$("#dadosClienteSelecionado1").fadeOut();
			$("#dadosClienteSelecionado2").fadeOut()
			$("#consultaConsultoria").fadeOut();
			$("#consultaFornecedorConsultoriaData").fadeOut();			
			$("#dadosClienteSAS1").fadeOut();
			$("#dadosClienteSAS2").fadeOut();
			$("#dadosClienteSAS3").fadeOut();
			$("#dadosClienteSAS4").fadeOut();			
			//JEan Varlet -- 18/11/2019
			$("#formasDePagamentoNovo").fadeOut();
			
		}
		
	}// 28 end --- 
	
}	//fim VIEW MODE	
if(MODO_ATUAL == 'ADD' || 'MOD'){
	
	if(ATIVIDADE_ATUAL == '0' || ATIVIDADE_ATUAL == '4'){
		// Início
		//$("#tbCodPessoa").val(parent.WCMAPI.getUserCode());
		//$("#tbNomePessoa").val(parent.WCMAPI.getUser());		
		$("#gestorAssinaContrato").hide();
		$("#agenciaEmiteRecibo").hide();
		$("#financeiroAutoriza").hide();
		$("#financeiroEmiteNota").hide();
		$("#tbNomeSolicitante").val(parent.WCMAPI.getUser());
		
		setTimeout(function(){
			autoScroll('somaPagamentos', 100);
		},500)
	}
	if(ATIVIDADE_ATUAL == '15'){
		//Gerente assina contrato
		$("#financeiroAuto").hide();
		$("#financeiroEmiteNota").hide();
		$("#agenciaEmiteRecibo").hide();
		$("#gestorAssinaContrato").show();
		exibeSolicitacaoEtapas();
		setTimeout(function(){
			autoScroll('nomeGestorAgencia', 100);
		},500)
		$("#nomeGestorAgencia").val(parent.WCMAPI.getUser());
		$("#dataAssinaturaGestor").val(dataAtual());
		$("#CodGestroAgencia").val(parent.WCMAPI.getUserCode());
		$("#bntEmtContrato").mouseover(function(){			
			validaFormaPagamento(naturezaSel);			
		});
	}
	if(ATIVIDADE_ATUAL == '11'){
		//Gerente Atesta 
		$("#gerenteAtesta").show();
		$("#financeiroAutoriza").hide();
		$("#gestorAssinaContrato").hide();		
		$("#financeiroEmiteNota").hide();	
		$("#agenciaEmiteRecibo").hide();		
		$("#nomeGerenteAtesta").val(parent.WCMAPI.getUser());
		$("#dataGerenteAtesta").val(dataAtual());		
		exibeSolicitacaoEtapas();		
		setTimeout(function(){
			autoScroll('dataGerenteAtesta', 100);
		},500)
	}
	if(ATIVIDADE_ATUAL == '13'){
		//Diretor Assina Contrato -- 
		$("#bntEmtContrato").mouseover(function(){			
			validaFormaPagamento(naturezaSel);			
		});
		setTimeout(function(){
			autoScroll('nomeAgenciaRecibo', 100);
		},500)
	}
	if(ATIVIDADE_ATUAL == '17'){
		//Emite Recibo de Pagamento e  Envia Contrato Assinado para o cliente-- 
		$("#gestorAssinaContrato").hide();		
		$("#financeiroAutoriza").hide();
		$("#financeiroEmiteNota").hide();	
		$("#agenciaEmiteRecibo").fadeIn();		
		$("#nomeAgenciaRecibo").val(parent.WCMAPI.getUser());
		$("#dataAgenciaRecibo").val(dataAtual());
		$("#CodAgenciaRecibo").val(parent.WCMAPI.getUserCode())
		exibeSolicitacaoEtapas();
		setTimeout(function(){
			autoScroll('nomeAgenciaRecibo', 100);
		},500)
		$("#bntEmtReciboAgencia").click(function(){			
			gerarRecibo();			
		});
	}
	if(ATIVIDADE_ATUAL == '20'){
		//Financeiro Confere --- 
		$("#financeiroAutoriza").fadeIn();
		$("#gestorAssinaContrato").hide();		
		$("#financeiroEmiteNota").hide();	
		$("#agenciaEmiteRecibo").hide();		
		$("#nomeFinanceiroConfere").val(parent.WCMAPI.getUser());
		$("#dataFinanceiroConfere").val(dataAtual());
		$("#CodFinanceiroConfere").val(parent.WCMAPI.getUserCode());
		exibeSolicitacaoEtapas();
		
		if(retornaNatureza() == "consultoriaSGF" || retornaNatureza() == "instrutoriaSGF"){
			fnChecarPastasX($("#codConsultoria").val());
			fnLinkArquivoX($("#idsComprovantes").val());
		}
		$("#bntBaixarComprovanteFinanceiro").remove();
		$(".financeiroAutNF").click(function(){
		    console.log('click');
		    if($(".financeiroAutNF")[0].checked == true){
				$("#conferenciaFinanceiro").val('sim');
		        console.log('sim');
			}else{
				$("#conferenciaFinanceiro").val('nao');
				$("#DivJustificaNaoNF").fadeIn();
		        console.log('nao');
			}
		    
		});
		setTimeout(function(){
			autoScroll('nomeFinanceiroConfere', 100);
		},500);
		
		
	}
	if(ATIVIDADE_ATUAL == '24'){
		$("#cancelaExecucao").show();
		$("#DivJustificaCancela").hide()
		$("#financeiroAutoriza").hide();
		$("#gestorAssinaContrato").hide();		
		$("#financeiroEmiteNota").hide();	
		$("#agenciaEmiteRecibo").hide();
		$("#nomeCancelaExecucao").val(parent.WCMAPI.getUser());
		$("#dataCancelaExecucao").val(dataAtual());
		$("#CodCancelaExecucao").val(parent.WCMAPI.getUserCode());
		exibeSolicitacaoEtapas();
		$(".radioCancelaExecucao").click(function(){
		    console.log('click');
		    if($(".radioCancelaExecucao")[0].checked == true){
				$("#execucaoServico").val('nao');
				$("#DivJustificaCancela").fadeIn();
		        console.log('sim');
			}else{
				$("#execucaoServico").val('sim');
		        console.log('nao');
			}
		    
		});
		setTimeout(function(){
			autoScroll('nomeCancelaExecucao', 100);	
		},500)
		
	}
	if(ATIVIDADE_ATUAL == '31'){
		//Gerente Homologa Prestação do Serviço --- 
		$("#gerentehomologa").fadeIn();
		$("#cancelaExecucao").hide();
		$("#DivJustificaCancela").hide()
		$("#financeiroAutoriza").hide();
		$("#gestorAssinaContrato").hide();		
		$("#financeiroEmiteNota").hide();	
		$("#agenciaEmiteRecibo").hide();
		$("#nomeGerenteHomologa").val(parent.WCMAPI.getUser());
		$("#dataGerenteHomologa").val(dataAtual());
		//$("#CodCancelaExecucao").val(parent.WCMAPI.getUserCode());
		exibeSolicitacaoEtapas();
		$(".gerenteHomServico").click(function(){
		    console.log('click');
		    if($(".gerenteHomServico")[0].checked == true){				
		        console.log('sim');
			}else{				
		        console.log('nao');
			}		    
		});
		setTimeout(function(){
			autoScroll('gerentehomologa', 100);			
		},500)
		
	}
	if(ATIVIDADE_ATUAL == '33'){
		$("#etapaAnexarNotaFiscal").show();
		$("#financeiroEmiteNota").show();
		$("#financeiroAgNota").show();
		$("#gerentehomologa").hide();
		$("#cancelaExecucao").hide();
		$("#DivJustificaCancela").hide()
		$("#financeiroAutoriza").hide();
		$("#gestorAssinaContrato").hide();		
		$("#financeiroEmiteNota").hide();	
		$("#agenciaEmiteRecibo").hide();
		$("#nomeAgenciaFinanceiroNF").val(parent.WCMAPI.getUser());
		$("#dataAgenciaFinanceiroNF").val(dataAtual());
		 $('#anexaFileNotaFiscal').off().on('change', function(){
             fnCarregaDocAnexo(this, 'pagamento');
         });
		setTimeout(function(){
			autoScroll('nomeAgenciaFinanceiroNF', 100);			
		},500)
	}
	if(ATIVIDADE_ATUAL == '35'){
		$("#contabilizaReceita").show();
		
		
		$("#movimentarRecebimentos").click(function(){
			console.log('Movimentar Receitas -> 2.1.93 para 2.1.05');			
			faturarReceitas();
			
		});
	}
	
	}
});/* Document.ready end -> */
/**
 * Calcula o valor de desconto atualizando via evento .change no campo #perDesconto 
 * Update Jean Varlet 30/05/2020 - Corrigir saída NaN quando desconto === zero */
function newDesconto( subtotal, percent){	
		//Checa natureza da operação ---
		let discount;
		let rate;
		if(!isEmpty(subtotal)){
			if(percent > 0){
	            if(typeof subtotal == 'string'){
	                console.log('string')
	                subtotal = parseFloat(subtotal);               
	            
				  rate = subtotal / 100;
	              discount = percent * rate;
	             return discount;
			}
			
		}else{
			discount = 0;
			return discount; 
		}
		
	}
}
/*Jean Varlet - 30/05/2020 - resolve o problema ao aplicar rateio sem haver desconto.
 * Bloqueia o campo de desconto e atualiza os fields de desconto com os totais gerados
 * pelo Rateio
 **/
function fillDiscountArea(){
    $("#perDesconto").val(0);
    $("#perDesconto").attr("readonly", true);
    $("#subTotalDesconto").val(reais('0'));
    $("#totalDesconto").val($("#valorCliente").val());
    $("#rateioSebrae").change(function(){
        $("#totalDesconto").val($("#valorCliente").val());
    });
}

function fnCapturaCgc(id){		
		var campo = id;		
		console.log('Campo consultado - > ' + campo );			
		fnCampoCpfSF(campo);		
}
//Jean Varlet 13/11/2019 - Falta chamar validador de CNPJ WS pra Consultorias ---
function fnCampoCpfSF(campo){	
	/*Explains -> Se chamada passando o campo ->cpfconsultaFornecedorF será uma consulta de consultorias 
	 * ou passando o campo -> cpfconsultasas  */	
	console.log('fnCampoCpfSF(campo) -> ' + campo );
	//Consultoria/Instrutoria
	if(campo == 'cpfconsultaFornecedor'){
		//valida se o campo está preenchido ---
		if(!isEmpty($("#cpfconsultaFornecedor").val()) ){			
			var valorCampo = $("#" + campo).val();			
			$("#cpfconsultaFornecedorF").val(valorCampo);			
			validacgc("cpfconsultaFornecedorF", $("#cpfconsultaFornecedorF").val());
			
		}else if(isEmpty($("#cpfconsultaFornecedor").val())){			
			FLUIGC.toast({
				title: 'CPF/CNPJ Não informado!',
				message: '<br>O campo de CPF/CNPJ está vazio',
				type: 'warning'
			});
		}
		//Soluções de Educação SAS
	}else if(campo == 'cpfconsultasas'){
		//valida se o campo está preenchido ---
		if(!isEmpty($("#cpfconsultasas").val()) ){			
			var valorCampo = $("#" + campo).val();			
			$("#cpfconsultaF").val(valorCampo);			
			validacgc("cpfconsultaF", $("#cpfconsultaF").val());
			//revalidação inversa sobre preenchimento do campo para 
		}else if(isEmpty($("#cpfconsultaFornecedor").val())){			
			FLUIGC.toast({
				title: 'CPF/CNPJ Não Informado!',
				message: '<br>O campo de CPF/CNPJ está vazio',
				type: 'warning'
			});
		}
		//Outros Serviços
	}else if(campo == 'cpfconsultasasOutros' ){		
		var valorCampo = $("#"+campo).val();
		$("#cpfconsultaOutrosF").val(valorCampo);
		validacgc("cpfconsultaOutrosF", $("#cpfconsultaOutrosF").val());
	}
}
function fnPreparaDados(){
	$("#cpfCnpjValidado").val('1');
	$("#nomeCliente").val($("#nomeCliente1").val());
	$("#razaoCliente").val($("#razaoCliente1").val());
	$("#cpfCnpjFormatado").val($("#cpfconsultaF").val());
	$("#cpfCnpj").val($("#cpfconsultasas").val());
	$("#codClienteSAS").val($("#codSAS").val());
	$("#codClienteRM").val($("#codcfoCliente1").val());	
}
function fnAplicaDescontoEventos(){	
	/*
	if($("#tipoSerInstrutoria").is(":checked") == true){		
		var _retorno = floatReais($("#somaSubTotal").val()) - floatReais($("#descontoInformado").val());		
		if( floatReais($("#descontoInformado").val()) <= floatReais($("#somaSubTotal").val())){			
			$("#valorTotalFinal").val(reais(_retorno));			
		}else{			
			FLUIGC.toast({
				title: 'Valor Desconto!',
				message: '<br>O valor de desconto não pode ser maior que o Sub Total',
				type: 'danger'
			});			
		}	
	}
	
	else 
	*/if($("#tipoSerConsultoria").is(":checked") == true){
		console.log('Consultoria');		
	}else if($("#tipoSerOutros").is(":checked") == true){				
		var _retornoOutros = floatReais($("#somaSubTotalOutros").val()) - floatReais($("#descontoInformadoOutros").val());		
		if( floatReais($("#descontoInformadoOutros").val()) <= floatReais($("#somaSubTotalOutros").val())){			
			$("#valorTotalFinalOutros").val(reais(_retornoOutros));				
		}else{				
			FLUIGC.toast({
				title: 'Valor Desconto!',
				message: '<br>O valor de desconto não pode ser maior que o Sub Total',
				type: 'danger'
			});			
		}		
	}	
}

function fnBuscaClienteConsultoria(){	
	var valorCpf = $("#cpfconsultaFornecedorF").val();	
	if(valorCpf.length > 0 && valorCpf.length != null ){		
		var valorRecebido = valorCpf.replace(/\./g,'');		
		var valorFinal = valorRecebido.replace(/\.|\-|\//g,'');		
		console.log(`Valor para consulta -> ${valorFinal}`);		
		if(valorCpf.length == 14){			
			 var myLoading = FLUIGC.loading(window);
			 myLoading.show();	
			setTimeout(function(){
				if(fnRenderModal(valorFinal, valorCpf, 'cliente') == false){
					myLoading.hide();
					FLUIGC.toast({
					     title: 'Cliente',
					     message: '<br>O cliente não existe na base do sistema SAS' ,
					     type: 'warning'
						});
				}else{
					myLoading.hide();
					$("#selecionaConsultInstrut").fadeIn();
				}				
				
			},2000);			
		}else if(valorCpf.length == 18){			
			 var myLoading = FLUIGC.loading(window);
			 myLoading.show();			 
			 setTimeout(function(){					 
				 if(!fnRenderModal(valorFinal, valorCpf, 'cliente') == false){
					 myLoading.hide();
					 FLUIGC.toast({
					     title: 'Cliente',
					     message: '<br>O cliente não existe na base do sistema SAS' ,
					     type: 'warning'
						});
				 }else{
					 myLoading.hide();
					 $("#selecionaConsultInstrut").fadeIn();					
				 }				
				},2000);			
		}		
	}else{		
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'warning'
		});
	}	
}
//Jean Varlet 18/11/2019---
//Chama modal para Outros Serviços---
function fnConsultaSASOutros(){			
	var valorCpf = $("#cpfconsultaOutrosF").val();	
	if(valorCpf.length > 0 && valorCpf.length != null ){		
		var valorRecebido = valorCpf.replace(/\./g,'');		
		var valorFinal = valorRecebido.replace(/\.|\-|\//g,'');		
		console.log(`Valor para consulta -> ${valorFinal}`);		
		if(valorCpf.length == 14){			
			 var myLoading = FLUIGC.loading(window);
			 myLoading.show();			
			 setTimeout(function(){
				fnRenderModal(valorFinal, valorCpf, 'cliente');				
				myLoading.hide();				
			},2000);				
		}else if(valorCpf.length == 18){			
			 var myLoading = FLUIGC.loading(window);
			 myLoading.show();			 
			 setTimeout(function(){				 
				 fnRenderModal(valorFinal, valorCpf, 'cliente');
				 
				 //fnPreparaDados();
				 myLoading.hide();					
				},2000);			
		}		
	}	
}
// Chama modal para Eventos SAS
function fnAddEventButton(){	
	// Se a listagem de eventos possuir algum evento, destrua/limpe	
	if($("#listaEnvioApoio tr").length == 1 ){		
		var valorCpf = $("#cpfconsultaF").val();		
		if(valorCpf.length > 0 && valorCpf.length != null ){			
			var valorRecebido = valorCpf.replace(/\./g,'');			
			var valorFinal = valorRecebido.replace(/\.|\-|\//g,'');			
			console.log(`Valor para consulta -> ${valorFinal}`);			
			if(valorCpf.length == 14){				
				 var myLoading = FLUIGC.loading(window);
				 myLoading.show();
				 setTimeout(function(){
					fnRenderModal(valorFinal, valorCpf, 'cliente');
					fnDataTable($("#cpfconsultasas").val());
					fnPreparaDados();
					myLoading.hide();					
				},2000);				
			}else if(valorCpf.length == 18){				
				 var myLoading = FLUIGC.loading(window);
				 myLoading.show();				 
				 setTimeout(function(){					 
					 fnRenderModal(valorFinal, valorCpf, 'cliente');
					 fnDataTable($("#cpfconsultasas").val());
					 fnPreparaDados();
					 myLoading.hide();						
					},2000);				
			}			
		}else{			
			 FLUIGC.message.confirm({
				    message: 'Erro!',
				    title: 'O CPF/CNPJ informado não é válido.',
				    labelYes: 'OK',
				    labelNo: 'Cancelar'
				}, function(result, el, ev) {
				   
					if(result){						
						
						
					}else{
						
						console.log('Janela fechada!');
					}
				     
				 
				});
		}
		
	}else if($("#listaEnvioApoio tr").length > 1 ){
		// remove apenas rows de eventos, não removendo o cabeçalho ---
		$(" tr:not('.info')", $("#listaEnvioApoio")).remove();
		
	}

}

function validacgc(el, str){    		
	str = str.replace(/[^\d]+/g, '');
	if (str.length == 11) {
		validacpf(el, str)
	}  else	if (str.length == 14){
		validacnpj(el, str)
	} else if(str.length > 0 && str.length < 11) {
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		});
	}
}
function validacpf(el, strCPF) {
	var Soma;
	var Resto;
	Soma = 0;
	if (strCPF == "00000000000") {
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		})
		return false
	}
    		for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    		Resto = (Soma * 10) % 11;
    		if ((Resto == 10) || (Resto == 11)) Resto = 0;
    		if (Resto != parseInt(strCPF.substring(9, 10))) {
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			})
    			return false
    		}
    		Soma = 0;
    		for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    		Resto = (Soma * 10) % 11;
    		if ((Resto == 10) || (Resto == 11)) Resto = 0;
    		if (Resto != parseInt(strCPF.substring(10, 11))) {
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			})
    			return false
    		}
    		strCPF = strCPF.replace(/(\d{3})(\d)/, "$1.$2"); // Coloca um
																// ponto entre o
																// terceiro e o
																// quarto
																// dígitos
    		strCPF = strCPF.replace(/(\d{3})(\d)/, "$1.$2"); // Coloca um
																// ponto entre o
																// terceiro e o
																// quarto
																// dígitos
    		// de novo (para o segundo bloco de números)
    		strCPF = strCPF.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); // Coloca um
																	// hífen
																	// entre o
																	// terceiro
																	// e o
																	// quarto
																	// dígitos
    		// console.log('cpf alterado:' + strCPF)
    		$('#' + el).val(strCPF);    		
    		return true;
    	}
    	function validacnpj(el, cnpj) {
    		cnpj = cnpj.replace(/[^\d]+/g, '');
    		if (cnpj == '') {
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			})
    			return false
    		}
    		if (cnpj.length != 14){    		
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			});
    			return false
    		}
    		// Elimina CNPJs invalidos conhecidos
    		if (cnpj == "00000000000000" ||
    			cnpj == "11111111111111" ||
    			cnpj == "22222222222222" ||
    			cnpj == "33333333333333" ||
    			cnpj == "44444444444444" ||
    			cnpj == "55555555555555" ||
    			cnpj == "66666666666666" ||
    			cnpj == "77777777777777" ||
    			cnpj == "88888888888888" ||
    			cnpj == "99999999999999")
    		{
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			})
    			return false
    		}

    		// Valida DVs
    		tamanho = cnpj.length - 2
    		numeros = cnpj.substring(0, tamanho);
    		digitos = cnpj.substring(tamanho);
    		soma = 0;
    		pos = tamanho - 7;
    		for (i = tamanho; i >= 1; i--) {
    			soma += numeros.charAt(tamanho - i) * pos--;
    			if (pos < 2)
    				pos = 9;
    		}
    		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    		if (resultado != digitos.charAt(0))
    		{
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			})
    			return false
    		}

    		tamanho = tamanho + 1;
    		numeros = cnpj.substring(0, tamanho);
    		soma = 0;
    		pos = tamanho - 7;
    		for (i = tamanho; i >= 1; i--) {
    			soma += numeros.charAt(tamanho - i) * pos--;
    			if (pos < 2)
    				pos = 9;
    		}
    		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    		if (resultado != digitos.charAt(1))
    		{
    			FLUIGC.toast({
    				title: 'CPF/CNPJ Inválido!',
    				message: '<br>Favor digitar CPF/CNPJ válido',
    				type: 'danger'
    			})
    			return false
    		}

    		cnpj = cnpj.replace(/(\d{2})(\d)/, "$1.$2"); // Coloca um ponto
															// entre o SEGUNDO e
															// o TERCEIRO
															// dígitos
    		// console.log(cnpj)
    		cnpj = cnpj.replace(/(\d{3})(\d)/, "$1.$2"); // Coloca um ponto
															// entre o terceiro
															// e o quarto
															// dígitos
    		// console.log(cnpj)
    		cnpj = cnpj.replace(/(\d{3})(\d)/, "$1/$2"); // Coloca um ponto
															// entre o terceiro
															// e o quarto
															// dígitos
    		// console.log(cnpj)
    		// de novo (para o segundo bloco de números)
    		cnpj = cnpj.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); // Coloca um
																// hífen entre o
																// terceiro e o
																// quarto
																// dígitos
    		// console.log('cnpj alterado:' + cnpj)
    		$('#' + el).val(cnpj);    		
    		return true;
   }
    	
 function fnBuscaDadosInicias(txtCpfCNPJ1){    		
    let valorRecebido = txtCpfCNPJ1.replace(/\./g,'');    		
	let valorFinal = valorRecebido.replace(/\.|\-|\//g,'');    		
	if(valorFinal.length == 11){    			
		console.log('fnBuscaDadosInicias() -> ' + valorFinal.length);    			
		var c1 = DatasetFactory.createConstraint("CgcCpf", valorFinal, valorFinal, ConstraintType.MUST);    			
		var constraints   = new Array(c1);	    			
		var dataset = DatasetFactory.getDataset("dsSASClientePF", null, constraints, null);
		var controle = dataset.values.length; 
		/* Jean Varlet comentado 01/12/2019 -- 
		if(controle > 0  ){
			for(var i = 0; i < controle; i++){    					
			}
		}
		*/
	}else if(valorFinal.length == 14){    			
		console.log('fnBuscaDadosInicias() -> ' + valorFinal.length);    			
		var c1 = DatasetFactory.createConstraint("CgcCpf", valorFinal, valorFinal, ConstraintType.MUST);			
		var constraints   = new Array(c1);			
		var dataset = DatasetFactory.getDataset("dsSASClientePJ", null, constraints, null);
		var controle = dataset;    			
		if(controle == '' ){
    				for(var i = 0; i < controle; i++){    			
			}
		}
		
	}
	
}
    	
function fnBuscaClienteRM(txtCpfCNPJ1){	
	var cgcrm = txtCpfCNPJ1;	
	var c1rm = DatasetFactory.createConstraint("CGCCFO", cgcrm, cgcrm, ConstraintType.MUST);
	var constraintsRM = new Array(c1rm);
	var datasetRM = DatasetFactory.getDataset("dsClienteRM", null, constraintsRM, null);	
	if(datasetRM.values.length > 0 ){		
		console.log('fnBuscaClienteRM(txtCpfCNPJ1) ->  true');		
		return true;		
	}else if(datasetRM.values.length == '' || datasetRM.values.length == null || datasetRM.values.length == undefined){		
		console.log('fnBuscaClienteRM(txtCpfCNPJ1) ->  false');				
		return false;			
	} 				
}


//Bypass -- erro displayfields --- 11/11/2019 ---
/*
function carregaDadosUsuario(){	
	var tbCodPessoa = $("#tbCodPessoa").val();
    var c1 = DatasetFactory.createConstraint("colleagueId", tbCodPessoa, tbCodPessoa , ConstraintType.MUST);	
    var colleague = DatasetFactory.getDataset("colleague", null, new Array(c1), null);	
	var userId = colleague.values[0].login;
	$("#tbNomePessoa").val(colleague.values[0].colleagueName);
	$("#tbCodPessoa").val(colleague.values[0].mail);
	$("#tbUsuario").val(colleague.values[0].colleagueId);
	$("#dadomat01").val(colleague.values[0].colleagueId);
	$("#dataatesto01").val(dataAtual());
	var colab = colleague.values[0].mail;
	var d1 = DatasetFactory.createConstraint("EMAIL", colab, colab, ConstraintType.MUST);
	var constraintsD = new Array(d1);	
	var ds_colab = DatasetFactory.getDataset("rm_consulta_dadoscolaborador", null, constraintsD, null);	
	$("#tbAgencia").val(ds_colab.values[0].SECAO);
	$("#rm_depto").val(ds_colab.values[0].DEPTO);
	$("#rm_dadoscolab").val(ds_colab.values[0].NOME);
	$("#rm_coddepto").val(ds_colab.values[0].CODDEPTO);
	$("#tbDiretorUsuario").val(ds_colab.values[0].CHAPADIRETOR);
}
*/
// Jean Varlet - 11/11/2019 --- incompleta
// Jean Varlet - 12/11/2019 --- Atualizada para evitar a repetição da consulta ao ws toda vez que clicar fora do campo, por conta do evento .blur --
function validarCGCWs(cnpjFormatado){	
	var _valcodSAS = $("#cpfconsultasas").val();
	var _valcliForForn ;	
	var _tipo;// = cnpjFormatado.length;
	var _saida;	
	if($("#tipoSerInstrutoria").is(":checked") == true){
		
		_tipo = $("#cpfconsultaF").val().length;
		_valcliForForn = $("#cpfconsultasas").val();
		_saida = $("#cpfCnpjOk");
		
	}else if($("#tipoSerConsultoria").is(":checked")){
		
		_tipo = $("#cpfconsultaFornecedorF").val().length;
		_valcliForForn = $("#cpfconsultaFornecedor").val();
		_saida = $("#cpfCnpjOk");
	}else if($("#tipoSerOutros").is(":checked")){
		
		_tipo =  $("#cpfconsultaOutrosF").val().length;
		_valcliForForn = $("#cpfconsultasasOutros").val();	
		_saida = $("#cpfCnpjOkOutros");
	}	
	if(_tipo == 18 ){		
		var cnpj = cnpjFormatado;
		var constraints = new Array();
		constraints.push(DatasetFactory.createConstraint('cnpj', cnpj.trim(), null, ConstraintType.MUST));
		var dsTranslate = DatasetFactory.getDataset('dsReceitaWs', null, constraints, null);
		var retorno = dsTranslate.values[0].status;	
		
		if(retorno == 'OK'){			
			$("#cpfCnpjValidado").val('1');
			_saida.val('CNPJ Validado com Sucesso!');			
			FLUIGC.toast({
					title: 'CPF/CNPJ ',
					message: '<br>CNPJ Validado com Sucesso!',
					type: 'success'
				});
		}else if(retorno != 'OK'){	
			var cnpj = cnpjFormatado;
			var constraints = new Array();
			constraints.push(DatasetFactory.createConstraint('cnpj', cnpj, null, ConstraintType.MUST));
			var dsTranslate = DatasetFactory.getDataset('dsReceitaWs', null, constraints, null);
			var retorno = dsTranslate.values[0].status;	
			
		}
			
	}else if(_tipo == 14){	
				
			$("#cpfCnpjValidado").val('1');
			_saida.val('CNPJ Validado com Sucesso!');			
			FLUIGC.toast({
					title: 'CPF/CNPJ ',
					message: '<br>CNPJ Validado com Sucesso!',
					type: 'success'
				});
		
	}

}

function getId(elemento) {
    return id = $(elemento).prop('id').split('___')[1];
}
function isEmpty(value) {
    if (value == null || value === '' || typeof value === 'undefined'){
    	return true;
    }else{
    	return false;	
    }
    
}
function dataAtual() {
	  var data = new Date();
	  var dia  = data.getDate();
	  var mes  = data.getMonth() + 1;
	  var ano  = data.getFullYear();
	  dia  = (dia<=9 ? "0"+dia : dia);
	  mes  = (mes<=9 ? "0"+mes : mes);
	  var newData = dia+"/"+mes+"/"+ano;
	  return newData;
}
function limparDiv(...div) {
    div.forEach(d => {
        $(`#${d}`).find("input,select").val("");
        $(`#${d} :radio`).attr("checked", false);
    });
}
function autoScroll(field, correctPosition) {
	$('html, body').animate({
		scrollTop : $("#" + field).offset().top - correctPosition
	}, 1000);
}
function removerArquivoPasta(idArquivo) {
    let dado = JSON.stringify({
          'id': idArquivo
    });
    return ajaxApi('remover', dado);
}
function comparaDatas2(dataAtual, dataComparar){	
	var partesDataAtual = dataAtual.split("/");
	var partesDataComparar = dataComparar.split("/");
	var dataAtualComp = new Date(partesDataAtual[2], partesDataAtual[1] -1, partesDataAtual[0]);
	var dataCompararComp = new Date(partesDataComparar[2], partesDataComparar[1] -1, partesDataComparar[0]);	
	if(dataAtualComp <= dataCompararComp){		
		return true;
	}else{		
		return false;
	}
}
function comparaDatas(dataIni, dataFim){
	   
    var partesDataIni = dataIni.split("/");
    var partesDataFim = dataFim.split("/");
    var dataIniComp = new Date(partesDataIni[2], partesDataIni[1] - 1, partesDataIni[0]);
    var dataFimComp = new Date(partesDataFim[2], partesDataFim[1] - 1, partesDataFim[0]);
    if(dataIniComp > dataFimComp){

        return false;
    }else{

        return true;
    }
}
function reais(value) {
    return parseFloat(value).toLocaleString('pt-BR', {
        minimumFractionDigits: 2,
        style: 'currency',
        currency: 'BRL'
    });
}
function floatReais(value) {
    return localeParseFloat(value.split('R$')[1].trim(), ',', '.');
}
//Jean Varlet 30/05/2020 --
function localeParseFloat(value) {
    var out = [];
    if(value.indexOf(',') != -1){
         value.split(',').map(function (x) {
			x = x.replace('.', '');
			out.push(x);
        });
        return parseFloat(out.join('.'));
    }else{
    	return 0;
    }
   
    
}
/*
function localeParseFloat(value) {
    var out = [];
    value.split(',').map(function (x) {
        x = x.replace('.', '');
        out.push(x);
    });
    return parseFloat(out.join('.'));
}
*/
function validaHorarios(){	
	var _fim = $("#horarioFimOutros").val();
	var _inicio = $("#horarioIniOutros").val();	
	if(_inicio > _fim){		
		FLUIGC.toast({
			title: 'Erro!',
			message: '<br>A hora inicial não pode ser maior que a hora final.',
			type: 'warning'
		});		
		$("#horarioIniOutros").val('');		
	}else{		
		console.log('Horários ok');
	}	
}
function notificarGerenteContratoAssinaturaMov(){	
	//Dados para definir o Gerente e compor o envio
	//var nomeGerenteUnidade = $("#").val();
	var email = emailColab($("#tbCodPessoa").val())
	var nomeGerenteUnidade = $("#tbNomePessoa").val();
	var cpfCnpjCliente ;
	var emailGerenteUnidade = email;
	var tipoNaturezaServico = $("#tipoServicoContratado").val();
	var nomeCliente;
	var servico;	
	if($("#tipoSerInstrutoria").is(':checked') == true ){		
		servico = $("#eventoTipo1").val();
	}else if($("#tipoSerConsultoria").is(':checked')== true){
		 if($("#selConsultInstConsultoria").is(':checked') == true ){			 
			 servico = 'Consultoria';					 
		 }else if($("#selConsultInstInstrutoria").is(':checked') == true){			 
			 servico = 'Instrutoria';				 
		 }		
	} else  if($("#tipoSerOutros").is(':checked') == true ){		
		if($("#servicosSeleciona").val() != 'outros'){
			servico = $("#servicosSeleciona").val() ;			
		}else if($("#servicosSeleciona").val() == 'outros'){			
			servico =  $("#servicosSeleciona").val() +' - ' + $("#textServicoOutros").val();
		}
	}	
	if($("#tbGerenteUsuario").val() == null){
		nomeGerenteUnidade = $("#tbDiretorUsuario").val();		
	}else{
		nomeGerenteUnidade = $("#tbGerenteUsuario").val();		
	}
	if(tipoNaturezaServico == 1){		
		return null;		
	}else if(tipoNaturezaServico == 2 ){		
		nomeCliente = $("#nomeCliente1").val();
		cpfCnpjCliente = $("#cpfCnpjFormatado").val();		
	}	
	var parametros = {
	        param: [
	           // { "SERVER_URL": "http://fluighomolog.sebraepb.com.br:8080/" },
	            { "SERVER_URL": "http://fluig.sebraepb.com.br:8080/" },
	            //{ "SOLICITACAO_NUM": WKNumProces },
	            { "SOLICITANTE": nomeGerenteUnidade },
	            { 'MENSAGEM': 'Gerente, há um contrato para assinatura no Fluig, no Processo de Recebimento, que trata de \n'+
	            	' um(a)  '+servico+'\n'+
	            	'.'},
	            { 'TENANT_ID': 1 } // company id
	        ]
	    }
		var destinatarios = [emailGerenteUnidade]
		var fields = ['Blue', 'notificaGestorCnpj', JSON.stringify(parametros), JSON.stringify(destinatarios)]
		var ds = DatasetFactory.getDataset('dsEnvioEmail', fields, null, null)
		ds;
	if($("#tipoSerConsultoria").is(':checked')== true){
		FLUIGC.toast({
	        title: 'Notificação!',
	        message: '<br>E-mail enviado - Assinar Contrato' ,
	        type: 'success'
	
			});
	}
}
function baixarComprovante(){	
	altert("Comprovante de Pagamento não Encontrado.");
}
function notificarGerenteAgenciaIrregularidade(){
	
	var email = emailColab($("#tbCodPessoa").val())
	//Dados para definir o Gerente e compor o envio
	//var nomeGerenteUnidade = $("#").val();
	var nomeGerenteUnidade;
	var cpfCnpjCliente ;
	var emailGerenteUnidade = email;
	var tipoNaturezaServico = $("#tipoServicoContratado").val();
	var nomeCliente;
	
	if($("#tbGerenteUsuario").val() == null){
		nomeGerenteUnidade = $("#tbNomePessoa").val();
		
	}else{
		nomeGerenteUnidade = $("#tbNomePessoa").val();
		
	}
	if(tipoNaturezaServico == 1){
		
		return null;
		
	}else if(tipoNaturezaServico == 2 ){
		
		nomeCliente = $("#nomeCliente1").val();
		cpfCnpjCliente = $("#cpfCnpjFormatado").val();
		
	}	
	var parametros = {
	        param: [
	            //{ "SERVER_URL": "http://fluighomolog.sebraepb.com.br:8080/" },
	            { "SERVER_URL": "http://fluig.sebraepb.com.br:8080/" },
	            //{ "SOLICITACAO_NUM": WKNumProces },
	            { "SOLICITANTE": nomeGerenteUnidade },
	            { 'MENSAGEM': 'Caro(a) Gestor(a), o Cliente '+ nomeCliente + ',CPF/CNPJ No  '+cpfCnpjCliente+ ' encontra-se com inconformidade \n'+
	            	' nos dados cadastrais. Se faz necessário contato para efetuar conferência de todos os dados, para assim ser possível \n'+
	            	' prosseguir com a geração do contrato, emissão do recibo e registro da receita.'},
	            { 'TENANT_ID': 1 } // company id
	        ]
	    }

		var destinatarios = [emailGerenteUnidade]
		var fields = ['Blue', 'notificaGestorCnpj', JSON.stringify(parametros), JSON.stringify(destinatarios)]
		var ds = DatasetFactory.getDataset('dsEnvioEmail', fields, null, null)
		ds;
	
		FLUIGC.toast({
         title: 'Notificação!',
         message: '<br>E-mail enviado - CPF/CNPJ Irregular' ,
         type: 'success'

		});
	
}
/*Exibe div com as regras de desconto --- */
function modalRegrasDesconto(){
	if($("#modalRegrasDesconto").is(':visible')){		
		$("#modalRegrasDesconto").fadeOut();
	}else{
		$("#modalRegrasDesconto").fadeIn();
	}
}
function emailColab(chave){
	var cst1 = DatasetFactory.createConstraint('colleagueId', chave, chave, ConstraintType.MUST);
	var retorno = DatasetFactory.getDataset('colleague', null , new Array(cst1), null);
	return retorno.values[0].mail;
}


/*Simplifica descobrir natureza selecionada ---
 * Jean Varlet - 28/01/2020
 * Retorna - consultoriaSGF, instrutoriaSGF, instrutoriaSAS, outrosServicos ---
 * */
function retornaNatureza(){
    let checked = $(".consultInst:checked").val();
    if(checked == 'Consultoria'){
        if($(".selConsultInst:checked").val() == 'consultoria'){
            return 'consultoriaSGF';
        }else if($(".selConsultInst:checked").val() == 'instrutoria'){
            return 'instrutoriaSGF';
        }
    }else if($(".consultInst:checked").val() == 'Instrutoria'){
        return   'instrutoriaSAS';
    }else if($(".consultInst:checked").val() == 'Outros'){
        return   'outrosServicos';
    }
}
/*
 * Simplifica descobrir o ultimo tipo de pagamento selecionado.
 * Jean Varlet  - 27/01/2020
 * */
function retornaTipoPagamento(){
    //.formaPagamento
    let id = $(".formaPagamento:last").attr('id');
    let number = id.split('___')[1];
    return $("#formaPagamento___"+number).val();    
}
/*
 * Confere se o pagamento informado é == ao valor total a ser pago. Considera se houve desconto ou não
 * Jean Varlet = 27/01/2020 - incompleta
 * */
function conferePagamento2(){
    let valorProduto;
    let valorPagamento;
    if(retornaNatureza() == 'consultoriaSGF'){
        if(retornaTipoPagamento() == '1'||'2'){
            valorPagamento = $(".valorMaquineta:last").val();
            if($("#totalDesconto").val() != '' && '0'){
                valorProduto = $("#totalDesconto").val();
            }else{
                valorProduto = $("#cargaHorariaValorTotal").val();
            }            
        }else if(retornaTipoPagamento() == '3'){
            valorPagamento = $(".valorDeposito:last").val();
            if($("#totalDesconto").val() != '' && '0'){
                valorProduto = $("#totalDesconto").val();
            }else{
                valorProduto = $("#cargaHorariaValorTotal").val();
            }
        }else if(retornaTipoPagamento() == '4'){
            valorPagamento = $(".valorBoleto:last").val();
            if($("#totalDesconto").val() != '' && '0'){
                valorProduto = $("#totalDesconto").val();
            }else{
                valorProduto = $("#cargaHorariaValorTotal").val();
            }
        }
    }//continuar outras naturezas---    
}
/*
 * Valida se a data de pagamento é maior que a data de início do serviço contratado
 * Considerando os 3 tipos de naturezas e os tipos de pagamentos selecionados.
 * Recebe os ids -> dataPagamento e dataInicio
 * */
function validarDataPagamento(dataPagamento, dataInicio){
	let dtPgt = invertData($("#"+dataPagamento).val());
	let dtInicio = invertData($("#"+dataInicio).val());
	
	if(moment(dtPgt).isAfter(dtInicio)){
		$("#"+dataPagamento).val('');
		if($("#toaster").children().length < 2){
			FLUIGC.toast({
				title: 'Aviso!',
				message: '<br>A data de pagamento não pode ser superior a data de início do serviço',
				type: 'warning'
			});
			
		}else{
			$("#toaster").children().remove();
			FLUIGC.toast({
				title: 'Aviso!',
				message: '<br>A data de pagamento não pode ser superior a data de início do serviço',
				type: 'warning'
			});
		}
		
		return false;
	}else{
		return true;
	}
	
}
function invertData(data){
	 return data.split('/').reverse().join('-');
}
function anexarNotaFiscal(){	
	$("#anexaFileNotaFiscal").click();
}
function validaCasas(valor){
	var valorx = valor.toString();
	if(valorx.split('.')[1].length > 0){
		console.log('Casas decimais -> ' + valorx.split('.')[1]);
		if(valorx.split('.')[1].length > 1){
			var junction = valorx.split('.')[0] +'.'+ valorx.split('.')[1].substr(0,1);
			
			return parseFloat(junction).toPrecision(junction.length);
		}else{
			var junction = valorx.split('.')[0] + '.0'+ valorx.split('.')[1];
			return parseFloat(junction).toPrecision(junction.length);
		}
	}else{
        var junction = valorx.split('.')[0] + '.00';
        return parseFloat(junction).toPrecision(junction.length);
	}
}
function gerarCodigoOutrosServicos() {
    $("#codServicoOutros").val( Math.random().toString(36).slice(-10));
}
function exibeSolicitacaoEtapas(){
	if(retornaNatureza() === 'instrutoriaSAS'){
		$("#consultaInstrutoria").fadeIn();				
		$("#Instrutoria").fadeIn();
		$("#Consultoria").fadeOut();
		$("#OutrosServs").fadeOut();
		//$("#consultaFornecedorConsultoria").fadeOut();
		$("#selecionaConsultInstrut").fadeOut();
		$("#consultaConsultoria").fadeOut();
		$("#consultaFornecedorConsultoriaData").fadeOut();
		$("#dadosClienteOUtrosSelecionado1").fadeOut();
		$("#dadosClienteOutrosSelecionado2").fadeOut();
		$("#ProjetoAcaoSelecionar").fadeOut();
		$("#dadosServicoOutros2").fadeOut();
		$("#tipoServicoOutros").fadeOut();
		//JEan Varlet -- 18/11/2019
		$("#formasDePagamentoNovo").fadeOut();
		$("#dadosEventoSASselecionado").fadeIn();
		$("#consultaFornecedorConsultoriaData").fadeOut();
		
		$("#dadosClienteSelecionado1").fadeIn();
		$("#dadosClienteSAS4").fadeIn();
		$("#dadosClienteSAS1").fadeIn();
		$("#dadosClienteSAS2").fadeIn();
		
		$("#dadosClienteSelecionado2").fadeIn();
		
		$("#dadosEventoSASselecionado").fadeIn();
		$("#valorTotalBruto").fadeIn();
		
		$("#valorTotalBruto").fadeIn();
		$("#newDescontoArea").fadeIn();
		$("#formasDePagamentoNovo").fadeIn();
		desabilitaCamposDiv('Instrutoria','rateio', 'newDescontoArea', 'valorTotalBruto', 'formasDePagamentoNovo', 'tipoSer', 'consultaInstrutoria');
	}else if(retornaNatureza() === 'consultoriaSGF'){
		 $("#Consultoria").fadeIn();
		 $("#formasDePagamentoNovo").fadeIn();
		 $("#newRateioArea").fadeIn();
		 $("#exibeConsultoria").fadeIn();
		 $("#dadosConsultoriaSelecionada").fadeIn();
		 $("#dadosConsultoriaSelecionada2").fadeIn();
		 $("#dadosConsultoriaSelecionada1").fadeIn();
		 $("#selecionaConsultInstrut").fadeIn();
		 $("#consultaConsultoria").fadeIn();
		 $("#consultaFornecedorConsultoriaData").fadeIn();
		 desabilitaCamposDiv('exibeConsultoria', 'consultaFornecedorConsultoriaData', 'selecionaConsultInstrut',
				 'rateio', 'newDescontoArea', 'valorTotalBruto','consultaConsultoria', 'formasDePagamentoNovo', 'tipoSer')
	}
}
/*Altera comportamento de exibição da tela, baseado na natureza selecionada.
 * incompleta
 * */
function alternaNaturezas(natureza){    
    if(natureza == 2){
        //show
        $("#consultaInstrutoria").fadeIn();
        $("#Instrutoria").fadeIn();  
        //hide
        $("#newRateioArea").fadeOut();
        $("#dadosClienteSelecionado2").fadeOut();
		$("#Consultoria").fadeOut();	
        $("#percentRateioDiv").hide();
        $("#valorSebraeDiv").hide();
        $("#valorSebrae").hide();
        $("#valorClienteDiv").hide();
        $("#Consultoria").fadeOut();
        $("#OutrosServs").fadeOut();
        //$("#consultaFornecedorConsultoria").fadeOut();
        $("#selecionaConsultInstrut").fadeOut();
        $("#consultaConsultoria").fadeOut();
        $("#consultaFornecedorConsultoriaData").fadeOut();
        $("#dadosClienteOUtrosSelecionado1").fadeOut();
        $("#dadosClienteOutrosSelecionado2").fadeOut();
        $("#ProjetoAcaoSelecionar").fadeOut();
        $("#dadosServicoOutros2").fadeOut();
        $("#tipoServicoOutros").fadeOut();
        //JEan Varlet -- 18/11/2019
        $("#formasDePagamentoNovo").fadeOut();
        $("#dadosEventoSASselecionado").fadeOut();
        $("#consultaFornecedorConsultoriaData").fadeOut();
        $("#newDescontoArea").fadeOut();
    }else if(natureza == 1 ){
        $("#consultaConsultoria").fadeIn();
        $("#Consultoria").fadeIn();
        
        $("#Instrutoria").fadeOut();			
        $("#OutrosServs").fadeOut();
        $("#consultaInstrutoria").fadeOut();
        $("#dadosClienteSelecionado1").fadeOut();
        $("#dadosClienteSelecionado2").fadeOut();
        $("#dadosClienteOUtrosSelecionado1").fadeOut();
        $("#dadosClienteOutrosSelecionado2").fadeOut();
        $("#ProjetoAcaoSelecionar").fadeOut();
        $("#dadosServicoOutros2").fadeOut();
        $("#tipoServicoOutros").fadeOut();		
        $("#dadosEventoSASselecionado").fadeOut();
        $("#formasDePagamentoNovo").fadeOut();			
        $("#dadosClienteSAS1").fadeOut();
        $("#dadosClienteSAS2").fadeOut();
        $("#dadosClienteSAS3").fadeOut();
        $("#dadosClienteSAS4").fadeOut();
        $("#newDescontoArea").fadeOut();
    }else if(natureza == 3){
        $("#OutrosServs").fadeIn(); 
        //$("#newRateioArea").fadeIn();

        $("#Consultoria").fadeOut();
        $("#dadosClienteSelecionado2").fadeOut();
        $("#Instrutoria").fadeOut();
        $("#Consultoria").fadeOut();
        $("#OutrosServs").fadeIn();
        $("#consultaInstrutoria").fadeOut();
        $("#consultaInstrutoria").fadeOut();       
        $("#selecionaConsultInstrut").fadeOut();
        $("#dadosClienteSelecionado1").fadeOut();
        $("#dadosClienteSelecionado2").fadeOut()
        $("#consultaConsultoria").fadeOut();
        $("#consultaFornecedorConsultoriaData").fadeOut();
        $("#dadosClienteSAS1").fadeOut();
        $("#dadosClienteSAS2").fadeOut();
        $("#dadosClienteSAS3").fadeOut();
        $("#dadosClienteSAS4").fadeOut();
        $("#newDescontoArea").fadeOut();
        $("#formasDePagamentoNovo").fadeOut();
    }
}
function fnLinkArquivoX(folderId){	
	console.log('folderId -> '+ folderId);
	$.ajax({
		url: "http://fluig.sebraepb.com.br:8080/api/public/ecm/document/listDocumentWithChildren/"+parseInt(folderId) ,
		async : false,        
        dataType : 'application/json; charset=utf-8',
		type : "GET",
        dataType : 'JSON',        	
		success : function(data){
            console.log(data);
            console.log(data.content[0].children.length)
			if(data.content[0].children.length > 0 ){
				for(var p = 0; p < data.content[0].children.length; p++){
                    if(data.content[0].children[p]["removed"] == false){
                        console.log(data.content[0].children[p]["fileURL"])
                        console.log('added link')                                    	 
                        $("#linksComprovantes").append('<br><label><i class="fluigicon fluigicon-download icon-sm"></i>&nbsp;  <a name="visualizarArquivo" href="'+data.content[0].children[p]["fileURL"]+'" target="_blank" >&nbsp;Comprovante de Pagamento Anexo</a></label>')
                    }                   
				}
			}else{
				console.log('zero files')
			}					
		}, error: function(e){
            console.log('error-> ')
            console.log(e.responseText.values)
        }
	});	
}

function fnChecarPastasX(codEvento){
	var contadorp = 0 ;	
    var counter = 0;	
		$.ajax({			
			url: urlBase+'/api/public/ecm/document/listDocumentWithChildren/'+ '15880',
			crossDomain: true,
			type : "GET",
			async : false,
			//contentType: "application/json",
			dataType : "json", 
			success : function(data){
                //console.log(data);
                var contadorx = data.content[0].children.length;
                console.log('contadorx -> ' + contadorx);
                console.log(data);
              if(contadorx > 0){
                  for(var x = 0; x < contadorx; x++ ){					
                         if((data.content[0].children[x]["description"] == codEvento) ){
 	                    	 
	                          var pastaSelecionada = data.content[0].children[x]["id"];
                              console.log(data);
	                          console.log('pastaSelecionada -> '+ pastaSelecionada);
							  counter = counter +1;
                               //fnLinkArquivoX(pastaSelecionada);
                              $("#idsComprovantes").val( pastaSelecionada);
	                          //return pastaSelecionada;
	                       //compara counter == 0 e se i é == ao valor máximo do indexador do loop !!
	                      }else if(counter == 0 && i == contadorx){
	                          console.log('Não Encontrado!'); 
	                          return false;
	                      } 	
                	
                		 
                		               		  
                	                	 
                  }//end for =----
              }// if contadorx
			},error: function(e){				
				console.log(e);
			},//fim chamada ajax 1
			
		});
}

function debuginho(){
	console.log('debuguinho()-> ')
    let arrayDados = new Array();
    let idsDados = new Array();
   idsDados.push('tbNomePessoa','tbCodPessoa','tbUsuario','dadomat01','dataatesto01','tbAgencia','rm_depto','rm_dadoscolab','rm_coddepto','tbDiretorUsuario') ; 
   arrayDados.push($("#tbNomePessoa").val(),$("#tbCodPessoa").val(),$("#tbUsuario").val(),$("#dadomat01").val(),$("#dataatesto01").val(),$("#tbAgencia").val(),$("#rm_depto").val(),$("#rm_dadoscolab").val(),$("#rm_coddepto").val(),$("#tbDiretorUsuario").val() );
    for(var i = 0; i < arrayDados.length; i++){
        console.log(idsDados[i] + ' -> '+arrayDados[i] );
    }
    
}
/*Desabilitar Campo zoom */
function desabilitaZoom(...campos) {
    campos.forEach(campo => {
        window[`${campo}`].clear();
        window[`${campo}`].disable(true);
    });
}
/*Habilitar campo zoom*/
function habilitaZoom(...campos) {
    campos.forEach(campo => {
        //window[`${campo}`].clear();
       window[`${campo}`].disable(false);
    });
}
function retornaDadosLogado(){
    let tbUsuario = $("#tbUsuario").val();
    let tbCodPessoa =  $("#tbCodPessoa").val();
    let tbNomePessoa = $("#tbNomePessoa").val();

    console.log(`tbNomePessoa-> ${tbNomePessoa} - tbCodPessoa-> ${tbCodPessoa} - tbUsuario-> ${tbUsuario}`);
}

function selectUser(colaborador){
    let arrayColabs = new Array();
    let idConsulta = 'yf8xqo35ctxschu41531148008103';
    let retorno;
    arrayColabs.push('03539857fa9211e997e20a5864604340','5e6a242bad3f11e9a5b70a5864600c69',             '606557e5ad3f11e9819e0a5864612d26','70bf9accad4311e9a5b70a5864600c69', '62687b74ad3f11e99ca20a586460437b');
    for(var i = 0; i< arrayColabs.length; i++){
        if(colaborador == arrayColabs[i]){
            console.log('Id Colab Encontrado -> '+ colaborador);
            retorno = idConsulta;
           // return retorno;
            return false;
        }else{
            console.log('Id Colab Não Encontrado -> ');
            return false;
        }        
    }
}

function loadUserData(){
    let logado = selectUser(colabLogado1);
    let c2, d1, colleague, ds_colab;
    if(logado != false || logado != 'false'){
       c2 = DatasetFactory.createConstraint('colleagueId', logado, logado, ConstraintType.MUST);
       colleague = DatasetFactory.getDataset('colleague', null, [c2], null);
        $("#tbNomePessoa").val(colleague.values[0].colleagueName);
        $("#tbCodPessoa").val(colleague.values[0].mail);
        $("#tbUsuario").val(colleague.values[0]['colleaguePK.colleagueId']);
        $("#dadomat01").val(colleague.values[0]['colleaguePK.colleagueId']);
        $("#dataatesto01").val(dataAtual()); 

        let colab = colleague.values[0].mail;
        d1 = DatasetFactory.createConstraint('EMAIL', colab, colab, ConstraintType.MUST);
        ds_colab = DatasetFactory.getDataset('rm_consulta_dadoscolaborador', null, [d1], null);

        $("#tbAgencia").val(ds_colab.values[0].SECAO);
        $("#rm_depto").val(ds_colab.values[0].DEPTO);
        $("#rm_dadoscolab").val(ds_colab.values[0].NOME);
        $("#rm_coddepto").val(ds_colab.values[0].CODDEPTO);
        $("#tbDiretorUsuario").val(ds_colab.values[0].CHAPADIRETOR);
    }else{
        console.log('Erro ao retornar o Id do Colab -- Checar sub function - selectUser()')
    }
}
/*
 * Toast - 
 */
function toast(titulo, msg, tipo) {
    FLUIGC.toast({ title: titulo, message: msg, type: tipo });
}