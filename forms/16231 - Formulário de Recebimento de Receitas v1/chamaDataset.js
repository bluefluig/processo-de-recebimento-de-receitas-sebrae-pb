function chamaDataset(dsName, fields, constraints, orders) {


    loadOauth1();

    var request_data = {
        url: WCMAPI.serverURL + '/api/public/ecm/dataset/datasets',
        method: 'POST',
    };

    var data = { 
        name: dsName, 
        fields: fields, 
        constraints: constraints, 
        order: orders 
    }
    
    var ret = null;

    $.ajax({
        url: request_data.url,
        crossDomain: true,
        async: false,
        type: request_data.method,
        data: JSON.stringify(data),
        //headers: oauth.toHeader(oauth.authorize(request_data, token)),
        headers:inOauth.oauth1_fluig,
        contentType: "application/json"
    }).done(function (data) {
        ret = data.content;
    });

    //mostraDetalhesClientes(getCpf(), getCodEmprestimo())
    return ret;
}

/** Object Oauth fluig **/
var inOauth = new Object({
	oauth1_fluig: new Object(),
	consumer_post: new Object({
		'public': 'post-app-1',
		'secret': 'post-app-secret-1'
	}),
	token_post: new Object({
		'public': 'b3654ab8-05c3-4bb0-8b8c-9992b54c3e0a',
		'secret': '1d40649c-f986-4bb7-a581-b7611c26e1a4a30d2f26-6475-4bae-adf0-f35910a1731a'
	}),
	consumer_get: new Object({
		'public': 'get-app-1',
		'secret': 'get-app-secret-1'
	}),
	token_get: new Object({
		'public': '2bdfa846-3246-462e-bf02-f16400f77f68',
		'secret': '2d85fa34-4547-431f-a735-c57ca4b94650486d7b16-29b7-4caf-b804-49c777febe86'
	})
});

function loadOauth1() {			
	this.oauth = OAuth({
		consumer: inOauth.consumer_post,
		signature_method: 'HMAC-SHA1',
		parameter_seperator: ",",
		nonce_length: 6
	});
	this.serverUrl = top.WCMAPI.serverURL;
	this.request_data = {
			url: this.serverUrl + '/api/public/ecm/dataset/datasets', //NOTE: Api
			method: 'POST',
			data: ''
	};
	this.token = inOauth.token_post;
	this.headers = this.oauth.toHeader(this.oauth.authorize(this.request_data, this.token));
	inOauth.oauth1_fluig = this.headers;
}

