function geraMovimento(CODCOLIGADA, IDMOV, CODTMV, SERIE, DATAEMISSAO, DATASAIDA, DATAMOVIMENTO, DATAENTREGA, CODLOC, SEGUNDONUMERO,
    CODCOLCXA, CODCXA, CODCPG,CODTB1FLX,CODTB2FLX,HISTORICOCURTO, PRECOUNITARIO, CODDEPARTAMENTO, CODCOLTBORCAMENTO, CODTBORCAMENTO,
    INTEGRAAPLICACAO, STATUS, CODVEN1, NUMEROMOV, CODCOLCFO, ENQUADRAMENTO, MODALIDADE, PERIODOEXECACS, NATUREZAACS, IDMOVRATCCU,
    IDPRD, QUANTIDADE, CODTB1FAT, CAMPOLIVRE1, NUMEROSEQUENCIAL,NSEQITMMOV, CODCCUSTO, CODFILIAL, CODCOLCFO ){
	
	var citens = new Array();
	citens.push(DatasetFactory.createConstraint("CODCOLIGADA", CODCOLIGADA, CODCOLIGADA, ConstraintType.MUST));
	citens.push(DatasetFactory.createConstraint("IDMOV", IDMOV, IDMOV, ConstraintType.MUST));
	citens.push(DatasetFactory.createConstraint("CODTMV", CODTMV, CODTMV, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("SERIE", SERIE, SERIE, ConstraintType.MUST));//new
    citens.push(DatasetFactory.createConstraint("DATAEMISSAO", DATAEMISSAO, DATAEMISSAO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("DATASAIDA", DATASAIDA, DATASAIDA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("DATAMOVIMENTO", DATAMOVIMENTO, DATAMOVIMENTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("DATAENTREGA", DATAENTREGA, DATAENTREGA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODLOC", CODLOC, CODLOC, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("SEGUNDONUMERO", SEGUNDONUMERO, SEGUNDONUMERO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCOLCXA", CODCOLCXA, CODCOLCXA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCXA", CODCXA, CODCXA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCPG", CODCPG, CODCPG, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODTB1FLX", CODTB1FLX, CODTB1FLX, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODTB2FLX", CODTB2FLX, CODTB2FLX, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("HISTORICOCURTO", HISTORICOCURTO, HISTORICOCURTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("PRECOUNITARIO", PRECOUNITARIO, PRECOUNITARIO, ConstraintType.MUST)); // valor hora
    citens.push(DatasetFactory.createConstraint("CODUND", "UN", "UN", ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODDEPARTAMENTO", CODDEPARTAMENTO, CODDEPARTAMENTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCOLTBORCAMENTO", CODCOLTBORCAMENTO, CODCOLTBORCAMENTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODTBORCAMENTO", CODTBORCAMENTO, CODTBORCAMENTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("INTEGRAAPLICACAO", INTEGRAAPLICACAO, INTEGRAAPLICACAO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("STATUS", STATUS, STATUS, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODVEN1", CODVEN1, CODVEN1, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("NUMEROMOV", NUMEROMOV, NUMEROMOV, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCFO", CODCOLCFO, CODCOLCFO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("ENQUADRAMENTO", ENQUADRAMENTO, ENQUADRAMENTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("MODALIDADE", MODALIDADE, MODALIDADE, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("PERIODOEXECACS", PERIODOEXECACS, PERIODOEXECACS, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("NATUREZAACS", NATUREZAACS, NATUREZAACS, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("IDMOVRATCCU", '-1', '-1', ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("IDPRD", IDPRD, IDPRD, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("QUANTIDADE", QUANTIDADE, QUANTIDADE, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODTB1FAT", CODTB1FAT, CODTB1FAT, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CAMPOLIVRE1", CAMPOLIVRE1, CAMPOLIVRE1, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("NUMEROSEQUENCIAL", NUMEROSEQUENCIAL, NUMEROSEQUENCIAL, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("NSEQITMMOV", NSEQITMMOV, NSEQITMMOV, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("QUANTIDADETOTAL", QUANTIDADE, QUANTIDADE, ConstraintType.MUST));//horas
    citens.push(DatasetFactory.createConstraint("CODCCUSTO", CODCCUSTO, CODCCUSTO, ConstraintType.MUST));//rm_codunidade___

	citens.push(DatasetFactory.createConstraint("CODFILIAL", CODFILIAL, CODFILIAL, ConstraintType.MUST));
	citens.push(DatasetFactory.createConstraint("CODCOLCFO", CODCOLCFO, CODCOLCFO, ConstraintType.MUST));		
					
	console.log(citens);

	var dts = DatasetFactory.getDataset("dsInseriMovWSNative", null, citens, null); 				
	dts; 	
}