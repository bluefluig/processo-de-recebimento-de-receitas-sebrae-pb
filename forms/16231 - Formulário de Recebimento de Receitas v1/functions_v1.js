/*Novas functions para a v1 de 2020 --*/

/*Soma retorna a soma dos pagamentos realizados*/

/**
 *Cria/Seleciona pasta para salvar contrato assinado na etapa 17  */
function criaPastaContrato(){
    let pastaDestino = "15877";
    let tituloPasta = $("#_codConsultoria").val();
    $.ajax({			
			url: urlBase+'/api/public/ecm/document/listDocumentWithChildren/'+ pastaDestino,
			crossDomain: true,
			type : "GET",
			async : false,
			//contentType: "application/json",									 					    		
			dataType : "json", 
			success : function(data){
				console.log(data);				
				var contadorx = data.content[0].children.length;
				console.log('contadorx -> ' + contadorx);
				var counter = 0;
              if(contadorx > 0){
                  for(var x = 0; x < contadorx; x++ ){					
                  	//counter -> força a não repetição do loop após encontrar a pasta.
                	  if(tituloPasta != "" && tituloPasta != null && tituloPasta != undefined){                		
                		   if((data.content[0].children[x]["description"] == tituloPasta) && counter == 0 ){ 	                    	 
	                          pastaSelecionada = data.content[0].children[x]["id"];
	                          console.log('pastaSelecionada -> '+ pastaSelecionada);
                              $("#idPastaContratoAssinado").val(pastaSelecionada);
								counter = counter +1;
	                          //return pastaSelecionada;                           
	                       //compara counter == 0 e se i é == ao valor máximo do indexador do loop !!    			
	                      }else if(counter == 0 && i == contadorx){
                               console.log('criar pastas ')
	                          ajaxApi('criarPasta', JSON.stringify({"parentFolderId":pastaDestino ,"documentDescription": tituloPasta , "expires": "false",
	           "publisherId":"Todos", "inheritSecurity":"true" }));
	                          criaPastaContrato();
	                      }                		  
                	  }else{                		  
                		  console.log('Erro!')
                	  }       	
                	 

                  }//end for =----
              }// if contadorx 
				
			},error: function(e){
				
				console.log(e);
			},//fim chamada ajax 1 
			
		});

}
function totalPagamentos(){
    let total = 0 ;
    let valorPgt;
    let counter =  $(".valorGenerico").length;
    if(counter > 0){
        for(var i = 1; i < counter; i++){
           valorPgt =  floatReais($(".valorGenerico")[i].value);
           total = total + valorPgt;
           console.log(total);
        }
        return total;
    }    
}
function desabilitaCamposDiv(...div) {
    div.forEach(d => {
        $(`#${d} :input`).each(function () {
            $(this).attr("readonly", true);
        });
    });
}
/*Lista os pgamentos realizados - exibe na etapa - Financeiro confere*/
function listaPagamentos(){
    let tipoPagamentoFinanceiro;
    let valorPagamentoFinanceiro;
    let dataPagamentoFinanceiro;
    let condPagamentoFinanceiro;
    let identPagamentoFinanceiro;
    if($("#pagamentodividido").val() == 'sim'){
        
    }else{
        let idTipopg = $(".formaPagamento:last").attr("id");
        let idCondpg = $(".condicaoPagamentoGenerico:last").attr("id");
        tipoPagamentoFinanceiro = $("#"+idTipopg).find("option:selected").text();
        valorPagamentoFinanceiro = $(".valorGenerico:last").val();
        dataPagamentoFinanceiro = $(".dataPagamentoGenerico:last").val();
        condPagamentoFinanceiro = $("#"+idCondpg).find("option:selected").text();
        identPagamentoFinanceiro = $(".identificadorGenericoPagamento:last").val();
        $("#repeatPagamentos").append('<div class="row">'+
        '<div class="col-md-3 col-sm-12 form-group">'+
        '<label for="tipoPagamentoFinanceiro">Tipo de Pagamento</label>'+
        '<input type="text" name="tipoPagamentoFinanceiro" id="tipoPagamentoFinanceiro" value="'+tipoPagamentoFinanceiro+'" class="form-control" disabled>'+
        '</div>'+
        '<div class="col-md-2 col-sm-12 form-group">'+
        '<label for="valorPagamentoFinanceiro">Valor Pago</label>'+
		'<input type="text" name="valorPagamentoFinanceiro" id="valorPagamentoFinanceiro" value="'+valorPagamentoFinanceiro+'"class="form-control"  disabled>'+
        '</div>'+
        '<div class="col-md-2 col-sm-12 form-group">'+
        '<label for="dataPagamentoFinanceiro">Data</label>'+
        '<input type="text" name="dataPagamentoFinanceiro" id="dataPagamentoFinanceiro" value="'+dataPagamentoFinanceiro+'" class="form-control" disabled>'+
        '</div>'+
        '<div class="col-md-3 col-sm-12 form-group">'+
        '<label for="condPagamentoFinanceiro">Condição</label>'+
        '<input type="text" name="condPagamentoFinanceiro" id="condPagamentoFinanceiro" value="'+condPagamentoFinanceiro+'" class="form-control"  disabled>'+
        '</div>'+
        '<div class="col-md-2 col-sm-12 form-group">'+
        '<label for="identPagamentoFinanceiro">identificador</label>'+
        '<input type="text" name="identPagamentoFinanceiro" id="identPagamentoFinanceiro" value="'+identPagamentoFinanceiro+'"class="form-control" disabled>'+
        '</div>'+
        '</div> <!--row-->'
    )
    }
}

/*Função para retornar o valor a ser pago considerando pagamento dividido, rateio e descontos
 * Jean Varlet - 29/01/2020---- falta considerar + que 2 pgts -- para subtrair do valor total
 * */
function retornaValorPagar(){
    let desconto = $("#existeDesconto").val();
    let rateio = $("#existeRateio").val();
    let dividido = $("#pagamentodividido").val();
    let resta = $("#proximoPagamento").val();
    let valorBruto = floatReais($("#fieldValorBruto").val());
    let valor;
    let pago;
    let filhos = $(".valorGenerico").length - 1;
    let retornar;
    if(dividido === 'sim'){
    	console.log('retornaValorPagar()-> dividido-> sim');
    	if( resta != 0){
    		console.log('retornaValorPagar()->proximopagamento != 0 -> true');
    		$(".valorGenerico:last").val(reais($("#proximoPagamento").val()));
    	}else{
    		if(rateio === 'sim'){// possui rateio e valor pagamento dividido
                valor = floatReais($("#valorCliente").val());
                pago = parseFloat($("#valorGenericoPagamento___"+ filhos).val());
                retornar = valor- pago;
                return reais(retornar);
            }else if(rateio === 'nao' && desconto === 'sim'){
                valor = floatReais($("#totalDesconto").val());
                pago = parseFloat($("#valorGenericoPagamento___"+ filhos).val());
                retornar = valor - pago;
                return reais(retornar);
            }else if(rateio === 'nao' && desconto === 'nao'){
            	valor = floatReais($("#fieldValorBruto").val());
            	pago = parseFloat($("#valorGenericoPagamento___"+ filhos).val());
            	retornar = valor - pago;
                return reais(retornar);
            }    		
    	}        
    }else if(dividido === 'nao'){
        if(rateio === 'sim'){
            valor = floatReais($("#valorCliente").val());
            return reais(valor);
        }else if(rateio === 'nao' && desconto === 'sim'){
            valor = floatReais($("#totalDesconto").val());
            return reais(valor);
        }else if(rateio === 'nao' && desconto === 'nao'){
            return $("#fieldValorBruto").val();
        }
    }   
    
}

/**Centraliza em 1 campo o valor bruto de qualquer tipo de natureza de serviço ---
 * Checa o tipo de natureza selecionada, lê o campo que possui o valor total bruto
 * do serviço a ser contratado, desabilita o campo que possui o valor, para evitar 
 * alteração do valor após captura. Copia o valor para os campos fieldValorBrutoh(float)
 * fieldValorBruto(reais) --- chamar em evento de seleção de pagamento --- 
 * @param none
 * @return field type text existeContrato sim/nao -  type text contratoMaior15mil sim/nao
 * */
function valorBrutoServico(){
	let valor;	
		if(retornaNatureza() == 'consultoriaSGF' || retornaNatureza() == 'instrutoriaSGF'){
			valor = $("#cargaHorariaValorTotal").val(); //valor nao formatado
			$("#cargaHorariaValorTotal").prop("disabled", true);
			if( retornaNatureza() == 'consultoriaSGF' ){
				if(valor >= 15000){
					$("#existeContrato").val('sim');
					$("#contratoMaior15mil").val('sim');
				}else{
					$("#existeContrato").val('sim');
					$("#contratoMaior15mil").val('nao');				
				}
				
			}else{
				$("#existeContrato").val('nao');
				$("#contratoMaior15mil").val('nao');	
			}			
		}else if(retornaNatureza() == 'instrutoriaSAS'){
			valor = $("#valorTotalFinal").val(); // valor nao formatado 			
		}else if(retornaNatureza() == 'outrosServicos'){		
				valor = floatReais($("#somaSubTotalOutros").val()); //o valor vem formatado R$ 0,00
				$("#somaSubTotalOutros").prop("disabled", true);				
		}		
		$("#fieldValorBruto").val(reais(valor));
		$("#fieldValorBrutoh").val(parseFloat(valor));
		
}
/*Controla exibição do botão para emissão de Recibos de pagamento na primeira etapa do processo 
 * Condicional - Exibe botão se tipo de evento for diferente de consultoriaSGF
 * retorna append div button
 * */
function exibeEmiteReciboPagamento(){
	if(retornaNatureza() !== 'consultoriaSGF'){		
		$("#buttonEmtRecibo").fadeIn();
	}else{		
		$("#buttonEmtRecibo").fadeOut();
	}
}
/*Exibe Div com button para emissão de movimento financeiro --- */
function exibeMovimentoFinanceiro(){
	$("#buttonMovFinanceiro").fadeIn();	
}

/*
 * Checa se há necessidade de emissão de contrato --- 
 * altera o value do campo hidden existeContrato = sim/nao
 * e contratoMaior15mil = sim/nao
 * controla condicionais do fluxo 
 */
function existeContrato(){
	let valorContrato;
	if(retornaNatureza() == 'consultoriaSGF'){
		$("#existeContrato").val('sim'); // sim/nao
		valorContrato = $("#cargaHorariaValorTotal").val();
		if(checaValorContrato(valorContrato)){
			$("#contratoMaior15mil").val('sim'); // sim/nao
		}else{
			$("#contratoMaior15mil").val('nao'); // sim/nao
		}		
		
	}else if(retornaNatureza() == 'instrutoriaSAS'){
		//soluções SAS
		$("#contratoMaior15mil").val('nao'); // sim/nao
		$("#existeContrato").val('nao');
	}else if(retornaNatureza() == 'outrosServicos'){
		//outros Serviços
		$("#contratoMaior15mil").val('nao'); // sim/nao
		$("#existeContrato").val('nao');
	}
	
	
}
/*Função auxiliar para existeContrato()
 * Checa se o valor do contrato selecionado é maior que 15 mil reais 
 * Jean Varlet 28/01/2020---*/
function checaValorContrato(valor){
    if(typeof parseFloat(valor) === 'number'){
        if(parseFloat(valor) >= 15000){
            console.log('Valor > 15k ' + parseFloat(valor));
            return true;
        }else{
             console.log('Valor < 15k ' + parseFloat(valor));
            return false;
        }
        
    }else{
        console.log('Erro no formato do valor de contrato -> ' + typeof valor);
    }

}
/*
 * Controla o comportamento de emissão para o recibo de pagamento
 * Se não houver emissão de contrato, exibe emissão de recibo na etapa 4
 * Se houver emissao de contrato, exibe botao de recibo na etapa 17
 * */
function controlaEmissaoRecibo(contrato){
	if(contrato == 'nao'){
		if(ATIVIDADE_ATUAL == '4'){
			//mostra botao emissao recibo de pagamento na etapa 4  ---
			
		}				
	}
	if(contrato == 'sim'){
		if(ATIVIDADE_ATUAL == '17'){
			//mostra botao emissao recibo de pagamento na etapa 17  ---
			
		}
		
	}
	
}
//controlaEmissaoRecibo($("#existeContrato").val())
function enviaContratoCliente(contrato, emailCliente){
	if(contrato == 'sim'){
		if(ATIVIDADE_ATUAL == '17'){
			
		}
		
	}
	
}
//enviaContratoCliente($("#existeContrato").val(), emailCliente);

/*Envia email cliente 
 *Deve receber o endereço de email da função pai e buscar os anexos contrato e recibo no GED Fluig
 *
 * */
function enviaEmailCliente(emailCliente, tiposervico){
	let nomeEmpresa; // localizar /!!
	let gestor;
	let emailGestor;
	let tipoServico;
	let nomeCliente;
	
	if(tiposervico == 1 ){
		nomeEmpresa = $("#rm_fcfonomeForn").val();
		tipoServico = 'Consultoria';
		nomeCliente = $("#responsavelForn").val();
		gestor = $("#tbNomePessoa2").val();
		emailGestor = emailColab($("#tbCodPessoa").val());
	}
		
	
	var parametros = {
        param: [            
            { "SERVER_URL": "http://fluig.sebraepb.com.br:8080/" },            
            { "SOLICITANTE": nomeCliente },
            { 'MENSAGEM': 'Caro(a) Cliente segue contrato celebrado entre '+ nomeEmpresa + ' e o SEBRAE Paraíba, seguindo o estabelecido no momento da contrataçãp. \n '+
            	'O gestor '+ gestor +', fica à disposição para informações/suporte durante o período de execução dos serviços estabelecidos e pode ser contatado por telefone \n '+
            	'ou por e-mail '+emailGestor+'. O Sebrae agradece e estará em permanente conexão. \n'+
            	' /n'+
            	' Esta é uma mensagem remetida automaticamente, para dúvidas e informações contate o gestor do contrato. \n'},
            { 'TENANT_ID': 1 } // company id
        ]
    }

	var destinatarios = [emailCliente]
	var fields = ['Blue', 'notificaGestorCnpj', JSON.stringify(parametros), JSON.stringify(destinatarios)]
	var ds = DatasetFactory.getDataset('dsEnvioEmail', fields, null, null)
	ds;

	FLUIGC.toast({
     title: 'Contrato Cliente',
     message: '<br>E-mail enviado - Contrato do Cliente' ,
     type: 'success'

	});
	
}
//enviaEmailCliente(emailCliente, tiposervico)
//enviaEmailCliente($("#tbCodPessoa").val(), 1 );
/**
 * Função auxiliar enviaEmailCliente() --
 * Retorna o email do gerente de contrato ---
 * 
 * */
function emailColab(matricula){
    let cst1 = DatasetFactory.createConstraint('colleaguePK.colleagueId', matricula, matricula, ConstraintType.MUST);
    let retorno = DatasetFactory.getDataset('colleague', null, [cst1], null);
    if(retorno.values.length){
        return retorno.values[0].mail;
    }else{
        console.log('emailColab()-> Colaborador não encontrado');
        return null;
    }
}
//emailColab($("#tbCodPessoa").val())
