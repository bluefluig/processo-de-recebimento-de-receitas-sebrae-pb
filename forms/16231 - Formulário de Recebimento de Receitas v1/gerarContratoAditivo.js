function gerarContratoAditivo() {
	var txt_nomepessoa  		= $("#tbNomePessoa").val();
	var txt_agencia   			= $("#tbAgencia").val();
	var txt_contratos  			= $("#rm_contratosativos").val();
	var txt_projeto   			= $("#rmprojeto").val();
	
	var txt_area				= $("#areaconh").val();
	var txt_subareaconh			= $("#subareaconh").val();
	var txt_tituloEvento		= $("#tituloEvento").val();
	var txt_pubAlvo				= $("#pubAlvo").val();
	var txt_localEvento			= $("#localEvento").val();
	var txt_descricaoEven		= $("#descricaoEven").val();
	
	var NumMovimento			= ($("#nmRespostaWSPag").val()).substring(0, ($("#nmRespostaWSPag").val()).indexOf("-"));
	var AnoEmissao				= '2019'; //($("#datainicial").val()).substring(6,10);
	
	var dataFinalFormat			= $("#datafinalFormat").val().split('-').reverse().join('/');
	
	var txt_aditivo				= $("#justificativaAditivo").val()
	
	//Empresa
	var txt_forn_razaosocial	= $("#rm_fcforazaoForn").val();
	var txt_forn_endereco		= $("#rm_fcfoendForn").val();
	var txt_forn_replegal		= $("#respTecnico").val();
	
	var txt_forn_cnpj			= ($("#cpfconsultaForn").val()).substring(0,2) + '.' + ($("#cpfconsultaForn").val()).substring(2,5) + '.' + ($("#cpfconsultaForn").val()).substring(5,8) + '/' + ($("#cpfconsultaForn").val()).substring(8,12) + '-' + ($("#cpfconsultaForn").val()).substring(12,14);
	
	var txt_forn_prestador		= $("#rm_fcfonomeForn").val();
	
	var txt_VALOR 				= $("#cargaHorariaValorTotContrat").val().replace(".",",").extenso(true);
	var txt_VALORHORA 			= $("#valorHoraContrat").val().replace(".",",").extenso(true);
	
	var txt_VALORHORAADIT_n		= $("#cargaHorariaTotalsemAditivo").val();
	var txt_VALORHORAADIT 		= $("#cargaHorariaTotalsemAditivo").val().replace(".",",").extensoF(true);
	
	//Agendamento Geral	
	var txt_tiposervico			= $("#tipoSer").val();
	var txt_protocolo			= $("#tbProtocolo").val();
	
	//Evento
	var txt_descservico			= $("#descricaoEven").val();
	var txt_titulosolucao		= $("#nomeevento").val();
	var txt_evento_rua			= $("#ruaEvento").val();
	var txt_evento_bairro		= $("#bairroEvento").val();
	var txt_evento_cidade		= $("#cidadeEvento").val();
	var txt_evento_estado		= $("#estadoEvento").val();
	var txt_evento_cep			= $("#cepEvento").val();
	var txt_complendereco		= $("#complementoEnd").val();
	
	var txt_periodoinicial 		= $("#datainicial").val().split('-').reverse().join('/');
	
	var txt_periodofinal		= $("#datafinal").val().split('-').reverse().join('/');
	var txt_horinicial			= $("#horarioIni").val();
	var txt_horfinal			= $("#horarioFim").val();
	
	//Contratação
	var txt_justificativa		= $("#necessidadeContrato").val();
	var txt_valorhora			= $("#valorHoraContrat").val().replace(".",",");
	var txt_totalhora			= $("#cargaHorariaValorTotContrat").val().replace(".",",");
	var txt_chtotal				= $("#cargaHorariaTotal").val().replace(".00","");
	
	var codigoSGF				= $("#codigoSGF").val();
	
	var cronogramaFisFin = [];
	
	/*Montando cabeçalho da table de Cronograma Físico Financeiro*/
	cronogramaFisFin.push(
		[
			{text: 'Atividade', style: 'tableHeader', alignment:'center'}, 
			{text: 'Início', style: 'tableHeader', alignment:'center'}, 
			{text: 'Término', style: 'tableHeader', alignment:'center'}, 
			{text: 'Carga Horária', style: 'tableHeader', alignment:'center'},
			{text: 'Previsão de Pagamento', style: 'tableHeader', alignment:'center'}
		]);
	
	/*Inserindo os dados da table de Cronograma Físico Financeiro*/
	$('input[id^="atividade___"]').each(function(index, value){
        var seq = $(this).attr("id").split("___")[1];

        var cff_atividade = $("#atividade___" + seq).val();        
        var cff_dtIni = $("#dtInicio___" + seq).val().split('-').reverse().join('/');
        var cff_dtTerm = $("#dtTermino___" + seq).val().split('-').reverse().join('/');
        var cff_chMes = $("#chMes___" + seq).val();
        var cff_dtPrev = $("#dtPrevisaoPgto___" + seq).val().split('-').reverse().join('/');
        
        cronogramaFisFin.push(
    			[
    				{text: cff_atividade, alignment:'center'},
    				{text:cff_dtIni, alignment:'center'},
    				{text:cff_dtTerm, alignment:'center'},
    				{text:cff_chMes, alignment:'center'},
    				{text:cff_dtPrev, alignment:'center'}
    			]
    	);
	});
		
	var docDefinition = { 
			
			pageOrientation: 'portrait',
			pageMargins: [60, 100, 60, 100],
			
			footer: {
				columns: [
					{
						text: 'Teste znczc  nzc zcn cznda adna nanad'
					}
				]
			},
			
			footer: function (currentPage, pageCount) {
				return {
						columns: [
							[
								{ text: 
			                    	[
			                    		'_______________________________________________________________________________________\n',
			                    		'Serviço de Apoio às Micro e Pequenas Empresas da Paraíba\n',
					                    'Avenida Maranhão, 983 - Bairro dos Estados - 58030-261 - João Pessoa - Paraíba\n',
					                    'Telefones: (83) 2108-1100 / 0800 570 0800\n',
					                    'www.sebraepb.com.br\n', 
					                    'CNPJ: 09.139.551/0001-05\n'
			                    	],
			                    	fontSize:8, 
			                    	alignment:'center', 
			                    	color:'gray'
			                    },
			                    { text: 'Página ' + currentPage.toString() + ' de ' + pageCount, fontSize:8, alignment:'right', color:'gray', margin: [0,0,40,0] }
			                ],
			            ]
					}
			},
			
			header: {
		        columns: [
		            {
		               image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAABJCAMAAAAHdUQUAAAAhFBMVEX///8vc7IqcbFPhLYAY6oiba79//vo8fRQhbrh6fH1+PdAfbc5drb///1GgrYAX6v4+/4tc6xvmsVUir3N2ukaa6/w9Pa80OGOrMyguNR/osi1x93Z5e4NY6bF0+GJqs1fjLhslMODp8Shv9KRsMtzncKtxNVAea4ATqQAU6Wqw9xdksM/oDCXAAAGoElEQVRogeVa63abOBAGGYVbRAxGYBuI7Taxd533f7+VZkYgIMmp2kbtOTu/jBCab+4jyUHws9SHuQOF959m5Eh3HjpQlPrCdXTCxXzBekiYC6yzL1xx6IRr7wtXLV3MKJ994Tpw5kAy9oXr4oKLF1tfuDI3Er5wBYFY0npkfOEP1d9JQjy4kDd9ierRgcKNL1yZU1bllS9c6eCUVTtfuDq3bF/7wnVyaSZY2fjCtXNRF0t8wRKRE66TL1z1P08O9G/nKX+JB9cPfo3db1nld5Our+bXHwViESBp6u6w3x+6+vOo1lO3Fs1ebrcfvppP+FFgQXsppSIu5cCqnmwZl8WCct2oz7aOxaa6Gkma2Z6yKDanfu4VdWm2k9bw9pwv2bzSq76QbCp5bMBdqGhXTWl0VYzuvDSkP+KSd8i+HkqL1EJc7mfACoav2Os0JuIVF4nbFNFFgEmNEDLahq7TOu/VcLUs2mzACnhYl6fI2nGIgyzpg5sFtl0VW47LtZAmmVRW4MiSH+EF7RFLTiR5mSn5Ea16VHY3fDLNt8JHqaYa6MzqJdJJnsjysD1JwyRxYVEMekQLylMsRFzgt1A6MmDDws3OUFLp+bAOOx8UHUkBUitY0MNJUZUT5GICUE3qH/pJi2c01mZiU1jWki/o/3LC1UvEG8wJp8gWPRPlGDSuGPWOXaB4Ral243fP2l78DuDldRxuQusri7Y5A62gS7VPkaLhTf++IuBl9/SCw7glFDkCa0bAnOToB8sjxpl5UM6HzbxVEU1RShZDFktrIACJEsvlUcwGkOQZMEMn0B2pCPbcUqQKTvtJoDgqDBJgl49xepXvSm8sxy6rPfENV8gW+gUkrMJ17xI8ttdPZ3RpWofcg2qpACOznRAYzdG43vF96UkuJfNQvNRTRVYxEJaLgAKNotfxgx7IOoQFSqHuh9M0YBe1Ri+g5ag2eIfULFlgtFiI8IvGRG9Z8oGfr7F5WXPANcZQyME3qYd+BCm5TqsbDK4ecSW98oMWuEs+WqcDp9fpFJOcOUkRzVCGM2I5lY+DnddUEnkFo4h1luSQho9svsqRundBgNmgSAFm/HYyxhGZTv8hT8dpEP76Tb3sMc1ZmVBpeJbXOX8GTV6WaV3nboF+a+Miz6YwGV+8fR9dRijjlSbmWm5zX0tvEGsLnMJBWigk+A5lAJPsOXvSCsYAZByqPNbAM5p+KTcLX0zM9Tr/szC2zG0cCsOAjVy4XbkC0R/e5GAEhlcpypVXI0GxxQM5+a1TdLjBHPS7Bt2B6/xHOhhasggUNP4dnjKY98jJj4nlxKVa5oVtfaKyJnUbQml9ecB9x2F09QwrKDwhYHZsmia+YriY7InfMOKLDxzdEmsEf11wCR6gTcvw2ChIUVDZiTFL9osP0OswZswk0MsBdYcRiOOQY0QQm+qOhA8cBW4/yKoV2PTJuCjGLFTV81RgLMqgsJc78hz0DpD9bfrUgGSgL/G6apcmFWEzMSylDxJAMsSQzESDOtY+SeV0Z5+2ab1bNVClXnLadGomsDqc2DSNEvfU9sFMVgT6wA6LGt/OuQRBbiuVOhGIVCyn7Nt00gWHkc9WVhRpgmrRfp9S94PLYv/DoUfBuL6NXUxOMMESyGU3nZVlKNgFxeTf6r7vdtQH6leY/tjj1K8/vowOFZ61A2/IV3R1MYD3oHeBXqiKlcqOWJCmKKOCDDXVJA1rW3CBSVdK0rDlgMXKqNdHonTPYtyUQRUU1MSV2iolWS7SeE33g5VSkHmUGWKoM9K6WEBLwFRxN73qmD1Nw5PPnZLxEnyQVrbf6KZ/fSDHImSJ82UK+qKcrJMMNhnMOpFuMBuBkde3TJzKR7OzMr1S22kLIdAMy01KWGoviuYbFymLGuejX+cZxg8+qX1VBx/I71aoNTke6utO1g4HOusfq1d7LuWgOKh0fz4YN+hvmwUlR2XdNp+ek+Ry6kyA93miB8kIDU65tQJGN2f7ROOhwrfnLGhWXDY70+4pAbO+btu27pvPDyl++CB+fZLw7ofvr/fXnEL8DnIUxpfsWRq7kKdrIdXCwi7uB2mIVlXvq+jdCvwRlcud1FeRCNgyq35G/i61G6dzaNzK+aBnp2sO7u2aY+/0nwnu7ZojcbrUzn3BCm6JC3m75vhr6aOL9Q+u2//fpHaXbuQLWMZdSK620F9FPXepQtbJ7heT26W2v2bC7R9y3Fs8Fi6wrHP7L6bYBdZsI/u1VMvVXu8T8vcPuXvuUhzzX/uH3H+OaoCzwPI04wAAAABJRU5ErkJggg==',
		               width: 100,
		               height: 50,
		               margin:[0,15,-250,0],
		               alignment: 'center'
		            }
		        ]
		    },
		    
		    content: [
		    	{
		    		text:[
		    		{text: 'TERMO ADITIVO À ORDEM DE SERVIÇO N° ' + NumMovimento + '/' + AnoEmissao, alignment:'center', bold:true, margin:[0,20,0,0]},
		    		{text: '\nSISTEMA DE GESTÃO DE FORNECEDORES - SGF', alignment:'center', fontSize:9, bold:true, margin:[0,20,0,0]}
		    		]
		    	},
				{
					alignment:'justify',
					fontSize:8,
					text:[
						
						 {text:'\n\n\n\nDAS PARTES CONTRATANTES\n\n',bold:true},
					     'I. O ', {text:'SERVIÇO DE APOIO ÀS MICRO E PEQUENAS EMPRESAS DO ESTADO DA PARAÍBA – SEBRAE/PB',bold:true}, ', Serviço Social Autônomo, ',
					     'inscrito no CNPJ sob o nº. 09.139.551/0001-05, com sede na cidade de João Pessoa, na Avenida Maranhão, 983, Bairro dos Estados, ',
					     'doravante denominado ', {text:'CONTRATANTE',bold:true}, ', e de outro lado,\n\n',
					     
					     'II. A empresa ', txt_forn_razaosocial, ', inscrita no CNPJ sob o nº ', txt_forn_cnpj, ', doravante designada CONTRATADA,', 
					     'com sede ', txt_forn_endereco, ', neste ato representada por ', txt_forn_replegal, '.\n\n',
						
					     {text:'\nCLÁUSULA PRIMEIRA – DO OBJETO\n\n',bold:true},
					     
					     'O presente Termo Aditivo tem por objeto a alteração de prazo de execução/vigência e/ou do valor contratado, relativo à prestação de serviços de  ', txt_tiposervico,' pela CONTRATADA na Área de Conhecimento ', txt_area,' e ',
					     'Subárea ', txt_subareaconh,', conforme solicitação FLUIG nº ', txt_protocolo,', de ', txt_nomepessoa,', que expressou a seguinte justificativa para celebração deste Termo Aditivo:\n\n',
					     
					     txt_aditivo,'\n\n',
					     
					     {text:'CLÁUSULA SEGUNDA – DO VALOR DO CONTRATO E DA FORMA DE PAGAMENTO\n\n',bold:true},
					     
					     'Em função da alterações processadas a carga horária total contratada passa a ser de ', txt_VALORHORAADIT_n, ' (', txt_VALORHORAADIT, ') horas. Portanto, o SEBRAE/PB pelo cumprimento integral do objeto deste contrato, o total de R$ ', txt_totalhora, ' (', txt_VALOR, '), considerando o valor unitário de R$ ', txt_valorhora, ' (', txt_VALORHORA, ') por hora contratada.\n\n',
					     
					     'II - O pagamento dos serviços executados pela Contratada e aceitos definitivamente pelo SEBRAE/PB será efetuado em conformidade com o Cronograma Físico Financeiro atualizado:'
					]
					     
				},
			
			/*Monta uma table com o Cronograma Físico Financeiro*/
			{text: 'Cronograma Físico Financeiro\n', margin: [0, 20, 0, 8]}, 
			{
		    	style: 'table',
		    	alignment:'justify',
		    	fontSize:8,
				table: {
					body: cronogramaFisFin
				}
			},
						  
			{
				alignment:'justify',
				fontSize:8,
				text:[
					     {text:'\n\nCLÁUSULA TERCEIRA – DA VIGÊNCIA \n\n',bold:true},
					     
					     'O prazo de vigência atualizado se encerrará em ', dataFinalFormat, '.\n\n',
					     
					     {text:'João Pessoa/PB, '+data(),alignment:'center'},
					   
					     '\n\n\n\n',
					     'ASSINATURAS\n\n\n',
					   
					     'Pelo SEBRAE [Assinatura Eletrônica]:\n\n'
					]
				
				}
		    	
		    ]
	};
	
	// open the PDF in a new window
	//pdfMake.createPdf(docDefinition).open();
	
	const pdfd = pdfMake.createPdf(docDefinition);
    pdfd.getBase64((data) => {
        var form = new FormData();
        form.append('data', data);
        $.ajax({
            url: 'http://dev.sebraepb.com.br/assigns/ugal/embbeded2.php',
            data: form,
            processData: false,
            contentType: false,
            type: 'POST',
            crossDomain: true,
            success: function (ret) {
                console.log(ret);
                window.open(ret, "_blank")
            }
        });
    });

	// print the PDF
	//pdfMake.createPdf(docDefinition).print();

	// download the PDF
	//pdfMake.createPdf(docDefinition).download('optionalName.pdf');
}

function data(){
	var dataAtual = new Date();
	var dia = dataAtual.getUTCDate();
	var mes = mesPorExtenso(dataAtual.getMonth());
	var ano = dataAtual.getFullYear()
	
	//return dia+' de de '+ano+'.';
	return dia + ' de ' + mes + ' de ' + ano + '.';
}

function dataAtual(){
	var dataAtual = new Date();
	var dia = dataAtual.getUTCDate();
	var mes = dataAtual.getMonth()+1;
	var ano = dataAtual.getFullYear();
	
	//return dia+' de de '+ano+'.';
	return dia + '/' + mes + '/' + ano;
}

function dataAtualFim(){
	var dataAtual = new Date();
	var newdate = new Date();
	newdate.setDate(dataAtual.getDate()+60);
	var dia = newdate.getUTCDate();
	var mes = newdate.getMonth();
	var ano = newdate.getFullYear()
	
	//return dia+' de de '+ano+'.';
	return dia + '/' + mes + '/' + ano;
}

function parse(dateStr) {
	var parts = dateStr.split('/').map(function (digits) {
		// Beware of leading zeroes...
		return parseInt(digits, 10);
	});
	return new Date(1900 + parts[2], parts[1], parts[0]);
}

function format(date) {
	var parts = [
		date.getDate(),
	    date.getMonth(),
	    date.getYear()
	  ];
	if (parts[0] < 10) {
		parts[0] = '0' + parts[0];
	}
	if (parts[1] < 10) {
		parts[1] = '0' + parts[1];
	}
	return parts.join('/');
}

function add(date, days) {
	date.setDate(date.getDate() + days);
	return date;
}

function mesPorExtenso(mes){
	var arrayMes = new Array(12);
	
	arrayMes[0] = "Janeiro";
	arrayMes[1] = "Fevereiro";
	arrayMes[2] = "Março";
	arrayMes[3] = "Abril";
	arrayMes[4] = "Maio";
	arrayMes[5] = "Junho";
	arrayMes[6] = "Julho";
	arrayMes[7] = "Agosto";
	arrayMes[8] = "Setembro";
	arrayMes[9] = "Outubro";
	arrayMes[10] = "Novembro";
	arrayMes[11] = "Dezembro";
	
	return arrayMes[mes];
}

String.prototype.extensoF = function(c){
	var ex = [
		["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
		["dez", "vinte", "trinta", "quarenta", "cinqüenta", "sessenta", "setenta", "oitenta", "noventa"],
		["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
		["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
	];
	var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "", d = "", sl;
	for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
		j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
		if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
		for(a = -1, l = v.length; ++a < l; t = ""){
			if(!(i = v[a] * 1)) continue;
			i % 100 < 20 && (t += ex[0][i % 100]) ||
			i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
			s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
			((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
		}
		a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
		a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
	}
	return r.join(e);
}

String.prototype.extenso = function(c){
	var ex = [
		["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
		["dez", "vinte", "trinta", "quarenta", "cinqüenta", "sessenta", "setenta", "oitenta", "noventa"],
		["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
		["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
	];
	var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
	for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
		j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
		if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
		for(a = -1, l = v.length; ++a < l; t = ""){
			if(!(i = v[a] * 1)) continue;
			i % 100 < 20 && (t += ex[0][i % 100]) ||
			i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
			s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
			((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
		}
		a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
		a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
	}
	return r.join(e);
}