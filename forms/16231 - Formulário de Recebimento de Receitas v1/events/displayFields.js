
function displayFields(form, customHTML) {
   
	form.setShowDisabledFields(true);	
	
	var modoVisualizacao = form.getFormMode();
	var atividade = getValue("WKNumState");	
	
	var numProces = getValue("WKNumProces");
	var numproc = parseInt(getValue("WKNumProces"));
	
	var habilitar = false; // Informe True para Habilitar ou False para Desabilitar os campos
	
    var mapaForm = new java.util.HashMap();
    mapaForm = form.getCardData();
    var it = mapaForm.keySet().iterator();     
    while (it.hasNext()) { // Laço de repetição para habilitar/desabilitar os campos
        var key = it.next();
        form.setEnabled(key, habilitar);
    }
    
	customHTML.append("<script>");
	customHTML.append("var ATIVIDADE_ATUAL = "+ atividade + ";" );
	customHTML.append("var MODO_ATUAL = '"+ modoVisualizacao + "';" );
	customHTML.append("</script>");		
	
	

    customHTML.append("<script>");
    customHTML.append("\n   var WKAtv     =  " + atividade + ";");
    customHTML.append("\n   var WKProc    =  " + numProces + ";");
    customHTML.append("\n   var isMobile  =  " + form.getMobile() + ";");
    customHTML.append("\n </script>");
    
	form.setEnabled('linkContratoAssinado', true);
	form.setEnabled('tipoSer', true);
	form.setEnabled('numControleRecibo', true); // recebe o código para compor o novo recibo a ser gerado na etapa 17 -- 
	form.setEnabled('idPastaContratoAssinado', true); // código da pasta onde o contrato assinado foi salvo -- 
	form.setEnabled('idsComprovantes', true); // códigos dos comprovantes de pagamentos  -- 
	
	form.setEnabled('movimentosFaturar',true);
	form.setEnabled('faturamentoGeradoRM',true);
	form.setEnabled('codigoFatRmGer',true);
	    

	    // inicio ---- 
	    if(atividade == 0 || atividade == 4){	
	    	
	    	form.setEnabled("tbNomeSolicitante", true);
	    	
	    	//dados empresa cliente
	    	form.setEnabled("cnpjRmEmpresa", true);
	    	form.setEnabled("codRmEmpresa", true);
	    	form.setEnabled("fornRmEmpresa", true);
	    	form.setEnabled("inscEstRmEmpresa", true);
	    	form.setEnabled("fantasiaRmEmpresa", true);
	    	form.setEnabled("razaoRmEmpresa", true);
	    	form.setEnabled("faturaEmpresaCliente", true); 
	    	form.setEnabled("estado", true); 
	    	
	    	
	    	form.setEnabled("fieldValorBruto",true);
	    	form.setEnabled("fieldValorBrutoh",true);
	    	form.setEnabled("pagamentodividido",true);
	    	form.setEnabled("existeContrato",true);
	    	form.setEnabled("proximoPagamento",true);
	    	form.setEnabled("somaPagamentos",true);	    	
	    	form.setEnabled("existeRateio",true);
	    	form.setEnabled("existeDesconto",true);	    	
	    	form.setEnabled("hiddenAnexoGenerico",true);
	    	form.setEnabled("anexoGenerico",true);
	    	form.setEnabled("dataPagamentoGenerico",true);
	    	form.setEnabled("identificadorGenericoPagamento",true);
	    	form.setEnabled("valorGenericoPagamento",true);	    	
	    	form.setEnabled("condicaoPagamentoGenerico",true);
	    	form.setEnabled("bandeiraPagamentoGenerico",true);	    	
	    	//New Desconto Area -- 16/01/2020
	    	form.setEnabled("perDesconto",true);
	    	form.setEnabled("subTotalDesconto",true);
	    	form.setEnabled("totalDesconto",true);
	    	form.setEnabled("addPeriodo",true);
	    	form.setEnabled("valorPagamentoCliente",true);	    	
	    	//Jean Varlet 23/12/2019	    	
	    	form.setEnabled("decisaoOperador", true);
	    	form.setEnabled("cnpjEmpresaVinculada", true);
	    	form.setEnabled("codCfoEmpresa", true);
	    	form.setEnabled("razaoEmpresa", true);
	    	form.setEnabled("fantasiaEmpresa", true);
	    	form.setEnabled("fornecedorEmpresa", true);	    	
	    	//Jean Varlet 01/12/2019
	    	form.setEnabled('rateioSebrae', true);
	    	form.setEnabled('valorSebrae', true);
	    	form.setEnabled('valorCliente', true);	    	
	    	//Jean Varlet 18/11/2019	    	
	    	form.setEnabled('cpfconsultasasOutros', true);
	    	form.setEnabled('linkContratoAssinado', true);
	    	form.setEnabled('cpfconsultaOutrosF', true);
	    	form.setEnabled('codSASOutros', true);
	    	form.setEnabled('codcfoClienteOutros1', true);
	    	form.setEnabled('nomeClienteOutros1', true);
	    	form.setEnabled('razaoClienteOutros1', true);
	    	form.setEnabled('tipoClienteOutros1', true);
	    	form.setEnabled('cpfCnpjOkOutros', true);
	    	form.setEnabled('codServicoOutros', true);	    	
	    	form.setEnabled('rmprojeto', true);
	    	form.setEnabled('rmacao', true);
	    	form.setEnabled('rm_saldoprojeto', true);
	    	form.setEnabled('rmunidade', true);
	    	form.setEnabled('rm_codunidade', true);
	    	form.setEnabled('rmrecurso', true);
	    	form.setEnabled('cpRateioValor', true);
	    	form.setEnabled('cpRateioPercent', true);
	    	form.setEnabled('cpRateioTot', true);
	    	form.setEnabled('rm_codrecurso', true);	    	
	    	form.setEnabled('servicosSeleciona', true);	    	
	    	form.setEnabled('dataInicioOutros', true);
	    	form.setEnabled('dataFimOutros', true);
	    	form.setEnabled('cepOutros', true);
	    	form.setEnabled('tituloOutros', true);
	    	form.setEnabled('ruaOutros', true);
	    	form.setEnabled('bairroOutros', true);
	    	form.setEnabled('cidadeOutros', true);
	    	form.setEnabled('estadoOutros', true);
	    	form.setEnabled('complementoEndOutros', true);
	    	form.setEnabled('descrevServOutros', true);
	    	form.setEnabled('horarioIniOutros', true);
	    	form.setEnabled('horarioFimOutros', true);	    	
	    	form.setEnabled('tipoEventoSaSselecionado', true);
	    	form.setEnabled('dataIniEventoSaSselecionado', true);
	    	form.setEnabled('dataFimEventoSaSselecionado', true);	    	
	    	form.setEnabled('horaFimEventoSaSselecionado', true);
	    	form.setEnabled('horaIniEventoSaSselecionado', true);
	    	form.setEnabled('tituloEventoSaSselecionado', true);
	    	form.setEnabled('modalidadeEventoSaSselecionado', true);
	    	form.setEnabled('localEventoSaSselecionado', true);		    	
	    	form.setEnabled('movimentoGeradoRM', true);    	
	    	// Links de arquivos salvos
	    	form.setEnabled('linkReciboSalvo', true);
	    	form.setEnabled('linkContratoSalvo', true);
	    	form.setEnabled('linkComprovantePagamentoSalvo', true);	    	
	    	form.setEnabled('triggerReciboUpload', true);
	    	form.setEnabled('triggerContratoUpload', true);	    	
	    	// Jean Varlet 19/11/2019
	    	form.setEnabled('emailClienteOutros', true);
	    	form.setEnabled('cidadeClienteOutros', true);
	    	form.setEnabled('estadoClienteOutros', true);
	    	form.setEnabled('enderecoClienteOutros', true);
	    	form.setEnabled('bairroClienteOutros', true);
	    	form.setEnabled('numeroClienteOutros', true);	    	
	    	//Jean Varlet 20/11/2019	    	
	    	form.setEnabled('textServicoOutros', true);
	    	form.setEnabled('somaSubTotalOutros', true);
	    	form.setEnabled('somaSubTotal2Outros', true);
	    	form.setEnabled('descontoInformadoOutros', true);
	    	form.setEnabled('valorTotalFinal2Outros', true);
	    	form.setEnabled('valorTotalFinalOutros', true);	    	
	    	// Jean Varlet 17/11/2019 ---
	    	form.setEnabled('codConsultoria', true);
	    	form.setEnabled('tituloConsultoria', true);
	    	form.setEnabled('tipoConsultoria', true);
	    	form.setEnabled('necessidadeContrato', true);
	    	form.setEnabled('descricaoEven', true);
	    	form.setEnabled('datainicial', true);
	    	form.setEnabled('datafinal', true);
	    	form.setEnabled('horarioIni', true);
	    	form.setEnabled('horarioFim', true);
	    	form.setEnabled('cepEvento', true);
	    	form.setEnabled('cidadeEvento', true);
	    	form.setEnabled('complementoEnd', true);
	    	form.setEnabled('estadoEvento', true);
	    	form.setEnabled('valorHora', true);
	    	form.setEnabled('cargaHorariaValorTotal', true);
	    	form.setEnabled('cargaHorariaTotal', true);
	    	form.setEnabled('numberVlrTotRateio', true);
	    	form.setEnabled('tbAgencia2', true);
	    	form.setEnabled('tbNomePessoa2', true);
	    	form.setEnabled('tbProtocolo2', true);
	    	form.setEnabled('respTecnico', true);
	    	form.setEnabled('rm_fcfonomeForn', true);	    	
	    	//JEan Varlet 14/11/2019
	    	//Nova área de pesquisa para consultorias ---
	    	form.setEnabled('dataIniPesqConsult', true);
	    	form.setEnabled('dataFimPesqConsult', true);
	    	//Radio Seleciona entre consultoria e instrutoria 
	    	form.setEnabled('selConsultInst', true);
	    	form.setEnabled('ConsultoriaInstrutoria', true);	    	
	    	form.setEnabled('endereco', true);
	    	form.setEnabled('bairro', true);
	    	form.setEnabled('cliCodSas', true);
	    	form.setEnabled('numero', true);
	    	form.setEnabled('emailForn', true);	    	
	    	//Jean Varlet 12/11/2019
	    	//Campo de pesquisa para área de outros serviços 
	    	form.setEnabled('cpfConsultaOutrosServicosF', true);
	    	form.setEnabled('cpfConsultaOutrosServicos', true);
	    	form.setEnabled('testeGerenteEmail', true);
	    	form.setEnabled('testeGerenteId', true);
	    	form.setEnabled('dataFimPesqConsult', true);	    	
	    	//Jean Varlet 11/11/2019 - 
	    	//Campo para controlar a condicional de fluxo -> Assinatura.
	    	form.setEnabled('necessitaAssinatura', true);
	    	form.setEnabled('tipoServicoContratado', true);	    	
	    	form.setEnabled('cpfconsultaFornecedor', true);
	    	form.setEnabled('buscaFornecedor', true);
	    	form.setEnabled('cpfconsultaFornecedorF', true);
	    	form.setEnabled('cliForForn', true);
	    	form.setEnabled('cnpjForn', true);
	    	form.setEnabled('razaoForn', true);
	    	form.setEnabled('fantasiaForn', true);
	    	form.setEnabled('responsavelForn', true);
	    	form.setEnabled('responsavelForn', true);
	    	form.setEnabled('tipoForn', true);
	    	form.setEnabled('estadoForn', true);
	    	form.setEnabled('cidadeForn', true);
	    	form.setEnabled('cargaConsult', true);
	    	form.setEnabled('valorHoraConsult', true);
	    	form.setEnabled('cpfFornecedorConsultoria', true); 	    	
	    	//Alteração 28/10/2019 -- Incluir pagamento no processo ---
	    	form.setEnabled("consideracoesConfReceb", true);
			form.setEnabled("anexaReciboBoleto", true);
			form.setEnabled("dataBoletoPagamento", true);
			form.setEnabled("valorBoletoPagamento", true);
			form.setEnabled("identificadorBoletoPagamento", true);
			form.setEnabled("condicaoBoletoPagamento", true);
			form.setEnabled("hiddenAnexarReciboDeposito", true);
			form.setEnabled("anexaReciboDeposito", true);
			form.setEnabled("dataDepositoPagamento", true);
			form.setEnabled("valorDepositoPagamento", true);
			form.setEnabled("identificadorDepositoPagamento", true);
			form.setEnabled("condicaoDepositoPagamento", true);
			form.setEnabled("hiddenAnexarReciboMaquineta", true);
			form.setEnabled("anexaReciboMaquineta", true);
			form.setEnabled("dataMaquinetaPagamento", true);
			form.setEnabled("valorMaquinetaPagamento", true);
			form.setEnabled("codMaquinetaPagamento", true);
			form.setEnabled("bandeiraMaquinetaPagamento", true);
			form.setEnabled("condicaoMaquinetaPagamento", true);
			form.setEnabled("radioEventoCancelado", true);
	    	form.setEnabled("formaPagamento", true);
			form.setEnabled("tbCodRecebedor",true);
			form.setEnabled("dataRecebimento",true);
			form.setEnabled("tbNomeRecebedor",true);
			form.setEnabled("tbChapaRecebedor",true);
			form.setEnabled("radioRecebimento",true);
			form.setEnabled("consideracoesConfReceb",true);
			form.setEnabled("radioEventoCancelado",true);
			form.setEnabled("justificaEventoCancelado",true);
	    	//Alteração 28/10/2019 -- Incluir pagamento no processo ---     	
	    	form.setEnabled("quantidadeEventos", true);
	    	form.setEnabled("codigosEventos", true);
	    	form.setEnabled("fisicoJuridico", true);	    	
	    	//tabela de eventos SAS e campos de valores
	    	form.setEnabled("somaSubTotal", true);
	    	form.setEnabled("somaSubTotal2", true);
	    	form.setEnabled("valorTotalFinal2", true);
	    	form.setEnabled("desconto", true);
	    	form.setEnabled("descontoInformado", true);	    	
	    	//tabela de eventos SAS e campos de valores	    	
	    	form.setEnabled("cpfCnpjValidado",true);
	    	form.setEnabled("nomeCliente",true);
	    	form.setEnabled("razaoCliente",true);
	    	form.setEnabled("cpfCnpjFormatado",true);
	    	form.setEnabled("cpfCnpj",true);
	    	form.setEnabled("codClienteSAS",true);
	    	form.setEnabled("codClienteRM",true);
	    	form.setEnabled("nomeProcesso",true);	
	    	
	    	form.setEnabled("eventoNome1",true);
	    	form.setEnabled("eventoDataInicio1",true);
	    	form.setEnabled("eventoDataFim1",true);
	    	form.setEnabled("eventoTipo1",true);
	    	form.setEnabled("eventoCarga1",true);
	    	form.setEnabled("eventoPreco1",true);
	    	form.setEnabled("eventoCodigo1",true);
	    	//Jean Varlet --- 27/11/2019
	    	form.setEnabled("senhaPortalClienteAcesso",true);
	    	form.setEnabled("cepClienteSAS",true);
	    	form.setEnabled("ruaClienteSAS",true);
	    	form.setEnabled("bairroClienteSAS",true);
	    	form.setEnabled("cidadeClienteSAS",true);
	    	form.setEnabled("estadoClienteSAS",true);
	    	form.setEnabled("emailClienteSAS",true);
	    	form.setEnabled("contatoClienteSAS",true);
	    	form.setEnabled("telefoneClienteSAS",true);
	    	form.setEnabled("inscEstClienteSAS",true);
	    	form.setEnabled("identidadeClienteSAS",true);	    	
	    	form.setEnabled("codSAS", true);
	    	form.setEnabled("somaTotal", true); //Somatorio de cursos ---
	    	form.setEnabled("descontoInformado", true); //Somatorio de cursos ---
	    	form.setEnabled("valorTotalFinal", true); //Somatorio de cursos ---
	    	form.setEnabled("codcfoCliente1", true);
	    	form.setEnabled("nomeCliente1", true);
	    	form.setEnabled("razaoCliente1", true);
	    	form.setEnabled("tipoCliente1", true);
	    	form.setEnabled("cpfCnpjOk", true);	    	
			form.setEnabled("tbCodPessoa",true);
			form.setEnabled("cpfconsultasas",true);
			form.setEnabled("tbNomePessoa",true);
			form.setEnabled("tbChapaPessoa",true);
			form.setEnabled("rm_dadoscolab",true);
			form.setEnabled("rm_dadoscolab2",true);
			form.setEnabled("colabzoom",true);
			form.setEnabled("tbUsuario",true);
			form.setEnabled("tbGerente",true);
			form.setEnabled("tbDiretor",true);
			form.setEnabled("tbAgencia" ,true);
			form.setEnabled("tbFilial",true);
			form.setEnabled("tbGerenteUsuario",true);
			form.setEnabled("tbDiretorUsuario",true);
			form.setEnabled("tbCodVen",true);
			form.setEnabled("rm_depto",true);
			form.setEnabled("rm_coddepto",true);			
			form.setEnabled("tbProtocolo",true);
			form.setEnabled("dataEvento",true);
			form.setEnabled("existeConsEspontaea",true);
			form.setEnabled("cpfconsulta",true);
			form.setEnabled("cpfconsultaF",true);
			form.setEnabled("rm_fcfo",true);
			form.setEnabled("rm_fcfocodcfo",true);
			form.setEnabled("rm_fcfocolig",true);
			form.setEnabled("rm_fcfonome",true);
			form.setEnabled("rm_fcfosituacao",true);
			form.setEnabled("rm_fcfoporte",true);
			form.setEnabled("rm_fcfocgc",true);
			form.setEnabled("rm_fcforazao",true);
			form.setEnabled("rm_fcfotelefone",true);
			form.setEnabled("rm_fcfoemail",true);
			form.setEnabled("descPublicoIndefinido",true);
			form.setEnabled("dadomat01",true);
			form.setEnabled("dataPrevisao",true);
			form.setEnabled("atividade",true);
			form.setEnabled("complementoPag",true);
			form.setEnabled("rmprojeto",true);
			form.setEnabled("rmprojeto",true);
			form.setEnabled("dtInicio",true);
			form.setEnabled("dtTermino",true);
			form.setEnabled("chMes",true);
			form.setEnabled("dtPrevisaoPgto",true);
			form.setEnabled("chTotal",true);
			form.setEnabled("rmacao",true);
			form.setEnabled("rm_saldoprojeto",true);
			form.setEnabled("rmunidade",true);
			form.setEnabled("rm_codunidade",true);
			form.setEnabled("rmrecurso",true);
			form.setEnabled("cpRateioValor",true);
			form.setEnabled("cpRateioPercent",true);
			form.setEnabled("cpRateioTot",true);
			form.setEnabled("rm_codrecurso",true);
			form.setEnabled("numberVlrTotRateio",true);
			form.setEnabled("rmacao",true);
			form.setEnabled("rm_saldoprojeto",true);
			form.setEnabled("rm_codunidade",true);
			form.setEnabled("rmunidade",true);
			form.setEnabled("rmrecurso",true);
			form.setEnabled("cpRateioValor",true);
			form.setEnabled("cpRateioPercent",true);
			form.setEnabled("cpRateioTot",true);
			form.setEnabled("rm_codrecurso",true);			
			form.setValue("tbProtocolo", numproc);//ok		
			form.setEnabled("pgtunico",true);
			form.setEnabled("nomeProcesso",true);
			form.setEnabled("datainicial",true);
			form.setEnabled("datafinal",true);
			form.setEnabled("necessidadeContrato",true);
			form.setEnabled("tipoSer",true);
			form.setEnabled("areaconh",true);
			form.setEnabled("subareaconh",true);
			form.setEnabled("nomeevento",true);
			form.setEnabled("cepEvento",true);
			form.setEnabled("complementoEnd",true);
			form.setEnabled("descricaoEven",true);
			form.setEnabled("horarioIni",true);
			form.setEnabled("horarioFim",true);
			form.setEnabled("ruaEvento",true);
			form.setEnabled("bairroEvento",true);
			form.setEnabled("cidadeEvento",true);
			form.setEnabled("estadoEvento",true);
			form.setEnabled("cargaHorariaTotal",true);
			form.setEnabled("valorHora",true);
			form.setEnabled("cargaHorariaValorTotal",true);
			form.setEnabled("valorHoraContrat",true);
			form.setEnabled("cargaHorariaValorTotContrat",true);			
			//carregaUsuario(form);			
			var fullDate = new Date();
			var date = fullDate.getDate().toString();
			if(date.length == 1) { date = 0+date; }
			var mes = (fullDate.getMonth()+1).toString();
			if(mes.length == 1) { mes = 0+mes; }
			var data = date+"/"+mes+"/"+fullDate.getFullYear();
			
			form.setValue('dataEvento', data)
			
		
		}if(atividade == 17){
			
			form.setEnabled("nomeAgenciaRecibo",true);
			form.setEnabled("dataAgenciaRecibo",true);
			form.setEnabled("CodAgenciaRecibo",true);
			form.setEnabled("bntEmtReciboAgencia",true); //button - emite recibo -- 
			
			
			//Campos para exibir dados do cliente e serviço 
			form.setEnabled("fieldValorBruto",true);
	    	form.setEnabled("fieldValorBrutoh",true);
	    	form.setEnabled("pagamentodividido",true);
	    	form.setEnabled("existeContrato",true);
	    	form.setEnabled("proximoPagamento",true);
	    	
	    	form.setEnabled("existeRateio",true);
	    	form.setEnabled("existeDesconto",true);
	    	
	    	form.setEnabled("hiddenAnexoGenerico",true);
	    	form.setEnabled("anexoGenerico",true);
	    	form.setEnabled("dataPagamentoGenerico",true);
	    	form.setEnabled("identificadorGenericoPagamento",true);
	    	form.setEnabled("valorGenericoPagamento",true);
	    	
	    	form.setEnabled("condicaoPagamentoGenerico",true);
	    	form.setEnabled("bandeiraPagamentoGenerico",true);
	    	
	    	//New Desconto Area -- 16/01/2020
	    	form.setEnabled("perDesconto",true);
	    	form.setEnabled("subTotalDesconto",true);
	    	form.setEnabled("totalDesconto",true);
	    	form.setEnabled("addPeriodo",true);
	    	form.setEnabled("valorPagamentoCliente",true);
	    	
	    	//Jean Varlet 23/12/2019
	    	
	    	form.setEnabled("decisaoOperador", true);
	    	form.setEnabled("cnpjEmpresaVinculada", true);
	    	form.setEnabled("codCfoEmpresa", true);
	    	form.setEnabled("razaoEmpresa", true);
	    	form.setEnabled("fantasiaEmpresa", true);
	    	form.setEnabled("fornecedorEmpresa", true);
	    	
	    	//Jean Varlet 01/12/2019
	    	form.setEnabled('rateioSebrae', true);
	    	form.setEnabled('valorSebrae', true);
	    	form.setEnabled('valorCliente', true);
	    	
	    	//Jean Varlet 18/11/2019	    	
	    	form.setEnabled('cpfconsultasasOutros', true);
	    	form.setEnabled('linkContratoAssinado', true);
	    	form.setEnabled('cpfconsultaOutrosF', true);
	    	form.setEnabled('codSASOutros', true);
	    	form.setEnabled('codcfoClienteOutros1', true);
	    	form.setEnabled('nomeClienteOutros1', true);
	    	form.setEnabled('razaoClienteOutros1', true);
	    	form.setEnabled('tipoClienteOutros1', true);
	    	form.setEnabled('cpfCnpjOkOutros', true);
	    	form.setEnabled('codServicoOutros', true);
	    	
	    	form.setEnabled('rmprojeto', true);
	    	form.setEnabled('rmacao', true);
	    	form.setEnabled('rm_saldoprojeto', true);
	    	form.setEnabled('rmunidade', true);
	    	form.setEnabled('rm_codunidade', true);
	    	form.setEnabled('rmrecurso', true);
	    	form.setEnabled('cpRateioValor', true);
	    	form.setEnabled('cpRateioPercent', true);
	    	form.setEnabled('cpRateioTot', true);
	    	form.setEnabled('rm_codrecurso', true);
	    	
	    	form.setEnabled('servicosSeleciona', true);
	    	
	    	form.setEnabled('dataInicioOutros', true);
	    	form.setEnabled('dataFimOutros', true);
	    	form.setEnabled('cepOutros', true);
	    	form.setEnabled('tituloOutros', true);
	    	form.setEnabled('ruaOutros', true);
	    	form.setEnabled('bairroOutros', true);
	    	form.setEnabled('cidadeOutros', true);
	    	form.setEnabled('estadoOutros', true);
	    	form.setEnabled('complementoEndOutros', true);
	    	form.setEnabled('descrevServOutros', true);
	    	form.setEnabled('horarioIniOutros', true);
	    	form.setEnabled('horarioFimOutros', true);
	    	
	    	
	    	form.setEnabled('tipoEventoSaSselecionado', true);
	    	form.setEnabled('dataIniEventoSaSselecionado', true);
	    	form.setEnabled('dataFimEventoSaSselecionado', true);	    	
	    	form.setEnabled('horaFimEventoSaSselecionado', true);
	    	form.setEnabled('horaIniEventoSaSselecionado', true);
	    	form.setEnabled('tituloEventoSaSselecionado', true);
	    	form.setEnabled('modalidadeEventoSaSselecionado', true);
	    	form.setEnabled('localEventoSaSselecionado', true);
	    	
	    	
	    	
	    	form.setEnabled('movimentoGeradoRM', true);
	    	
	    	// Links de arquivos salvos
	    	form.setEnabled('linkReciboSalvo', true);
	    	form.setEnabled('linkContratoSalvo', true);
	    	form.setEnabled('linkComprovantePagamentoSalvo', true);
	    	
	    	form.setEnabled('triggerReciboUpload', true);
	    	form.setEnabled('triggerContratoUpload', true);
	    	
	    	// Jean Varlet 19/11/2019
	    	form.setEnabled('emailClienteOutros', true);
	    	form.setEnabled('cidadeClienteOutros', true);
	    	form.setEnabled('estadoClienteOutros', true);
	    	form.setEnabled('enderecoClienteOutros', true);
	    	form.setEnabled('bairroClienteOutros', true);
	    	form.setEnabled('numeroClienteOutros', true);
	    	
	    	//Jean Varlet 20/11/2019
	    	
	    	form.setEnabled('textServicoOutros', true);
	    	
	    	
	    	form.setEnabled('somaSubTotalOutros', true);
	    	form.setEnabled('somaSubTotal2Outros', true);
	    	form.setEnabled('descontoInformadoOutros', true);
	    	form.setEnabled('valorTotalFinal2Outros', true);
	    	form.setEnabled('valorTotalFinalOutros', true);
	    	
	    	
	    	
	    	
	    	// Jean Varlet 17/11/2019 ---
	    	form.setEnabled('codConsultoria', true);
	    	form.setEnabled('tituloConsultoria', true);
	    	form.setEnabled('tipoConsultoria', true);
	    	form.setEnabled('necessidadeContrato', true);
	    	form.setEnabled('descricaoEven', true);
	    	form.setEnabled('datainicial', true);
	    	form.setEnabled('datafinal', true);
	    	form.setEnabled('horarioIni', true);
	    	form.setEnabled('horarioFim', true);
	    	form.setEnabled('cepEvento', true);
	    	form.setEnabled('cidadeEvento', true);
	    	form.setEnabled('complementoEnd', true);
	    	form.setEnabled('estadoEvento', true);
	    	form.setEnabled('valorHora', true);
	    	form.setEnabled('cargaHorariaValorTotal', true);
	    	form.setEnabled('cargaHorariaTotal', true);
	    	form.setEnabled('numberVlrTotRateio', true);
	    	form.setEnabled('tbAgencia2', true);
	    	form.setEnabled('tbNomePessoa2', true);
	    	form.setEnabled('tbProtocolo2', true);
	    	form.setEnabled('respTecnico', true);
	    	form.setEnabled('rm_fcfonomeForn', true);
	    	form.setEnabled('estado', true);
	    
	    	
	    	//JEan Varlet 14/11/2019
	    	//Nova área de pesquisa para consultorias ---
	    	form.setEnabled('dataIniPesqConsult', true);
	    	form.setEnabled('dataFimPesqConsult', true);
	    	//Radio Seleciona entre consultoria e instrutoria 
	    	form.setEnabled('selConsultInst', true);
	    	form.setEnabled('ConsultoriaInstrutoria', true);
	    	
	    	
	    	
	    	form.setEnabled('endereco', true);
	    	form.setEnabled('bairro', true);
	    	form.setEnabled('cliCodSas', true);
	    	form.setEnabled('numero', true);
	    	form.setEnabled('emailForn', true);
	    	
	    	
	    	//Jean Varlet 12/11/2019
	    	//Campo de pesquisa para área de outros serviços 
	    	form.setEnabled('cpfConsultaOutrosServicosF', true);
	    	form.setEnabled('cpfConsultaOutrosServicos', true);
	    	form.setEnabled('testeGerenteEmail', true);
	    	form.setEnabled('testeGerenteId', true);
	    	form.setEnabled('dataFimPesqConsult', true);
	    	
	    	
	    	
	    	//Jean Varlet 11/11/2019 - 
	    	//Campo para controlar a condicional de fluxo -> Assinatura.
	    	form.setEnabled('necessitaAssinatura', true);
	    	form.setEnabled('tipoServicoContratado', true);
	    	
	    	
	    	form.setEnabled('cpfconsultaFornecedor', true);
	    	form.setEnabled('buscaFornecedor', true);
	    	form.setEnabled('cpfconsultaFornecedorF', true);
	    	form.setEnabled('cliForForn', true);
	    	form.setEnabled('cnpjForn', true);
	    	form.setEnabled('razaoForn', true);
	    	form.setEnabled('fantasiaForn', true);
	    	form.setEnabled('responsavelForn', true);
	    	form.setEnabled('responsavelForn', true);
	    	form.setEnabled('tipoForn', true);
	    	form.setEnabled('estadoForn', true);
	    	form.setEnabled('cidadeForn', true);
	    	form.setEnabled('cargaConsult', true);
	    	form.setEnabled('valorHoraConsult', true);
	    	form.setEnabled('cpfFornecedorConsultoria', true);   	
	    	
	    	//Alteração 28/10/2019 -- Incluir pagamento no processo ---
	    	form.setEnabled("consideracoesConfReceb", true);
			form.setEnabled("anexaReciboBoleto", true);
			form.setEnabled("dataBoletoPagamento", true);
			form.setEnabled("valorBoletoPagamento", true);
			form.setEnabled("identificadorBoletoPagamento", true);
			form.setEnabled("condicaoBoletoPagamento", true);
			form.setEnabled("hiddenAnexarReciboDeposito", true);
			form.setEnabled("anexaReciboDeposito", true);
			form.setEnabled("dataDepositoPagamento", true);
			form.setEnabled("valorDepositoPagamento", true);
			form.setEnabled("identificadorDepositoPagamento", true);
			form.setEnabled("condicaoDepositoPagamento", true);
			form.setEnabled("hiddenAnexarReciboMaquineta", true);
			form.setEnabled("anexaReciboMaquineta", true);
			form.setEnabled("dataMaquinetaPagamento", true);
			form.setEnabled("valorMaquinetaPagamento", true);
			form.setEnabled("codMaquinetaPagamento", true);
			form.setEnabled("bandeiraMaquinetaPagamento", true);
			form.setEnabled("condicaoMaquinetaPagamento", true);
			form.setEnabled("radioEventoCancelado", true);
	    	form.setEnabled("formaPagamento", true);
			form.setEnabled("tbCodRecebedor",true);
			form.setEnabled("dataRecebimento",true);
			form.setEnabled("tbNomeRecebedor",true);
			form.setEnabled("tbChapaRecebedor",true);
			form.setEnabled("radioRecebimento",true);
			form.setEnabled("consideracoesConfReceb",true);
			form.setEnabled("radioEventoCancelado",true);
			form.setEnabled("justificaEventoCancelado",true);
	    	//Alteração 28/10/2019 -- Incluir pagamento no processo ---    	
	    	
	    	form.setEnabled("quantidadeEventos", true);
	    	form.setEnabled("codigosEventos", true);
	    	form.setEnabled("fisicoJuridico", true);	
	    	
	    	//tabela de eventos SAS e campos de valores
	    	form.setEnabled("somaSubTotal", true);
	    	form.setEnabled("somaSubTotal2", true);
	    	form.setEnabled("valorTotalFinal2", true);
	    	form.setEnabled("desconto", true);
	    	form.setEnabled("descontoInformado", true);	    	
	    	//tabela de eventos SAS e campos de valores
	    	
	    	form.setEnabled("cpfCnpjValidado",true);
	    	form.setEnabled("nomeCliente",true);
	    	form.setEnabled("razaoCliente",true);
	    	form.setEnabled("cpfCnpjFormatado",true);
	    	form.setEnabled("cpfCnpj",true);
	    	form.setEnabled("codClienteSAS",true);
	    	form.setEnabled("codClienteRM",true);
	    	form.setEnabled("nomeProcesso",true);    	
	    	
	    	form.setEnabled("eventoNome1",true);
	    	form.setEnabled("eventoDataInicio1",true);
	    	form.setEnabled("eventoDataFim1",true);
	    	form.setEnabled("eventoTipo1",true);
	    	form.setEnabled("eventoCarga1",true);
	    	form.setEnabled("eventoPreco1",true);
	    	form.setEnabled("eventoCodigo1",true);
	    	//Jean Varlet --- 27/11/2019
	    	form.setEnabled("senhaPortalClienteAcesso",true);

	    	
	    	form.setEnabled("cepClienteSAS",true);
	    	form.setEnabled("ruaClienteSAS",true);
	    	form.setEnabled("bairroClienteSAS",true);
	    	form.setEnabled("cidadeClienteSAS",true);
	    	form.setEnabled("estadoClienteSAS",true);
	    	form.setEnabled("emailClienteSAS",true);
	    	form.setEnabled("contatoClienteSAS",true);
	    	form.setEnabled("telefoneClienteSAS",true);
	    	form.setEnabled("inscEstClienteSAS",true);
	    	form.setEnabled("identidadeClienteSAS",true);
	    	
	    	
	    	form.setEnabled("codSAS", true);
	    	form.setEnabled("somaTotal", true); //Somatorio de cursos ---
	    	form.setEnabled("descontoInformado", true); //Somatorio de cursos ---
	    	form.setEnabled("valorTotalFinal", true); //Somatorio de cursos ---
	    	form.setEnabled("codcfoCliente1", true);
	    	form.setEnabled("nomeCliente1", true);
	    	form.setEnabled("razaoCliente1", true);
	    	form.setEnabled("tipoCliente1", true);
	    	form.setEnabled("cpfCnpjOk", true);
	    	
			form.setEnabled("tbCodPessoa",true);
			form.setEnabled("cpfconsultasas",true);
			form.setEnabled("tbNomePessoa",true);
			form.setEnabled("tbChapaPessoa",true);
			form.setEnabled("rm_dadoscolab",true);
			form.setEnabled("colabzoom",true);
			form.setEnabled("tbUsuario",true);
			form.setEnabled("tbGerente",true);
			form.setEnabled("tbDiretor",true);
			form.setEnabled("tbAgencia" ,true);
			form.setEnabled("tbFilial",true);
			form.setEnabled("tbGerenteUsuario",true);
			form.setEnabled("tbDiretorUsuario",true);
			form.setEnabled("tbCodVen",true);
			form.setEnabled("rm_depto",true);
			form.setEnabled("rm_coddepto",true);			
			form.setEnabled("tbProtocolo",true);
			form.setEnabled("dataEvento",true);
			form.setEnabled("existeConsEspontaea",true);
			form.setEnabled("cpfconsulta",true);
			form.setEnabled("cpfconsultaF",true);
			form.setEnabled("rm_fcfo",true);
			form.setEnabled("rm_fcfocodcfo",true);
			form.setEnabled("rm_fcfocolig",true);
			form.setEnabled("rm_fcfonome",true);
			form.setEnabled("rm_fcfosituacao",true);
			form.setEnabled("rm_fcfoporte",true);
			form.setEnabled("rm_fcfocgc",true);
			form.setEnabled("rm_fcforazao",true);
			form.setEnabled("rm_fcfotelefone",true);
			form.setEnabled("rm_fcfoemail",true);
			form.setEnabled("descPublicoIndefinido",true);
			form.setEnabled("dadomat01",true);
			form.setEnabled("dataPrevisao",true);
			form.setEnabled("atividade",true);
			form.setEnabled("complementoPag",true);
			form.setEnabled("rmprojeto",true);
			form.setEnabled("rmprojeto",true);
			form.setEnabled("dtInicio",true);
			form.setEnabled("dtTermino",true);
			form.setEnabled("chMes",true);
			form.setEnabled("dtPrevisaoPgto",true);
			form.setEnabled("chTotal",true);
			form.setEnabled("rmacao",true);
			form.setEnabled("rm_saldoprojeto",true);
			form.setEnabled("rmunidade",true);
			form.setEnabled("rm_codunidade",true);
			form.setEnabled("rmrecurso",true);
			form.setEnabled("cpRateioValor",true);
			form.setEnabled("cpRateioPercent",true);
			form.setEnabled("cpRateioTot",true);
			form.setEnabled("rm_codrecurso",true);
			form.setEnabled("numberVlrTotRateio",true);
			form.setEnabled("rmacao",true);
			form.setEnabled("rm_saldoprojeto",true);
			form.setEnabled("rm_codunidade",true);
			form.setEnabled("rmunidade",true);
			form.setEnabled("rmrecurso",true);
			form.setEnabled("cpRateioValor",true);
			form.setEnabled("cpRateioPercent",true);
			form.setEnabled("cpRateioTot",true);
			form.setEnabled("rm_codrecurso",true);
			
			form.setValue("tbProtocolo", numproc);//ok
		
		
			form.setEnabled("pgtunico",true);
			form.setEnabled("nomeProcesso",true);
			form.setEnabled("datainicial",true);
			form.setEnabled("datafinal",true);
			form.setEnabled("necessidadeContrato",true);
			form.setEnabled("tipoSer",true);
			form.setEnabled("areaconh",true);
			form.setEnabled("subareaconh",true);
			form.setEnabled("nomeevento",true);
			form.setEnabled("cepEvento",true);
			form.setEnabled("complementoEnd",true);
			form.setEnabled("descricaoEven",true);
			form.setEnabled("horarioIni",true);
			form.setEnabled("horarioFim",true);
			form.setEnabled("ruaEvento",true);
			form.setEnabled("bairroEvento",true);
			form.setEnabled("cidadeEvento",true);
			form.setEnabled("estadoEvento",true);
			form.setEnabled("cargaHorariaTotal",true);
			form.setEnabled("valorHora",true);
			form.setEnabled("cargaHorariaValorTotal",true);
			form.setEnabled("valorHoraContrat",true);
			form.setEnabled("cargaHorariaValorTotContrat",true);
			
		}
		if(atividade == '11'){
			form.setEnabled("nomeGerenteAtesta",true);
			form.setEnabled("dataGerenteAtesta",true);
			form.setEnabled("gerenteAtestaServico",true);
			
		}
			if(atividade == '15'){
			form.setEnabled('nomeGestorAgencia', true);
			form.setEnabled('dataAssinaturaGestor', true);
			form.setEnabled('CodGestroAgencia', true);
			//form.setEnabled('codConsultoria', true);
			// campos filhos de pagamentos -- 
			form.setEnabled('dataPagamentoGenerico', true);
			form.setEnabled('hiddenAnexoGenerico', true);
			form.setEnabled('identificadorGenericoPagamento', true);
			form.setEnabled('valorGenericoPagamento', true);
			form.setEnabled('condicaoPagamentoGenerico', true);
			form.setEnabled('bandeiraPagamentoGenerico', true);
			form.setEnabled('formaPagamento', true);
			// campos filhos de pagamentos --
			
		}
		if(atividade == '20'){
			
			form.setEnabled("nomeFinanceiroConfere",true);
			form.setEnabled("dataFinanceiroConfere",true);
			form.setEnabled("CodFinanceiroConfere",true);
			form.setEnabled("financeiroAutNF",true);
			form.setEnabled("JustificaNaoNF",true);
			
		}
	    if(atividade == '24'){
	    	
	    	form.setEnabled("nomeCancelaExecucao", true);
	    	form.setEnabled("dataCancelaExecucao", true);
	    	form.setEnabled("CodCancelaExecucao", true);
	    	form.setEnabled("radioCancelaExecucao", true);
	    	
	    	form.setEnabled("JustificaCancelaExecucao", true);
	    }
	    if(atividade == '31'){
	    	form.setEnabled("gerenteHomServico", true);
	    	form.setEnabled("nomeGerenteHomologa", true);
	    	form.setEnabled("dataGerenteHomologa", true);
	    }
	    if(atividade == '33'){
	    	form.setEnabled('nomeAgenciaFinanceiroNF', true);
	    	form.setEnabled('dataAgenciaFinanceiroNF', true);
	    	form.setEnabled('anexaFileNotaFiscal', true);
	    }
	    if(atividade == '35'){
	    	form.setEnabled('nomeContabilizaReceitas',true);
	    	form.setEnabled('dataContabilizaReceitas',true);
	    	form.setEnabled('movimentosFaturar',true);
	    }
	    
	    

    
function getUsuario(user) {

    var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", user, user, ConstraintType.MUST);
    var datasetUser = DatasetFactory.getDataset("colleague", null, [c1], null);
    var colleague = new Array();
    if (datasetUser.rowsCount > 0) {

        colleague = [
            datasetUser.getValue(0, "colleaguePK.colleagueId"),
            datasetUser.getValue(0, "colleagueName"),
            datasetUser.getValue(0, "adminUser"),
            datasetUser.getValue(0, "mail"),
            datasetUser.getValue(0, "currentProject"),
            datasetUser.getValue(0, "especializationArea"),
            datasetUser.getValue(0, "groupId")
        ]
    }
    return colleague;
}

//Carrega usuário da solicitação

var colabLogado1 = getValue("WKUser");
function selectUser(colaborador){
	var arrayColabs = new Array();
	arrayColabs.push('03539857fa9211e997e20a5864604340','5e6a242bad3f11e9a5b70a5864600c69',
			'606557e5ad3f11e9819e0a5864612d26','70bf9accad4311e9a5b70a5864600c69', '62687b74ad3f11e99ca20a586460437b');
    var idConsulta = 'yf8xqo35ctxschu41531148008103';
    var retorno;    
    for(var i = 0; i< arrayColabs.length; i++){
        if(colaborador == arrayColabs[i]){            
            retorno = idConsulta;           
        }else{
            retorno = colabLogado1;            
        }        
    }
    return retorno;
}
function carregaUsuario(form){
	var logado = selectUser(colabLogado1);
	var c1 = DatasetFactory.createConstraint("companyId", getValue("WKCompany"), getValue("WKCompany"), ConstraintType.MUST);
	var c2 = DatasetFactory.createConstraint('colleagueId', logado, logado, ConstraintType.MUST);
    var colleague = DatasetFactory.getDataset('colleague', null, [c1,c2], null);
    
    form.setValue('tbNomePessoa', colleague.getValue(0,"colleagueName"));
	form.setValue('tbCodPessoa', colleague.getValue(0,"mail"));
	form.setValue('tbUsuario', colleague.getValue(0,"colleaguePK.colleagueId"));	
	form.setValue('dadomat01', colleague.getValue(0,"colleaguePK.colleagueId"));
	form.setValue('dataatesto01', dataAtual());
	
	var colab = colleague.values[0].mail;
	
	var d1 = DatasetFactory.createConstraint('EMAIL', colab, colab, ConstraintType.MUST);
	var ds_colab = DatasetFactory.getDataset('rm_consulta_dadoscolaborador', null, [d1], null);
    
	form.setValue('tbAgencia', ds_colab.getValue(0, "SECAO"));
	form.setValue('rm_depto', ds_colab.getValue(0, "DEPTO"));
	form.setValue('rm_dadoscolab', ds_colab.getValue(0, "NOME"));
	form.setValue('rm_coddepto', ds_colab.getValue(0, "CODDEPTO"));
	form.setValue('tbDiretorUsuario', ds_colab.getValue(0, "CHAPADIRETOR")); 		
	
}

function dataAtual() {
  var data = new Date();
  var dia  = data.getDate();
  var mes  = data.getMonth() + 1;
  var ano  = data.getFullYear();

  dia  = (dia<=9 ? "0"+dia : dia);
  mes  = (mes<=9 ? "0"+mes : mes);

  var newData = dia+"/"+mes+"/"+ano;

  
}

}
