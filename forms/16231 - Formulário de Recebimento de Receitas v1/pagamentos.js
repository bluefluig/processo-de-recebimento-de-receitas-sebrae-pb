/*
 * Jean Varlet  - 15/01/2020  
 * 
 */

/*
 * Valida forma se forma de pagamento foi selecionada.
 * Falta mapear função pai da chamada
 * */
function validaFormaPagamento(naturezaSel){	
	//formaPagamento
	if($("#formaPagamento").val() == 0  ){
		if($(".alert-dismissible").length == 0){			
			FLUIGC.toast({
				title: 'Aviso',
				message: '<br>É necessário Informar a forma de Pagamento Primeiro',
				type: 'danger'
			});			
			autoScroll('formaPagamento', 100);			
			$(`#formaPagamento`).parent().addClass('has-error');
		}		
	}else if($("#formaPagamento").val() != 0 && naturezaSel == 1){		
		$(".alert-dismissible").remove()
		$(`#formaPagamento`).parent().removeClass('has-error');
		$("#bntEmtContrato").click(function(){			
				gerarContrato();				
		});		
	}else if($("#formaPagamento").val() != 0 && naturezaSel == 2 || 3){
		$(".alert-dismissible").remove()
		$(`#formaPagamento`).parent().removeClass('has-error');
		$("#bntEmtRecibo").click(function(){			
			gerarRecibo();			
		});
		$("#bntEmtContrato").click(function(){			
			gerarContrato();			
		});		
	}	
}
/*Controla liberação dos botões de emissão de contrato, emissão de recibo e gerar movimento financeiro 
 * Falta contexto da chamada da função
 * */
function seqButtons(naturezaSel){	
	if(naturezaSel == 1){
		$("#bntlancMovimento").prop("disabled",true );
		$("#bntEmtRecibo").prop("disabled",true );
		$("#bntEmtContrato").prop("disabled",false );		
	}else if(naturezaSel == 2 || 3){		
		$("#bntEmtContrato").prop("disabled",false );		
		$("#bntEmtRecibo").prop("disabled",false );
		$("#bntlancMovimento").prop("disabled",true );
	}	
}

//Jean Varlet 13/11/2019 ---Retorna os dados do pagamento para a etapa de conferência do Financeiro----
function carregaDadosPagamentoFinanceiro(){
	//checa forma de pagamento--	
	var formaPagamento = $("#formaPagamento").val();	
	var _dataPagamento ;
	var _valorPagamento;
	var _codTransacao;
	var _bandeiraPagamento;
	var _parcelas;	
	if(formaPagamento == 1 || 2){
		//cartao -- debito / credito --- 			
		 _bandeiraPagamento = $("#bandeiraMaquinetaPagamento").val();		
		 $("#tipoPagamentoFinanceiro").val('Crédito/Débito');
		 $("#valorPagamentoFinanceiro").val($("#valorMaquinetaPagamento").val());
		 $("#dataPagamentoFinanceiro").val($("#dataMaquinetaPagamento").val());
		 $("#condPagamentoFinanceiro").val($("#condicaoMaquinetaPagamento").val());
		 $("#identPagamentoFinanceiro").val($("#codMaquinetaPagamento").val());		 
	}else if(formaPagamento == 3 ){
		//deposito ---		 		
		 _bandeiraPagamento = $('none').val();	
		 $("#tipoPagamentoFinanceiro").val('Depósito Bancário');
		 $("#valorPagamentoFinanceiro").val($("#valorDepositoPagamento").val());
		 $("#dataPagamentoFinanceiro").val($("#dataDepositoPagamento").val());
		 $("#condPagamentoFinanceiro").val( $("#condicaoDepositoPagamento").val());
		 $("#identPagamentoFinanceiro").val($("#identificadorDepositoPagamento").val());		
	}else if(formaPagamento == 4 ){
		//boleto ---	 
		 _bandeiraPagamento = $('none').val(); 
		 $("#tipoPagamentoFinanceiro").val('Boleto Bancário');
		 $("#valorPagamentoFinanceiro").val($("#valorBoletoPagamento").val());
		 $("#dataPagamentoFinanceiro").val($("#dataBoletoPagamento").val());
		 $("#condPagamentoFinanceiro").val($("#condicaoBoletoPagamento").val());
		 $("#identPagamentoFinanceiro").val($("#identificadorBoletoPagamento").val());
	}
}