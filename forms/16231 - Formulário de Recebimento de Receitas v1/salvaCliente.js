/*Jean Varlet*/

function fnSalvaCliente(){
	var myLoading = FLUIGC.loading(window);
	 myLoading.show();	
	console.log('fnSalvaCliente() -> ')	
	//valida Campos Modal -- 	
	let FISJURREC;
	if($('#txtCodCfo' ).val() == null || $('#txtCodCfo' ).val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
            message: 'O Campo Código do RM está em Branco.',
            type: 'danger'
        });		
	}if($('#txtRazaoSocial').val() == null || $('#txtRazaoSocial').val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
            message: 'O Campo Razão Social está em Branco.',
            type: 'danger'
        });
		
	}if($('#txtFantasia').val() == null || $('#txtFantasia').val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
            message: 'O Campo Nome Fantasia está em Branco.',
            type: 'danger'
        });
		
	}if($('#txtmCGC' ).val() == null || $('#txtmCGC' ).val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
            message: 'O Campo CPF/CNPJ está em Branco.',
            type: 'danger'
        });
		
	}if($('#txtmCGC' ).val().length > 14){//se for cnpj deve informar  inscrição estadual 
		if($('#txtIE' ).val() == null || $('#txtIE' ).val() == undefined ){
			myLoading.hide()
			FLUIGC.toast({
	            message: 'O Campo Inscrição Estadual está em Branco.',
	            type: 'danger'
	        });
		}
	}if($('#txtlogradouro').val() == null || $('#txtlogradouro').val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
            message: 'O Campo Rua está em Branco.',
            type: 'danger'
        });
		
	}
	if($('#txtnumero').val() == null || $('#txtnumero').val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
            message: 'O Campo Número está em Branco.',
            type: 'danger'
        });
		
	}
	if($('#txtcidade').val() == null || $('#txtcidade').val() == undefined ){
		myLoading.hide()
		FLUIGC.toast({
			message: 'O Campo Cidade está em Branco.',
			type: 'danger'
		});
		
	}
	if($('#txtCategoria' ).val() == "F" ){
		FISJURREC = "0588";
	}else{
		FISJURREC = "1708B";
	}	
	
	console.log('FISJURREC' + FISJURREC)
	var c1 = DatasetFactory.createConstraint("CODCOLIGADA", 1, 1, ConstraintType.MUST);
	var c2 = DatasetFactory.createConstraint("CODCFO", $('#txtCodCfo' ).val(), $('#txtCodCfo' ).val(), ConstraintType.MUST);
	var c3 = DatasetFactory.createConstraint("NOME", $('#txtRazaoSocial').val(), $('#txtRazaoSocial' ).val(), ConstraintType.MUST);
	var c4 = DatasetFactory.createConstraint("NOMEFANTASIA", $('#txtFantasia').val(), $('#txtFantasia' ).val(), ConstraintType.MUST);
	var c5 = DatasetFactory.createConstraint("CGCCFO", $('#txtmCGC' ).val(), $('#txtmCGC').val(), ConstraintType.MUST);
	var c6 = DatasetFactory.createConstraint("PAGREC", $('#txtClassificacao' ).val(), $('#txtClassificacao' ).val(), ConstraintType.MUST);
	var c7 = DatasetFactory.createConstraint("INSCRESTADUAL", $('#txtIE' ).val(), $('#txtIE' ).val(), ConstraintType.MUST);
	var c8 = DatasetFactory.createConstraint("PESSOAFISOUJUR", $('#txtCategoria' ).val(), $('#txtCategoria' ).val(), ConstraintType.MUST);
	var c9 = DatasetFactory.createConstraint("CODTCF", $('#txtTipoCliFor').val(), $('#txtTipoCliFor').val(), ConstraintType.MUST);
	var c10 = DatasetFactory.createConstraint("RUA", $('#txtlogradouro').val(), $('#txtlogradouro').val(), ConstraintType.MUST);
	var c11 = DatasetFactory.createConstraint("NUMERO", $('#txtnumero').val(), $('#txtnumero').val(), ConstraintType.MUST);
	var c12 = DatasetFactory.createConstraint("BAIRRO", $('#txtbairro').val(), $('#txtbairro').val(), ConstraintType.MUST);
	var c13 = DatasetFactory.createConstraint("CIDADE", $('#txtcidade').val(), $('#txtcidade').val(), ConstraintType.MUST);
	var c14 = DatasetFactory.createConstraint("CODETD", $('#txtUF').val(), $('#txtUF').val(), ConstraintType.MUST);
	var c15 = DatasetFactory.createConstraint("CEP", $('#txtCEP').val(), $('#txtCEP').val(), ConstraintType.MUST);
	var c16 = DatasetFactory.createConstraint("COMPLEMENTO", $('#txtcomplemento').val(), $('#txtcomplemento').val(), ConstraintType.MUST);
	var c17 = DatasetFactory.createConstraint("TELEFONE", $('#txttelefone').val(), $('#txttelefone').val(), ConstraintType.MUST);
	var c18 = DatasetFactory.createConstraint("TELEX", $('#txtcelular').val(), $('#txtcelular').val(), ConstraintType.MUST);
	var c19 = DatasetFactory.createConstraint("EMAIL", $('#txtemail').val(), $('#txtemail').val(), ConstraintType.MUST);
	var c20 = DatasetFactory.createConstraint("CODRECEITA", FISJURREC, FISJURREC, ConstraintType.MUST);
	var c21 = DatasetFactory.createConstraint("CONTATO", $('#txtcontato').val(), $('#txtcontato').val(), ConstraintType.MUST);
	var constraint = new Array(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c16, c15, c16, c17, c18, c19, c20, c21);
	var ret = DatasetFactory.getDataset("dsInseriCliForWS", null, constraint, null);	
	if (ret.values.length > 1) {			
		myLoading.hide()
		FLUIGC.toast({
			message: "Não Foi possível salvar os dados do Cliente." + ret.values[0]["Result"],
			type: 'danger'
		});		
	} else {
		myLoading.hide();
		FLUIGC.toast({
			message: "" + ret.values[0]["Result"],
			type: 'success'
		});
		codigo = ret.values[0]["Result"].split(";");
		codigo = codigo[1].toString()
		$('#txtCodCfo' ).val(codigo.substring(0, 6))
	}
	
}