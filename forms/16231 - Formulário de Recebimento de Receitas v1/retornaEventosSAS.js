/*Jean Varlet*/


// Dados do cliente para compor buscas nos sistemas SAS e RM -> 

var idClientePaiFilho;


// Busca Eventos do cliente no SAS -> 

function fnRetornaEventosCliente(cliente, idClientePaiFilho){	
	if (cgccliente.length == 11) {		
		var ce1 = DatasetFactory.createConstraint("CodPessoaF", codparc, codparc, ConstraintType.MUST);		
	} else {		
		var ce1 = DatasetFactory.createConstraint("CodPessoaJ", codparc, codparc, ConstraintType.MUST);		
	}
	var c = new Array(ce1);		
	var returnEvent = DatasetFactory.getDataset("dsSASEventosCliente", null, c, null);	
	if (returnEvent != null || returnEvent !== undefined) {	
		var i = returnEvent.values;
		that.mydata = [];
		for (var index in i) {
			var val = i[index];
	
			that.mydata.push({
	
				EventoID: val.EventoID,
				EventoNome: val.EventoNome,
				PeriodoInicial: moment(val.PeriodoInicial).format('DD/MM/YYYY'),
				PeriodoFinal: moment(val.PeriodoFinal).format('DD/MM/YYYY'),
				Aprovado: val.Aprovado === 1 ? 'Aprovado' : 'Reprovado',
				DataDesistencia: val.DataDesistencia,
				Situacao: val.Situacao,
				SituacaoCliente: val.SituacaoCliente === 'A' ? 'Ativo' : 'Finalizado',
				Valor: val.ValorPago,
				CodCentroCusto: val.CodCentroCusto
			});
		}
	
	} else {
	
		//exibe msg - Cliente não possui eventos cadastrados !!
	
	}
		
	
}	

// Mota datatable de eventos para o cliente selecionado - > 

function fnMontaDataTableEventosCliente(){
	
	if(fnRetornaEventosCliente(cliente)){
		
		var tableEventos;
		// se tabela já existir - > 
		if (tableEventos) {
			tableEventos.destroy();
			
		}
		
		tableEventos = FLUIGC.datatable('#tabela_eventos_' + idClientePaiFilho , {
			dataRequest: that.mydata,
			renderContent: '.template_datatable_eventos',
			header: [
				{ 'title': 'Selecionar' },
				{ 'title': 'ID Evento' },
				{ 'title': 'Evento' },
				{ 'title': 'Início' },
				{ 'title': 'Términio' },
				{ 'title': 'Resultado' },
				{ 'title': 'Desistência' },
				{ 'title': 'Situacao' },
				{ 'title': 'Situacao do Cliente' },
				{ 'title': 'Valor' }
				// {'title': 'CodCentroCusto'}
			],
			scroll: {
				target: '.table',
				enabled: true
			},
			multiSelect: true,
			//classSelected: 'info',
			selected: function (el, ev) {
				var linhaSelecionada = $(that).closest('tr').index();
				var linha = tableEventos.getRow(linhaSelecionada);
			},
			search: {
				enabled: true,
				onlyEnterkey: false,
				searchAreaStyle: 'col-md-12',
				onSearch: function (res) {
					tableEventos.reload(that.mydata);
					if (res) {
						var data = that.mydata;
						var search = data.filter(function (el) {
							return el.EventoNome.toUpperCase().indexOf(res.toUpperCase()) >= 0 || String(el.EventoID).indexOf(res.toUpperCase()) >= 0
								|| el.PeriodoInicial.toUpperCase().indexOf(res.toUpperCase()) >= 0 || el.PeriodoFinal.toUpperCase().indexOf(res.toUpperCase()) >= 0
								|| el.Aprovado.toUpperCase().indexOf(res.toUpperCase()) >= 0 || el.Situacao.toUpperCase().indexOf(res.toUpperCase()) >= 0
								|| el.SituacaoCliente.toUpperCase().indexOf(res.toUpperCase()) >= 0;
						});
						tableEventos.reload(search);
					}
				}
			},
			function(err, data) {
				if (err) {
					FLUIGC.toast({
						message: err,
						type: 'warning'
					});
				}
			}
		})
		
	}else{
		
		//exibe msg - Cliente não possui eventos cadastrados !!
	}
	
}