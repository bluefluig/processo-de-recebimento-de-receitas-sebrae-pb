
/*
var myModal = parent.FLUIGC.modal({
    title: 'Title',
    content: '<h1>Modal Content</h1>',
    id: 'fluig-modal',
    actions: [{
        'label': 'Save',
        'bind': 'data-open-modal',
    },{
        'label': 'Close',
        'autoClose': true
    }]
}, function(err, data) {
    if(err) {
        // do error handling
    } else {
        // do something with data
    }
});
*/
function gerarPassword() {
    return Math.random().toString(36).slice(-10);
}
function consultaCadCliente(){
	var cliente;
	if($("#tipoSerConsultoria").is(':checked')== true){			
		cliente = $("#cpfconsultaFornecedor").val();
	}else if($("#tipoSerInstrutoria").is(':checked') == true){
		
		cliente= $("#cpfconsultasas").val();
	}
	else if($("#tipoSerOutros").is(':checked')== true){
		cliente = $("#cpfconsultasasOutros").val();		
	}
	var ct1 = DatasetFactory.createConstraint('cpfCnpj', cliente, cliente, ConstraintType.MUST);
	
	
	var consultaCliente = DatasetFactory.getDataset('ds_insere_login_portal_clientes', null , new Array(ct1), null);
	var counter = consultaCliente.values.length;
	
		if(counter > 0  &&  consultaCliente != undefined){
			var razao = consultaCliente.values[0].razaoSocial;
			var nome = consultaCliente.values[0].nomeFantasia;
			var cgc = consultaCliente.values[0].cpfCnpj;
			var email = consultaCliente.values[0].email;
			var situacao = consultaCliente.values[0].situacao;
			
			var senha = consultaCliente.values[0].senha;
			
			if(razao == null || razao == '' || razao == undefined){
				
				razao =   '...';
			}
			if(nome == null || nome == '' || nome == undefined){
				
				nome =   '...';
			}
			if(email == null || email == '' || email == undefined){
				
				email =   '...';
			}
			if(email == null || email == '' || email == undefined){
				
				email =   '...';
			}

			openSchoolModal();
			console.log('retornei do dataset')
			FLUIGC.toast({
						title: 'Cliente!',
						message: '<br>O cliente Já possui Cadastro.',
						type: 'success'
			});
			setTimeout(function(){
				$(".save-cliente:contains('Adicionar')").prop("disabled",true);
				retornaModalCliente(razao,nome, cgc, email, senha, situacao );
			},2000)
			

		}else{
			openSchoolModal();
			console.log('retornei do form')
			setTimeout(function(){
				$(".save-cliente:contains('Adicionar')").prop("disabled",false);
				popModalCliente();				
				
			},2000)
			

		}
	}

function popModalCliente(){
	
	if($("#tipoSerConsultoria").is(':checked')== true ){	
		
		$("#razaoSocialModal").val($("#fantasiaForn").val())
		$("#nomeFantasiaModal").val($("#razaoForn").val())
		$("#cpfCnpjModal").val($("#cpfconsultaFornecedor").val())
		$("#emailModal").val($("#emailForn").val())
		
		$("#senhaModal").val(gerarPassword());
		
		$("#senhaPortalClienteAcesso").val($("#senhaModal").val());
		
	}else if($("#tipoSerInstrutoria").is(':checked') == true ){
		
		$("#razaoSocialModal").val($("#nomeCliente1").val())
		$("#nomeFantasiaModal").val($("#razaoCliente1").val())
		$("#cpfCnpjModal").val($("#cpfconsultasas").val())
		$("#emailModal").val('')
		
		$("#senhaModal").val(gerarPassword())
		$("#senhaPortalClienteAcesso").val($("#senhaModal").val());
		
	}else if($("#tipoSerOutros").is(':checked')== true){
		
		$("#razaoSocialModal").val($("#nomeClienteOutros1").val())
		$("#nomeFantasiaModal").val($("#razaoClienteOutros1").val())
		$("#cpfCnpjModal").val($("#cpfconsultasasOutros").val())
		$("#emailModal").val($("#emailClienteOutros").val())
		
		$("#senhaModal").val(gerarPassword())
		$("#senhaPortalClienteAcesso").val($("#senhaModal").val());
		
	}
	
}
function retornaModalCliente(razao,nome, cgc, email, senha, situacao ){
	
	if($(".consultInst").val() == 'Consultoria'  ){		
		
		$("#razaoSocialModal").prop("disabled",true);
		$("#nomeFantasiaModal").prop("disabled",true);
		$("#cpfCnpjModal").prop("disabled",true);
		$("#emailModal").prop("disabled",true);
		$("#senhaModal").prop("disabled",true);
		
		$("#razaoSocialModal").val(razao)
		$("#nomeFantasiaModal").val(nome)
		$("#cpfCnpjModal").val(cgc)
		$("#emailModal").val(email)
		
		$("#senhaModal").val(senha)
		$("#senhaPortalClienteAcesso").val($("#senhaModal").val());
		
	}else if($(".consultInst").val() == 'Instrutoria' ){
		
		$("#razaoSocialModal").prop("disabled",true);
		$("#nomeFantasiaModal").prop("disabled",true);
		$("#cpfCnpjModal").prop("disabled",true);
		$("#emailModal").prop("disabled",true);
		
		$("#senhaModal").prop("disabled",true);
		
		$("#razaoSocialModal").val(razao)
		$("#nomeFantasiaModal").val(nome)
		$("#cpfCnpjModal").val(cgc)
		$("#emailModal").val(email)
		
		$("#senhaModal").val(senha)
		$("#senhaPortalClienteAcesso").val($("#senhaModal").val());
		
	}else if($(".consultInst").val() == 'Outros'){
		
		$("#razaoSocialModal").prop("disabled",true);
		$("#nomeFantasiaModal").prop("disabled",true);
		$("#cpfCnpjModal").prop("disabled",true);
		$("#emailModal").prop("disabled",true);
		
		$("#senhaModal").val(senha)
		
		$("#razaoSocialModal").val(razao)
		$("#nomeFantasiaModal").val(nome)
		$("#cpfCnpjModal").val(cgc)
		$("#emailModal").val(email)
		
		$("#senhaModal").val(senha)
		$("#senhaPortalClienteAcesso").val($("#senhaModal").val());
		
	}
	
}

function openSchoolModal(){
	var tpl = $('.tpl-cadastra-cliente').html();
	var html = Mustache.render(tpl, null);
	
	modal = FLUIGC.modal({
		title: 'Dados de Acesso ao Portal do Cliente Sebrae',
		content: html,
		id: 'fluig-modal',
		size: 'full',
		actions: [{
			'label': 'Adicionar',
			'classType' : 'btn-primary save-cliente',
			'autoClose': true
		},{
			'label': 'Fechar',
			'autoClose': true
		}]
	}, function(err, data) {
		$(".save-cliente").click(function(){
			saveSchool();
		});
	});
}

function saveSchool(){
	var parentId = getDatasetId();
	
	if(parentId == null || parentId == undefined || parentId == ""){
		FLUIGC.toast({
			title: '',
			message: "O cadastro de Clientes não foi configurado no Fluig. Contate o administrador do sistema.",
			type: 'danger'
		});
		return false;
	}
	
	parent.WCMAPI.Create({
	url: '/api/public/2.0/cards/create',
	data:
		JSON.stringify({
			"documentDescription": "Novo Cliente",
			"parentDocumentId": parentId,
			"version": 1000,
			"formData": [
				{
					"name": "razaoSocial",
					"value": $("#razaoSocialModal").val()
					},
				{
					"name": "nomeFantasia",
					"value": $("#nomeFantasiaModal").val()
				},
				{
					"name": "cpfCnpj",
					"value": $("#cpfCnpjModal").val()
				},
				{
					"name": "email",
					"value": $("#emailModal").val()
				},
				{
					"name": "senha",
					"value": $("#senhaModal").val()
				},
				{
					"name": "situacao",
					"value": $("#situacaoModal").val()
				}
				
				
			]
		}),
	success: function (data, status, xhr) {
		FLUIGC.toast({
	        title: '',
	        message: "Cliente Cadastrado.",
	        type: 'success'
		});
	},
	error: function(xhr, status, error) {
		FLUIGC.toast({
			title: '',
			message: "Ocorreu um erro ao cadastrar o Cliente. Se o problema persistir, cadastre o cliente pelo GED.",
			type: 'danger'
		});
	}
});
}
function getDatasetId(){
	var DATASET = "ds_insere_login_portal_clientes";
	var datasetName = DatasetFactory.createConstraint("datasetName", DATASET, DATASET, ConstraintType.MUST);
	var document = DatasetFactory.getDataset("document", null, [datasetName], null);
	return document.values[0]["documentPK.documentId"];
}