/*Jean Varlet --  */


//Questiona se deseja inserir empresa cliente


/*Jean Varlet --  */


//Questiona se deseja inserir empresa cliente

function adicionarEmpresaCliente(){
	FLUIGC.message.confirm({
	    message: 'Deseja Vincular esse pagamento a empresa do Cliente?',
	    title: 'Vincular Empresa',
	    labelYes: 'Sim',
	    labelNo: 'Não'
	}, function(result, el, ev) {
	  if(result == 'true'){		  
		  modalBuscaEmpresa();
	  }else if(result == false){		  
		  console.log('false')
	  }
	     
	   
	});
	
}

function modalBuscaEmpresa(){
	
	setTimeout(function(){
		
		var modalCliente = FLUIGC.modal({
			title: 'Pesquisar Empresa',
			content: 		
				'<div class="panel panel-primary">' +
			//	'<div class="panel-heading">' +
			//	'<h3 class="panel-title">Eventos para o Cliente: ' + codparc + ' - ' + nomecliente + ' </h3>' +
			//	'</div>' +
				'	<div class="panel-body">' +
				'		<div class="row">' +
				'			<div class="col-md-4 col-sm-8 form-inline">'+
				'				<label for="cpfconsultaFornecedor">Informe o CNPJ </label> '+
				'				<span class="required text-danger"><strong>*</strong></span>'+
				'				<input type="text" class="form-control" id="cnpjEmpresaCliente" name="cnpjEmpresaCliente" value=""> '+
				'			</div>'+
				'			<div class="col-md-2 col-sm-4 form-group">'+
				'				<button type="button" class="btn btn-info" name="buscaEmpresaCliente" id="buscaEmpresaCliente" style="margi-top:23px;" >'+
				'		Verificar Cadastro <i class="fluigicon fluigicon-adduser icon-sm"></i></button>'+
							
				
				'			</div>'+
				'		</div><!--row-->' +
				'		<div  name="dadosEmpresa" id="dadosEmpresa">'	+	
						
				'		</div><!--dadosEmpresa-->' +
				'	</div><!--panel-body-->' +
			
				
				'</div> <!--panel-primary-->',
			id: 'modal-cliente-evento' ,
			size: 'full',
			actions: [{
				'label': 'Selecionar',
				//'bind': 'data-confdadoscli',
			}, {
				'label': 'Fechar',
				'autoClose': true,
				//'bind': 'data-fecharcli'
			}]
		},
			function (err, data) {

			});
		//validacgc("txtmCGC", $("#txtmCGC").val());

		$('.modal-body').css("max-height", window.innerHeight / 1.5 + 'px');
		$(".btn-primary", $(".modal-footer")).click(function (){
			//fnBuscaEmpresaCliente();
			
			
		});
		$("#buscaEmpresaCliente").click(function(){
			
			fnBuscaEmpresaCliente($("#cnpjEmpresaCliente").val());
			
		});
		
	}, 400)
	
	
}

function selecionaEmpresa(codRm, cnpj, inscEst, forn, razao, fantasia){
	console.log('selecionaEmpresa()->');
	$("#faturaEmpresaCliente").val('sim');	
	$("#codRmEmpresa").val(codRm);
	$("#cnpjRmEmpresa").val(cnpj);
	$("#inscEstRmEmpresa").val(inscEst);
	$("#fornRmEmpresa").val(forn);
	$("#razaoRmEmpresa").val(razao);
	$("#fantasiaRmEmpresa").val(fantasia);
	
}
function fnBuscaEmpresaCliente(cnpjEmpresaCliente){	
	var _codRM, _codInscEstadual, _nomeFantasia, _razaoSocial, _pagrec;
	console.log('fnBuscaEmpresaCliente()->'+ cnpjEmpresaCliente);	
	if(validacnpj('cnpjEmpresaCliente', cnpjEmpresaCliente ) == true){		
		if($("#checkDadosEmpresaDiv").length > 0 ){			
			$("#checkDadosEmpresaDiv").remove();
		}
		var c1rm = DatasetFactory.createConstraint("CGCCFO", $("#cnpjEmpresaCliente").val(), $("#cnpjEmpresaCliente").val(), ConstraintType.MUST);
		var constraintsRM = new Array(c1rm);
		var datasetRM = DatasetFactory.getDataset("dsClienteRM", null, constraintsRM, null);		
		var counter = datasetRM.values.length;		
		if(counter > 0){
			_codRM = datasetRM.values[0].CODCFO;
			_codInscEstadual = datasetRM.values[0].INSCRESTADUAL;
			_nomeFantasia = datasetRM.values[0].NOMEFANTASIA;
			_razaoSocial = datasetRM.values[0].NOME;
			_pagrec = datasetRM.values[0].PAGREC;
			
			$("#dadosEmpresaCliente").fadeOut();
			$(".modal-content").find(".btn-primary").click(function(){
				if($("#txtCodCfo").val() == '-1'){
					console.log('Salvar empresa -> ')
					setTimeout(function(){
						fnSalvaCliente();						
					},500);
					selecionaEmpresa(_codRM, $("#cnpjEmpresaCliente").val(), _codInscEstadual, _pagrec, _razaoSocial, _nomeFantasia);
										
				}else{
					console.log('Mostrar empresa -> ')
					selecionaEmpresa(_codRM, $("#cnpjEmpresaCliente").val(), _codInscEstadual, _pagrec, _razaoSocial, _nomeFantasia);
				}
				
				$("#dadosEmpresaCliente").fadeIn();
			});
			$("#dadosEmpresa").append(
					'<div id="checkDadosEmpresaDiv">'+
					'<div class="row" > '+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="codcfoEmpresa1">Código no RM</label>'+
					'		<input type="text" name="codcfoEmpresa1" class="form-control" id="codcfoEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="cnpjEmpresa1">CNPJ</label>'+
					'		<input type="text" name="cnpjEmpresa1"  class="form-control" id="cnpjEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="estadualEmpresa1">Inscrição Estadual</label>'+
					'		<input type="text" name="estadualEmpresa1"  class="form-control" id="estadualEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="fornecedorEmpresa1">Fornecedor</label>'+
					'		<input type="text" name="fornecedorEmpresa1" class="form-control" id="fornecedorEmpresa1" value="..." disabled> '+
					' 	</div>'+
					' </div><!--row-->'+	
					'<div class="row"> '+
					'	<div class="col-md-6 col-sm-12 form-group">'+
					'		<label for="razaoEmpresa1">Razão Social</label>'+
					'		<input type="text" name="razaoEmpresa1" class="form-control"  id="razaoEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-6 col-sm-6 form-group">'+
					'		<label for="fantasiaEmpresa1">Nome Fantasia</label>'+
					'		<input type="text" name="fantasiaEmpresa1"  class="form-control" id="fantasiaEmpresa1" value="..." disabled> '+
					' 	</div>'+
					
					' </div><!--row-->'+
					'</div> <!--checkDadosEmpresaDiv-->'
					);
			
			setTimeout(function(){
				
				$("#decisaoOperador").val(1);
				$("#cnpjEmpresaVinculada").val($("#cnpjEmpresaCliente").val());
				$("#codCfoEmpresa").val(_codRM);
				$("#razaoEmpresa").val(_razaoSocial);
				$("#fantasiaEmpresa").val(_nomeFantasia);
				
				$("#codcfoEmpresa1").val(_codRM);
				$("#estadualEmpresa1").val(_codInscEstadual);
				$("#fantasiaEmpresa1").val(_nomeFantasia);
				$("#razaoEmpresa1").val(_razaoSocial);
				if(_pagrec == 3 ){					
					$("#fornecedorEmpresa1").val('Forncedor');
					$("#fornecedorEmpresa").val('Forncedor');
				}else{
					$("#fornecedorEmpresa1").val('Cliente');						
					$("#fornecedorEmpresa").val('Cliente');					
					
				}
				$("#cnpjEmpresa1").val($("#cnpjEmpresaCliente").val());
				
			},700)
			
		}else{
			//fnRenderModal(cnpjEmpresaCliente, cnpjEmpresaCliente, 'vinculada');
			cnpjEmpresaCliente = cnpjEmpresaCliente.trim().replace(' ', '');
			cnpjEmpresaCliente = cnpjEmpresaCliente.replace(/\./g,'');
			cnpjEmpresaCliente = cnpjEmpresaCliente.replace(/\.|\-|\//g,'');
			var cpfCpnj = cnpjEmpresaCliente;

			console.log('Valor Consulta -> cpfCnpj' + cpfCpnj.length);
			var constraint = new Array( DatasetFactory.createConstraint('CgcCpf', cpfCpnj, cpfCpnj, ConstraintType.MUST));
			var _sasPF = DatasetFactory.getDataset('dsSASClientePJ',null, constraint, null);
			console.log(_sasPF.values);
            

            var codCFO = '-1';
            var cgcrm = $("#cnpjEmpresaCliente").val();
            var ierm = '';
            var codtcf ='003';
            var nomecliente = _sasPF.values[0].NomeRazaoSocial;
		    var nomefantcliente = _sasPF.values[0].NomeAbrevFantasia;
		    var cidcli = _sasPF.values[0].DescCid;
            var UFcli = "PB";
            var telefone = ""//retornar do sas
            var celular = "" //retornar do sas

			console.log('Chamou modal de cadastro CliFor');
			$("#dadosEmpresa").append(
                '<div class="row">' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtCodCfo">Código no RM</label>' +
				'<input type="text" name="txtCodCfo" id="txtCodCfo" class="form-control" readonly value="' + codCFO + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtmCGC">CPF/CNPJ</label>' +
				'<input type="text" name="txtmCGC" id="txtmCGC" class="form-control" readonly="readonly" value="' + cgcrm + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtIE">Inscrição Estadual</label>' +
				'<input type="text" name="txtIE" id="txtIE" class="form-control" value="' + ierm + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtClassificacao">Classificação</label>' +
				'<select name="txtClassificacao" class="form-control" id="txtClassificacao">' +
				'<option value="1"  >Cliente</option>' +
				'<option value="2"  >Fornecedor</option>' +
				'<option value="3"  >Ambos</option>' +
				'</select>' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtCategoria">Categoria</label>' +
				'<select name="txtCategoria" class="form-control" id="txtCategoria" >' +
				'<option value="J"  >Juridica</option>' +
				
				
				'</select>' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtTipoCliFor">Cliente/Fornecedor</label>' +
				//'<select name="txtTipoCliFor" class="form-control" id="txtTipoCliFor" dataset="dsTipoClienteFornecedor" datasetkey="CODTCF" datasetvalue="DESCRICAO"></select>' +
				'<input type="text" name="txtTipoCliFor" class="form-control" id="txtTipoCliFor" readonly value="' + codtcf + '">' +

				'</div>' +
				'</div>' + //end first row
				'<div class="row">' +
				'<div class="form-group col-md-6">' +
				'<label class="control-label" for="txtRazaoSocial">Razão Social</label>' +
				'<input type="text" name="txtRazaoSocial" id="txtRazaoSocial" class="form-control required" data-validarcampo required="required" value="' + nomecliente + '">' +
				'</div>' +
				'<div class="form-group col-md-6">' +
				'<label class="control-label" for="txtFantasia">Nome Fantasia</label>' +
				'<input type="text" name="txtFantasia" id="txtFantasia" class="form-control required" data-validarcampo required="required" value="' + nomefantcliente + '">' +
				'</div>' +
				'</div>' + //end second row
				
				
				'<div class="panel-heading panel-primary" style=" border-bottom-width: 1px;margin-bottom: 10px;">' +
				'<h5 class="panel-title">Endereço do Cliente</h5>' +
				'</div>' +
				'<div class="row">' +
				'<div class="form-group col-md-6">' +
				'<label class="control-label" for="txtlogradouro">Rua</label>' +
				'<input type="text" name="txtlogradouro" id="txtlogradouro" class="form-control" value="">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtnumero">Numero</label>' +
				'<input type="text" name="txtnumero" id="txtnumero" class="form-control" value="">' +
				'</div>' +
				'<div class="form-group col-md-4">' +
				'<label class="control-label" for="txtcomplemento">Complemento</label>' +
				'<input type="text" name="txtcomplemento" id="txtcomplemento" class="form-control" value="">' +
				'</div>' +
				'</div>' +
				'<div class="row">' +
				'<div class="form-group col-md-4">' +
				'<label class="control-label" for="txtbairro">Bairro</label>' +
				'<input type="text" name="txtbairro" id="txtbairro" class="form-control" value="">' +
				'</div>' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txtcidade">Cidade</label>' +
				'<input type="text" name="txtcidade" id="txtcidade" class="form-control" value="' + cidcli + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtUF">UF</label>' +
				'<input type="text" name="txtUF" id="txtUF" class="form-control" value="' + UFcli + '">' +
				'</div>' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txtCEP">CEP</label>' +
				'<input type="text" name="txtCEP" id="txtCEP" class="form-control" onBlur="validaCEP(this.value)" value="">' +
				'</div>' +
				'</div>' +
				'<div class="row">' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txttelefone">Telefone</label>' +
				'<input type="text" name="txttelefone" id="txttelefone" class="form-control" value="' + telefone + '">' +
				'</div>' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txtcelular">Celular</label>' +
				'<input type="text" name="txtcelular" id="txtcelular" class="form-control" value="' + celular + '">' +
				'</div>' +
				'<div class="form-group col-md-5">' +
				'<label class="control-label" for="txtemail">e-mail</label>' +
				'<input type="text" name="txtemail" id="txtemail" class="form-control" value="">' +
				'</div>' +
				'<div class="form-group col-md-4">' +
				'<label class="control-label" for="txtcontato">Contato</label>' +
				'<input type="text" name="txtcontato" id="txtcontato" class="form-control" value="">' +
				'</div>' +
				'</div>' +
				'</div>' 
			);
			FLUIGC.toast({
				title: 'Empresa',
				message: '<br>A consulta não retornou nenhum registro.',
				type: 'warning'
			});
		}
		
		
		
	
		
		//1 - validar cnpj
		
		//2 - consultar PESSOAJ no RM
		
		//3 - Retornar dados da empresa
		
		//retornar decisao do operador para -> 
		$("#decisaoOperador").val(1);
		
	}
	
	
	
	
}





/* 21/02/2020
function adicionarEmpresaCliente(){
	FLUIGC.message.confirm({
	    message: 'Deseja Vincular esse pagamento a empresa do Cliente?',
	    title: 'Vincular Empresa',
	    labelYes: 'Sim',
	    labelNo: 'Não'
	}, function(result, el, ev) {
	  if(result == 'true'){		  
		  modalBuscaEmpresa();
	  }else if(result == false){		  
		  console.log('false')
	  }
	     
	   
	});
	
}

function modalBuscaEmpresa(){
	
	setTimeout(function(){
		
		var modalCliente = FLUIGC.modal({
			title: 'Pesquisar Empresa',
			content: 		
				'<div class="panel panel-primary">' +
			//	'<div class="panel-heading">' +
			//	'<h3 class="panel-title">Eventos para o Cliente: ' + codparc + ' - ' + nomecliente + ' </h3>' +
			//	'</div>' +
				'	<div class="panel-body">' +
				'		<div class="row">' +
				'			<div class="col-md-4 col-sm-8 form-inline">'+
				'				<label for="cpfconsultaFornecedor">Informe o CNPJ </label> '+
				'				<span class="required text-danger"><strong>*</strong></span>'+
				'				<input type="text" class="form-control" id="cnpjEmpresaCliente" name="cnpjEmpresaCliente" value=""> '+
				'			</div>'+
				'			<div class="col-md-2 col-sm-4 form-group">'+
				'				<button type="button" class="btn btn-info" name="buscaEmpresaCliente" id="buscaEmpresaCliente" style="margi-top:23px;" >'+
				'		Verificar Cadastro <i class="fluigicon fluigicon-adduser icon-sm"></i></button>'+
							
				
				'			</div>'+
				'		</div><!--row-->' +
				'		<div  name="dadosEmpresa" id="dadosEmpresa">'	+	
						
				'		</div><!--dadosEmpresa-->' +
				'	</div><!--panel-body-->' +
			
				
				'</div> <!--panel-primary-->',
			id: 'modal-cliente-evento' ,
			size: 'full',
			actions: [{
				'label': 'Selecionar',
				//'bind': 'data-confdadoscli',
			}, {
				'label': 'Fechar',
				'autoClose': true,
				//'bind': 'data-fecharcli'
			}]
		},
			function (err, data) {

			});
		//validacgc("txtmCGC", $("#txtmCGC").val());

		$('.modal-body').css("max-height", window.innerHeight / 1.5 + 'px');
		$(".btn-primary", $(".modal-footer")).click(function (){
			//fnBuscaEmpresaCliente();
			
		});
		$("#buscaEmpresaCliente").click(function(){
			
			fnBuscaEmpresaCliente($("#cnpjEmpresaCliente").val());
			
		});
		
	}, 400)
	
	
}

function selecionaEmpresa(codRm, cnpj, inscEst, forn, razao, fantasia){
	console.log('selecionaEmpresa()->');
	$("#faturaEmpresaCliente").val('sim');	
	$("#codRmEmpresa").val(codRm);
	$("#cnpjRmEmpresa").val(cnpj);
	$("#inscEstRmEmpresa").val(inscEst);
	$("#fornRmEmpresa").val(forn);
	$("#razaoRmEmpresa").val(razao);
	$("#fantasiaRmEmpresa").val(fantasia);
	
}
function fnBuscaEmpresaCliente(cnpjEmpresaCliente){	
	var _codRM, _codInscEstadual, _nomeFantasia, _razaoSocial, _pagrec;
	console.log('fnBuscaEmpresaCliente()->'+ cnpjEmpresaCliente);	
	if(validacnpj('cnpjEmpresaCliente', cnpjEmpresaCliente ) == true){		
		if($("#checkDadosEmpresaDiv").length > 0 ){			
			$("#checkDadosEmpresaDiv").remove();
		}
		var c1rm = DatasetFactory.createConstraint("CGCCFO", $("#cnpjEmpresaCliente").val(), $("#cnpjEmpresaCliente").val(), ConstraintType.MUST);
		var constraintsRM = new Array(c1rm);
		var datasetRM = DatasetFactory.getDataset("dsClienteRM", null, constraintsRM, null);		
		var counter = datasetRM.values.length;		
		if(counter > 0){
			_codRM = datasetRM.values[0].CODCFO;
			_codInscEstadual = datasetRM.values[0].INSCRESTADUAL;
			_nomeFantasia = datasetRM.values[0].NOMEFANTASIA;
			_razaoSocial = datasetRM.values[0].NOME;
			_pagrec = datasetRM.values[0].PAGREC;
			
			$(".modal-content").find(".btn-primary").click(function(){
				selecionaEmpresa(_codRM, $("#cnpjEmpresaCliente").val(), _codInscEstadual, _pagrec, _razaoSocial, _nomeFantasia)
				$("#dadosEmpresaCliente").fadeIn();
			});
			$("#dadosEmpresa").append(
					'<div id="checkDadosEmpresaDiv">'+
					'<div class="row" > '+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="codcfoEmpresa1">Código no RM</label>'+
					'		<input type="text" name="codcfoEmpresa1" class="form-control" id="codcfoEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="cnpjEmpresa1">CNPJ</label>'+
					'		<input type="text" name="cnpjEmpresa1"  class="form-control" id="cnpjEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="estadualEmpresa1">Inscrição Estadual</label>'+
					'		<input type="text" name="estadualEmpresa1"  class="form-control" id="estadualEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-3 col-sm-6 form-group">'+
					'		<label for="fornecedorEmpresa1">Fornecedor</label>'+
					'		<input type="text" name="fornecedorEmpresa1" class="form-control" id="fornecedorEmpresa1" value="..." disabled> '+
					' 	</div>'+
					' </div><!--row-->'+	
					'<div class="row"> '+
					'	<div class="col-md-6 col-sm-12 form-group">'+
					'		<label for="razaoEmpresa1">Razão Social</label>'+
					'		<input type="text" name="razaoEmpresa1" class="form-control"  id="razaoEmpresa1" value="..." disabled> '+
					' 	</div>'+
					'	<div class="col-md-6 col-sm-6 form-group">'+
					'		<label for="fantasiaEmpresa1">Nome Fantasia</label>'+
					'		<input type="text" name="fantasiaEmpresa1"  class="form-control" id="fantasiaEmpresa1" value="..." disabled> '+
					' 	</div>'+
					
					' </div><!--row-->'+
					'</div> <!--checkDadosEmpresaDiv-->'
					);
			
			setTimeout(function(){
				
				$("#decisaoOperador").val(1);
				$("#cnpjEmpresaVinculada").val($("#cnpjEmpresaCliente").val());
				$("#codCfoEmpresa").val(_codRM);
				$("#razaoEmpresa").val(_razaoSocial);
				$("#fantasiaEmpresa").val(_nomeFantasia);
				
				$("#codcfoEmpresa1").val(_codRM);
				$("#estadualEmpresa1").val(_codInscEstadual);
				$("#fantasiaEmpresa1").val(_nomeFantasia);
				$("#razaoEmpresa1").val(_razaoSocial);
				if(_pagrec == 3 ){					
					$("#fornecedorEmpresa1").val('Forncedor');
					$("#fornecedorEmpresa").val('Forncedor');
				}else{
					$("#fornecedorEmpresa1").val('Cliente');						
					$("#fornecedorEmpresa").val('Cliente');					
					
				}
				$("#cnpjEmpresa1").val($("#cnpjEmpresaCliente").val());
				
			},700)
			
		}else{
			fnRenderModal(cnpjEmpresaCliente, cnpjEmpresaCliente, 'vinculada');
			console.log('Chamou modal de cadastro CliFor');
			
			FLUIGC.toast({
				title: 'Empresa',
				message: '<br>A consulta não retornou nenhum registro.',
				type: 'warning'
			});
		}
		
		
		
	
		
		//1 - validar cnpj
		
		//2 - consultar PESSOAJ no RM
		
		//3 - Retornar dados da empresa
		
		//retornar decisao do operador para -> 
		$("#decisaoOperador").val(1);
		
	}
	
	
	
	
}

*/


