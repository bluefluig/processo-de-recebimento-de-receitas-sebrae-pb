
var codRecibo = '';
var enderecoCliente, cidadeCliente, estadoCliente, bairroCliente,  nomeRepresentanteCliente,  cpfRepresentanteCliente, rgRepresentanteCliente;
var enderecoRepresentanteCliente, bairroRepresentanteCliente, cidadeRepresentanteCliente;
var contador = 0;
var data1 = new Date();
var titulo = 'SERVIÇO DE APOIO AS MICRO E PEQUENAS EMPRESAS DA PARAÍBA';
//var subtitulo = '		SEBRAE/PB';
var contatoSebrae = 'CONTATO (83) 2108-1100/ 0800.570.0800'	
var cpnjSebrae = 'CNPJ 09.139.551/0001-05';
var enderecoSebrae = 'AV. MARANHÃO 983 BAIRRO DOS ESTADOS  CEP 58030-261 JOÃO PESSOA-PB';
var diaHoje  = ""; //retornaDiaHoje(); //FLUIGC.calendar.formatDate(new Date(), "DD/MM/YYYY") ; //ok
var mesExtenso = mesPorExtenso(data1.getMonth());
var ano  = data1.getFullYear();
var tipoEvento ="";
var tituloEvento ="";	
var codCurso ="";
var localEvento ="";
var dataEvento ="";
var dataEventoFim ="";
var horarioEvento ="";
var cargaHorariaEvento ="";
var nomeCliente  ="";
var razaoCliente ="";
var cpfCliente ="";
var valorPagamento ="";
var valorBruto = "";
var quantidadeEventos ="01"; 
var formaPagamento ="";
var condPagamento   ="";
var valorDesconto  ="";
var identificador ="";
var tipo; // = $("#formaPagamento").val();
/*Retorna a data do dia atual */
var _codEvento = $("#eventoCodigo1").val()
function retornaDiaHoje(){
	var datax = new Date();
	var dia = datax.getDate();
	console.log('datax-> ' + datax)
	console.log('dia-> ' + dia)
	console.log('dia.length-> ' + dia.toString().length)
	if(dia.toString().length == 2 ){
		var diah = dia ;
		return diah;
	}else if(dia.toString().length == 1 ){
		var diah = dia ;
		return '0'+ diah;
	}
	
}
function textPagamento(posicao){	
	var idSelect = $(".formaPagamento:last").attr("id");
	$("#"+idSelect).find("option:selected").text();
}


/*Controla a geração de código para os recibos de pagamentos. */
/**Consulta via dataset o código do ultimo recibo gerado e incrementa +1
 *  @params {null} 
 *  @returns {varchar} 
 *  updated 17/08/2020 - Jean Varlet
 */
function codigoRecibo(){
	let codigo, size;
    let max = 4;
    let consult = DatasetFactory.getDataset('ds_recebimento_receitas_v1', null, null, null);
    if(!jQuery.isEmptyObject(consult)){
        if(consult.values.length){
            for(var i = 0; i < consult.values.length; i++){
                if(consult.values[i].numControleRecibo != '' && consult.values[i].numControleRecibo != null ){
                    codigo = parseInt(consult.values[i].numControleRecibo.split('-')[1]) + 1;
                    
                }
            }//end for
            size = max - codigo.length;
            var zeros = "0";
            return moment().format("YYYY").toString() + "-"+ zeros.repeat(size) + codigo;
        }
    }else{
        toast('Erro!', 'Não foi possível gerar o código deste recibo!', 'danger');
    }
   
       
}

/*Jean Varlet - 10/02/2020 */
function pagamentos(){
	let counterPagamentos = $(".valorGenerico").length - 1 ;
	if($("#pagamentodividido").val() === 'nao'){
		var idFormaPgt = $(".formaPagamento:last").attr("id");
	    formaPagamento = $("#"+idFormaPgt).find("option:selected").text();
		var idCondPagamento = $(".condicaoPagamentoGenerico:last").attr("id");
		condPagamento = $("#"+idCondPagamento).find("option:selected").text();
		identificador = $(".identificadorGenericoPagamento:last").val();
		valorPagamento = $(".valorGenerico:last").val();		 
	}else{
		if(counterPagamentos > 0){
			for(var i = 0; i < counterPagamentos; i++){
				
				var obj = new Array({
					data: $(".dataPagamentoGenerico")[x].value ,
					
				},
				{
					identificador: $(".identificadorGenericoPagamento")[x].value,
				},
				{
					valor: $(".valorGenerico")[x].value,
				},
				{
					formaPagamento: $("#formaPagamento___"+x).find("option:selected").text(),
				},
				{
					condicao:  $("#condicaoPagamentoGenerico___"+x).find("option:selected").text(),
				})	
			}
			
		}
		
	}
	
}


 
 function engineStart(){
	 console.log(' engineStart()-> engine Started');
	 assinaturaRecibo($("#tbUsuario").val());
	 formaPagamento = $("#formaPagamento").val(); //avaliar 
	 tipo = $("#formaPagamento").val();// avaliar 
	 diaHoje  = retornaDiaHoje();
	//Soluções de Educação SAS		
	 if ($("#existeRateio").val() == 'nao' && $("#existeDesconto").val() == 'nao' ){
		 valorPagamento = $("#fieldValorBruto").val();
		 valorBruto = $("#fieldValorBruto").val();
		 valorDesconto =  'R$ 0,00';
	 }else if($("#existeRateio").val() == 'sim' &&   $("#existeDesconto").val() == 'nao'  ){
		 valorPagamento = $("#valorCliente").val();
		 valorBruto = $("#fieldValorBruto").val();
		 valorDesconto =  'R$ 0,00';
	 }else if($("#existeRateio").val() == 'nao' &&   $("#existeDesconto").val() == 'sim' ){
		 valorPagamento = $("#totalDesconto").val();
		 valorDesconto =  $("#subTotalDesconto").val();
	 	 valorBruto = $("#fieldValorBruto").val();
	 }
	 if($("#pagamentodividido").val() != 'nao'){
	 	 
	 }else{
		 var idSelectForma = $(".formaPagamento:last").attr("id");
		 var idSelectCondicao = $(".condicaoPagamentoGenerico:last").attr("id");
		 formaPagamento = $("#"+idSelectForma).find("option:selected").text();
		 condPagamento = $("#"+idSelectCondicao).find("option:selected").text();
		 identificador = $(".identificadorGenericoPagamento:last").val();
		 
	 }
	 if($("#tipoSerInstrutoria").is(':checked') == true ){			 
		 quantidadeEventos = '01'; 
		 tipoEvento = $("#eventoTipo1").val();
		 tituloEvento = $("#eventoNome1").val();
		 codCurso = $("#eventoCodigo1").val();
		 if($("#localEventoSaSselecionado").val() == "" || $("#localEventoSaSselecionado").val() == 'null'){
			localEvento = 'Local não informado.'
		}else{
			localEvento = $("#localEventoSaSselecionado").val();	
		}
		 
		 dataEvento = $("#eventoDataInicio1").val();
		 dataEventoFim = $("#eventoDataFim1").val();
		 horarioEvento = $("#horarioIni").val() +' às '+ $("#horarioFim").val(); // conferir os ids nessa etapa
		 cargaHorariaEvento = $("#eventoCarga1").val();
		
		 nomeCliente = $("#nomeCliente1").val();
		 razaoCliente = $("#razaoCliente1").val();
		 cpfCliente = $("#cpfCnpj").val();
	
		if($("#cnpjRmEmpresa").val() != ""){
			nomeCliente += ' vinculado a '+ $("#fantasiaRmEmpresa").val();
			razaoCliente += ' vinculado a razão: '+ $("#razaoRmEmpresa").val();
			cpfCliente +=  ' vinculado ao CNPJ: '+ $("#cnpjRmEmpresa").val();
		}			 
	 }//Consultoria/Instrutoria
	 else  if($("#tipoSerConsultoria").is(':checked') == true ){
		 if($("#selConsultInstConsultoria").is(':checked') == true ){				 
			 tipoEvento = 'Consultoria';						 
		 }else if($("#selConsultInstInstrutoria").is(':checked') == true){				 
			 tipoEvento = 'Instrutoria';				 
		 }
		 tipo = $("#formaPagamento").val();
		 quantidadeEventos = '01'; 
		// tituloEvento = $("#tituloConsultoria").val();
		 if($("#descricaoEven").val() != ''){				 
			 tituloEvento = $("#descricaoEven").val();
		 }else{				 
			 tituloEvento = "Evento Sem Descrição.";
		 }			
		 codCurso = $("#codConsultoria").val();
		 localEvento = $("#complementoEnd").val() + ' - ' + $("#cidadeEvento").val() ;
		 dataEvento = $("#datainicial").val();
		 dataEventoFim = $("#datafinal").val();
		 horarioEvento = $("#horarioIni").val() +' às '+ $("#horarioFim").val();
		 cargaHorariaEvento = $("#cargaHorariaTotal").val();
		 diaHoje  = retornaDiaHoje();
		 if($("#razaoForn").val() !== $("#responsavelForn").val()){				 
		 	nomeCliente = $("#responsavelForn").val();				 
	 	}else{				 
			 nomeCliente = $("#razaoForn").val();
		}
			razaoCliente = $("#razaoForn").val();
	 	if($("#cnpjForn").val().length == 14){				 
		 	cpfCliente = $("#cnpjForn").val();				 
	 	}else if($("#cnpjForn").val().length == 18){				 
			 cpfCliente = $("#cnpjForn").val();				 
	 	}
		if($("#cnpjRmEmpresa").val() != ""){
			nomeCliente += ' Sob a razão social: '+ $("#razaoRmEmpresa").val();
			
			cpfCliente += ' sob o CNPJ: '+ $("#cnpjRmEmpresa").val(); 
		}
		 
	 }//Outros Servicos
	 else  if($("#tipoSerOutros").is(':checked') == true ){

		//
		valorPagamento = $("#fieldValorBruto").val();
		 valorBruto = $("#somaSubTotalOutros").val();
		 if($("#perDesconto").val() == 0 || $("#perDesconto").val() == '0' || $("#perDesconto").val() == ''){
			valorDesconto =  'R$ 0,00';
		 }else{
			valorDesconto = $("#subTotalDesconto").val();	
		 }
		 //rateio $("#valorSebrae").val()
		 _codEvento = '0';
		//
		 
		 tipo = $(".formaPagamento:last").val();
		 quantidadeEventos = '01'; 
		 tituloEvento = $("#tituloOutros").val();
		 tipoEvento = $("#servicosSeleciona").val();
		 
		 codCurso = $("#servicosSeleciona").find(":selected").text()
		 localEvento = $("#cidadeOutros").val(); // Cidade ok 
		 dataEvento = $("#dataInicioOutros").val();
		 dataEventoFim = $("#dataFimOutros").val();
		 horarioEvento = $("#horarioIniOutros").val() +' às '+ $("#horarioFimOutros").val();
		 cargaHorariaEvento = calculaCargaHorariaOutros($("#dataInicioOutros").val(),$("#dataFimOutros").val(), $("#horarioIniOutros").val(),$("#horarioFimOutros").val() );
		 nomeCliente = $("#nomeClienteOutros1").val();
		 cpfCliente = $("#cpfconsultaOutrosF").val();
		 valorPagamento = $("#valorCliente").val();		
		 diaHoje  = retornaDiaHoje();	
		 }
		 
	 }
var assinaturas = [];	 
function assinaturaRecibo(logado){
	let nome;
	let cpf;
	if(logado == '5e6a242bad3f11e9a5b70a5864600c69'){
	nome = 'Fabiana Nunes Soares da Silva';
	cpf = 'CPF: 077.184.554-59';	
	assinaturas.push([nome, cpf]);
	}else if(logado == '606557e5ad3f11e9819e0a5864612d26'){
		nome = 'Giordanya Lays de Almeida Lira';
		cpf = 'CPF: 086.531.174-90';
		assinaturas.push([nome, cpf]);
	}else if(logado == '70bf9accad4311e9a5b70a5864600c69'){
		nome = 'Juliana Lobato Picanço';
		cpf = 'CPF: 945.872.302-78';
		assinaturas.push([nome, cpf]);
	}else if(logado == '62687b74ad3f11e99ca20a586460437b'){
		nome = 'Luís Sandro Lima Ferreira Júnior ';
		cpf = 'CPF: 013.419.724-02';
		assinaturas.push([nome, cpf]);
	}else{
        nome = parent.WCMAPI.getUser();
        cpf = '000.000.000-00';
        assinaturas.push([nome, cpf]);

	}
		
}
	 	
function valorPago(){		
	var eventoGratuito = 'Evento Gratuito';
	var codMonetario = 'R$';
	var retorno;
	
	if($("#valorBoletoPagamento").val() != null &&  $("#valorBoletoPagamento").val() != undefined){
		retorno = codMonetario + '' + $("#valorBoletoPagamento").val();
		return retorno; 
	}
	else if($("#valorDepositoPagamento").val() != null && $("#valorDepositoPagamento").val() != undefined){
		
		retorno = codMonetario + '' + $("#valorDepositoPagamento").val();
		return retorno; 
	}
	else if($("#valorMaquinetaPagamento").val() != null && $("#valorMaquinetaPagamento").val() != undefined){
		
		retorno = codMonetario + '' + $("#valorMaquinetaPagamento").val();
		return retorno; 
	}	
	else{		
		return  eventoGratuito;		
	}
}
function gerarRecibo(){	
	engineStart();
	//if(contador < 1 ){		
		 contador = contador +1;	
var docDefinition = { 	
		 content: [	{			
		        columns: [
		            {
		               image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAABJCAMAAAAHdUQUAAAAhFBMVEX///8vc7IqcbFPhLYAY6oiba79//vo8fRQhbrh6fH1+PdAfbc5drb///1GgrYAX6v4+/4tc6xvmsVUir3N2ukaa6/w9Pa80OGOrMyguNR/osi1x93Z5e4NY6bF0+GJqs1fjLhslMODp8Shv9KRsMtzncKtxNVAea4ATqQAU6Wqw9xdksM/oDCXAAAGoElEQVRogeVa63abOBAGGYVbRAxGYBuI7Taxd533f7+VZkYgIMmp2kbtOTu/jBCab+4jyUHws9SHuQOF959m5Eh3HjpQlPrCdXTCxXzBekiYC6yzL1xx6IRr7wtXLV3MKJ994Tpw5kAy9oXr4oKLF1tfuDI3Er5wBYFY0npkfOEP1d9JQjy4kDd9ierRgcKNL1yZU1bllS9c6eCUVTtfuDq3bF/7wnVyaSZY2fjCtXNRF0t8wRKRE66TL1z1P08O9G/nKX+JB9cPfo3db1nld5Our+bXHwViESBp6u6w3x+6+vOo1lO3Fs1ebrcfvppP+FFgQXsppSIu5cCqnmwZl8WCct2oz7aOxaa6Gkma2Z6yKDanfu4VdWm2k9bw9pwv2bzSq76QbCp5bMBdqGhXTWl0VYzuvDSkP+KSd8i+HkqL1EJc7mfACoav2Os0JuIVF4nbFNFFgEmNEDLahq7TOu/VcLUs2mzACnhYl6fI2nGIgyzpg5sFtl0VW47LtZAmmVRW4MiSH+EF7RFLTiR5mSn5Ea16VHY3fDLNt8JHqaYa6MzqJdJJnsjysD1JwyRxYVEMekQLylMsRFzgt1A6MmDDws3OUFLp+bAOOx8UHUkBUitY0MNJUZUT5GICUE3qH/pJi2c01mZiU1jWki/o/3LC1UvEG8wJp8gWPRPlGDSuGPWOXaB4Ral243fP2l78DuDldRxuQusri7Y5A62gS7VPkaLhTf++IuBl9/SCw7glFDkCa0bAnOToB8sjxpl5UM6HzbxVEU1RShZDFktrIACJEsvlUcwGkOQZMEMn0B2pCPbcUqQKTvtJoDgqDBJgl49xepXvSm8sxy6rPfENV8gW+gUkrMJ17xI8ttdPZ3RpWofcg2qpACOznRAYzdG43vF96UkuJfNQvNRTRVYxEJaLgAKNotfxgx7IOoQFSqHuh9M0YBe1Ri+g5ag2eIfULFlgtFiI8IvGRG9Z8oGfr7F5WXPANcZQyME3qYd+BCm5TqsbDK4ecSW98oMWuEs+WqcDp9fpFJOcOUkRzVCGM2I5lY+DnddUEnkFo4h1luSQho9svsqRundBgNmgSAFm/HYyxhGZTv8hT8dpEP76Tb3sMc1ZmVBpeJbXOX8GTV6WaV3nboF+a+Miz6YwGV+8fR9dRijjlSbmWm5zX0tvEGsLnMJBWigk+A5lAJPsOXvSCsYAZByqPNbAM5p+KTcLX0zM9Tr/szC2zG0cCsOAjVy4XbkC0R/e5GAEhlcpypVXI0GxxQM5+a1TdLjBHPS7Bt2B6/xHOhhasggUNP4dnjKY98jJj4nlxKVa5oVtfaKyJnUbQml9ecB9x2F09QwrKDwhYHZsmia+YriY7InfMOKLDxzdEmsEf11wCR6gTcvw2ChIUVDZiTFL9osP0OswZswk0MsBdYcRiOOQY0QQm+qOhA8cBW4/yKoV2PTJuCjGLFTV81RgLMqgsJc78hz0DpD9bfrUgGSgL/G6apcmFWEzMSylDxJAMsSQzESDOtY+SeV0Z5+2ab1bNVClXnLadGomsDqc2DSNEvfU9sFMVgT6wA6LGt/OuQRBbiuVOhGIVCyn7Nt00gWHkc9WVhRpgmrRfp9S94PLYv/DoUfBuL6NXUxOMMESyGU3nZVlKNgFxeTf6r7vdtQH6leY/tjj1K8/vowOFZ61A2/IV3R1MYD3oHeBXqiKlcqOWJCmKKOCDDXVJA1rW3CBSVdK0rDlgMXKqNdHonTPYtyUQRUU1MSV2iolWS7SeE33g5VSkHmUGWKoM9K6WEBLwFRxN73qmD1Nw5PPnZLxEnyQVrbf6KZ/fSDHImSJ82UK+qKcrJMMNhnMOpFuMBuBkde3TJzKR7OzMr1S22kLIdAMy01KWGoviuYbFymLGuejX+cZxg8+qX1VBx/I71aoNTke6utO1g4HOusfq1d7LuWgOKh0fz4YN+hvmwUlR2XdNp+ek+Ry6kyA93miB8kIDU65tQJGN2f7ROOhwrfnLGhWXDY70+4pAbO+btu27pvPDyl++CB+fZLw7ofvr/fXnEL8DnIUxpfsWRq7kKdrIdXCwi7uB2mIVlXvq+jdCvwRlcud1FeRCNgyq35G/i61G6dzaNzK+aBnp2sO7u2aY+/0nwnu7ZojcbrUzn3BCm6JC3m75vhr6aOL9Q+u2//fpHaXbuQLWMZdSK620F9FPXepQtbJ7heT26W2v2bC7R9y3Fs8Fi6wrHP7L6bYBdZsI/u1VMvVXu8T8vcPuXvuUhzzX/uH3H+OaoCzwPI04wAAAABJRU5ErkJggg==',
		               
		               width: 100,
		               height: 50,
		               alignment: 'left'
		            },
                    { margin:[5,5,0,0], 
		            	width: 'auto',
		            	heigth:'auto',
						text:[							
		            	
                            {text: titulo + '\n', fontSize:13, bold:true   },
							 {text:	 cpnjSebrae + '\n', fontSize:11, bold:false, color:'black' },
		            		{text:  enderecoSebrae + '\n', fontSize:11, bold:false, color:'black' },
                        	{text: contatoSebrae + '\n', fontSize:11, bold:false, color:'black'  }                       
		           			
						
		           ]}		            
		          
		        ]		   
		
		 },
		    	
		    	{
		    		text:[
		    			{text: '_____________________________________________________________________________________\n', color:'gray'},
                       	{text: '				\n', fontSize:11}
	
                        
		    	
		    		]
		    		
		    	},
                {alignment: 'justify',
		    		columns:[
                    
		    			{text: 'RECIBO Nº '+  '\n', fontSize:12, bold:true},
		    			{text:  codigoRecibo() + '\n', fontSize:12, bold:true},
		    			]
		    	},//fecha colunas 1	
		    	{alignment: 'justify',
		    			columns:[
		    				{text: 'RECEBI DE' + '\n', fontSize:10},
		    				{text: nomeCliente  + ' CPF ' + cpfCliente  + '\n', fontSize:10, bold:false},
		    			]	
		    				
		    	},
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'A IMPORTÂNCIA DE	\n', fontSize:10},
	    				{text: valorPagamento    + ' ( '+floatReais(valorPagamento).toString().extenso(true)+' )\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'REFERENTE A	\n' ,  fontSize:10},
	    				//{text: quantidadeEventos    + ' INSCRIÇÃO NO(A) ' + tituloEvento + '  REALIZADA NO(A)  ' + localEvento + '\n',fontSize:10, bold:false},
	    				{text:  tituloEvento + '  REALIZADA NO(A)  ' + localEvento + '\n',fontSize:10, bold:false},
	    			]	
	    				
		    	},
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'DATA DO EVENTO/SERVIÇO \n', fontSize:10},
	    				{text: dataEvento + ' A ' + dataEventoFim + '\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},	
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'COD EVENTO/ SERVIÇO \n',fontSize:10},
	    				{text: codCurso + '\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},	
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'Nº INSCRIÇÃO/ Nº CONTRATO \n', fontSize:10},
	    				{text: _codEvento  + '\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'Produto/Serviços \n', fontSize:10, bold:false},
	    				{text: tipoEvento  +'\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},
									
		    	{
		    		text:[
						{text: '				\n', fontSize:11},
						{text: '_____________________________________________________________________________________\n', color:'gray'},
                       	{text: '				\n', fontSize:11}		    			
		    	
		    		]
		    		
		    	},// linha	
		    	{
		    		stack:[{
		    			text:'PAGAMENTOS REALIZADOS',
		    			fontSize: 12,
		    			alignment:'left',
		    			margin:[0,10,0,0],
		    			bold:true
		    		},]
		    		
		    		
		    	},
		    	/*{alignment: 'justify',
	    			columns:[
	    				{text: 'FORMA DE PAGAMENTO '+  '\n', fontSize:12, bold:true},
	    				{text:  formaPagamento + '\n', fontSize:12, bold:true},
	    			]	
	    				
		    	},	
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'CONDIÇÕES DE PAGAMENTO' + '\n', fontSize:10},
	    				{text:  condPagamento +  '\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},*/
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'VALOR BRUTO	\n', fontSize:10},
	    				//{text: valorPagamento  +'\n', fontSize:10, bold:false},
	    				{text: valorBruto  +'\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},	
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'VALOR DO DESCONTO	\n' ,  fontSize:10},
	    				{text: valorDesconto + '\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'VALOR LIQUIDO \n', fontSize:10},
	    				{text: ''+ valorPagamento  +'\n',fontSize:10, bold:false},
	    			]	
	    				
		    	},
		    	/*
		    	{alignment: 'justify',
	    			columns:[
	    				{text: 'IDENTIFICADOR \n',fontSize:10},
	    				{text: $("#movimentoGeradoRM").val() +  '\n', fontSize:10, bold:false},
	    			]	
	    				
		    	},	*/		
		    
		    	{
		    		text:[
						 {text: '\n'},
		    			{text: 'João Pessoa, ' + diaHoje + ' de ' + mesExtenso + ' de ' + ano +'\n',  align:'right', fontSize:12, bold:true},
                        {text: '		\n', fontSize:11},
		    			{text: '		\n', fontSize:11},
		    			{text: 'Recibo Emitido por:  ' + assinaturas[0][0] + ' - CPF: ' + assinaturas[0][1] + ' .'},
						//{text: '______________________________\n', fontSize:11, align:'center'},
						//{text: 'ASSINATURA AUTENTICADA \n', fontSize:11, align:'center'}
		    			
		    			
		    	
		    		]
		    		
		    	}
		    	
		    ]
	};//docDefinition end -
	/*Jean Varlet 01/03/2020 - Loop de pagamentos -- */
	
	var obj2 = new Array();
	var pgtCounter = $(".valorGenerico").length - 1; //Count of payments entries --- 
	if($("#pagamentodividido").val() == 'sim' && pgtCounter > 0 ){
		
		for(var i = 0; i < pgtCounter; i++){
			var posicao = i + 1;
			 var obj = new Array(
					{
						text: 'FORMA DE PAGAMENTO: ' + $("#formaPagamento___"+posicao).find("option:selected").text(),
						fontSize: 10,
						alignment: 'left',
						margin: [ 0, 3, 0, 0],
					},
					{
						text: 'CONDIÇÕES DE PAGAMENTO: ' + $("#condicaoPagamentoGenerico___"+posicao).find("option:selected").text(),
						fontSize: 10,
						alignment: 'left',
						margin: [ 0, 3, 0, 0],
					},
					{
						text: 'VALOR PAGO: ' + $("#valorGenericoPagamento___"+posicao).val(),
						fontSize: 10,
						alignment: 'left',
						margin: [ 0, 3, 0, 0],
					},
					{
						text: 'IDENTIFICADOR: ' + $("#identificadorGenericoPagamento___"+posicao).val(),
						fontSize: 10,
						alignment: 'left',
						margin: [ 0, 3, 0, 0],
					},
					{
						text: '__________________________________________________________\n',
						color:'gray',
						alignment: 'left',
						margin: [ 0, 3, 0, 0],
					}
			 );
			 obj2 = obj2.concat(obj);
			
		}
		
	}
	docDefinition.content[11].stack = docDefinition.content[11].stack.concat(obj2);
	
	$("#bntlancMovimento").prop("disabled",false );
	//const pdfd = pdfMake.createPdf(docDefinition).download('Recibo-Evento-'+tituloEvento+'.pdf');
	const pdfd = pdfMake.createPdf(docDefinition).download('Recibo-'+codRecibo+'.pdf');
	$("#triggerReciboUpload")[0].files = pdfd;
	//fnCarregaDocAnexo($("#triggerReciboUpload"), 'recibo');
		//}
}
function data(){
	var dataAtual = new Date();
	var dia = dataAtual.getUTCDate();
	var mes = mesPorExtenso(dataAtual.getMonth());
	var ano = dataAtual.getFullYear()	
	//return dia+' de de '+ano+'.';
	return dia + ' de ' + mes + ' de ' + ano + '.';
}
function mesPorExtenso(mes){
	var arrayMes = new Array(12);	
	arrayMes[0] = "Janeiro";
	arrayMes[1] = "Fevereiro";
	arrayMes[2] = "Março";
	arrayMes[3] = "Abril";
	arrayMes[4] = "Maio";
	arrayMes[5] = "Junho";
	arrayMes[6] = "Julho";
	arrayMes[7] = "Agosto";
	arrayMes[8] = "Setembro";
	arrayMes[9] = "Outubro";
	arrayMes[10] = "Novembro";
	arrayMes[11] = "Dezembro";	
	return arrayMes[mes];
}
