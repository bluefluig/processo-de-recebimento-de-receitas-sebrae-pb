

//var urlBase = 'http://fluighomolog.sebraepb.com.br:8080'
var urlBase = parent.WCMAPI.getServerURL();
var tipoPagamento = $("#formaPagamento").val();
var pastaSelecionada = '';
var codsasCliente = $("#codSAS").val();
var pastaDestino = '';
var tituloPasta ="";

//Função para varrer diretório do tipo de pagamento selecionado em busca de uma pasta com o mesmo codSAS que o cliente da solicitação.
//Caso encontre, retorna o id da pasta para a variável -> pastaSelecionada. 
//Caso não encontre, cria uma nova pasta, repete a varredura e retorna o id para -> pastaSelecionada <---

function linkDeposito(link){
	
	$(".linkDeposito").append('<div class="col-md-2"><a href="'+link+'">Conferir Arquivo. </a></div>');
}
function retornaNaturezaOper(){
	if($("#tipoSerInstrutoria").is(':checked') == true ){
		tituloPasta = $("#codSAS").val();
		console.log('retornaNaturezaOper()-> '+tituloPasta)
		return tituloPasta;
	}else if($("#tipoSerConsultoria").is(':checked')== true){
		tituloPasta =	$("#codConsultoria").val();
		console.log('retornaNaturezaOper()-> '+tituloPasta)
		return tituloPasta;
  	}else if($("#tipoSerOutros").is(':checked')== true){
		tituloPasta = $("#codServicoOutros").val();
		console.log('retornaNaturezaOper()-> '+tituloPasta)
		return tituloPasta;
	}
}
function fnChecarPastas(tipo){
	var contadorp = 0 ;	
	if(urlBase.indexOf('fluighomolog.sebraepb')){
		if(tipo == '1' || tipo == '2'){					
			pastaDestino = "15880";	// Maquinetas ok 		 
		}else if(tipo == '3'){		
			pastaDestino = "15878";	//Depósitos  ok	
		}else if(tipo == '4'){		
			pastaDestino = "15875";	 // Boletos ok 	
		}else if(tipo == '5'){
			
			pastaDestino = "15881"; //Recibo de Pagamento ok
		}else if(tipo == '6'){
			//Contrato 
			pastaDestino = '15876'; // Contratos ok 
		}
	}
	if(urlBase.indexOf('fluig.sebraepb')){
		if(tipo == '1' || tipo == '2'){					
			pastaDestino = "15880";			 
		}else if(tipo == '3'){		
			pastaDestino = "15878";		
		}else if(tipo == '4'){		
			pastaDestino = "15875";		
		}else if(tipo == '5'){
			//Recibo de Pagamento
			pastaDestino = "15881";
		}else if(tipo == '6'){
		//Contrato 
			pastaDestino = '15876';
		}
	}
			
	
		$.ajax({			
			url: urlBase+'/api/public/ecm/document/listDocumentWithChildren/'+ pastaDestino,
			crossDomain: true,
			type : "GET",
			async : false,
			//contentType: "application/json",									 					    		
			dataType : "json", 
			success : function(data){
				//console.log(data);
				
				var contadorx = data.content[0].children.length;
				console.log('contadorx -> ' + contadorx);
				var counter = 0;
              if(contadorx > 0){
                  for(var x = 0; x < contadorx; x++ ){					
                  	//counter -> força a não repetição do loop após encontrar a pasta.
                	  if(tituloPasta != "" && tituloPasta != null && tituloPasta != undefined){
                		 // if((data.content[0].children[x]["description"] == $("#codSAS").val()) && counter == 0 ){
                		   if((data.content[0].children[x]["description"] == tituloPasta) && counter == 0 ){
 	                    	 
	                          pastaSelecionada = data.content[0].children[x]["id"];
	                          console.log('pastaSelecionada -> '+ pastaSelecionada);
								counter = counter +1;
	                          //return pastaSelecionada;                           
	                       //compara counter == 0 e se i é == ao valor máximo do indexador do loop !!    			
	                      }else if(counter == 0 && i == contadorx){
	                          
	                          return false;
	                      }
                		  
                	  }else{
                		  
                		  console.log('Erro!')
                	  }
	                     	
                	 

                  }//end for =----
              }// if contadorx 
				
			},error: function(e){
				
				console.log(e);
			},//fim chamada ajax 1 
			
		});
}
function ajaxApi(funcao, dadosJson) {
	
  let restApiEcm = 'api/public/ecm/document';
  let url;
  let msgSucesso;
  //let metodo = requestType.POST;
  let assincrono = false;
  let resultado = null;
	
  switch (funcao) {
        case 'criarArquivo':
             url = `/${restApiEcm}/createDocument`;
             msgSucesso = 'Documento inserido com sucesso!';
             break;
        case 'criarPasta':
             // url = `/${restApiEcm}/createFolder`;
             url = `/api/public/2.0/folderdocuments/create`;
             msgSucesso = 'Pasta criada com sucesso!';
             break;
        case 'listarDocumentos':
             url =  `/${restApiEcm}/listDocument/${dadosJson}`;
             metodo = requestType.GET;
             msgSucesso = 'Listagem realizada com sucesso!';
             break;
        case 'remover':
             url = `/${restApiEcm}/remove`;
             msgSucesso = 'Remoção realizada com sucesso!';
             break;
        case 'alterar':
             url = `/${restApiEcm}/updateDocumentFolder`;
             msgSucesso = 'Documento alterado com sucesso!';
             break;
        default:
             toast('Erro!', 'Função não encontrada.',  'danger', 4000);
             console.log('Função não encontrada.');
             return null;
  }
  $.ajax({
        async: assincrono,
        url: url,
        type: 'POST',
        data: dadosJson,
        contentType: 'application/json',
        success: function (dados) {
             // toast(msgSucesso, '', 'success', 4000);  //Aparecendo demais
             console.log(`Requisição para URL: ${url}  executada com sucesso!`);
             resultado = dados;
        },
        error: function (objErro, status, msg) {
             // toast('Erro!', 'Não foi possível concluir a  operação.', 'danger', 4000);
             console.log(`Erro: ${msg}`);
             resultado = objErro;
        }
  });
  return resultado;
}
	
$(function(){
  
      $('.docAnexo').off().on('change', function(){
    	  console.log('docAnexo - ');
          fnCarregaDocAnexo(this, 'pagamento');
      });
  
  });

$(function(){
	
	$('.reciboUpload').off().on('change', function(){
		console.log('reciboUpload - ');
		fnCarregaDocAnexo(this, 'recibo');
	});
	
});
$(function(){
	
	$('.contratoUpload').off().on('change', function(){
		console.log('contratoUpload - ');
		fnCarregaDocAnexo(this, 'contrato');
	});
	
});
$(function(){
	
	$('.anexarContratoAssinadoGer').off().on('change', function(){
		console.log('anexarContratoAssinadoGer - ');
		fnCarregaDocAnexo2(this, 'contratoAssinado');
	});
	
});



/*Checa se a pasta existe. Caso não, cria uma nova utilizando o código do evento/produto.*/
function prepFolder(acao){
	
	if(acao == 'pagamento'){		
		if(fnChecarPastas($("#"+$(".formaPagamento:last").attr('id')).val())){
			console.log('prepFolder() -> pastaSelecionada -> ' + pastaSelecionada);
			return true;
		}else if(pastaSelecionada == 0 ){
			console.log('Pasta não encontrada,criando...');
			ajaxApi('criarPasta', JSON.stringify({"parentFolderId":pastaDestino ,"documentDescription": tituloPasta , "expires": "false",
	           "publisherId":"Todos", "inheritSecurity":"true" }));
			if(fnChecarPastas($("#formaPagamento").val())){
				console.log('pastaSelecionada -> ' + pastaSelecionada);
				return true;
			}else{
				
				return false;
			}
			
		}
		
	}else if(acao == 'recibo'){
		if(fnChecarPastas('5') ){
			console.log('prepFolder() -> pastaSelecionada -> ' + pastaSelecionada);
			return true;
		}else if(pastaSelecionada == 0 ){
			console.log('Pasta não encontrada,criando...');
			ajaxApi('criarPasta', JSON.stringify({"parentFolderId":pastaDestino ,"documentDescription": tituloPasta , "expires": "false",
	           "publisherId":"Todos", "inheritSecurity":"true" }));
			if(fnChecarPastas('5')){
				console.log('pastaSelecionada -> ' + pastaSelecionada);
				return true;
			}else{
				
				return false;
			}
			
		}
		
	}else if(acao == 'contrato'){
		if(fnChecarPastas('6') /*== codsasCliente*/ ){
			console.log('prepFolder() -> pastaSelecionada -> ' + pastaSelecionada);
			return true;
		}else if(pastaSelecionada == 0 ){
			console.log('Pasta não encontrada,criando...');
			ajaxApi('criarPasta', JSON.stringify({"parentFolderId":pastaDestino ,"documentDescription": $("#codSAS").val() , "expires": "false",
	           "publisherId":"Todos", "inheritSecurity":"true" }));
			if(fnChecarPastas('6')){
				console.log('pastaSelecionada -> ' + pastaSelecionada);
				return true;
			}else{
				
				return false;
			}
			
		}
		
	}
	


}

function fnCarregaDocAnexo(objFile, operacao) {  
		retornaNaturezaOper();
		prepFolder(operacao);	
		if ($(objFile)[0].files[0]!=null&&$(objFile)[0].files[0]!=undefined) {
          var documentData = new FormData();
          documentData.append('file', $(objFile)[0].files[0]);          
  
          var request_data = {
              url: urlBase+'/ecm/upload',
              method: 'POST',
          };
  
          try {
              $.ajax({
                  url: request_data.url,
                  crossDomain: true,
                  type: 'POST',
                  data: documentData,                   	    
                  cache: false,
                  contentType: false,
                  processData: false,
                  success: function (response) {
                      var request_data = {
                              url: urlBase+'/api/public/ecm/document/createDocument',
                              method: 'POST',
                      };
                     
                      var obj = JSON.parse(response);
                      var idPasta = parseInt(pastaSelecionada);
                      console.log('idPasta -> ' + idPasta);
                      $.each(obj.files, function (index, file) {
                          $.ajax({
                              url: request_data.url,
                              crossDomain: true,
                              type: 'POST',
                              async : false,
                              contentType: "application/json",                                
                              data: JSON.stringify({
                                  //"description":  'arquivo1', //document.getElementById("idTrabalhoFile").value,  
                                  "description": tituloPasta,//$("#eventoCodigo1").val(), 
                                  "parentId": parseInt(idPasta), //parseInt(idPasta),
                                  "expirationDate": "9999-12-31",
                                  "inheritSecurity": false,
                                  "attachments": [{
                                      "fileName": file.name
                                      //"fileName": $("#codSAS").val()
  
                                  }],
                              }),
                              error: function(e) {
                            	  
                            	  FLUIGC.toast({
                        				title: 'Uplodad de Arquivo:',
                        				message: '<br>Não foi possível salvar o arquivo!',
                        				type: 'danger'
                        			});
                                  console.log(e)
                                  //	$myUtils.byMessage.alertError('', "Erro ao anexar Documento - " + e);		            	   	
                              },
                              success: function(data) {                            	  
                            	  if(operacao == 'pagamento'){                            		  
                            		  FLUIGC.toast({
                            				title: 'Uplodad de Arquivo:',
                            				message: '<br>Arquivo Enviado com Sucesso!',
                            				type: 'success'
                            			});
                            		  
                            	  }else if(operacao == 'recibo'){
                            		  FLUIGC.toast({
                          				title: 'Recibo:',
                          				message: '<br>Recibo Salvo com sucesso!',
                          				type: 'success'
                          			});
                            		  
                            	  }else if(operacao == 'contrato'){
                            		  FLUIGC.toast({
                            				title: 'Contrato:',
                            				message: '<br>Contrato Salvo com sucesso!',
                            				type: 'success'
                            			});
                            		  
                            	  }                            	 
                                  urlView=urlBase+'/api/public/2.0/documents/getDownloadURL/'+parseInt(data.content.id),
                                  
                                  $.ajax({
                                      url: urlView,
                                      async:false, 
                                      dataType : 'application/json; charset=utf-8', 
                                      type : 'GET',
                                      dataType: 'JSON',
                                      success: function(dataDownload) {
                                    	  //Adicionar aqui Link para visualização (chamando a function abreDocumento) e a lixeira para exclusão
                                    	  // 1 Checar o tipo de pagamento escolhido
                                    	  console.log('added link')
                                    	  let posicao = $(".linksRecebidos:last").attr("id");
                                    	  $("#"+posicao).append('<br><label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank" >Comprovante de Pagamento Anexo</a></label>')
                                    	  if(operacao == 'pagamento'){
                                    		  
                                    		  var _tipoPagamento =  $("#formaPagamento").val();                                   	  
                                        	  
                                        	  if(_tipoPagamento == 2 ||  _tipoPagamento == 1 ){
                                        		  
                                        		  $("#tituloArquivosAnexados").append('<div class="row"><label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank" >Comprovante de Pagamento Anexo</a></label></div>');
                                        		  
                                        	  }else if(_tipoPagamento == 3  ){
                                        		  
                                        		  $("#tituloArquivosAnexados").after('<div class="row"><label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank">Comprovante de Pagamento Anexo</a></label></div>');
                                        		  
                                        	  }else if(_tipoPagamento == 4  ){
                                        		  
                                        		  $("#tituloArquivosAnexados").after('<div class="row"><label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank">Comprovante de Pagamento Anexo</a></label></div>');
                                        		  
                                        	  }
                                        	  
                                              console.log(dataDownload);
                                              console.log('success')
                                              console.log('dataDownload.content:'+dataDownload.content)
                                              //linkDeposito(dataDownload.content)
                                              console.log('dataDownload.content:'+dataDownload.message.message)
                                               $("#linkComprovantePagamentoSalvo").val(dataDownload.content)
                                              /*
                                                  var idx = objFile.id.split('___');
                                                  if (idx[1]) {
                                                      $('#view'+$(objFile).attr('name-hidden')+'___'+idx[1]).attr('href', dataDownload.content).removeClass('disabled')
                                                  } else {
                                                      $('#view'+$(objFile).attr('name-hidden')).attr('href', dataDownload.content).removeClass('disabled')
                                                  } */
                                    		  
                                    	  }else if(operacao == 'recibo'){
                                    		  console.log(dataDownload);
                                              console.log('success')
                                              console.log('dataDownload.content:'+dataDownload.content)
                                              //linkDeposito(dataDownload.content)
                                              console.log('dataDownload.content:'+dataDownload.message.message)
                                    		  $("#linkReciboSalvo").val(dataDownload.content)
                                    	  }else if(operacao == 'contrato'){
                                    		  
                                    		  console.log(dataDownload);
                                              console.log('success')
                                              console.log('dataDownload.content:'+dataDownload.content)
                                              //linkDeposito(dataDownload.content)
                                              console.log('dataDownload.content:'+dataDownload.message.message)
                                               $("#linkContratoSalvo").val(dataDownload.content)
                                    	  }
                                    	  
                                      },
                                      error: function(e) {
                                          console.log('error')
                                          console.log(e)
                                          console.log('dataDownload.content:'+e.content)
                                          console.log('dataDownload.content:'+e.message.message)
                                          console.log('dataDownload.detail:'+e.message.detail)
                                      }
                                  });
                                 /* $.ajax({
                                      url: urlLogout,
                                      async:false, 
                                      dataType : 'application/json; charset=utf-8', 
                                      type : 'GET',
                                      dataType: 'JSON',
                                      success: function(data) {
                                          console.log(data);
                                          console.log('success')
                                      },
                                      error: function(e) {
                                          console.log('error')
                                          console.log(e)
                                      }
                                  });*/
                              },
                          });
                      });        	 
                  }
              });
          } catch (e) {
              console.log(e)
          }
  
      }

  }

function fnLinkArquivo(){
	
	//loadOauth7('/api/public/ecm/document/listDocumentWithChildren/'+ idFolderMa );

	$.ajax({
		url: 'http://fluighomolog.sebraepb.com.br:8080/api/public/ecm/document/listDocumentWithChildren/15877' ,
		crossDomain: true,
		type : "GET",
		async : false,
		contentType: "application/json",
		//headers:inOauth.oauth1_fluig,
		dataType : 'application/json', 
		//data: ({limit: 120}),
		limit: '155',
		success : function(data){
			if(data.content[0].children.length > 0 ){

				for(var p = 0; p < data.content[0].children.length; p++){

				if(data.content[0].children[p]["description"] == $("#codConsultoria").val() && data.content[0].children[p]["removed"] == false ){
						//if(data.content[0].children[p]["description"] == $("#idTrabalhoFile").val() && data.content[0].children[p]["removed"] == false ){

						//$("#viewAnexoTrabalho").attr('href', data.content[0].children[p]["fileURL"]);
						console.log('fnLinkArquivo() -> ' + data.content[0].children[p]["fileURL"]);
						$("#baixarContratoAssinar").val(data.content[0].children[p]["fileURL"]);
					}

				}
			}else{
				console.log('zero files')
			}					
		}
	});	
}
/*Apenas para o contrato assinado pelo gerente ->*/
function fnCarregaDocAnexo2(objFile, operacao) {    
	
	
	//prepFolder(operacao);	
	 if($("#tipoSerConsultoria").is(':checked')== true){
		
		 var codigoArquivo = $("#codConsultoria").val();
		 
	}else if($("#_tipoSerConsultoria").is(':checked')== true){
		
		var codigoArquivo = $("#codConsultoria").val();
		
	}
	

	
	if ($(objFile)[0].files[0]!=null&&$(objFile)[0].files[0]!=undefined) {
      var documentData = new FormData();
      documentData.append('file', $(objFile)[0].files[0]);          

      var request_data = {
          url: urlBase+'/ecm/upload',
          method: 'POST',
      };

      try {
          $.ajax({
              url: request_data.url,
              crossDomain: true,
              type: 'POST',
              data: documentData,                   	    
              cache: false,
              contentType: false,
              processData: false,
              success: function (response) {
                  var request_data = {
                          url: urlBase+'/api/public/ecm/document/createDocument',
                          method: 'POST',
                  };
                 
                  var obj = JSON.parse(response);
                  //var idPasta = parseInt(pastaSelecionada);
                 
                  $.each(obj.files, function (index, file) {
                      $.ajax({
                          url: request_data.url,
                          crossDomain: true,
                          type: 'POST',
                          async : false,
                          contentType: "application/json",                                
                          data: JSON.stringify({
                              //"description":  'arquivo1', //document.getElementById("idTrabalhoFile").value,  
                              "description": codigoArquivo + '-Assinado.pdf', 
                              "parentId": 15877, //parseInt(idPasta),
                              "expirationDate": "9999-12-31",
                              "inheritSecurity": false,
                              "attachments": [{
                                  //"fileName": codigoArquivo + '-Assinado.pdf'
                                  "fileName": file.name
                                  //"fileName": $("#codSAS").val()

                              }],
                          }),
                          error: function(e) {
                        	  
                        	  FLUIGC.toast({
                    				title: 'Uplodad de Arquivo:',
                    				message: '<br>Não foi possível salvar o arquivo!',
                    				type: 'danger'
                    			});
                              console.log(e)
                              //	$myUtils.byMessage.alertError('', "Erro ao anexar Documento - " + e);		            	   	
                          },
                          success: function(data) {
                        	  
                        	  if(operacao == 'pagamento'){
                        		  FLUIGC.toast({
                        				title: 'Uplodad de Arquivo:',
                        				message: '<br>Arquivo Enviado com Sucesso!',
                        				type: 'success'
                        			});
                        		  
                        	  }else if(operacao == 'recibo'){
                        		  FLUIGC.toast({
                      				title: 'Recibo:',
                      				message: '<br>Recibo Salvo com sucesso!',
                      				type: 'success'
                      			});
                        		  
                        	  }else if(operacao == 'contratoAssinado'){
                        		 
                        		  FLUIGC.toast({
                        				title: 'Contrato:',
                        				message: '<br>Contrato Assinado Salvo com sucesso!',
                        				type: 'success'
                        			});
                        		  
                        	  }
                        	 
                        	
                        	  
                        	  /*
                              var idx = objFile.id.split('___');
                              if (idx[1]) {
                                  $('#cod'+$(objFile).attr('name-hidden')+'___'+idx[1]).val(data.content.id)
                                  $('#delete'+$(objFile).attr('name-hidden')+'___'+idx[1]).attr('onclick', 'deleteDoc('+data.content.id+',this);').removeClass('disabled')
                              } else {
                                  $('#cod'+$(objFile).attr('name-hidden')).val(data.content.id)
                                  $('#delete'+$(objFile).attr('name-hidden')).attr('onclick', 'deleteDoc('+data.content.id+',this);').removeClass('disabled')
                              }*/
                              // $myUtils.byMessage.alertInfo('', "Documento anexado - " + file.name + " - ID - " + data.content.id);
                              urlView=urlBase+'/api/public/2.0/documents/getDownloadURL/'+parseInt(data.content.id),
                              //urlLogout= WCMAPI.serverURL+'/portal/p/api/servlet/logout.do',
                              $.ajax({
                                  url: urlView,
                                  async:false, 
                                  dataType : 'application/json; charset=utf-8', 
                                  type : 'GET',
                                  dataType: 'JSON',
                                  success: function(dataDownload) {
                                	  //Adicionar aqui Link para visualização (chamando a function abreDocumento) e a lixeira para exclusão
                                	  // 1 Checar o tipo de pagamento escolhido
                                	  let posicao = $(".linksRecebidos:last").attr("id");
                            		  $("#"+posicao).append('<label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank" >Comprovante de Pagamento Anexo</a></label>')
                                	  if(operacao == 'pagamento'){
                                		  
                                		  var _tipoPagamento =  $("#formaPagamento").val();                                   	  
                                    	  
                                    	  if(_tipoPagamento == 2 ||  _tipoPagamento == 1 ){
                                    		
                                    		 // $(".file-upload").after('<label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank" >Comprovante de Pagamento Anexo</a></label>');
                                    		  
                                    	  }else if(_tipoPagamento == 3  ){
                                    		  
                                    		  $(".file-upload").after('<label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank">Comprovante de Pagamento Anexo</a></label>');
                                    		  
                                    	  }else if(_tipoPagamento == 4  ){
                                    		  
                                    		  $(".file-upload").after('<label><a name="visualizarArquivo" href="'+dataDownload.content+'" target="_blank">Comprovante de Pagamento Anexo</a></label>');
                                    		  
                                    	  }
                                    	  
                                    	  
                                          console.log(dataDownload);
                                          console.log('success')
                                          console.log('dataDownload.content:'+dataDownload.content)
                                          //linkDeposito(dataDownload.content)
                                          console.log('dataDownload.content:'+dataDownload.message.message)
                                           //$("#linkComprovantePagamentoSalvo").val(dataDownload.content)
                                          /*
                                              var idx = objFile.id.split('___');
                                              if (idx[1]) {
                                                  $('#view'+$(objFile).attr('name-hidden')+'___'+idx[1]).attr('href', dataDownload.content).removeClass('disabled')
                                              } else {
                                                  $('#view'+$(objFile).attr('name-hidden')).attr('href', dataDownload.content).removeClass('disabled')
                                              } */
                                		  
                                	  }else if(operacao == 'recibo'){
                                		  console.log(dataDownload);
                                          console.log('success')
                                          console.log('dataDownload.content:'+dataDownload.content)
                                          //linkDeposito(dataDownload.content)
                                          console.log('dataDownload.content:'+dataDownload.message.message)
                                		  $("#linkReciboSalvo").val(dataDownload.content)
                                	  }else if(operacao == 'contrato'){
                                		  
                                		  console.log(dataDownload);
                                          console.log('success')
                                          console.log('dataDownload.content:'+dataDownload.content)
                                          //linkDeposito(dataDownload.content)
                                          console.log('dataDownload.content:'+dataDownload.message.message)
                                           $("#linkContratoSalvo").val(dataDownload.content)
                                	  }
                                  else if(operacao == 'contratoAssinado'){
                                	  $("#linkContratoAssinado").val(dataDownload.content);
                                	  console.log(dataDownload);
                                	  console.log('success')
                                	  console.log('dataDownload.content:'+dataDownload.content)
                                	  //linkDeposito(dataDownload.content)
                                	  console.log('dataDownload.content:'+dataDownload.message.message)
                                	 // $("#linkContratoSalvo").val(dataDownload.content)
                                  }
                                	  
                                  },
                                  error: function(e) {
                                      console.log('error')
                                      console.log(e)
                                      console.log('dataDownload.content:'+e.content)
                                      console.log('dataDownload.content:'+e.message.message)
                                      console.log('dataDownload.detail:'+e.message.detail)
                                  }
                              });
                             /* $.ajax({
                                  url: urlLogout,
                                  async:false, 
                                  dataType : 'application/json; charset=utf-8', 
                                  type : 'GET',
                                  dataType: 'JSON',
                                  success: function(data) {
                                      console.log(data);
                                      console.log('success')
                                  },
                                  error: function(e) {
                                      console.log('error')
                                      console.log(e)
                                  }
                              });*/
                          },
                      });
                  });        	 
              }
          });
      } catch (e) {
          console.log(e)
      }

  }

}