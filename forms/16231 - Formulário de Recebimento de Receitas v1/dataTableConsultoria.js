//Jean Varlet -- Alteração 14/11/2019---
//parametro tipo de servico -> $("#ConsultoriaInstrutoria").val('Consultoria'); para montagem da consulta do dataset de form.


var arrayEventosConsultoria = new Array();
/*Jean Varlet - 23/12/2019 - */

function removeTable(){
	
	if($("tr", $("#corpoEventosConsultoria")).length > 0){
		
		$("tr", $("#corpoEventosConsultoria")).remove();
	}else{
		
		console.log('Empty Table -> ');
	}
}
function formatDataEvento(data){
	if(data.indexOf('/') != -1){
		return data;
	}else{
		if(data.indexOf('-') != -1){
			return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY");
		}
	}
	
	
}
//27692972000140
var arraySolsPeriodo ;
var arraySolsCPF ;
function removeSolsCpf(arraySolsCPF){
    let countRows = $("tr", $("#corpoEventosConsultoria")).length;
    if(countRows > 0 && arraySolsCPF.length){
        for(var i = 0; i < arraySolsCPF.length; i++){
            console.log('arraySolsCPF[i]'+ arraySolsCPF[i])
            $("#"+arraySolsCPF[i]+":not('.cpf')", $("#corpoEventosConsultoria")).remove();
        }
        
    }
}

function fnRetornaConsultoria(tiposervico, dataInicio, dataFim, cpfCliente){	
	arraySolsPeriodo = new Array();
	arraySolsCPF = new Array();
	var myLoading = FLUIGC.loading(window);
		
	removeTable();//Avaliar ---
	
	let _inicio = invertData(dataInicio);
	let _fim = invertData(dataFim);
	let pfcst1, pfcst2, pfConsulta;
	let user = 'Blue';
	let cst1 = DatasetFactory.createConstraint("userSecurityId", user, user, ConstraintType.MUST);
	let cst2 = DatasetFactory.createConstraint("tipoSer", tiposervico, tiposervico, ConstraintType.MUST);
	let cst3 = DatasetFactory.createConstraint("dataEvento", _inicio, _fim, ConstraintType.MUST);
	let cst4 = DatasetFactory.createConstraint("metadata#active", "true", "true", ConstraintType.MUST);
	let consult = DatasetFactory.getDataset('FormuláriodeContrataçãodeConsultoria4166', null,new Array(cst1,cst2,cst3, cst4) ,null);
	
	
	if(!jQuery.isEmptyObject(consult)){// Busca todas as solicitações dentro do período informado
		
		let countEventos = consult.values.length;
		let retorno = consult.values;
		
		if(consult.values.length ){
			
			for(let i = 0; i < countEventos; i++){	
				arraySolsPeriodo.push([consult.values[i].documentid]);			
				var docId = consult.values[i].documentid;
				pfcst1 = DatasetFactory.createConstraint('documentId', docId, docId, ConstraintType.MUST);
				pfcst2 = DatasetFactory.createConstraint('cpfCliente', cpfCliente, cpfCliente, ConstraintType.MUST);//Checar --
				pfConsulta = DatasetFactory.getDataset('ds_clientes_contratacao_v1', null, [pfcst2, pfcst1], null);
				
				if(!jQuery.isEmptyObject(pfConsulta)){//Utilizando as solicitações encontradas no período retorna as que possuem o cpf desejado
					if(pfConsulta.values.length){
						for(let x = 0; x < pfConsulta.values.length; x++){
							arraySolsCPF.push([pfConsulta.values[x].documentId]);	
							//if(pfConsulta.values[x].documentid == consult.values[i].documentid){
								$("#corpoEventosConsultoria").append('<tr id="'+ retorno[i].documentid +'" class="cpf" style="background-color:#00A3DB; color:white;">'+
									'<td class="col-md-1"><input type="checkbox" class="selEvento " id="evento_'+retorno[i].documentid  +'"</td>'+
									'</td><td id="nome_'+retorno[i].documentid +'">'+retorno[i].nomeevento +
						            '</td><td id="dataini_'+retorno[i].documentid + '">'+formatDataEvento(retorno[i].datainicial) +
						            '</td><td  id="datafim_'+retorno[i].documentid +'">'+formatDataEvento(retorno[i].datafinalFormat) +
						           // '</td><td  >'+ (datasetReturn.values[i].TipoServicoNome == 'null') ? 'Outros' : datasetReturn.values[i].TipoServicoNome +
						            '</td><td id="tipo_'+retorno[i].documentid +'" >'+ retorno[i].tipoSer    +
						            '</td><td  id="carga_'+retorno[i].documentid +'">'+retorno[i].cargaHorariaTotal + ' horas' +
						            '</td><td id="preco_'+retorno[i].documentid +'" >'+retorno[i].cargaHorariaValorTotal +
						            '</td> </tr>');
							//}
						}
					}
					
				}
				
					
				
			}//fim for
			var arraysResultado;
			 if(arraySolsCPF.length){
		        for(var i = 0; i < arraySolsPeriodo.length; i++ ){
		            if(arraySolsPeriodo[i].indexOf(arraySolsCPF) == -1 ){
		                console.log('Removendo -> ' + i)
		                arraySolsPeriodo.splice(i,1)
		            }
		        }
				arrayResultado = arraySolsPeriodo;
				console.log('arrayResultado-> '+ arrayResultado.length)
		    }
			for(let i = 0; i < arrayResultado.length; i++){
				var cstx1 = DatasetFactory.createConstraint("metadata#active", "true", "true", ConstraintType.MUST);
				var cstx2 = DatasetFactory.createConstraint("documentid", arrayResultado[i], arrayResultado[i], ConstraintType.MUST);
				var consult2 = DatasetFactory.getDataset('FormuláriodeContrataçãodeConsultoria4166', null,new Array(cstx1, cstx2) ,null);
				if(!jQuery.isEmptyObject(consult2)){
					$("#corpoEventosConsultoria").append('<tr id="'+ retorno[i].documentid +'" style="background-color:white; color:black;">'+
									'<td class="col-md-1"><input type="checkbox" class="selEvento" id="evento_'+retorno[i].documentid  +'"</td>'+
									'</td><td id="nome_'+retorno[i].documentid +'">'+retorno[i].nomeevento +
						            '</td><td id="dataini_'+retorno[i].documentid + '">'+formatDataEvento(retorno[i].datainicial) +
						            '</td><td  id="datafim_'+retorno[i].documentid +'">'+formatDataEvento(retorno[i].datafinalFormat) +
						           // '</td><td  >'+ (datasetReturn.values[i].TipoServicoNome == 'null') ? 'Outros' : datasetReturn.values[i].TipoServicoNome +
						            '</td><td id="tipo_'+retorno[i].documentid +'" >'+ retorno[i].tipoSer    +
						            '</td><td  id="carga_'+retorno[i].documentid +'">'+retorno[i].cargaHorariaTotal + ' horas' +
						            '</td><td id="preco_'+retorno[i].documentid +'" >'+retorno[i].cargaHorariaValorTotal +
						            '</td> </tr>');
					
				}	
				
			}
				
			autoScroll('endereco', 200);
			$("#listaConsultorias").fadeIn();
			
			$("input[id^='evento_']").click(function(){	
				
			    var rowEvento = $(this).parent().parent().attr('id');
			    
			    if($("#evento_" + rowEvento).is(":checked")){
			    	$("#"+rowEvento).attr('class', 'success');
			    	$("#formaPagamento").prop('disabled', false);
			    	var usera = 'Blue';
			    	var cst1a = DatasetFactory.createConstraint("userSecurityId", usera, usera, ConstraintType.MUST);
			    	//var cst2 = DatasetFactory.createConstraint("tipoSer", tiposervico, tiposervico, ConstraintType.MUST);
			    	var cst3a = DatasetFactory.createConstraint("documentid", rowEvento, rowEvento, ConstraintType.MUST);
			    	var cst4a = DatasetFactory.createConstraint("metadata#active", "true", "true", ConstraintType.MUST);
			    	var retornaEventoConsult = DatasetFactory.getDataset('FormuláriodeContrataçãodeConsultoria4166', null,new Array(cst1a,cst3a, cst4a) ,null);
			    	//var retornaEventoConsult = DatasetFactory.getDataset('FormuláriodeConsultoria1230', null,new Array(cst1a,cst3a, cst4a) ,null);
			    	for(var p = 0; p < retornaEventoConsult.values.length; p++){
			    		
			    		$("#codConsultoria").val(retornaEventoConsult.values[p].documentid);
					    $("#tituloConsultoria").val(retornaEventoConsult.values[p].nomeevento);
					    $("#tipoConsultoria").val(retornaEventoConsult.values[p].tipoSer);
					    $("#necessidadeContrato").val(retornaEventoConsult.values[p].necessidadeContrato);
					    $("#descricaoEven").val(retornaEventoConsult.values[p].descricaoEven);
					    $("#datainicial").val(formatDataEvento(retornaEventoConsult.values[p].datainicial));
					    $("#datafinal").val(formatDataEvento(retornaEventoConsult.values[p].datafinal));
					    $("#horarioIni").val(retornaEventoConsult.values[p].horarioIni);
					    $("#horarioFim").val(retornaEventoConsult.values[p].horarioFim);
					    $("#cepEvento").val(retornaEventoConsult.values[p].cepEvento);
					    $("#cidadeEvento").val(retornaEventoConsult.values[p].cidadeEvento);
					    $("#complementoEnd").val(retornaEventoConsult.values[p].complementoEnd);
					    $("#estadoEvento").val(retornaEventoConsult.values[p].estadoEvento);
					    $("#valorHora").val(retornaEventoConsult.values[p].valorHora);
					    $("#cargaHorariaValorTotal").val(retornaEventoConsult.values[p].cargaHorariaValorTotal);
					    $("#cargaHorariaTotal").val(retornaEventoConsult.values[p].cargaHorariaTotal);
					    $("#numberVlrTotRateio").val(retornaEventoConsult.values[p].numberVlrTotRateio);//rateio
					    $("#tbAgencia2").val(retornaEventoConsult.values[p].tbAgencia);
					    $("#tbNomePessoa2").val(retornaEventoConsult.values[p].tbNomePessoa);//rateio
					    $("#tbProtocolo2").val(retornaEventoConsult.values[p].tbProtocolo);//rateio
					    $("#respTecnico").val(retornaEventoConsult.values[p].respTecnico);//rateio
					    $("#rm_fcfonomeForn").val(retornaEventoConsult.values[p].rm_fcfonomeForn);//rateio
			    	}
			    	  
			    		
						//JEan 12/11/2019 -- Exibe área de pagamento ao selecionar um evento --- 
						$("#formasDePagamentoNovo").fadeIn();
						$("#newDescontoArea").fadeIn();
						$("#dadosConsultoriaSelecionada").fadeIn();
						//$("#newRateioArea").fadeIn();
						
						autoScroll('numberVlrTotRateio', 100);
						/*
						$("#bntCadClienteAcesso").one(function(){
							$(this).click();
	
						})
						*/
						//$("#bntCadClienteAcesso").click();
						$("#valorTotalBruto").fadeIn();
						valorBrutoServico();
						setTimeout(function(){
							
						$(".save-cliente:contains('Adicionar')").prop("disabled",true);
						
						},1000);
						$("#totalDesconto").val($("#fieldValorBruto").val());
						$("#valorCliente").val($("#fieldValorBruto").val());
						$("#subTotalDesconto").val('R$ 0,00');
						$("#valorSebrae").val('R$ 0,00');
						
						$("#dataMaquinetaPagamento").click(function(){
							//autoScroll('cepEvento', 100);	
							$("#valorMaquinetaPagamento").val($("#cargaHorariaValorTotal").val());
							
							});
							$("#dataDepositoPagamento").click(function(){
								
								$("#valorDepositoPagamento").val($("#cargaHorariaValorTotal").val());
								
							});
							$("#dataBoletoPagamento").click(function(){
								
								$("#valorBoletoPagamento").val($("#cargaHorariaValorTotal").val());
								
							});
						
			    }else if (!$("#evento_" + rowEvento).is(":checked")) {
			    	cleanValues();
			    	$("#"+rowEvento).removeAttr('class', 'success');
			    	limparDiv('dadosConsultoriaSelecionada');
			    	$("#formaPagamento").prop('disabled', true);
			    	$("#formasDePagamentoNovo").fadeOut();
					$("#dadosConsultoriaSelecionada").fadeOut();
					//$("#newRateioArea").fadeOut();
			    }
				  
			});
		
				
		}
	}
		
		$(".selEvento").click(function(){
		    let idClicked = $(this).attr("id");
			console.log('Clicado -> '+ idClicked)
		    if($(".selEvento:checked").length > 1){
		        let uncheck = $(".selEvento:checked").not($("#"+idClicked)).attr("id");
				console.log('uncheck-> '+uncheck);
				let trId = uncheck.split('_')[1];
		        
				$("#"+trId).removeAttr('class', 'success');
		        $("#"+uncheck).prop("checked", false).removeAttr('checked');
		    }
		});
		removeSolsCpf(arraySolsCPF);
	
	setTimeout(function(){
		myLoading.hide();
		
	},2000)
	
}
function cleanValues(){
	let blankFields = ["fieldValorBruto", "fieldValorBrutoh" ]
	let zeroFields = ["perDesconto", "subTotalDesconto", "totalDesconto", "rateioSebrae", "valorSebrae", "valorCliente" ]
	for(let i = 0; i < blankFields.length; i++){
		let idField = blankFields[i];
		$("#"+idField).val('');
	}
	for(let x = 0; x < zeroFields.length; x++){
		let idField = zeroFields[x];
		$("#"+idField).val(0);
	}
	
	
}cleanValues()