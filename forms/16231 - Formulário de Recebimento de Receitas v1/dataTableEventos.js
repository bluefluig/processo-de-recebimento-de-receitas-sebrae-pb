/*Jean Varlet*/
function formatData(dataRm){
	
	var dataRm = dataRm.substr(0,10).split('-').reverse().join('/');
	return dataRm;
}
function formatHora(data){
	
	var hora = data.split('T')[1];
	
	return hora.substr(0,5);
}




// function para buscar dados extras dos eventos para compor recibo de pgt
var CodParceiro;// = '20475012';
var codEvento; //= '2037072';
var arrayEventos; //= new Array();

function fnRetornaDadosEventoSAS(codEvento){
 var counterEventos;
 arrayEventos = {};
 var codEventoPesquisa  = '&CodEvento='+codEvento;
      
 c1 = DatasetFactory.createConstraint('CodEvento', codEventoPesquisa, codEventoPesquisa, ConstraintType.MUST);        
 ct = new Array(c1);
 datasetReturn = DatasetFactory.getDataset("dsSASEventos", null, ct, null);
	if (!jQuery.isEmptyObject(datasetReturn)){
		 counterEventos = datasetReturn.values.length;
		for(var i = 0; i < counterEventos; i++){
			 
			 $("#tipoEventoSaSselecionado").val(datasetReturn.values[i].TipoServicoNome);
			 $("#dataIniEventoSaSselecionado").val(datasetReturn.values[i].PeriodoInicial);
			 $("#dataFimEventoSaSselecionado").val(datasetReturn.values[i].PeriodoFinal);
			 $("#horaIniEventoSaSselecionado").val(datasetReturn.values[i].PeriodoInicial);
			 $("#horaFimEventoSaSselecionado").val(datasetReturn.values[i].PeriodoFinal);
			 $("#tituloEventoSaSselecionado").val(datasetReturn.values[i].TituloEvento);
			 $("#modalidadeEventoSaSselecionado").val(datasetReturn.values[i].ModalidadeNome);
			 $("#localEventoSaSselecionado").val(datasetReturn.values[i].Local);
			 
		}	
	}
}

function fnDataTable(cpfCnpj){
	cpfCnpj = cpfCnpj.trim().replace(' ', '');
	cpfCnpj = cpfCnpj.replace(/\./g,'');
	cpfCnpj = cpfCnpj.replace(/\.|\-|\//g,'');
	console.log(`fnDataTable(cpfCnpj)-> ${cpfCnpj.length}`);
	console.log(`fnDataTable(cpfCnpj)-> ${cpfCnpj}`);
	var mydata; //avaliar necessidade --- 
	var that = this;
	var dataset, constraints, datasetReturn, c1, c2, c3, ct, CodParceiro, counterEventos;
	var ca = DatasetFactory.createConstraint("CgcCpf", cpfCnpj, cpfCnpj, ConstraintType.MUST);	
 	constraints   = new Array(ca);	
	if(cpfCnpj.length == 11){					
    	 dataset = DatasetFactory.getDataset("dsSASClientePF", null, constraints, null);
	}else if(cpfCnpj.length == 14){    	 			
    	 dataset = DatasetFactory.getDataset("dsSASClientePJ", null, constraints, null);
	}else{
		
		//toast
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		});
		
	}

	console.log(dataset.values[0]);
	 CodParceiro = dataset.values[0].CodParceiro;
	console.log('CodParceiro -> ' + CodParceiro); 
	
	if(cpfCnpj.length == 11){
		
         c1 = DatasetFactory.createConstraint('CodPessoaF', CodParceiro, CodParceiro, ConstraintType.MUST);
         c2 = DatasetFactory.createConstraint('Situacao', "Reserva", "Reserva", ConstraintType.MUST);
         ct = new Array(c1,c2);
         datasetReturn = DatasetFactory.getDataset("dsSASEventosCliente", null, ct, null);
		 
		if (!jQuery.isEmptyObject(datasetReturn)){
		 counterEventos = datasetReturn.values.length;
		} else { counterEventos = 0 }
		 console.log(counterEventos)
		 console.log(datasetReturn)
		 
    }else if(cpfCnpj.length == 14){
    	c1 = DatasetFactory.createConstraint('CodPessoaJ', CodParceiro, CodParceiro, ConstraintType.MUST);
        //c2 = DatasetFactory.createConstraint('PeriodoInicial', '2019/01/01', '2019/01/01', ConstraintType.MUST);
       // c2 = DatasetFactory.createConstraint('PeriodoFinal', '2019/12/31', '2019/12/31', ConstraintType.MUST);
    	c2 = DatasetFactory.createConstraint('Situacao', "Reserva", "Reserva", ConstraintType.MUST);
        ct = new Array(c1, c2);
        datasetReturn = DatasetFactory.getDataset("dsSASEventosCliente", null, ct, null);
		
		if (!jQuery.isEmptyObject(datasetReturn)) {
			counterEventos = datasetReturn.values.length;
		} else { counterEventos = 0 }
		
		console.log(counterEventos)
		console.log(datasetReturn)
    	
    }
	
	if(datasetReturn != null && datasetReturn.values != null ){
		$("#target").show();
		
		for(var i = 0; i < counterEventos; i++){
			if(datasetReturn.values[i].Preco != '0.00' && datasetReturn.values[i].ValorPago != '0.00' && datasetReturn.values[i].Situacao == 'Inscrito'){
				$("#corpoEventos").append('<tr  id="'+datasetReturn.values[i].EventoID +'">'+
				'<td class="col-md-1"><input type="checkbox" class="eventoSAS" id="evento_'+datasetReturn.values[i].EventoID +'"</td>'+
				'</td><td id="nome_'+datasetReturn.values[i].EventoID+'">'+datasetReturn.values[i].EventoNome+
				 '</td><td  id="local_'+datasetReturn.values[i].EventoID+'">'+datasetReturn.values[i].Local+
				 
				 '</td ><td style="display:none" id="horaini_'+datasetReturn.values[i].EventoID+'">'+formatHora(datasetReturn.values[i].PeriodoInicial)+
				 '</td ><td style="display:none" id="horafim_'+datasetReturn.values[i].EventoID+'">'+formatHora(datasetReturn.values[i].PeriodoFinal)+
	            '</td><td id="dataini_'+ datasetReturn.values[i].EventoID+ '">'+formatData(datasetReturn.values[i].PeriodoInicial)+
	            '</td><td  id="datafim_'+datasetReturn.values[i].EventoID+'">'+formatData(datasetReturn.values[i].PeriodoFinal)+
	            '</td><td style="display:none" id="codProduto_'+datasetReturn.values[i].EventoID+'">'+datasetReturn.values[i].CodProduto+
	          
	           // '</td><td  >'+ (datasetReturn.values[i].TipoServicoNome == 'null') ? 'Outros' : datasetReturn.values[i].TipoServicoNome +
	            '</td><td id="tipo_'+datasetReturn.values[i].EventoID+'" >'+ datasetReturn.values[i].TipoServicoNome   +
	            '</td><td  id="carga_'+datasetReturn.values[i].EventoID+'">'+datasetReturn.values[i].CargaHoraria+ ' horas' +
	            '</td><td id="preco_'+datasetReturn.values[i].EventoID +'" >'+datasetReturn.values[i].Preco+ ',00'+
	            '</td> </tr>');
			}
		}
		
		$("input[id^='evento_']").click(function(){	
			
		    var rowEvento = $(this).parent().parent().attr('id');
		    
			//var nomeEvento = $("#nome_"+rowEvento).text();
			
			var datainiEvento = $("#dataini_" + rowEvento).text();
			var datafimEvento = $("#datafim_" + rowEvento).text();
			var tipoEvento = $("#tipo_" +rowEvento).text();
			var cargaEvento = $("#carga_"+rowEvento).text();
		    var precoEvento = parseFloat($("#preco_" + rowEvento).text());
		    
			var subtotal = $("#somaSubTotal").val();
			var desconto = $("#descontoInformado").val();
			var total = $("#valorTotalFinal").val();
			//JEan 12/11/2019 -- Exibe área de pagamento ao selecionar um evento --- 
			$("#valorTotalBruto").fadeIn();
						
			$("#formasDePagamentoNovo").fadeIn();			
			$("#newDescontoArea").fadeIn();
		    console.log('Valor do Evento Selecionado -> '+ precoEvento);
		    
		   
		    if($("#evento_" + rowEvento).is(":checked")){

				console.log('rowEvento -> '+ rowEvento);
				$("#formaPagamento").prop('disabled', false);
				
				$("#dataMaquinetaPagamento").click(function(){					
				$("#valorMaquinetaPagamento").val($("#totalDesconto").val());				
				});
				
				$("#dataDepositoPagamento").click(function(){					
					$("#valorDepositoPagamento").val($("#totalDesconto").val());					
				});
				
				$("#dataBoletoPagamento").click(function(){					
					$("#valorBoletoPagamento").val($("#totalDesconto").val());					
				});
				
				$("#"+rowEvento).addClass('success');
				
				$("#eventoNome1").val($("#nome_"+rowEvento).text());
				$("#eventoDataInicio1").val($("#dataini_" + rowEvento).text());
				$("#eventoDataFim1").val($("#datafim_" + rowEvento).text());
				$("#eventoTipo1").val($("#tipo_" +rowEvento).text());
		        $("#eventoCarga1").val($("#carga_"+rowEvento).text());
		        $("#eventoPreco1").val( $("#valorTotalFinal").val());
				$("#eventoCodigo1").val(rowEvento);
				
				 $("#tipoEventoSaSselecionado").val($("#tipo_" +rowEvento).text());
				 $("#dataIniEventoSaSselecionado").val($("#dataini_" + rowEvento).text());
				 $("#dataFimEventoSaSselecionado").val($("#datafim_" + rowEvento).text());
				 $("#horaIniEventoSaSselecionado").val($("#horaIni_"+rowEvento).text());
				 $("#horaFimEventoSaSselecionado").val($("#horaFim_"+rowEvento).text());
				 $("#tituloEventoSaSselecionado").val($("#nome_"+rowEvento).text());
				 $("#modalidadeEventoSaSselecionado").val($("#codProduto_" +rowEvento).text());
				 $(".eventoSAS").click(function(){
					    let idClicked = $(this).attr("id");
					    if($(".eventoSAS:checked").length > 1 ){
					        let uncheck = $(".eventoSAS:checked").not($("#"+idClicked)).attr("id");
					        let trId = uncheck.split('_')[1];
					        $("#"+trId).removeAttr('class', 'success');
							$("#"+uncheck).prop("checked", false).removeAttr('checked');
					    }
					})
				
				if($("#local_"+rowEvento).text() == null){
					$("#localEventoSaSselecionado").val('Local Não Informado');
				}else{
					$("#localEventoSaSselecionado").val($("#local_"+rowEvento).text());
				}
				
				//fnRetornaDadosEventoSAS(rowEvento);	
				

				//$("#somaSubTotal").val(parseFloat(subtotal) + parseFloat(precoEvento) );
				$("#valorTotalFinal").val( precoEvento);
				
				/*
				$("#bntCadClienteAcesso").one(function(){
					$(this).click();
					
				})
				*/
				//$("#bntCadClienteAcesso").click();
				setTimeout(function(){
					
					$(".save-cliente:contains('Adicionar')").prop("disabled",true);
					
					},1000);
				$("#totalDesconto").val($("#fieldValorBruto").val());
				$("#valorCliente").val($("#fieldValorBruto").val());
				$("#subTotalDesconto").val('R$ 0,00');
				$("#valorSebrae").val('R$ 0,00');
				$("#dadosEventoSASselecionado").fadeIn();
		       
		        console.log('Evento Marcado');
		        valorBrutoServico();
		    }else if (!$("#evento_" + rowEvento).is(":checked")) {

				$("#"+rowEvento).removeClass( 'success');
				$("#formaPagamento").prop('disabled', true);
				$("#eventoNome1").val('');
				$("#eventoDataInicio1").val('');
				$("#eventoDataFim1").val('');
				$("#eventoTipo1").val('');
		        $("#eventoCarga1").val('');
		        $("#eventoPreco1").val('');
				$("#eventoCodigo1").val('');
				limparDiv('dadosEventoSASselecionado');
				//$("#somaSubTotal").val(parseFloat(subtotal) - parseFloat(precoEvento) );
				//$("#valorTotalFinal").val((parseFloat(subtotal) - parseFloat(precoEvento)) - parseFloat(desconto));
				$("#valorTotalFinal").val(precoEvento);
		        console.log('Evento desmarcado');
		        $("#dadosEventoSASselecionado").fadeOut();
		    }
		    
		});
		
		
	}
	

}

