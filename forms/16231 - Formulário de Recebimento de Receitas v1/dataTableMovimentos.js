/*Retorna todos os movimentos gerados na solicitação em curso.
 * Utiliza o campo #movimentoGeradoRM -  os valores estão separados por ','*/
function retornaMovimentos(movimentos){
	let valores = movimentos.split(',');
	let counter = valores.length;
	let movimento;
	let retorno;
	if(counter){
		for(var i = 0; i < counter; i++){
			if(valores[i] != ''){
				 movimento = DatasetFactory.createConstraint('IDMOV', valores[i], valores[i], ConstraintType.MUST);
				 retorno = DatasetFactory.getDataset('dsConsultaMovNative', null, [movimento], null);
				 
				 if(retorno.values.length){
					 for(var x = 0; x < retorno.values.length; x++){
						var numero = x;
						var tipoMov = retorno.values[x].CODTMV;
						var dataTemp = retorno.values[x].DATAEMISSAO;
						var data1 = dataTemp.split('T')[0];
						var dataMov = data1.split('-').reverse().join('/');
						var comprovanteMov = retorno.values[x].SEGUNDONUMERO;
						var situacaoMov = retorno.values[x].STATUS;
						if(situacaoMov == 'A'){
							situacaoMov = 'A Faturar';
						}
						var codigoMov = retorno.values[x].IDMOV;
						var valorMov = retorno.values[x].VALORBRUTO;
						
						$("#corpoMovimentos").append('<tr id="row_'+codigoMov+'">'+
								'<td > <input type="checkbox" class="checkMovimento" id="movimento_'+codigoMov+'"></td>'+
								'<td>'+codigoMov+'</td>'+
								'<td>'+tipoMov+'</td>'+
								'<td>'+situacaoMov+'</td>'+
								'<td>'+dataMov+'</td>'+
								'<td>'+comprovanteMov+'</td>'+
								
								'<td>'+valorMov+'</td>'+
								'</tr>'
								
						);
						
						$(".checkMovimento").click(function(){
							//quando clicado, ler todos os campos com status clicado e substituir o valor de #movimentosFaturar ---	
							console.log('Checkbox selecionado->' + $(this).attr("id"));
							let id = $(this).attr("id");
							
							if($("#"+id).is(":checked")){
								$("#row_"+id.split('_')[1]).attr('class', 'success');
								capturaSelecionados();
							}else{
								$("#row_"+id.split('_')[1]).removeAttr('class', 'success');
								capturaSelecionados();
							}
							
							
						});
						
					 }
					 
				 }
			}
		}
		
		
	}	
	
}

/*Auxiliar para funcao retornaMovimentos() 
 * Captura o id dos movimentos selecioandos e atribui ao campo movimentosFaturar
 */
function capturaSelecionados(){
    let counter = $(".checkMovimento").length;
    let selecionados = "";
    for(var s = 0; s < counter; s++){
        if($(".checkMovimento")[s].checked == true){
            selecionados += $(".checkMovimento")[s].id.split('_')[1] + ',';
        }
    }
    $("#movimentosFaturar").val(selecionados);
}
/*
 * Realiza o faturamento das receitas -- 
 * */
function faturarReceitas(){
	if($("#movimentosFaturar").val() == ''){
		FLUIGC.toast({
			     title: 'Atenção',
			     message: '<br>É necessário Selecionar ao menos 1 movimento' ,
			     type: 'warning'
				});	
	}else{
		let counter = $(".checkMovimento").length;
		let pointer = 0;
		let movimento
		for(var p = 0; p < counter; p++){
			if($(".checkMovimento")[p].checked == true){
				pointer += 1;
				movimento = $(".checkMovimento")[p].id.split('_')[1]
				geraFaturamento(movimento);
			}
		}
		
	}
	
}


/*************************Movimento 2.1.05  ********************************/

function geraFaturamento(idMovimento){	
	let myLoading = FLUIGC.loading(window);		
	myLoading.show();
	let _CODCOLIGADA,_IDMOV,_CODTMV,_CODFILIAL,_CODCOLCFO,_DATAEMISSAO,_DATASAIDA,_DATAENTREGA,_DATAMOVIMENTO,_CODLOC,_SEGUNDONUMERO,_CODCOLCXA;
	let _CODCXA,_CODCPG,_CODTB1FLX,_CODTB2FLX,_HISTORICOCURTO,_IDPRD,_QUANTIDADE,_PRECOUNITARIO,_CODTB1FAT,_CODUND,_QUANTIDADETOTAL,_NSEQITMMOV;
	let _CODCCUSTO,_IDMOVRATCCU,_CAMPOLIVRE3,_CAMPOLIVRE2, _CODCFO;
	
	let cst1 = DatasetFactory.createConstraint('IDMOV', idMovimento, idMovimento, ConstraintType.MUST);
	let retornaMovimento = DatasetFactory.getDataset('dsConsultaMovNative', null, [cst1], null);
	
	if(retornaMovimento.values.length){
		
		_CODCOLIGADA = retornaMovimento.values[0].CODCOLIGADA;
		//_IDMOV = retornaMovimento.values[0].IDMOV;
		_IDMOV = '-1'
		//_CODTMV = retornaMovimento.values[0].CODTMV;
		_CODTMV = "2.1.05";
		_CODFILIAL = retornaMovimento.values[0].CODFILIAL;
		//_CODCOLCFO = retornaMovimento.values[0].CODCOLCFO;
		_CODCOLCFO = 1;
		_CODCFO = $("#cliForForn").val();// Falta avaliar natureza para trazer campo correto--
		_DATAEMISSAO = retornaMovimento.values[0].DATAEMISSAO;//ok
		_DATASAIDA = retornaMovimento.values[0].DATASAIDA; //ok
		//_DATAENTREGA = retornaMovimento.values[0].DATAENTREGA;// em branco - checar
		//_DATAMOVIMENTO = retornaMovimento.values[0].DATAMOVIMENTO; // em branco - checar
		_DATAENTREGA = retornaMovimento.values[0].DATASAIDA;// em branco - checar
		_DATAMOVIMENTO = retornaMovimento.values[0].DATAEMISSAO; // em branco - checar
		_CODLOC = retornaMovimento.values[0].CODLOC; //ok 
		_SEGUNDONUMERO = retornaMovimento.values[0].SEGUNDONUMERO; // ok 
		//_CODCOLCXA = retornaMovimento.values[0].CODCOLCXA;
		_CODCOLCXA = 1;												// ok fixo --
		//_CODCXA = retornaMovimento.values[0].CODCXA;
		_CODCXA = '158';											// ok fixo --
		_CODCPG = retornaMovimento.values[0].CODCPG;				//forma de pagamento -- retornar do movimento pai - ok 
		_CODTB1FLX = retornaMovimento.values[0].CODTB1FLX;			//relacionado ao pagamento - 01.02.0001 reotornar do movimento pai - ok 
		_CODTB2FLX = retornaMovimento.values[0].CODTB2FLX;			//relacionado ao pagamento - 05, 14 reotornar do movimento pai - ok 
		_HISTORICOCURTO = retornaMovimento.values[0].HISTORICOCURTO;  // ok 
		//_IDPRD = retornaMovimento.values[0].IDPRD;
		_IDPRD = "607";												// ok fixo
		//_QUANTIDADE = retornaMovimento.values[0].QUANTIDADE;
		_QUANTIDADE = 1;											// ok fixo
		_PRECOUNITARIO = retornaMovimento.values[0].VALOROUTROSORIG;  	 // ok 
		//_CODTB1FAT = retornaMovimento.values[0].CODTB1FAT;
		_CODTB1FAT = retornaProduto('consultoria'); // Necessita validar a natureza do form --- 
		//_CODUND = retornaMovimento.values[0].CODUND;
		_CODUND = "UN";												// ok fixo --- 
		//_QUANTIDADETOTAL = retornaMovimento.values[0].QUANTIDADETOTAL;
		_QUANTIDADETOTAL = 1;										// ok fixo
		_NSEQITMMOV = 1; 											// ok fixo
		_CODCCUSTO = '00996.000008.003';							// ok fixo
		_IDMOVRATCCU = '-1';
		_CAMPOLIVRE3 = 'Código do Serviço SAS ou Consultoria SGF' ; // ok 
		_CAMPOLIVRE2 = 'Tipo de Evento Outros, sem insc. SAS ';	 // ok 

	}
	
	setTimeout(function(){							
				var citens = new Array();
				citens.push(DatasetFactory.createConstraint("CODCOLIGADA", _CODCOLIGADA, _CODCOLIGADA, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("IDMOV", _IDMOV, _IDMOV, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODTMV", _CODTMV, _CODTMV, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODFILIAL", _CODFILIAL, _CODFILIAL, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODCOLCFO", _CODCOLCFO, _CODCOLCFO, ConstraintType.MUST));		
				citens.push(DatasetFactory.createConstraint("CODCFO", _CODCFO, _CODCFO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("DATAEMISSAO", _DATAEMISSAO, _DATAEMISSAO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("DATASAIDA", _DATASAIDA, _DATASAIDA, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("DATAENTREGA", _DATAENTREGA, _DATAENTREGA, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("DATAMOVIMENTO", _DATAMOVIMENTO, _DATAMOVIMENTO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODLOC", _CODLOC, _CODLOC, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("SEGUNDONUMERO", _SEGUNDONUMERO, _SEGUNDONUMERO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODCOLCXA", _CODCOLCXA, _CODCOLCXA, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODCXA", _CODCXA, _CODCXA, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODCPG", _CODCPG, _CODCPG, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODTB1FLX", _CODTB1FLX, _CODTB1FLX, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODTB2FLX", _CODTB2FLX, _CODTB2FLX, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("HISTORICOCURTO", _HISTORICOCURTO, _HISTORICOCURTO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("IDPRD", _IDPRD, _IDPRD, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("QUANTIDADE", _QUANTIDADE, _QUANTIDADE, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("PRECOUNITARIO", _PRECOUNITARIO, _PRECOUNITARIO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODTB1FAT", _CODTB1FAT, _CODTB1FAT, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODUND", _CODUND, _CODUND, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("QUANTIDADETOTAL", _QUANTIDADETOTAL, _QUANTIDADETOTAL, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("NSEQITMMOV", _NSEQITMMOV, _NSEQITMMOV, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CODCCUSTO", _CODCCUSTO, _CODCCUSTO, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("IDMOVRATCCU", _IDMOVRATCCU, _IDMOVRATCCU, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CAMPOLIVRE3", _CAMPOLIVRE3, _CAMPOLIVRE3, ConstraintType.MUST));
				citens.push(DatasetFactory.createConstraint("CAMPOLIVRE2", _CAMPOLIVRE2,_CAMPOLIVRE2, ConstraintType.MUST)); 				
				console.log(citens);
				//citens.push(DatasetFactory.createConstraint("ITENS", xmlItens, xmlItens, ConstraintType.MUST)); // o xml <--- 
				var dts = DatasetFactory.getDataset("dsInsereMovWSNative2", null, citens, null); 				
				dts; 				
				if(dts.values[0]["Result"].split(";")[0] == 1){ 					
					$("#faturamentoGeradoRM").val(dts.values[0]["Result"].split(";")[1]);
					$("#codigoFatRmGer").val(dts.values[0]["Result"].split(";")[1]);
	 				FLUIGC.toast({
	                    title: 'Faturamento!',
	                    message: '<br>Faturamento Realizado Com Sucesso!' + dts.values[0]["Result"].split(";")[1],
	                    type: 'success'
	 				}); 	 				
	 				
	 				myLoading.hide();
				}else{ 					
					FLUIGC.toast({
	                    title: 'Movimento!',
	                    message: '<br>Não Foi Possível realizar o Faturamento!',
	                    type: 'danger'

	 					});
					
					myLoading.hide();
				} 				
	},3000);

}