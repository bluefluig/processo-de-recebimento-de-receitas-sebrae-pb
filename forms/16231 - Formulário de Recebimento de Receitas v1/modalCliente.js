/*JEan Varlet
 * Modal Cliente 
 * 
 * Retorna dados do cliente do SAS para gravar no RM
 */



//Jean Varlet -- 13/11/2019 --- Consultoria -> Remover Buscar Consultorias --- 

function fnRenderModal(cpfCpnj, valorCpf, tipo){
	cpfCpnj = cpfCpnj.trim().replace(' ', '');
	cpfCpnj = cpfCpnj.replace(/\./g,'');
	cpfCpnj = cpfCpnj.replace(/\.|\-|\//g,'');
	valorCpf = valorCpf.trim();
	var hidemyLoading = FLUIGC.loading('body');
	console.log(`fnRenderModal()->cpfCpnj->${cpfCpnj}  -  valorCpf->${valorCpf} `);	
	var codparc, nomecliente, codCFO, cgcrm, ierm, selcla1, selcla2, selcla3,  nomefantcliente, selcatF, selcatJ, codtcf, logradourocli, numerocli,  complemento;	
	var bairrocli, cidcli,  UFcli, CEPcli, telefone, email, celular, contato, CodParceiro, pagamentoRecebido, _counter;
	/*Consulta RM*/
	var c1rm = DatasetFactory.createConstraint("CGCCFO", valorCpf, valorCpf, ConstraintType.MUST);
	var constraintsRM = new Array(c1rm);
	var datasetRM = DatasetFactory.getDataset("dsClienteRM", null, constraintsRM, null);	
	var _counterRM = datasetRM.values.length;	
	
	/*Consulta SAS*/	
	if(cpfCpnj.length == 11){
		console.log('Valor Consulta -> cpfCnpj' + cpfCpnj.length);
		var constraint = new Array( DatasetFactory.createConstraint('CgcCpf', cpfCpnj, cpfCpnj, ConstraintType.MUST));
		//var _sasPF = DatasetFactory.getDataset('dsSASClientePF',null, constraint, null);
		var _sasPF = DatasetFactory.getDataset('dsSASSelecionarCliente',null, constraint, null);
		if(jQuery.isEmptyObject(_sasPF)){
		    //if(_sasPF.values == undefined){
			console.log('_sasPF.values == undefined');
			hidemyLoading.hide();
			toast("Aviso!", "Cliente não encontrado na base de dados do sistemas SAS", "danger");
		}else{
			console.log(_sasPF.values);
			_counter = _sasPF.values.length;
			CodParceiro = _sasPF.values[0].CodParceiro;	
		}
	}else if(cpfCpnj.length== 14){
		console.log('Valor Consulta -> cpfCnpj' + cpfCpnj.length);
		var constraint = new Array( DatasetFactory.createConstraint('CgcCpf', cpfCpnj, cpfCpnj, ConstraintType.MUST));
		//var _sasPF = DatasetFactory.getDataset('dsSASClientePJ',null, constraint, null);
		var _sasPF = DatasetFactory.getDataset('dsSASSelecionarCliente',null, constraint, null);
		if(jQuery.isEmptyObject(_sasPF)){
		    //if(_sasPF.values == undefined){
			console.log('_sasPF.values == undefined')
			hidemyLoading.hide();
			toast("Aviso!", "Cliente não encontrado na base de dados do sistemas SAS", "danger");
		}else{
			console.log(_sasPF.values);
			_counter = _sasPF.values.length;
			CodParceiro = _sasPF.values[0].CodParceiro;	
			
		}
		
	}	
	if(_counterRM > 0){
		console.log('Consulta RM -> ');		
		codCFO = datasetRM.values[0]['CODCFO'];		
		codparc = datasetRM.values[0]['CodParceiro'];
		nomecliente = datasetRM.values[0]['NOME'];
		nomefantcliente = datasetRM.values[0]['NOMEFANTASIA'];		
		pagamentoRecebido =  datasetRM.values[0]['PAGREC'];		
		if(datasetRM.values[0]['PAGREC'] == 1){			
			selcla1 = 'selected';
		}else if(datasetRM.values[0]['PAGREC'] == 2){			
			selcla2 = 'selected';
		}else if(datasetRM.values[0]['PAGREC'] == 3){			
			selcla3 = 'selected';
		}		
		if(datasetRM.values[0]['CIDADE'] == null || datasetRM.values[0]['CIDADE'] == 'null' 
			|| datasetRM.values[0]['CIDADE'] == undefined){			
			cidcli = '';				
		}else{			
			cidcli = datasetRM.values[0]['CIDADE'];
		}
		if(datasetRM.values[0]['NUMERO'] == null || datasetRM.values[0]['NUMERO'] == 'null'){			
			numerocli = '';			
		}else{			
			numerocli = datasetRM.values[0]['NUMERO'];			
		}
		if(datasetRM.values[0]['TELEX'] == null || datasetRM.values[0]['TELEX'] == 'null'){			
			celular = '';	
		}else{			
			celular = datasetRM.values[0]['TELEX'];			
		}		
		if(datasetRM.values[0]['RUA'] == null || datasetRM.values[0]['RUA'] == 'null'){			
			logradourocli = '';			
		}else{			
			logradourocli = datasetRM.values[0]['RUA'];			
		}
		if(datasetRM.values[0]['PESSOAFISOUJUR'] == 'J'){			
			selcatJ = 'selected';			
		}else if(datasetRM.values[0]['PESSOAFISOUJUR'] == 'F'){			
			selcatF = 'selected';
		}
		if(datasetRM.values[0]['CODCFO'] == null || datasetRM.values[0]['CODCFO'] == 'null'){
			codCFO = '-1';	
		}else{
			codCFO = datasetRM.values[0]['CODCFO'];
		}
		if(datasetRM.values[0]['CGCCFO'] == null || datasetRM.values[0]['CGCCFO'] == 'null'){
			cgcrm = '';	
		}else{
			cgcrm = datasetRM.values[0]['CGCCFO'];
		}		
			
		if(datasetRM.values[0]['INSCRESTADUAL'] == null || datasetRM.values[0]['INSCRESTADUAL'] == 'null'){			
			ierm = '';			
		}else{			
			ierm = datasetRM.values[0]['INSCRESTADUAL'];
		}
		if(datasetRM.values[0]['COMPLEMENTO'] == null || datasetRM.values[0]['COMPLEMENTO'] == 'null'){			
			complemento = '';			
		}else{			
			complemento =  datasetRM.values[0]['COMPLEMENTO'];			
		}		
		bairrocli = datasetRM.values[0]['BAIRRO'];
		UFcli = datasetRM.values[0]['CODETD'];		
		CEPcli = datasetRM.values[0]['CEP'];
		if(datasetRM.values[0]['TELEFONE'] == null || datasetRM.values[0]['TELEFONE'] == 'null'){			
			telefone = '';			
		}else{			
			telefone = datasetRM.values[0]['TELEFONE'];			
		}
		if(datasetRM.values[0]['EMAIL'] == null || datasetRM.values[0]['EMAIL'] ==  'null'){			
			email ='';			
		}else{			
			email =  datasetRM.values[0]['EMAIL'];			
		}
		if (datasetRM.values[0]['CODTCF'] == null || datasetRM.values[0]['CODTCF'] == 'null') {
			codtcf = '';
		} else {
			codtcf = datasetRM.values[0]['CODTCF'];
		}
		if(datasetRM.values[0]['CONTATO'] == null || datasetRM.values[0]['CONTATO'] == '' || datasetRM.values[0]['CONTATO'] == 'null'){			
			contato = '';			
		}else{			
			contato =  datasetRM.values[0]['CONTATO'];
		}			
	}else if(_counter == 1  && _counterRM == 0){
				
		codparc = _sasPF.values[0]['CodParceiro'];
		nomecliente = _sasPF.values[0]['NomeRazaoSocial'];
		if(_sasPF.values[0]['NomeAbrevFantasia'] == null || _sasPF.values[0]['NomeAbrevFantasia'] == 'null' || _sasPF.values[0]['NomeAbrevFantasia'] == ''){
			nomefantcliente = _sasPF.values[0]['NomeRazaoSocial'];
		}else{
			nomefantcliente = _sasPF.values[0]['NomeAbrevFantasia'];
		}
		cidcli = _sasPF.values[0]['DescCid'];
		//numerocli = _sasPF.values[0]['Complemento'];
		numerocli = '0';
		celular = _sasPF.values[0]['Celular'];
		logradourocli = _sasPF.values[0]['DescEndereco'];
		if(cpfCpnj.length == 11){
            selcatF = 'F';
		}else if(cpfCpnj.length == 14){
            selcatF = 'J';
		}else{
		    selcatF = '';//falta analizar 	
		}
		
		codCFO = "-1";
		cgcrm = cpfCpnj ;
		ierm = '';
		//complemento = '';
		complemento = _sasPF.values[0]['Complemento'];
		bairrocli = _sasPF.values[0]['DescBairro'];
		UFcli = 'PB';
		codtcf = '003';
		CEPcli = '';
		telefone = '';
		email = _sasPF.values[0]['Email'];
		contato = '';
		selcla1 = 'selected';		
	}else if (_counter == undefined || _counter == 'undefined'){		
		console.log('Cliente não encontrado na base de dados.');
		toast("Aviso!", "Cliente não encontrado na base de dados dos sistemas SAS & RM", "danger");
		return false;
	}	
	if(tipo == 'cliente'){
		//Cliente principal da solicitação
		if($("#tipoSerInstrutoria").is(':checked')== true){		
			$("#codSAS").val(CodParceiro);
			$("#codcfoCliente1").val(codCFO);
			$("#nomeCliente1").val(nomefantcliente);
			$("#razaoCliente1").val(nomecliente);
			
			$("#tipoServicoContratado").val("2"); // Não precisa assinatura
			if(selcatF == 'F'){			
				$("#tipoCliente1").val('Pessoa Física');
				$("#inscEstClienteSAS").val('Isento');			
			}else if(selcatF == 'J'){			
				$("#tipoCliente1").val('Pessoa Jurídica');
				$("#inscEstClienteSAS").val('0');			
			}else{
				//console.log('Seleção Pessoa Fisica Juridica retornou nenhum! ')
				$("#tipoCliente1").val('Pessoa Física');
			}
			if(_counterRM == 0){
	            $("#cepClienteSAS").val('');
				$("#ruaClienteSAS").val('');
				$("#bairroClienteSAS").val('');
				$("#cidadeClienteSAS").val(cidcli);
				$("#estadoClienteSAS").val('PB');
				$("#emailClienteSAS").val('');
				$("#contatoClienteSAS").val(celular);
				$("#telefoneClienteSAS").val(celular);		
				$("#identidadeClienteSAS").val('0');

			}else{
	            $("#cepClienteSAS").val(datasetRM.values[0]['CEP']);
				$("#ruaClienteSAS").val(datasetRM.values[0]['RUA']);
				$("#bairroClienteSAS").val(datasetRM.values[0]['BAIRRO']);
				$("#cidadeClienteSAS").val(datasetRM.values[0]['CIDADE']);
				$("#estadoClienteSAS").val(datasetRM.values[0]['CODETD']);
				$("#emailClienteSAS").val(datasetRM.values[0]['EMAIL']);
				$("#contatoClienteSAS").val(datasetRM.values[0]['CONTATO']);
				$("#telefoneClienteSAS").val(datasetRM.values[0]['TELEFONE']);
				$("#inscEstClienteSAS").val(datasetRM.values[0]['INSCRESTADUAL']);
				$("#identidadeClienteSAS").val(datasetRM.values[0]['CIDENTIDADE']);
			}
					
			$("#dadosClienteSelecionado1").fadeIn();
			$("#dadosClienteSelecionado2").fadeIn();
			$("#dadosClienteSAS1").fadeIn();
			$("#dadosClienteSAS2").fadeIn();
			$("#dadosClienteSAS3").fadeIn();
			$("#dadosClienteSAS4").fadeIn();	
			
		}else if($("#tipoSerConsultoria").is(':checked')== true){		
			$("#cliForForn").val(codCFO); // ok 
			$("#cliCodSas").val(CodParceiro);// ok - 
			
			$("#cnpjForn").val(cgcrm); // já tratado na condicional anterior
			
			$("#emailForn").val(email);
			if(_counterRM == 0){
	            $("#tipoForn").val('Cliente');
			}else{
				if(datasetRM.values[0].PAGREC == 1 ){
				$("#tipoForn").val('Cliente');
				}else if(datasetRM.values[0].PAGREC == 2 ){
					$("#tipoForn").val('Fornecedor');
				}else if(datasetRM.values[0].PAGREC == 3 ){
					$("#tipoForn").val('Colaborador');			
				}
			}		
			$("#estado").val(UFcli);
			if(_counterRM == 0){
	            $("#cidadeForn").val(cidcli);
				$("#endereco").val('');
				$("#numero").val('0');
				$("#bairro").val('');
				$("#razaoForn").val(nomecliente);
				$("#fantasiaForn").val(nomefantcliente);
				$("#responsavelForn").val(nomecliente);
			}else{
				
				$("#razaoForn").val(nomecliente);
				$("#fantasiaForn").val(nomefantcliente);
				$("#responsavelForn").val(nomecliente);

				$("#cidadeForn").val(datasetRM.values[0]['CIDADE']);
				$("#endereco").val(datasetRM.values[0]['RUA']);
				$("#numero").val(datasetRM.values[0]['NUMERO']);
				$("#bairro").val(datasetRM.values[0]['BAIRRO']);
			}			
			$("#tipoServicoContratado").val("1");// Não precisa assinatura		
			$("#exibeConsultoria").fadeIn();
			$("#dadosClienteSelecionado2").fadeIn();
		}else if($("#tipoSerOutros").is(':checked')== true){		
				
			$("#codSASOutros").val(CodParceiro);
			$("#codcfoClienteOutros1").val(codCFO);
			$("#nomeClienteOutros1").val(nomefantcliente);
			$("#razaoClienteOutros1").val(nomecliente);		
			//19/11/2019 ---Jean Varlet		
			$("#estadoClienteOutros").val(UFcli);
			if(_counterRM == 0){


			}else{
				if(datasetRM.values[0]['CIDADE'] != undefined && datasetRM.values[0]['CIDADE'] != null){
					$("#cidadeClienteOutros").val(datasetRM.values[0]['CIDADE']);		
				}else{
					$("#cidadeClienteOutros").val('Não informado');
				}
				if(datasetRM.values[0]['RUA'] != undefined && datasetRM.values[0]['RUA'] != null){
					$("#enderecoClienteOutros").val(datasetRM.values[0]['RUA']);
				}else{
					$("#enderecoClienteOutros").val('Não informada');
				}
				if(datasetRM.values[0]['NUMERO'] != undefined && datasetRM.values[0]['NUMERO'] != null){
					$("#numeroClienteOutros").val(datasetRM.values[0]['NUMERO']);
				}else{
					$("#numeroClienteOutros").val('0');
				}
				if(datasetRM.values[0]['BAIRRO'] != undefined && datasetRM.values[0]['BAIRRO'] != null){
					$("#bairroClienteOutros").val(datasetRM.values[0]['BAIRRO']);
				}else{
					$("#bairroClienteOutros").val('Não informado');
				}
				if(datasetRM.values[0]['EMAIL'] != undefined && datasetRM.values[0]['EMAIL'] != null){
					$("#emailClienteOutros").val(datasetRM.values[0]['EMAIL']);
				}else{
					$("#emailClienteOutros").val('Não informado');
				}
			}

			
			
			
			//Jean Varlet 25/11/2019    	
			$("#cpfconsultaFornecedor").val($("#cpfconsultasasOutros").val());
			$("#tipoServicoContratado").val("2"); // Não precisa assinatura
			if(selcatF == 'F'){			
				$("#tipoClienteOutros1").val('Pessoa Física');			
			}else if(selcatF == 'J'){			
				$("#tipoClienteOutros1").val('Pessoa Jurídica');			
			}		
			$("#dadosClienteOUtrosSelecionado1").fadeIn();
			$("#dadosClienteOutrosSelecionado2").fadeIn();
			$("#ProjetoAcaoSelecionar").fadeIn();
			$("#dadosServicoOutros2").fadeIn();
			$("#tipoServicoOutros").fadeIn();		
		}	
		
	}else if(tipo == 'vinculada'){
		//Empresa do Cliente vinculada para faturamento ---
		console.log('fnRenderModal()-> ' + tipo)
		
	}

var modalCliente = FLUIGC.modal({
			title: 'Cadastrar Cliente no RM',
			content: 		
				'<div class="panel panel-primary">' +
			//	'<div class="panel-heading">' +
			//	'<h3 class="panel-title">Eventos para o Cliente: ' + codparc + ' - ' + nomecliente + ' </h3>' +
			//	'</div>' +
				'<div class="panel-body">' +
				'<div class="row">' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtCodCfo">Código no RM</label>' +
				'<input type="text" name="txtCodCfo" id="txtCodCfo" class="form-control" readonly value="' + codCFO + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtmCGC">CPF/CNPJ</label>' +
				'<input type="text" name="txtmCGC" id="txtmCGC" class="form-control" readonly="readonly" value="' + cgcrm + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtIE">Inscrição Estadual</label>' +
				'<input type="text" name="txtIE" id="txtIE" class="form-control" value="' + ierm + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtClassificacao">Classificação</label>' +
				'<select name="txtClassificacao" class="form-control" id="txtClassificacao">' +
				'<option value="1" ' + selcla1 + ' >Cliente</option>' +
				'<option value="2" ' + selcla2 + ' >Fornecedor</option>' +
				'<option value="3" ' + selcla3 + ' >Ambos</option>' +
				'</select>' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtCategoria">Categoria</label>' +
				'<select name="txtCategoria" class="form-control" id="txtCategoria" >' +
				'<option value="F" ' + selcatF + ' >Pessoa Fisica</option>' +
				'<option value="J" ' + selcatJ + ' >Juridica</option>' +
				'</select>' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtTipoCliFor">Cliente/Fornecedor</label>' +
				//'<select name="txtTipoCliFor" class="form-control" id="txtTipoCliFor" dataset="dsTipoClienteFornecedor" datasetkey="CODTCF" datasetvalue="DESCRICAO"></select>' +
				'<input type="text" name="txtTipoCliFor" class="form-control" id="txtTipoCliFor" readonly value="' + codtcf + '">' +

				'</div>' +
				'</div>' + //end first row
				'<div class="row">' +
				'<div class="form-group col-md-6">' +
				'<label class="control-label" for="txtRazaoSocial">Razão Social</label>' +
				'<input type="text" name="txtRazaoSocial" id="txtRazaoSocial" class="form-control required" data-validarcampo required="required" value="' + nomecliente + '">' +
				'</div>' +
				'<div class="form-group col-md-6">' +
				'<label class="control-label" for="txtFantasia">Nome Fantasia</label>' +
				'<input type="text" name="txtFantasia" id="txtFantasia" class="form-control required" data-validarcampo required="required" value="' + nomefantcliente + '">' +
				'</div>' +
				'</div>' + //end second row
				
				
				'<div class="panel-heading panel-primary" style=" border-bottom-width: 1px;margin-bottom: 10px;">' +
				'<h5 class="panel-title">Endereço do Cliente</h5>' +
				'</div>' +
				'<div class="row">' +
				'<div class="form-group col-md-6">' +
				'<label class="control-label" for="txtlogradouro">Rua</label>' +
				'<input type="text" name="txtlogradouro" id="txtlogradouro" class="form-control" value="' + logradourocli + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtnumero">Numero</label>' +
				'<input type="text" name="txtnumero" id="txtnumero" class="form-control" value="' + numerocli + '">' +
				'</div>' +
				'<div class="form-group col-md-4">' +
				'<label class="control-label" for="txtcomplemento">Complemento</label>' +
				'<input type="text" name="txtcomplemento" id="txtcomplemento" class="form-control" value="' + complemento + '">' +
				'</div>' +
				'</div>' +
				'<div class="row">' +
				'<div class="form-group col-md-4">' +
				'<label class="control-label" for="txtbairro">Bairro</label>' +
				'<input type="text" name="txtbairro" id="txtbairro" class="form-control" value="' + bairrocli + '">' +
				'</div>' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txtcidade">Cidade</label>' +
				'<input type="text" name="txtcidade" id="txtcidade" class="form-control" value="' + cidcli + '">' +
				'</div>' +
				'<div class="form-group col-md-2">' +
				'<label class="control-label" for="txtUF">UF</label>' +
				'<input type="text" name="txtUF" id="txtUF" class="form-control" value="' + UFcli + '">' +
				'</div>' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txtCEP">CEP</label>' +
				'<input type="text" name="txtCEP" id="txtCEP" class="form-control" onBlur="validaCEP(this.value)" value="' + CEPcli + '">' +
				'</div>' +
				'</div>' +
				'<div class="row">' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txttelefone">Telefone</label>' +
				'<input type="text" name="txttelefone" id="txttelefone" class="form-control" value="' + telefone + '">' +
				'</div>' +
				'<div class="form-group col-md-3">' +
				'<label class="control-label" for="txtcelular">Celular</label>' +
				'<input type="text" name="txtcelular" id="txtcelular" class="form-control" value="' + celular + '">' +
				'</div>' +
				'<div class="form-group col-md-5">' +
				'<label class="control-label" for="txtemail">e-mail</label>' +
				'<input type="text" name="txtemail" id="txtemail" class="form-control" value="' + email + '">' +
				'</div>' +
				'<div class="form-group col-md-4">' +
				'<label class="control-label" for="txtcontato">Contato</label>' +
				'<input type="text" name="txtcontato" id="txtcontato" class="form-control" value="' + contato + '">' +
				'</div>' +
				'</div>' +
				'</div>' +
				
				'</div>',
			id: 'modal-cliente-evento' ,
			size: 'full',
			actions: [{
				'label': 'Salvar/Atualizar',
				'bind': 'data-confdadoscli',
			}, {
				'label': 'Fechar',
				'autoClose': true,
				'bind': 'data-fecharcli'
			}]
		},
			function (err, data) {
			});
		validacgc("txtmCGC", $("#txtmCGC").val());		
		$('.modal-body').css("max-height", window.innerHeight / 1.5 + 'px');
		$(".btn-primary", $(".modal-footer")).click(function (){		
			fnSalvaCliente();
		});
		//adicionarEmpresaCliente();
}
// Function que retorna os dados do cliente --- Cria o cabeçalho da solicitação --- Para as 3 naturezas
function fnExibeCadastroCliente(CodParceiro, codCFO, nomecliente, nomefantcliente,  selcatF, selcatJ, bairrocli,  UFcli, CEPcli, telefone, email, cidcli, logradourocli, pagamentoRecebido){	
	//Se for do tipo Evento SAS->
	if($("#tipoSerInstrutoria").is(':checked')== true){		
		$("#codSAS").val(CodParceiro);
		$("#codcfoCliente1").val(codCFO);
		$("#nomeCliente1").val(nomefantcliente);
		$("#razaoCliente1").val(nomecliente);		
		$("#tipoServicoContratado").val("2"); // Não precisa assinatura
		if(selcatF == 'F'){			
			$("#tipoCliente1").val('Pessoa Física');			
		}else if(selcatF == 'J'){			
			$("#tipoCliente1").val('Pessoa Jurídica');			
		}		
		$("#dadosClienteSelecionado1").fadeIn();
		$("#dadosClienteSelecionado2").fadeIn();		
	}else if($("#tipoSerConsultoria").is(':checked')== true){		
		$("#cliForForn").val(CodParceiro);
		$("#cnpjForn").val(codCFO);
		$("#razaoForn").val(nomefantcliente);
		$("#fantasiaForn").val(nomecliente);
		$("#responsavelForn").val(nomecliente);
		$("#emailForn").val(email);
		if(pagamentoRecebido == 1 ){
			$("#tipoForn").val('Cliente');
		}else if(pagamentoRecebido == 2 ){
			$("#tipoForn").val('Fornecedor');
		}else if(pagamentoRecebido == 3 ){
			$("#tipoForn").val('Colaborador');			
		}
		// pagrec -- 1 ou 3 
		$("#estadoForn").val(UFcli);
		$("#cidadeForn").val(cidcli);
		$("#endereco").val(logradourocli);	
		$("#tipoServicoContratado").val("1");// Não precisa assinatura	
		$("#exibeConsultoria").fadeIn();
		$("#dadosClienteSelecionado2").fadeIn();	
	}else if($("#tipoSerOutros").is(':checked')== true){		
		console.log('fnExibeCadastroCliente(...)-> tipoSerOutros');	
	}
	
}

