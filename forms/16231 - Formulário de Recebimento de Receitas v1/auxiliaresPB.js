
function sfQtdAditivo(oField) {
		var nPos 			= oField.name.lastIndexOf('_');
	var ciFldQtdVen		= "chMesMonitAdit_" + oField.name.substr(nPos-1, 5);
	var ciFldVlrTotal	= "chTotalAdit_" + oField.name.substr(nPos-1, 5);
	
	var niQtdVen 		= document.all[ciFldQtdVen].value;
	var niVlrTotal 		= niQtdVen;

	var ciVlrTotal 		= sfFormatZeros(niVlrTotal);
	
	document.all[ciFldVlrTotal].value = sfFormatZeros(niVlrTotal);
	sfSubTotalAditivo();
}

function sfTotalPrevisto(ciNomeTlb){
	var vlrTotal 		= Number(document.getElementById("cargaHorariaTotal").value);
	var vlrDesconto 	= Number(document.getElementById("valorHora").value);
	
	var vlrTotDesconto	= parseFloat(vlrTotal * vlrDesconto).toFixed(2);
	
	document.getElementById("cargaHorariaValorTotal").value	 = vlrTotDesconto;
};

function sfTotalContratado(ciNomeTlb){
	var vlrTotal 		= Number(document.getElementById("cargaHorariaTotal").value);
	var vlrHora 		= Number(document.getElementById("valorHoraContrat").value);
	
	var vlrTotHora		= parseFloat(vlrTotal * vlrHora).toFixed(2);
	
	document.getElementById("cargaHorariaValorTotContrat").value = vlrTotHora;
};
function sfSubTotal() {
	var niCont 			= 0;
	var niNumberVlrTot 	= 0;
	var ciCpoVlrTotal	= "chTotal___";
	var oiObjField 		= document.all[ciCpoVlrTotal + niCont];
	// Fixado em 80 para poder tratar o subtotal, poder ser alterado, conforme a
	// necessidade
	while(niCont < 80) {
		niCont 			= niCont+1;
		oiObjField 		= document.all[ciCpoVlrTotal + niCont];
				
		if(oiObjField != null) {					
			niNumberVlrTot += sfStringToNumber(oiObjField.value);	
		}
	} 
	document.all["cargaHorariaTotal"].value = sfFormatZeros(niNumberVlrTot);
};
function sfQtd(oField) {
		var nPos 			= oField.name.lastIndexOf('_');
	var ciFldQtdVen		= "chMes_" + oField.name.substr(nPos-1, 5);
	var ciFldVlrTotal	= "chTotal_" + oField.name.substr(nPos-1, 5);
	
	var niQtdVen 		= document.all[ciFldQtdVen].value;
	var niVlrTotal 		= niQtdVen;

	var ciVlrTotal 		= sfFormatZeros(niVlrTotal);
	
	document.all[ciFldVlrTotal].value = sfFormatZeros(niVlrTotal);
	
	sfSubTotal();
	};
function fnCustomDeleteAditivo(oElement){
    fnWdkRemoveChild(oElement);
};

function sfTableCargaHoraria(ciNomeTlb){
	// Cria a Linha da tabela (wdkAddChild padrao) e tambem captura o numero da
	// linha posicionada
	var niLinha	=	wdkAddChild(ciNomeTlb);

	var ciTabela	= document.all["tiposervico"].value;
	var niCont 		= 0;
	var ciCpoProd	= "rm_categoria___"+niLinha;
	var oiObjField 	= document.all[ciCpoProd];

	if(oiObjField != null)  {
		reloadZoomFilterValues(ciCpoProd, 'CODIGOGRUPO,'+ciTabela);
	}
};
function doLineTotalRateio() {
    var index = this.id.substring(this.id.indexOf('___') + 3);

    var chMes = $("#cpRateioPercent___" + index).val();

    if (chMes) {
        var total = (sfStringToNumber(chMes)).toFixed(2);
        $("#cpRateioTot___" + index).val(total);
        doFormTotalRateio();
    }    
};
function doLineTotal() {
    var index = this.id.substring(this.id.indexOf('___') + 3);

    var chMes = $("#chMes___" + index).val();

    if (chMes) {
        var total = (sfStringToNumber(chMes)).toFixed(2);
        $("#chTotal___" + index).val(total);
        doFormTotal();
    }    
};
function init() {
    $("input[id^='chMes___']:last").blur(doLineTotal);
}
function initRateio() {
    $("input[id^='cpRateioPercent___']:last").blur(doLineTotalRateio);
}

function fnCustomDelete(oElement){
    fnWdkRemoveChild(oElement);
    doFormTotal();
};

function doFormTotal() {
    var total = 0;
    $("input[id^='chTotal___']").each(function() {
        if ($(this).val()) {
            total += sfStringToNumber($(this).val()); 
        }
    });
    $("#cargaHorariaTotal").val(total.toFixed(2));
    // $("#cargaHorariaValorTotal").val(total.toFixed(2));
};

function setSelectedZoomItem(selectedItem) {
	
	var gerente = selectedItem["GERENTE"];
	// var gerenteusuario = selectedItem["GERENTEUSUARIO"];
	var gerenteusuario = selectedItem["GERENTEMAT"];
	var diretor = selectedItem["DIRETOR"];
	var agencia = selectedItem["SECAO"];
	var codfilial = selectedItem["CODFILIAL"];
	var codven = selectedItem["CODVEN"];
	var coddepto = selectedItem["CODDEPTO"];
	var depto = selectedItem["DEPTO"];
	var chapadiretor = selectedItem["CHAPADIRETOR"];
	var chapa = selectedItem["CHAPA"];
	
	var codigoprojeto = selectedItem["CODPROJETO"];
	var codigoacao = selectedItem["CODACAO"];
	var unidade = selectedItem["UNIDADE"];
	var codunidade = selectedItem["CODUNIDADE"];
	var saldoprojeto = selectedItem["SALDOFIN"];
	
	var fcfonomefantasia = selectedItem["NOMEFANTASIA"];
	var fcfonome= selectedItem["NOME"];
	var fcfotelefone = selectedItem["TELEFONE"];
	var fcfoemail = selectedItem["EMAIL"];
	var fcfocgc = selectedItem["CGCCFO"];
	var fcfocodcfo = selectedItem["CODCFO"];
	var fcfocolig = selectedItem["CODCOLIGADA"];
	
	var codcpg = selectedItem["CODCPG"];
	
	var fcfonomefantasiaf = selectedItem["NOMEFANTASIAF"];
	var fcfonomef = selectedItem["NOMEF"];
	var fcfotelefonef = selectedItem["TELEFONEF"];
	var fcfoemailf = selectedItem["EMAILF"];
	var fcfocgcf = selectedItem["CGCCFOF"];
	var fcfocodcfof = selectedItem["CODCFOF"];
	var fcfocoligf = selectedItem["CODCOLIGADAF"];
	var fcfotipocliforn = selectedItem["TIPOCLIFORNF"];
	var fcfoend = selectedItem["ENDERECOF"];
	var fcfocont = selectedItem["CONTATOF"];
	
	var idarea = selectedItem["IDAREA"];
	
	var zoomitm = selectedItem.inputName;			
	var ClienteLinha = zoomitm.substring(0,7);
	var ProjetoLinha = zoomitm.substring(0,9);
	var AcaoLinha = zoomitm.substring(0,6);
	
	if (selectedItem.inputId == "rm_dadoscolab"){
			/*
			document.getElementById("tbGerente").value = gerente;
			document.getElementById("tbGerenteUsuario").value = gerenteusuario;
			document.getElementById("tbDiretor").value = diretor;
			document.getElementById("tbAgencia").value = agencia;
			document.getElementById("tbFilial").value = codfilial;
			document.getElementById("tbCodVen").value = codven;
			document.getElementById("rm_depto").value = depto;
			document.getElementById("rm_coddepto").value = coddepto;
			document.getElementById("dadomat01").value = chapa;
			*/
		
			var chapaGerente1 = selectedItem["CHAPAGERENTE"];
			if(chapaGerente1 == null){
				chapaGerente1 = '0';
			}

			document.getElementById("tbGerente").value = selectedItem["GERENTE"];
			document.getElementById("tbGerenteUsuario").value = chapaGerente1
			document.getElementById("tbDiretor").value = selectedItem["DIRETOR"];
			document.getElementById("tbAgencia").value = selectedItem["SECAO"];
			document.getElementById("tbFilial").value = selectedItem["CODFILIAL"];
			document.getElementById("tbCodVen").value = selectedItem["CODVEN"];
			document.getElementById("rm_depto").value = selectedItem["CODDEPTO"];
			document.getElementById("rm_coddepto").value = selectedItem["DEPTO"];
			document.getElementById("dadomat01").value = selectedItem["CHAPA"];
		    document.getElementById("tbNomePessoa").value = selectedItem["NOME"]; //tbCodPessoa
            document.getElementById("tbCodPessoa").value = selectedItem["CHAPA"];
            document.getElementById("tbDiretorUsuario").value = selectedItem["DIRETOR"];
            document.getElementById("dadomat01").value = selectedItem["CHAPA"];
		    debuginho();
			
	} else if (selectedItem.inputId == "areaconh") {
		
		var zoomField = document.getElementById("subareaconh");
		var length = zoomField.options.length;
		for ( i = 0; i < length; i++ ) { 
			zoomField.options[i].remove(); 
			
		}
		
		document.getElementById("idareaconh").value = idarea;
		reloadZoomFilterValues("subareaconh", "IDAREA," + idarea);
	} else if (ClienteLinha == "rm_fcfo") {
		
		var cLinha = selectedItem.inputName.substring(7,selectedItem.inputName.length);
		
		var fcfonomefantasia_o = selectedItem["NOMEFANTASIA"];
		var fcfonome_o = selectedItem["NOME"];
		var fcfotelefone_o = selectedItem["TELEFONE"];
		var fcfoemail_o = selectedItem["EMAIL"];
		var fcfocgc_o = selectedItem["CGCCFO"];
		var fcfocodcfo_o = selectedItem["CODCFO"];
		var fcfocolig_o = selectedItem["CODCOLIGADA"];
		
		document.getElementById("rm_fcfonome" + cLinha).value = fcfonomefantasia_o;
		document.getElementById("rm_fcforazao" + cLinha).value = fcfonome_o;
		document.getElementById("rm_fcfotelefone" + cLinha).value = fcfotelefone_o;
		document.getElementById("rm_fcfoemail" + cLinha).value = fcfoemail_o;
		document.getElementById("rm_fcfocgc" + cLinha).value = fcfocgc_o;
		document.getElementById("rm_fcfocodcfo" + cLinha).value = fcfocodcfo_o;
		document.getElementById("rm_fcfocolig" + cLinha).value = fcfocolig_o;
	} else if ( ProjetoLinha == "rmprojeto" ){
		var cLinha = selectedItem.inputName.substring(9,selectedItem.inputName.length);
		console.log(cLinha);
		var codigoprojeto_o = selectedItem["CODPROJETO"];
		console.log(codigoprojeto_o);
		var saldoprojeto_o = selectedItem["SALDOFIN"];
		console.log(saldoprojeto_o);
		reloadZoomFilterValues("rmacao" + cLinha, "CODPROJETOACAO," + codigoprojeto_o);
		document.getElementById("rm_saldoprojeto" + cLinha).value = saldoprojeto_o;
	} else if ( AcaoLinha == "rmacao" ){
		var cLinhaA = selectedItem.inputName.substring(6,selectedItem.inputName.length);
		var unidade_o = selectedItem["UNIDADE"];
		var codunidade_o = selectedItem["CODUNIDADE"];
		document.getElementById("rmunidade" + cLinhaA).value = unidade_o;
		document.getElementById("rm_codunidade" + cLinhaA).value = codunidade_o;
	} else if ( selectedItem.inputId == "rmrecurso" ){
		document.getElementById("rm_codrecurso").value = codrecurso;
	} else if ( selectedItem.inputId == "tipoPgtoRecebimento" ) {
		document.getElementById("codtipoPgtoRecebimento").value = codcpg;
	} else if (selectedItem.inputId == "rm_fornfcfo") {
		document.getElementById("rm_fcfonomeForn").value = fcfonomefantasiaf;
		document.getElementById("rm_fcforazaoForn").value = fcfonomef;
		document.getElementById("rm_fcfotelefoneForn").value = fcfotelefonef;
		document.getElementById("rm_fcfoemailForn").value = fcfoemailf;
		// document.getElementById("rm_fcfocgcForn").value = fcfocgcf;
		document.getElementById("rm_fcfocodcfoForn").value = fcfocodcfof;
		document.getElementById("rm_fcfocoligForn").value = fcfocoligf;
		document.getElementById("rm_fcfoTipo").value = fcfotipocliforn;
		console.log(fcfoend);
		document.getElementById("rm_fcfoendForn").value = fcfoend;
		document.getElementById("rm_fcfoConsultorRespForn").value = fcfocont;
		}
}
function calculaPercent(oField){
		var nPos 			= oField.name.lastIndexOf('_');
		var pEarned = parseInt($('#cpRateioValor_' + oField.name.substr(nPos-1, 5)).val());
     var pPos = parseInt($('#cargaHorariaValorTotal').val());
     var perc="";
     if(isNaN(pPos) || isNaN(pEarned)){
        perc=" ";
     } else {
        perc = ((pEarned/pPos) * 100).toFixed(2);
     }
     
     $('#cpRateioPercent_' + oField.name.substr(nPos-1, 5)).val((perc));
	};
	
	function sfQtdRateio(oField) {		        	
	var nPos 			= oField.name.lastIndexOf('_');
	var ciFldQtdVen		= "cpRateioPercent_" + oField.name.substr(nPos-1, 5);
	var ciFldVlrTotal	= "cpRateioTot_" + oField.name.substr(nPos-1, 5);
	
	var niQtdVen 		= document.all[ciFldQtdVen].value;
	var niVlrTotal 		= niQtdVen;

	var ciVlrTotal 		= sfFormatZeros(niVlrTotal);
	
	document.all[ciFldVlrTotal].value = sfFormatZeros(niVlrTotal);
	
	sfSubTotalRateio();
	};
	
	function sfSubTotalRateio() {
	var niCont 			= 0;
	var niNumberVlrTot 	= 0;
	var ciCpoVlrTotal	= "cpRateioTot_";
	var oiObjField 		= document.all[ciCpoVlrTotal + niCont];
	// Fixado em 80 para poder tratar o subtotal, poder ser alterado, conforme a
	// necessidade
	while(niCont < 80) {
		niCont 			= niCont+1;
		oiObjField 		= document.all[ciCpoVlrTotal + niCont];
				
		if(oiObjField != null) {					
			niNumberVlrTot += sfStringToNumber(oiObjField.value);	
		}
	} 
	document.all["numberVlrTotRateio"].value = sfFormatZeros(niNumberVlrTot);
};

function sfFormatZeros(nNumber) {
	var cNumber = nNumber.toString().replace(".", ",");
	var nPos = cNumber.search(',');
	var nDif = cNumber.length - nPos;
	if(nPos == -1)
		cNumber = cNumber+".00";
	else if( nDif == 2 )
		cNumber = cNumber+"0";
	else if( nDif >= 4 )
		cNumber = cNumber.substr(0, cNumber.length - nDif + 3);
	return cNumber;
};

function sfStringToNumber(cNumber) {
	return parseFloat(cNumber.replace(",", "."));
};

/* Função que busca o Cliente pelo cpf/cnpj */
function atualizaConsultaCnpj(oField){
	var nPos = oField.name.lastIndexOf('_');
	
	var oiObjField = document.getElementById("cpfconsulta_" + oField.name.substr(nPos-1, 5)).value;
	
	if(oiObjField != null){
		reloadZoomFilterValues("rm_fcfo_" + oField.name.substr(nPos-1, 5), 'CGCCFO,'+oiObjField);
	}
}

/* Função que busca o Fornecedor pelo cpf/cnpj */
function atualizaConsultaCnpjFornec(){
	var oiObjField = document.all["cpfconsultaForn"].value;
	
	if(oiObjField != null){
		reloadZoomFilterValues("rm_fornfcfo", 'CGCCFOF,'+oiObjField);
	}
}


/** *************BUSCA CEP************** */

function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    document.getElementById('ruaOutros').value=("");
    document.getElementById('bairroOutros').value=("");
    document.getElementById('cidadeOutros').value=("");
    document.getElementById('estadoOutros').value=("");
}



function pesquisacep(valor) {

	// Nova variável "cep" somente com dígitos.
	var cep = valor.replace(/\D/g, '');

    // Verifica se campo cep possui valor informado.
    if (cep != "") {

            // Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            // Valida o formato do CEP.
            if(validacep.test(cep)) {

                // Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('ruaOutros').value="...";
                document.getElementById('bairroOutros').value="...";
                document.getElementById('cidadeOutros').value="...";
                document.getElementById('estadoOutros').value="...";

                // Cria um elemento javascript.
                var script = document.createElement('script');

                // Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                // Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } // end if.
            else {
                // cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } // end if.
        else {
            // cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };
    
    function limpa_formulário_cnpj() {
        // Limpa valores do formulário de cep.
        document.getElementById('rm_fcforazao').value=("");
        document.getElementById('rm_fcfonome').value=("");
        document.getElementById('rm_fcfotelefone').value=("");
        document.getElementById('rm_fcfoemail').value=("");
        document.getElementById('rm_fcfosituacao').value=("");
        document.getElementById('rm_fcfoporte').value=("");
	}
    
    
    function meu_callback_cnjp(conteudo) {
        if (!("erro" in conteudo)) {
            // Atualiza os campos com os valores.
            document.getElementById('rm_fcforazao').value=(conteudo.nome);
            document.getElementById('rm_fcfonome').value=(conteudo.fantasia);
            document.getElementById('rm_fcfotelefone').value=(conteudo.telefone);
            document.getElementById('rm_fcfoemail').value=(conteudo.email);
            document.getElementById('rm_fcfosituacao').value=(conteudo.situacao);
            document.getElementById('rm_fcfoporte').value=(conteudo.porte); 
        } // end if.
        else {
            // CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
    
  	function buscaCliente(cnpjCliente){
		// Nova variável "cnpj" somente com dígitos.
	  	var cnpj = cnpjCliente.replace(/\D/g, '');
	  	console.log('buscaCliente() -> cnpj -> ' + cnpj)
	  	if (cnpj != "") {
		  // var validacnpj = /^[0-9]{8}$/;
		  
		  // if (validacnpj.test(cnpj)) {
			  
			  // Preenche os campos com "..." enquanto consulta webservice.
              document.getElementById('rm_fcforazao').value="...";
              document.getElementById('rm_fcfonome').value="...";
              document.getElementById('rm_fcfotelefone').value="...";
              document.getElementById('rm_fcfoemail').value="...";
              document.getElementById('rm_fcfosituacao').value="...";
              document.getElementById('rm_fcfoporte').value="...";
			  
			  var script = document.createElement('script');
			  
			  script.src = 'https://www.receitaws.com.br/v1/cnpj/' + cnpj + '?callback=meu_callback_cnjp';
			  
			  document.body.appendChild(script);
// } else {
// //cep é inválido.
// limpa_formulário_cnpj();
// alert("Formato de CNPJ inválido.");
// }
	  	} // end if.
        else {
	        // cep sem valor, limpa formulário.
	    	limpa_formulário_cnpj();
        }
	};

function calculaPercent(oField){
		var nPos 			= oField.name.lastIndexOf('_');
		var pEarned = parseInt($('#cpRateioValor_' + oField.name.substr(nPos-1, 5)).val());
     var pPos = parseInt($('#cargaHorariaValorTotal').val());
     var perc="";
     if(isNaN(pPos) || isNaN(pEarned)){
        perc=" ";
     } else {
        perc = ((pEarned/pPos) * 100).toFixed(2);
     }
     
     $('#cpRateioPercent_' + oField.name.substr(nPos-1, 5)).val((perc));
	};
	
	function sfQtdRateio(oField) {		        	
	var nPos 			= oField.name.lastIndexOf('_');
	var ciFldQtdVen		= "cpRateioPercent_" + oField.name.substr(nPos-1, 5);
	var ciFldVlrTotal	= "cpRateioTot_" + oField.name.substr(nPos-1, 5);
	
	var niQtdVen 		= document.all[ciFldQtdVen].value;
	var niVlrTotal 		= niQtdVen;

	var ciVlrTotal 		= sfFormatZeros(niVlrTotal);
	
	document.all[ciFldVlrTotal].value = sfFormatZeros(niVlrTotal);
	
	sfSubTotalRateio();
	};
	
	function sfSubTotalRateio() {
	var niCont 			= 0;
	var niNumberVlrTot 	= 0;
	var ciCpoVlrTotal	= "cpRateioTot_";
	var oiObjField 		= document.all[ciCpoVlrTotal + niCont];
	// Fixado em 80 para poder tratar o subtotal, poder ser alterado, conforme a
	// necessidade
	while(niCont < 80) {
		niCont 			= niCont+1;
		oiObjField 		= document.all[ciCpoVlrTotal + niCont];
				
		if(oiObjField != null) {					
			niNumberVlrTot += sfStringToNumber(oiObjField.value);	
		}
	} 
	document.all["numberVlrTotRateio"].value = sfFormatZeros(niNumberVlrTot);
};

function sfFormatZeros(nNumber) {
	var cNumber = nNumber.toString().replace(".", ",");
	var nPos = cNumber.search(',');
	var nDif = cNumber.length - nPos;
	if(nPos == -1)
		cNumber = cNumber+".00";
	else if( nDif == 2 )
		cNumber = cNumber+"0";
	else if( nDif >= 4 )
		cNumber = cNumber.substr(0, cNumber.length - nDif + 3);
	return cNumber;
};

function sfStringToNumber(cNumber) {
	return parseFloat(cNumber.replace(",", "."));
};

/* Função que busca o Cliente pelo cpf/cnpj */
function atualizaConsultaCnpj(oField){
	var nPos = oField.name.lastIndexOf('_');
	
	var oiObjField = document.getElementById("cpfconsulta_" + oField.name.substr(nPos-1, 5)).value;
	
	if(oiObjField != null){
		reloadZoomFilterValues("rm_fcfo_" + oField.name.substr(nPos-1, 5), 'CGCCFO,'+oiObjField);
	}
}

/* Função que busca o Fornecedor pelo cpf/cnpj */
function atualizaConsultaCnpjFornec(){
	var oiObjField = document.all["cpfconsultaForn"].value;
	
	if(oiObjField != null){
		reloadZoomFilterValues("rm_fornfcfo", 'CGCCFOF,'+oiObjField);
	}
}


/** *************BUSCA CEP************** */



function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        // Atualiza os campos com os valores.
        document.getElementById('ruaOutros').value=(conteudo.logradouro);
        document.getElementById('bairroOutros').value=(conteudo.bairro);
        document.getElementById('cidadeOutros').value=(conteudo.localidade);
        document.getElementById('estadoOutros').value=(conteudo.uf);
    } // end if.
    else {
        // CEP não Encontrado.
        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}


    
    function desabilitaButtonEmail() {
    	document.getElementById("btnGerarContratoEmail").disabled = false;
    }
    
    function desabilitaButtonEmailAditivo() {
    	document.getElementById("btnGerarContratoEmailAditivo").disabled = false;
    }
    
    function desabilitaButtonEmailCancelamento() {
    	document.getElementById("btnGerarContratoEmailCancelamento").disabled = false;
    }
    
    // parte 02 - arquivo funcoes.js
    
    function onClickFormMostraCliente(sender){
    	if ($(sender).is(':checked')) {	
    		$('#divtabelaCliente').show();
    		$('#divtabelaClienteEspont').hide();
    	}
    }

    function onClickFormMostraClienteEspont(sender){
    	if ($(sender).is(':checked')) {	
    		$('#divtabelaClienteEspont').show();
    		$('#divtabelaCliente').hide();
    	}
    }

    function onClickFormMostraFormaContrat(sender){
    	if ($(sender).is(':checked')) {	
    		$('#divFormaContrat').show();
    	}
    }

    function onClickFormNaoMostraFormaContrat(sender){
    	if ($(sender).is(':checked')) {	
    		$('#divFormaContrat').hide();
    	}
    }
    
	function retornaData(){
		var data = new Date();
		var dia = data.getDate();
		var mes = data.getMonth()+1;
		var ano = data.getFullYear();
		res = dia+"/"+mes+"/"+ano;
		document.getElementById("dataEven").value = res;
	};

    function carregaAditivo() {	
    	var numprocesso = document.getElementById('tbProtocolo').value;
    	
    	var d = DatasetFactory.createConstraint("NUMPROCESSO", numprocesso, numprocesso, ConstraintType.MUST);
    	var constraintsD = new Array(d);
    	
    	var dataset = DatasetFactory.getDataset("fluig_consultoria_aditivo", null, constraintsD, null);
    	
    	for (var i = 0; i < dataset.values.length; i++){
            wdkAddChild('tableAditivo');
            // newId -> sequência do Pai x Filho que retorna dessa função
			// wdkAddChild
            $("#atividadeAdit___" + newId).val(dataset.values[i].ATIVIDADEADIT);
            $("#dtInicioMonitAdit___" + newId).val(dataset.values[i].DTINICIOADIT);
            $("#dtTerminoMonitAdit___" + newId).val(dataset.values[i].DTTERMINOADIT);
            $("#chMesMonitAdit___" + newId).val(dataset.values[i].CHMESADIT);
            $("#dtPrevisaoPgtoMonitAdit___" + newId).val(dataset.values[i].DTPREVPGTOADIT);
            $("#movAdit___" + newId).val(dataset.values[i].MOVADIT);
            $("#aditLimite").val(dataset.values[i].CHLIMITE);
            $("#cargaHorariaTotalsemAditivo").val(dataset.values[i].CHSOBRA);
        }
    }

    function moeda(z){
    	v = z.value;
    	v=v.replace(/\D/g,"") // permite digitar apenas números
    	v=v.replace(/[0-9]{9}/,"inválido") // limita pra máximo 999.999.999,99
    	v=v.replace(/(\d{1})(\d{8})$/,"$1,$2") // coloca ponto antes dos
												// últimos 8 digitos
    	v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") // coloca ponto antes dos
													// últimos 5 digitos
    	// v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") //coloca virgula antes dos
		// últimos 2 digitos
    	z.value = v;
	};
	function doFormTotalRateio() {
	    var total = 0;
	    $("input[id^='cpRateioTot___']").each(function() {
	        if ($(this).val()) {
	            total += sfStringToNumber($(this).val()); 
	        }
	    });
	    $("#numberVlrTotRateio").val(total.toFixed(2));
	};
	
	 var keyDown = false,
     ctrl = 17,
     vKey = 86,
     Vkey = 118;

 $(document).keydown(function(e) {
     if (e.keyCode == ctrl) keyDown = true;
 }).keyup(function(e) {
     if (e.keyCode == ctrl) keyDown = false;
 });

 $('[data-only-numbers]').on('keypress', function(e) {
     if (!e) {
         var e = window.event;
     }

     if (e.keyCode > 0 && e.which == 0) {
         return true;
     }

     if (e.keyCode) {
         code = e.keyCode;
     } else if (e.which) {
         code = e.which;
     }

     if (code == 46) {
         return true;
     }

     var character = String.fromCharCode(code);
     if (character == '\b' || character == ' ' || character == '\t') {
         return true;
     }
     if (keyDown && (code == vKey || code == Vkey)) {
         return (character);
     } else {
         return (/[0-9]$/.test(character));
     }
 }).on('focusout', function(e) {
     var $this = $(this);
     if ($this.val() == "") {
         return true;
     }
     $this.val($this.val().replace(/[^0-9\.]/g, ''));
 }).on('paste', function(e) {
     var $this = $(this);
     setTimeout(function() {
         $this.val($this.val().replace(/[^0-9\.]/g, ''));
     }, 5);
 });



 $('.create-form-components').on('keyup', 'input[required="required"][type="text"], input[required="required"][type="number"], input[required="required"][type="date"], textarea[required="required"]', function() {
     validationFieldsForm($(this), $(this).parents('.form-field').data('type'));
 });

 $('.create-form-components').on('change', 'input[required="required"][type="checkbox"], input[required="required"][type="radio"], select[required="required"]', function() {
     validationFieldsForm($(this), $(this).parents('.form-field').data('type'));
 });

 function validationFieldsForm(field, type) {
     if (type === "checkbox" || type === "radio") {
         if (!field.is(':checked')) {
             field.parents('.form-field').addClass('required');
         } else {
             field.parents('.form-field').removeClass('required');
         }
     } else {
         if (!field.val().trim()) {
             field.parents('.form-field').addClass('required');
         } else {
             field.parents('.form-field').removeClass('required');
         }
     }
 }

 /* Jean Varlet - 05/03/2020 - teste ---
 var $zoomPreview = $(".zoom-preview");
 if ($zoomPreview.length) {
     $zoomPreview.parent().removeClass("input-group");
     $zoomPreview.remove();
 }
*/
 var ratings = $(".rating");
 if (ratings.length > 0) ratingStars(ratings);

 function ratingStars(stars) {
     $.each(stars, function(i, obj) {
         var field = $(this).closest(".form-group").find(".rating-value");
         var tgt = $(obj);
         tgt.html("");
         var rating = FLUIGC.stars(tgt, {
             value: field.val()
         });
         rating.on("click", function(o) {
             field.val($(this).index() + 1);
         });
     });
 }

 $.each($("[data-date]"), function(i, o) {
     var id = $(o).attr("id");
     FLUIGC.calendar("#" + id);
 });
 